<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ArchivoMedico;

class ExpedienteMedico extends Model
{
    protected $table = "expedientes_medicos";

    public function archivos_medico()
    {
        return $this->hasMany(ArchivoMedico::class, 'expediente_medico_id', 'id');
    }

}
