<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspirometriaLnn extends Model {
    protected $table = 'espirometria_lln';
}
