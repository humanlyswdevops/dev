<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toma extends Model {
    protected $table = 'tomasSASS';


    public function estudios()
    {
        return $this->hasMany(EstudiosSass::class,'tomas_id','id');
    }

    public function expediente()
    {
        return $this->hasOne(Expediente::class,'id','expediente_id');
    }
}
