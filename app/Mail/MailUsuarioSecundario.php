<?php

namespace App\Mail;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailUsuarioSecundario extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $nombre;
    public $correo;
    public $user_password;
    public $empresa;

    public function __construct(Request $request, $password, $empresa)
    {
        $this->nombre = $request->input('nombre');
        $this->correo = $request->input('email');
        $this->user_password = $password;
        $this->empresa = $empresa;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mails.nuevoUsuarioSecundario')
            ->from("noreply@humanly-sw.com", "Health Administration Solution")
            ->subject("Un administrador de " . $this->empresa->nombre . " ha creado una cuenta para usted");
    }
}
