<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Espirometria extends Model {
    protected $table = 'espirometria';

    public function resultados()
    {
        return $this->hasMany(EspirometriaResultados::class,'espirometria_id','id');
    }

    public function ref()
    {
        return $this->hasMany(EspirometriaRef::class,'espirometria_id','id');
    }
    public function lln()
    {
        return $this->hasMany(EspirometriaLnn::class,'espirometria_id','id');
    }
    public function maneobras()
    {
        return $this->hasMany(EspirometriaMan::class,'espirometria_id','id');
    }
}
