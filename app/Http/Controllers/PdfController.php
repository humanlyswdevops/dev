<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archivo;
use App\Audiometria;
use App\Expediente;
use App\HistorialClinico;
use App\Empleado;
use App\Empresa;
use App\Configuracion;
use App\Consulta;
use App\ConsultaMedicamento;
use App\ConsultaProcedimiento;
use App\ConsultaTratamiento;
use App\datosUserHtds;
use Mail;
use Illuminate\Support\Facades\Session;


class PdfController extends Controller {
    public function MostrarPdf($id) {
        $archivo = Archivo::find($id);
        $content = base64_decode($archivo->archivo);
        $pdf = \PDF::loadHTML($content);
        return $pdf->stream();
    }
    public function viewPdf($encryptedID){
      $historialclinico = $this->identificacion($encryptedID);
        //return $historialclinico->examenFisico;
      if (empty($historialclinico)){
      $empleado = $this->empleado($encryptedID);
      ini_set ('max_execution_time', 300);
      ini_set ("memory_limit", "512M");
      $pdf=\PDF::loadView('PDF/ErrorPDF',compact('empleado'));
      return $pdf->stream();
      }
      else{
      $identificacion= json_decode($historialclinico->identificacion);
      $heredofamiliar =  json_decode($historialclinico->heredofamiliar);
      $nopatologico =  json_decode($historialclinico->noPatologico);
      $patologico =  json_decode($historialclinico->patologico);
      $genicoobstetrico = json_decode($historialclinico->genicoobstetrico);
      $historiaLaboral =  json_decode($historialclinico->historialLaboral);
      $examen =  json_decode($historialclinico->examenFisico);
      $resultados =  json_decode($historialclinico->resultado);
      // $empleado=Empleado::find($historialclinico->curp);
      $empleado = $this->empleado($encryptedID);
      ini_set ('max_execution_time', 300);
      ini_set ("memory_limit", "512M");
      $pdf=\PDF::loadView('PDF/historialClinico',compact('identificacion','empleado','heredofamiliar','nopatologico','patologico','genicoobstetrico','historiaLaboral','examen','resultados'));
      return $pdf->stream();
     
      }
      
    }
    public function identificacion($id){
      return  HistorialClinico::where('CURP',$id)->first();
      // return Empleado::where('CURP',$curp)->first();
    }
    public function Empleado($id){
      return Empleado::where('CURP',$id)->first();
    }

    public function recetaMiedica($id){
      //return \Auth::user();
      $json = $this->consulta($id);
      $user = $this->medico(\Auth::user()->id);
      $empresa = $this->empresa(Session::get('empresa')->id);
      $paciente = $this->paciente($json->expediente_id);
      $medicamentos = $this->medicamentos($json->id);
      $pdf=\PDF::loadView('PDF/receta',compact('medicamentos','user','json','empresa','paciente'));
      return $pdf->stream();
    }

    public function procedimientos($id){
      //return \Auth::user();
      $json = $this->consulta($id);
      $user = $this->medico(\Auth::user()->id);
      $empresa = $this->empresa(Session::get('empresa')->id);
      $paciente = $this->paciente($json->expediente_id);
      $procedimientos = consultaProcedimiento::where('consulta_id',$id)->get();
    //   dd($procedimientos);
      $pdf=\PDF::loadView('PDF/procedimientos',compact('procedimientos','user','json','empresa','paciente'));
      return $pdf->stream();
    }

    public function tratamiento($id){
        //return \Auth::user();
        $json = $this->consulta($id);
        $user = $this->medico(\Auth::user()->id);
        $empresa = $this->empresa(Session::get('empresa')->id);
        $paciente = $this->paciente($json->expediente_id);
        $tratamiento = ConsultaTratamiento::where('consulta_id',$id)->first()->tratamiento;
        $pdf=\PDF::loadView('PDF/tratamiento',compact('tratamiento','user','json','empresa','paciente'));
        return $pdf->stream();
      }

    public function medicamentos($consulta_id){
        return ConsultaMedicamento::where('consulta_id',$consulta_id)
      ->get();
    }


    public function consulta($id){
       return  Consulta::find($id);
    }

    public function medico($id){
      return Configuracion::where('id_user',$id)
      ->join('users','users.id','configuracion.id_user')
      ->first();
    }

    public function empresa($id)
    {
      return Empresa::find($id);
    }
    public function paciente($id){
      $expediente = Expediente::find($id);
      return Empleado::where('CURP',$expediente->curp)
      ->where('status_id',1)
      ->where('empresa_id',\Session::get('empresa')->id)
      ->first();
    }


    // NOTE: EMAIL


    public function emailNotas(Request $data){
        $id = $data->id;
        $email = $data->email;
        $json = $this->consulta($id);
        $user = $this->medico(\Auth::user()->id);
        $empresa = $this->empresa(Session::get('empresa')->id);
        $paciente = $this->paciente($json->expediente_id);
        $tratamiento = ConsultaTratamiento::where('consulta_id',$id)->first();

        $data = array(
            'tratamiento' => $tratamiento,
            'user'=> $user,
            'json'=> $json,
            'empresa'=> $empresa,
            'paciente'=> $paciente,
        );

        Mail::send('Mails/notas', $data, function ($message) use($email,$empresa) {
            $message->from('noreply@humanly-sw.com', $empresa->nombre);
            $message->to($email);
            $message->subject('Tratamiento médico');
        });

        return response()->json("Correcto",200);
    }

    public function emailMiedica($id){
        //return \Auth::user();
        try {
            $json = $this->consulta($id);
            $user = $this->medico(\Auth::user()->id);
            $empresa = $this->empresa(Session::get('empresa')->id);
            $paciente = $this->paciente($json->expediente_id);
            $medicamentos = $this->medicamentos($json->id);
            $data = array(
                'medicamentos' => $medicamentos,
                'user'=> $user,
                'json'=> $json,
                'empresa'=> $empresa,
                'paciente'=> $paciente,
            );
            Mail::send('Mails/receta', $data, function ($message) use ($paciente,$empresa) {
                $message->from('noreply@humanly-sw.com', $empresa->nombre);
                $message->to($paciente->email);
                $message->subject('Receta médica');
            });
            return response()->json('Correcto',200);
        } catch (\Throwable $th) {
            dd('Reboto?',$th);
            return response()->json($th,500);
            //throw $th;
        }
    }

    public function procedimientosEmail($id){
        //return \Auth::user();
        $json = $this->consulta($id);
        $user = $this->medico(\Auth::user()->id);
        $empresa = $this->empresa(Session::get('empresa')->id);
        $paciente = $this->paciente($json->expediente_id);
        $procedimientos = consultaProcedimiento::where('consulta_id',$id)->get();


        $data = array(
            'procedimientos' => $procedimientos,
            'user'=> $user,
            'json'=> $json,
            'empresa'=> $empresa,
            'paciente'=> $paciente,
        );

        Mail::send('Mails/procedimientos', $data, function ($message) use ($paciente,$empresa) {
            $message->from('noreply@humanly-sw.com', $empresa->nombre);
            $message->to($paciente->email);
            $message->subject('Procedimientos médicos');
        });

        return response()->json("Correcto",200);
    }

    public function tratamientoEmail($id){
        //return \Auth::user();
        $json = $this->consulta($id);
        $user = $this->medico(\Auth::user()->id);
        $empresa = $this->empresa(Session::get('empresa')->id);
        $paciente = $this->paciente($json->expediente_id);
        $tratamiento = ConsultaTratamiento::where('consulta_id',$id)->first();

        $data = array(
            'tratamiento' => $tratamiento,
            'user'=> $user,
            'json'=> $json,
            'empresa'=> $empresa,
            'paciente'=> $paciente,
        );

        Mail::send('Mails/tratamiento', $data, function ($message) use($paciente,$empresa) {
            $message->from('noreply@humanly-sw.com', $empresa->nombre);
            $message->to($paciente->email);
            $message->subject('Tratamiento médico');
        });

        return response()->json("Correcto",200);
    }


    public function resultadoAudiometria($audiometria_id)
    {
        $audiometria = Audiometria::find($audiometria_id);
        $empleado = Empleado::find($audiometria->registroEntrada->empleado_id);
        $user_htds = datosUserHtds::where('ID_empleado',Session::get('IdEmpleado'))->first();

        $d_500 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','500')->pluck('desibelios')->first();
        $d_1000 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','1000')->pluck('desibelios')->first();
        $d_2000 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','2000')->pluck('desibelios')->first();
        $d_4000 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','4000')->pluck('desibelios')->first();
        $dshl_d = $d_500 + $d_1000 + $d_2000 + $d_4000;
        $indice_sal_d = round(($d_500 + $d_1000 + $d_2000) / 3);

        $i_500 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','500')->pluck('desibelios')->first();
        $i_1000 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','1000')->pluck('desibelios')->first();
        $i_2000 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','2000')->pluck('desibelios')->first();
        $i_4000 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','4000')->pluck('desibelios')->first();
        $dshl_i = $i_500 + $i_1000 + $i_2000 + $i_4000;
        $indice_sal_i = round(($i_500 + $i_1000 + $i_2000) / 3);

        return view('PDF.resultado_audiometria')
        ->with('audiometria',$audiometria)
        ->with('dshl_d',$dshl_d)
        ->with('dshl_i',$dshl_i)
        ->with('indice_sal_d',$indice_sal_d)
        ->with('indice_sal_i',$indice_sal_i)
        ->with('user_htds',$user_htds)
        ->with('empleado',$empleado);
    }

    public function resultadoAudiometria2($audiometria_id)
    {
        $audiometria = Audiometria::find($audiometria_id);
        $empleado = Empleado::find($audiometria->registroEntrada->empleado_id);
        $user_htds = datosUserHtds::where('ID_empleado',Session::get('IdEmpleado'))->first();

        $d_500 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','500')->pluck('desibelios')->first();
        $d_1000 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','1000')->pluck('desibelios')->first();
        $d_2000 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','2000')->pluck('desibelios')->first();
        $d_4000 = $audiometria->resultados->where('oido','derecho')->where('frecuencia','4000')->pluck('desibelios')->first();
        $dshl_d = $d_500 + $d_1000 + $d_2000 + $d_4000;
        $indice_sal_d = round(($d_500 + $d_1000 + $d_2000) / 3);

        $i_500 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','500')->pluck('desibelios')->first();
        $i_1000 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','1000')->pluck('desibelios')->first();
        $i_2000 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','2000')->pluck('desibelios')->first();
        $i_4000 = $audiometria->resultados->where('oido','izquierdo')->where('frecuencia','4000')->pluck('desibelios')->first();
        $dshl_i = $i_500 + $i_1000 + $i_2000 + $i_4000;
        $indice_sal_i = round(($i_500 + $i_1000 + $i_2000) / 3);

        $pdf=\PDF::loadView('PDF/pruebaAudio',compact('audiometria','dshl_d','dshl_i','indice_sal_d','indice_sal_i','user_htds','empleado'));
        return $pdf->stream();

        return view('PDF.resultado_audiometria')
        ->with('audiometria',$audiometria)
        ->with('dshl_d',$dshl_d)
        ->with('dshl_i',$dshl_i)
        ->with('indice_sal_d',$indice_sal_d)
        ->with('indice_sal_i',$indice_sal_i)
        ->with('user_htds',$user_htds)
        ->with('empleado',$empleado);
    }

    //FORMATOS
    public function cadenacustodia(){
      //return \Auth::user();
      // $json = $this->consulta($id);
      // $user = $this->medico(\Auth::user()->id);
      // $empresa = $this->empresa(Session::get('empresa')->id);
      // $paciente = $this->paciente($json->expediente_id);
      // $tratamiento = ConsultaTratamiento::where('consulta_id',$id)->first()->tratamiento;
      $pdf=\PDF::loadView('PDF/cadenacustodia');
      return $pdf->stream();
    }
}
