<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Administrador\EstudiosController;
use App\Http\Controllers\Empresa\EmpleadosController;
use App\Http\Controllers\Empresa\GruposController;
use App\Http\Controllers\Empresa\EstudiosProgramadosController;
use App\Http\Controllers\Empresa\ConsultaController;

use App\Http\Controllers\Administrador\EmpresasController;
use Illuminate\Support\Facades\Auth;
use App\Empresa;
use App\Calculadora;
class RouteController extends Controller
{


    public function __construct() {
        $this->middleware('auth');
    }

    public function contacto() {
        return view('Administrador/ContactoAdmin');
    }

    public function jsonInfoPersonalCalculadora($data)
{
  return json_encode([
    'pesoActual' => $data->pesoActual,
    'hipertension' => $data->hipertension,
    'diabetes' => $data->diabetes,
    'con_tabaco' => $data->con_tabaco,
    'epoc' => $data->epoc,
    'enf_cardio' => $data->enf_cardio,
    'inmunosup' => $data->inmunosup
  ]);
}
    public function CalculadoraC(Request $request)
{
  $genero = $request->genero;
  $edad = $request->edad;

  $pesoActual = $request->pesoActual;
  $hipertension = $request->hipertension;
  $diabetes = $request->diabetes;
  $con_tabaco = $request->con_tabaco;
  $epoc = $request->epoc;
  $enf_cardio = $request->enf_cardio;
  $inmunosup = $request->inmunosup;


  $riesgoGen = 0;
  $riesgoHipertension = 0;
  $riesgoDiabetes = 0;
  $riesgoConTabaco = 0;
  $riesgoEpoc = 0;
  $riesgoEnfCardio = 0;
  $riesgoInmunosup = 0;
  $contador=0;

  //Calculos
  if($genero == 'masculino'){
     $contador++;
      $riesgoGen = log(1.807146);
      if($edad > 45){
        $contador++;
      }
    }
    else{ //SI ES MUJER
      $riesgoGen = 0;
      if($edad > 53){
        $contador++;
      }
    }

    if($edad != 0){
      $riesgoEdad = $edad * log(1.072065);
    }
    else{
      $riesgoEdad = 0;
    }

    if($pesoActual == 'sobrepeso' || $pesoActual == 'obesidad'){
      $riegoObesidad = log(1.282642);
      $contador++;
    }
    else{
      $riegoObesidad = 0;
    }


    if($hipertension == 'on'){
      //echo 'HIPERTENSION<br>';
      $contador++;
      $riesgoHipertension = log(1.112499);
    }
    if($diabetes == 'on'){
      //echo 'DIABETES<br>';
      $contador++;
      $riesgoDiabetes = log(2.176709);
    }
    if($con_tabaco == 'on'){
      //echo 'TABACO<br>';
      $contador++;
      $riesgoConTabaco = log(1.215024);
    }
    if($epoc == 'on'){
      //echo 'EPOC<br>';
      $contador++;
      $riesgoEpoc = log(1.250455);
    }
    if($enf_cardio == 'on'){
      //echo 'CARDIOVASCULAR<br>';
      $contador++;
      $riesgoEnfCardio = log(1.072815);
    }
    if($inmunosup == 'on'){
      //echo 'INMUNOSUPRESION<br>';
      $contador++;
      $riesgoInmunosup = log(2.759612);
    }


  $totalesRiesgo = log(0.0238616) + $riesgoGen + $riesgoEdad + $riegoObesidad + $riesgoHipertension + $riesgoDiabetes + $riesgoConTabaco + $riesgoEpoc + $riesgoEnfCardio + $riesgoInmunosup;
  $totalRiesgoNeg = (-1) * $totalesRiesgo;
  $totalExponencial = (1 / (1 + exp($totalRiesgoNeg))) * 100;

  $totalProb = round($totalExponencial,1);

  if($totalProb < 50){
    $riesgo = 'Riesgo medio';
  }
  elseif($totalProb >= 50 && $totalProb < 80){
    $riesgo = 'Riesgo alto';
  }
  else{
    $riesgo = 'Riesgo muy alto';
  }
  //Guarda en Base
  $Guardacalculadora = new Calculadora;
  $Guardacalculadora->paciente_id = $request->id;
  $Guardacalculadora->infoPersonal = $this->jsonInfoPersonalCalculadora($request);
  $Guardacalculadora->factoresRiesgo = $contador;
  $Guardacalculadora->fecha = date('Y-m-d H:i:s');
  $Guardacalculadora->categoríaRiesgo = $riesgo;
  $Guardacalculadora->NivelRiesgo = $totalProb;
  $Guardacalculadora->save();

  return [$contador,
  $riesgo,
  $totalProb];

}

    public function covid($paciente_id){
        $datosCalculadora = Calculadora::select('*')
            ->where('paciente_id',$paciente_id)
            ->orderBy('fecha','desc')
            ->first();
            return json_encode($datosCalculadora);
    }

    public function estudios() {
        $estudios = new EstudiosController();
        return $estudios->estudioPage();
    }

    public function getEstudio($id) {
        $estudios = new EstudiosController();
        return $estudios ->getEstudio($id);
    }

    public function getEstudios($id) {
        $estudios = new EstudiosController();
        return $estudios->getEstudios($id);
    }

    public function verEmpresas() {
        $empresas = new EmpresasController();
        return $empresas->verEmpresas();
    }

    public function showEmpresa($nombre) {
        $empresa = new EmpresasController();
        return $empresa->showEmpresa($nombre);
    }

    // Funciones de empresa.

    public function expediente() {
        return view('Empresa/Expediente');
    }



    public function getEmpleado($id) {
        $empleados_controller = new EmpleadosController();
        return $empleados_controller->getEmpleado($id);
    }

    public function getEmpleados() {
        $empleados_controller = new EmpleadosController();
        $grupos_controller = new GruposController();

        $empleados = $empleados_controller->getEmpleados();
        $grupos = $grupos_controller->getGrupos();
        $empleados = $empleados_controller->getEmpleadosGrupos($empleados, $grupos);

        return datatables()->of($empleados)->addColumn('btn', 'Empresa/empleadosActions')->rawColumns(['btn'])->toJson();
    }

    public function getEmpleadosDeGrupo($encryptedID) {
        $empleados_controller = new EmpleadosController();
        $grupos_controller = new GruposController();

        $empleados = $empleados_controller->getEmpleadosDeGrupo($encryptedID);
        $grupos = $grupos_controller->getGrupos();
        $empleados = $empleados_controller->getEmpleadosGrupos($empleados, $grupos);

        return datatables()->of($empleados)->addColumn('btn', 'Empresa/gruposEmpleadosActions')->rawColumns(['btn'])->toJson();
    }

    public function getGrupos() {
        $grupos_controller = new GruposController();
        $grupos = $grupos_controller->getGrupos();

        return datatables()->of($grupos)->addColumn('btn', 'Empresa/gruposActions')->rawColumns(['btn'])->toJson();
    }

    public function getEstudioProgramado($id) {
        $estudiosProgramados_controller = new EstudiosProgramadosController();
        return $estudiosProgramados_controller -> getEstudioProgramado($id);
    }

    public function getEstudiosProgramadosGrupo($nombre) {
        $grupos_controller = new GruposController();
        return $grupos_controller -> getEstudiosProgramadosGrupo($nombre);
    }

    public function vistaEmpresa() {
        return view ('Administrador/VistaEmpr');
    }

    public function AltaEmpleado() {
        $empleado = new EmpleadosController();
        return $empleado->AltaEmpleado();
    }

    public function AltaEmpleadoGrp(){
        return view ('Empresa/AltaEmGrup');
    }

    public function vistaPruebaLaboratorio() {
        return view('Laboratorio/pruebaIniciosesion');
    }

    public function labLogin(Request $request) {
        return json_encode(route('empleados'));
    }

    public function vistaIndicadores() {
        return view('Empresa/Indicadores');
    }
    public function vistaIndicadoresGrupo()
    {
        $grupos_controller = new GruposController();
        $grupos = $grupos_controller->getGrupos();
        return view('Empresa/indicadoresGrupo', [
            'grupos' => $grupos
        ]);
    }

    public function getEmpleadosSinGrupo() {
        $grupos_controller = new GruposController();
        return $grupos_controller->getEmpleadosSinGrupo();
    }
    public function vistaIndicadoresConsultas()
    {
        $controlador = new ConsultaController();
        return $controlador->vistaIndicadoresConsultas();
    }
}
