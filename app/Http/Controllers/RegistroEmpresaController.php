<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Mail;
use DB;

use App\User;
use App\Grupo;
use App\Categoria;
use App\Estudios;
use App\EstudioEmpresa;
use App\Empresa;
use App\Giro;
use App\EstudioGrupo;
use App\Roles;
use App\Modulos;
use App\Accesos;


class RegistroEmpresaController extends Controller {
    public function __construct() {
    	$this->middleware('auth');
    }

    public function viewRegistro() {
        $categoria = $this->getCategoria();
        $estudios  = $this->getEstudios();
        $giros = Giro::all();

        return view('Administrador/Registro', [
            'categoria' => $categoria,
            'estudios' => $estudios,
            'giros' => $giros
        ]);
    }

    public function registro(Request $request) {
        // dd('Alto',$request);
        try {
            DB::beginTransaction();
                $admin = new User;
                $empresa = new Empresa;
                $validate = $this->validate($request, [
                    'nombre_completo' => 'required|string|max:250',
                    'correo_contacto' => 'required|string|max:250',
                    // 'telefonoAdmin' => 'required|string|max:250',
                    'puesto' => 'required|string|max:250',
                    'area' => 'required|string|max:250',
                    'nombre_empresa' => 'required|string|max:250',
                    'direccion' => 'required|string|max:250',
                    'giro' => 'required|string|max:250',
                    'telefono' => 'required|string|max:250',
                    'pagina_web' => 'required|string|max:250',
                    'sucursal_id' => 'required|string|max:250',
                    'cliente_id' => 'required|string|max:250',
                    'medico_id' => 'required|string|max:250',
                ]);
                $email = User::where('email', $request->input('correo_contacto'))->first();
                // if (empty($estudios = $request->input('estudios'))) {
                //     return redirect()->route('empresa')
                //     ->with(['message_error' => "Seleccione los estudios autorizados"]);
                // }
                if ($request->input('giro')=="Selecciona el giro de la empresa") {
                    return redirect()->route('empresa')
                        ->with(['message_error' => "Seleccione el giro de la empresa"]);
                }
                if ($email) {
                    return redirect()->route('empresa')
                        ->with(['message_error' => "El correo ya exite en nuestros datos"]);
                }

                $nombre_completo = $request->input('nombre_completo');
                $correo_contacto = $request->input('correo_contacto');
                $password = Str::random(10);
                $passwor_encrypt = Hash::make($password);
                // $telefonoAdmin = $request->input('telefonoAdmin');
                $puesto = $request->input('puesto');
                $area = $request->input('area');
                $nombre_empresa = $request->input('nombre_empresa');
                $direccion = $request->input('direccion');
                $giro_id = Giro::where('nombre', $request->input('giro'))->first()->id;
                $telefono = $request->input('telefono');
                $pagina_web = $request->input('pagina_web');
                $sucursal_id = $request->input('sucursal_id');
                $cliente_id = $request->input('cliente_id');
                $medico_id = $request->input('medico_id');
                $image_path = $request->file('logo');

                $admin->nombre = $nombre_completo;
                $admin->email = $correo_contacto;
                $admin->password = $passwor_encrypt;
                // $admin->telefono = $telefonoAdmin;
                $admin->puesto = $puesto;
                $admin->area = $area;
                $admin->role_id = 2;
                $admin->save();

                $user_id = $admin->id;

                $empresa->nombre = $nombre_empresa;
                $empresa->direccion = $direccion;
                $empresa->giro_id = $giro_id;
                $empresa->telefono = $telefono;
                $empresa->pagina = $pagina_web;
                $empresa->user_id = $user_id;
                $empresa->sucursal_id = $sucursal_id;
                $empresa->cliente_id = $cliente_id;
                $empresa->medico_id = $medico_id;
                $empresa->logo = $this->upload_image($image_path);
                $empresa->save();
                $this->rolcreate($empresa);

                $empresa_id = $empresa->id;

                // Se guardan los estudios de la empresa si se seleccionó al menos uno.
                // $estudios = $request->input('estudios');

                // if (sizeof($estudios) >= 1) {
                //     $elements = [];
                //     foreach ($estudios as $key => $value) {
                //         array_push($elements, [
                //             'estudio_id' => $value,
                //             'empresa_id' => $empresa_id,
                //             'created_at' => \Carbon::now(),
                //             'updated_at' => \Carbon::now()
                //         ]);
                //     }
                // EstudioEmpresa::insert($elements);
                // }

                $Grupo = new Grupo;
                $Grupo->nombre = 'Predeterminado';
                $Grupo->empresa_id = $empresa_id;
                $Grupo->created_at = \Carbon::now();
                $Grupo->updated_at = \Carbon::now();
                $Grupo->save();

                // $estudiosGrupo = new EstudioGrupo;

                // if (sizeof($estudios) >= 1) {
                //     $elements = [];
                //     foreach ($estudios as $key => $value) {
                //         array_push($elements, [
                //             'estudio_id' => $value,
                //             'grupo_id' => $Grupo->id,
                //             'created_at' => \Carbon::now(),
                //             'updated_at' => \Carbon::now()
                //         ]);
                //     }
                // EstudioGrupo::insert($elements);
                // }

                $this->mail($request,$password);
                DB::commit();
                return response()->json("Correcto", 200);
                // return redirect()->route('empresa')
                //     ->with(['message' => "La empresa se registro correctamente"]);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            dd($th);
        }
    }

    public function rolcreate($empresa){
        $rol = new Roles();
        $rol->tipo = 'Sub Administrador';
        $rol->descripcion = 'Usuario Predeterminado, No tiene Permiso a Equipo y Accesos';
        $rol->empresa_id = $empresa->id;
        $rol->save();
        $modulos = Modulos::all();
        foreach ($modulos as $modulo) {

            for ($i=1; $i <= 4; $i++) {
            $acc = new Accesos();
            $acc->rol_id = $rol->id;
            $acc->modulo_id = $modulo->id;
            $acc->crud_id = $i;
            $acc->save();
            }
        }
    }

    public function upload_image($image) {
        $image_nombre = '';
        if ($image) {
            $image_nombre = time() . $image->getClientOriginalName();
            Storage::disk('empresa')->put($image_nombre, File::get($image));
        } else {
            $image_nombre = 'null';
        }
        return $image_nombre;
    }

    public function mail(Request $request,$password) {
        $correoAdmin = $request->input('correo_contacto');
        Mail::to( $correoAdmin)->send(new WelcomeMail($request, $password));
    }

    public function getImage($filename) {
        if ($filename == "null") {
            $file = Storage::disk('empresa')->get('noLogo.png');
        } else {
            $file = Storage::disk('empresa')->get($filename);
        }
        return new Response($file, 200);
    }

    public function getCategoria() {
		return Categoria::orderBy('created_at', 'desc')->get();
    }

	public function getEstudios() {
		return Estudios::select('id', 'nombre', 'categoria_id')
            ->where('statu_id', 1)
            ->get();
	}
}
