<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

use App\Empresa;
use App\User;
use App\Categoria;
use App\Estudios;
use App\EstudioEmpresa;
use App\Giro;

class EmpresasController extends Controller
{
    public function verEmpresas() {
        $empresas = Empresa::orderBy('nombre', 'ASC')->get();

        return view('Administrador/Empresas', [
            'empresas' => $empresas
        ]);
    }

    public function showEmpresa($nombre) {
        $empresa = Empresa::where('nombre', $nombre)
            ->with('giro')
            ->first();
        $contacto = User::find($empresa->user_id);
        $categorias = $this->getCategorias();
        $estudios = $this->getEstudios();

        $estudios_empresa = EstudioEmpresa::where('empresa_id', $empresa->id)->get();
        // dd($estudios_empresa);
        return view('Administrador/infoEmpresa', [
            'empresa' => $empresa,
            'contacto' => $contacto,
            'encryptedID' => encrypt($empresa->id),
            'categorias' => $categorias,
            'estudios' => $estudios,
            'estudios_empresa' => $estudios_empresa,
        ]);
    }

    public function cambiarLogotipo(Request $request) {
        $image_path = $request->file('logo');
        $encryptedID = $request->input('encryptedID');

        try {
            $empresa_id = decrypt($encryptedID);
        } catch (DecryptException $e) {
            return abort(404);
        }

        $empresa = Empresa::find($empresa_id);
        if ($image_path) {
            $this->eliminarLogoActual($empresa);
            $empresa->logo = $this->upload_image($image_path);
        }
        $empresa->save();

        return redirect()->route('admin-showEmpresa', $empresa->nombre)
            ->with(['message' => "El logotipo se cambió correctamente."]);
    }

    private function eliminarLogoActual($empresa) {
        Storage::disk('empresa')->delete($empresa->logo);
    }

    private function upload_image($image) {
        $image_nombre;
        if ($image) {
            $image_nombre = time() . $image->getClientOriginalName();
            Storage::disk('empresa')->put($image_nombre, File::get($image));
        } else {
            $image_nombre = 'null';
        }
        return $image_nombre;
    }

    private function getCategorias() {
		return Categoria::orderBy('created_at', 'desc')->get();
    }

	private function getEstudios() {
		return Estudios::select('id', 'nombre', 'categoria_id')
            ->where('statu_id', 1)
            ->get();
    }

    public function getEstudiosEmpresa($encryptedID) {
        try {
            $empresa_id = decrypt($encryptedID);
        } catch (DecryptException $e) {
            return abort(404);
        }

        $estudios_empresa = EstudioEmpresa::where('empresa_id', $empresa_id)->get();
      //  dd($estudios_empresa);
        return json_encode($estudios_empresa);
    }

    public function selectorEstudiosEmpresa(Request $request) {
        $empresa_id = decrypt($request->input('encryptedID'));
        $empresa = Empresa::find($empresa_id);

        // Se borran los estudios guardados anteriormente.
        $estudios_anteriores = EstudioEmpresa::where('empresa_id', $empresa_id);
        $estudios_anteriores->delete();

        // Se guardan los estudios de la empresa si se seleccionó al menos uno.
        $estudios = $request->input('estudios');
        if (sizeof($estudios) >= 1) {
            $elements = [];
            foreach ($estudios as $key => $value) {
                array_push($elements, [
                    'estudio_id' => $key,
                    'empresa_id' => $empresa_id,
                    'created_at' => \Carbon::now(),
                    'updated_at' => \Carbon::now()
                ]);
            }
        EstudioEmpresa::insert($elements);
        }

        return redirect()->route('admin-showEmpresa', $empresa->nombre)
                ->with(['message' => "Los estudios de la empresa se han actualizado correctamente."]);
    }

    public function editarEmpresa($nombreEmpresa) {
        $empresa = Empresa::where('nombre', $nombreEmpresa)
            ->with('giro')
            ->first();
        $categorias = $this->getCategorias();
        $estudios = $this->getEstudios();
        $giros = Giro::all();
        return view('Administrador/EditarEmpresa', [
            'empresa' => $empresa,
            'encryptedID' => encrypt($empresa->id),
            'categorias' => $categorias,
            'estudios' => $estudios,
            'giros' => $giros
        ]);
    }

    public function editarContacto($nombreEmpresa) {
        $empresa = Empresa::where('nombre', $nombreEmpresa)->first();
        $contacto = User::find($empresa->user_id);
        $categorias = $this->getCategorias();
        $estudios = $this->getEstudios();
        return view('Administrador/EditarContacto', [
            'empresa' => $empresa,
            'contacto' => $contacto,
            'encryptedID' => encrypt($contacto->id),
            'categorias' => $categorias,
            'estudios' => $estudios
        ]);
    }

    public function actualizarEmpresa(Request $request) {
		$empresa_id = decrypt($request->input('encryptedID'));
        $empresa = Empresa::find($empresa_id);
    	$validate = $this->validate($request, [
    		'nombre' => 'required|string|max:250',
    		'direccion' => 'required|string|max:250',
    		'giro' => 'required|string|max:250',
    		'telefono' => 'required|string|max:250',
    		'pagina' => 'required|string|max:250',
        ]);

    	$nombre = $request->input('nombre');
    	$direccion = $request->input('direccion');
    	$giro_id = Giro::where('nombre', $request->input('giro'))->first()->id;
    	$telefono = $request->input('telefono');
    	$pagina = $request->input('pagina');
        $image_path = $request->file('logo');

    	$empresa->nombre = $nombre;
    	$empresa->direccion = $direccion;
    	$empresa->giro_id = $giro_id;
    	$empresa->telefono = $telefono;
        $empresa->pagina = $pagina;
        if ($image_path) {
            $this->eliminarLogoActual($empresa);
            $empresa->logo = $this->upload_image($image_path);
        }
        $empresa->save();

        $elements = [];
        // foreach ($request->input('estudios') as $key => $value) {
        //     array_push($elements, [
        //         'estudio_id' => $key,
        //         'empresa_id' => $empresa_id,
        //         'created_at' => \Carbon::now(),
        //         'updated_at' => \Carbon::now()
        //     ]);
        // }

        // $estudios_anteriores = EstudioEmpresa::where('empresa_id', $empresa_id);
        // $estudios_anteriores->delete();

        // EstudioEmpresa::insert($elements);

        return redirect()->route('admin-showEmpresa', $empresa->nombre)
            ->with(['message' => "Los datos de la empresa se han actualizado correctamente."]);
    }

    public function actualizarContacto(Request $request) {
        $user_id = decrypt($request->input('encryptedID'));
        $admin = User::find($user_id);
		$empresa = Empresa::where('user_id', $admin->id)->first();
    	$validate = $this->validate($request, [
            'nombre' => 'required|string|max:250',
    		'email' => 'required|string|max:250',
    		'telefono' => 'required|string|max:250',
    		'puesto' => 'required|string|max:250',
    		'area' => 'required|string|max:250',
        ]);

        $usuarioEmail = User::where('email', $request->input('email'))->first();

        if ($usuarioEmail) {
            if ($usuarioEmail->id != $admin->id) {
                return redirect()->route('editarContacto', $empresa->nombre)
                    ->withErrors(["No se han guardado los cambios porque el correo electrónico ya existe en nuestros datos."]);
            }
        }

    	$nombreAdmin = $request->input('nombre');
    	$correoAdmin = $request->input('email');
    	$telefonoAdmin = $request->input('telefono');
    	$puestoAdmin = $request->input('puesto');
    	$areaAdmin = $request->input('area');

    	$admin->nombre = $nombreAdmin;
    	$admin->email = $correoAdmin;
    	$admin->telefono = $telefonoAdmin;
    	$admin->puesto = $puestoAdmin;
    	$admin->area = $areaAdmin;
        $admin->save();

        return redirect()->route('admin-showEmpresa', $empresa->nombre)
            ->with(['message' => "Los datos del contacto se han actualizado correctamente"]);
    }
}
