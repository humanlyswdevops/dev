<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Empleado;
use App\RegistroEntrada;
use App\EstudioProgramado;
use App\Estudios;
use App\Resultados;
use App\Archivo;
use App\HistorialClinico;

use App\Expediente;

class HistorialFormController extends Controller
{
    public function permiso() {
        if (Session::get('permiso') != 'medico') {
            return abort(403);
        }
    }

    public function view($curp_encrypt) {
        try {
            $curp = decrypt($curp_encrypt);
        } catch (DecryptException $e) {
            return abort(404);
        }
        $info=null;
        $empleado = $this->getEmpleado($curp);
        // $historial = $this->getHistorialesClinico($empleado->CURP);
        $historial = $this->getHistorialesClinico($curp);
        $estudios = $this->getEstudios($empleado->id);
       
        if ($historial) {
            
            $identificacion = json_decode($historial->identificacion, true);
            $FData = json_decode($historial->heredofamiliar, true);
            $Hfamiliar = json_decode($FData['data'], true);
            // return; FIXME: no se por que se coloco este return, lo comento para que pueda pasar y continuar probando.
            $noPatologico = json_decode($historial->noPatologico, true);
            $patologico = json_decode($historial->patologico,true);
            $genicoobstetrico = json_decode($historial->genicoobstetrico,true);
            $historialLaboral = json_decode($historial->historialLaboral,true);
            $historialfisico = json_decode($historial->examenFisico,true);
            $resultado = json_decode($historial->resultado,true);

            $info = array('identificacion' => $identificacion,
            'Heredofamiliar' => $Hfamiliar,
            'noPatologico' => $noPatologico,
            'patologico' => $patologico,
            'genicoobstetrico' => $genicoobstetrico,
            'historialLaboral' => $historialLaboral,
            'historialfisico' => $historialfisico,
            'resultado' => $resultado
            );
        }else {
            $info = null;
        }

        if ( !isset($estudios)) {
        $estudios=[];
        }

    //  dd($info['historialLaboral']['genitourinario']);
        return view('Laboratorio/Medicina/Estudio')->with([
            'paciente' => $empleado,
            'estudios'=> $estudios,
            'historial' =>$info
        ]);
    }
    public function RecargarHistorial($curp_encrypt) {

        try {
            $curp = decrypt($curp_encrypt);
        } catch (DecryptException $e) {
            return abort(404);
        }
        $info=null;
        try {
          $empleado = $this->getEmpleado($curp);
          $historial = $this->getHistorialesClinico($empleado->CURP);
          $estudios = $this->getEstudios($empleado->id);

          $identificacion = json_decode($historial->identificacion, true);
           $FData = json_decode($historial->heredofamiliar, true);
           $Hfamiliar = json_decode($FData['data'], true);
           $noPatologico = json_decode($historial->noPatologico, true);
           $patologico = json_decode($historial->patologico,true);
           $genicoobstetrico = json_decode($historial->genicoobstetrico,true);
           $historialLaboral = json_decode($historial->historialLaboral,true);
           $historialfisico = json_decode($historial->examenFisico,true);
           $resultado = json_decode($historial->resultado,true);

           $info = array('identificacion' => $identificacion,
           'Heredofamiliar' => $Hfamiliar,
           'noPatologico' => $noPatologico,
           'patologico' => $patologico,
           'genicoobstetrico' => $genicoobstetrico,
           'historialLaboral' => $historialLaboral,
           'historialfisico' => $historialfisico,
           'resultado' => $resultado
         );
        } catch (\Exception $e) {

        }
    //  dd($info['historialLaboral']['genitourinario']);
        return view('Laboratorio/Medicina/Estudio')->with([
            'paciente' => $empleado,
            'estudios'=> $estudios,
            'historial' =>$info,
        ]);
    }
    public function getEmpleado($curp) {
        return Empleado::where('CURP',$curp)->first();
    }
    public function getHistorialesClinico($curp){
    //   $expediente = Expediente::where('curp',$curp)->first();
    //   if ($expediente) {
        return HistorialClinico::where('curp',$curp)
              ->orderBy('created_at','desc')
              ->first();
    //   }else {
    //     return null;
    //   }

    }
    // public function getEstudios($id) {
    //     return  RegistroEntrada::select('e.id', 'e.nombre', 'registrosentradas.updated_at as fecha', 'c.nombre as categoria')
    //     ->where([
    //             'empleado_id' => $id,
    //             'status' => 2
    //         ])
    //     ->join('estudios as e', 'e.id','registrosentradas.estudios_id')
    //     ->join('categorias as c', 'c.id', 'e.categoria_id')
    //     ->get();
    // }
    public function getEstudios($id) {
        return  EstudioProgramado::select()
        ->where('empleado_id', $id)
        ->whereIn('status', [1, 2])
        ->get();

        // $toma =  EstudioProgramado::select()
        // ->where('empleado_id', $id)
        // ->whereIn('status', [1, 2])
        // ->get();
        // $estudios = \json_decode($toma->estudios);
        // $estudios = Estudios::whereIn('id',$estudios)->get();
        // return $toma = array(
        // 'toma' => $toma,
        // 'estudios' => $estudios
        // );
    
        // return response()->json($toma, 200);
      
    }


    public function getIdProgramados($empleado_id) {
        return EstudioProgramado::select()
        ->where([
            ['historialClinico',null],
            ['empleado_id', $empleado_id]
            ])
        ->whereIn('status', [1, 2])
        ->first();
    }

    public function getIdResultado($empleado_id, $estudio_id) {
        $programado = $this->getIdProgramados($empleado_id);
        return Resultados::select('id')
            ->where([
                ['estudiosProgramados_id', $programado->id],
                ['estudio_id', $estudio_id]
            ])
            ->first();
    }
    public function getArchivos($empleado_id, $estudio_id) {
        $resultadoId = $this->getIdResultado($empleado_id, $estudio_id);
        $document = Archivo::where('resultado_id', $resultadoId->id)->get();
        return json_encode($document);
    }

    public function historialForm(Request $request) {

        // $this->validate($request, [
        //     'fecha_estudio' => 'required|date',
        //     'estadoCivil' => 'required',
        //     'nacimiento' => 'required|date',
        //     'estado_salud' => 'required',
        //     'resultado_aptitud' => 'required',
        //     'pacienteId' => 'required',
        //     'generoPaciente' => 'required'
        // ]);
        $curp=$request->curp;
        try {
            $idPaciente = decrypt($request->get('pacienteId'));
        } catch (DecryptException $e) {
            return abort(404);
        }
        if($request->formulario=="cadenacustodia"){
            $cadenacustodia=$this->cadenacustodia($request);
            $idheredofamiliares = null;
            $no_patologicos = null;
            $patologico = null;
            $historial= null;
            $examen = null;
            $resultHistorial = null;
        }
        else{
            $cadenacustodia=null;
            $idheredofamiliares = $this->heredofamiliares($request);
            $no_patologicos = $this->noPatologicos($request);
            $patologico = $this->patologicos($request);
            $historial= $this->historia_laboral($request);
            $examen = $this->examen_fisico($request);
            $resultHistorial = $this->resultadosH($request);
    }

        if ($request->get('generoPaciente') == 'FEMENINO' || $request->get('generoPaciente') == 'Femenino') {
            $genicoobstetrico = $this->genicoobstetricos($request);
            $historial = $this->identificacionController($request, $idPaciente, $idheredofamiliares, $no_patologicos, $patologico, $historial, $examen, $resultHistorial, $genicoobstetrico,$cadenacustodia,$curp);
        } else {
            $historial =  $this->identificacionController($request, $idPaciente, $idheredofamiliares, $no_patologicos, $patologico, $historial, $examen, $resultHistorial,null,$cadenacustodia,$curp);
        }

        $empleado = Empleado::find($idPaciente);

        return json_encode($empleado);
    }

    #agregar id de todos las categorias de
    #los historiales clinicos
     public function identificacionController($data, $idPaciente, $idheredofamiliares, $no_patologicos, $patologico, $historial, $examen, $resultHistorial, $genicoobstetrico=NUll,$cadenacustodia,$curp){
        if (empty($cadenacustodia)){
            $cadenacustodia=null;
        }
      $identificacion =  json_encode([
          'fecha' => $data->get('fecha_estudio'),
          'numEmpleado' => $data->get('noEmpleado'),
          'departamento' => $data->get('departamento'),
          'estadoCivil' => $data->get('estadoCivil'),
          'escolaridad' => $data->get('escolaridad'),
          'domicilio' => $data->get('domicilio'),
          'lugarNacimiento' => $data->get('lugarnacimiento'),
          'ciudad' => $data->get('ciudad'),
          'municipio'=>$data->get('municipio'),
          'estado' => $data->get('estado'),
          'nom_per_em' => $data->get('emergencia'),
          'parentesco' => $data->get('parentesco'),
          'telefono_1' => $data->get('telefonoParenteso'),
          'domicilio_em' => $data->get('domicilioEm'),
          'lug_trabajo' => $data->get('lugtrabajo'),
          'telefono_2' => $data->get('telefonoParenteso2'),
        ]);
        // $hc = HistorialClinico::where('origen',1)
        // ->where('estado','proceso')
        // ->first();
        $hc=new HistorialClinico();
        
            // id de todos las secciones dentro del historial
            $hc->identificacion = $identificacion;
            $hc->estado = 'finalizado';
            $hc->heredofamiliar = $idheredofamiliares;
            $hc->noPatologico = $no_patologicos;
            $hc->patologico = $patologico;
            $hc->genicoobstetrico = $genicoobstetrico;
            $hc->historialLaboral = $historial;
            $hc->examenFisico = $examen;
            $hc->resultado = $resultHistorial;
            $hc->cadena_custodia = $cadenacustodia;
            $hc->curp = $curp;
            $hc->save();
            $idHistorial = $hc->id;
            $this->asignacion($idHistorial, $idPaciente);
            return $idHistorial;
        
    }
    public function cadenacustodia($data) {
        $folio_web_flow = $data->input('folio_web_flow');
        $carta_compromiso=$data->input('carta_compromiso');
        $carta_compromiso=$data->input('carta_compromiso');
        $nombre=$data->input('nombre');
        $expedida_por=$data->input('expedida_por');
        $numero=$data->input('numero');
        $n_fecha=$data->input('n_fecha');
        $orina=$data->input('orina');
        $sangre=$data->input('sangre');
        $peso=$data->input('peso');
        $h_ayuno=$data->input('h_ayuno');
        $talla=$data->input('talla');
        $examen_medico=$data->input('examen_medico');
        $electrocardiagrama=$data->input('electrocardiagrama');
        $papanicolau=$data->input('papanicolau');
        $laboratorio=$data->input('laboratorio');
        $fecha=$data->input('fecha');
        $hora=$data->input('hora');
        $ayuno=$data->input('ayuno');
        $medico=$data->input('medico');
        $cedula=$data->input('cedula');
        $glucosa=$data->input('glucosa');
        $resultado=$data->input('resultado');
        $centrifugada=$data->input('centrifugada');
        
        return  json_encode([
        'folio_web_flow'=>$folio_web_flow,
        'expedida_por' => $expedida_por,
        'carta_compromiso' =>$carta_compromiso,
        'nombre' =>$nombre,
        'expedida_por' =>$expedida_por,
        'numero' =>$numero,
        'n_fecha' =>$n_fecha,
        'orina' =>$orina,
        'sangre' =>$sangre,
        'peso' =>$peso,
        'h_ayuno' =>$h_ayuno,
        'talla' =>$talla,
        'examen_medico' =>$examen_medico,
        'electrocardiagrama' =>$electrocardiagrama,
        'papanicolau' =>$papanicolau,
        'laboratorio' =>$laboratorio,
        'fecha' =>$fecha,
        'hora' =>$hora,
        'ayuno' =>$ayuno,
        'medico' =>$medico,
        'cedula' =>$cedula,
        'glucosa' =>$glucosa,
        'resultado' =>$resultado,
        'centrifugada' =>$centrifugada
      ]);
    }
    public function heredofamiliares($data) {
        $padre = array(
            'familiar' => 'Padre' ,
            'estado' => $data->get('padre_estado'),
            'enfermedad' => $data->get('select_padre')
        );

        $madre = array(
            'familiar' =>'Madre',
            'estado'=> $data->get('madre_estado'),
            'enfermedad' => $data->get('select_madre')
        );

        $hermanos = array(
            'familiar' => 'Conyuge',
            'estado' => $data->get('conyuge_estado'),
            'enfermedad' => $data->get('select_conyuge')
        );

        $conyuge = [
            'familiar' => 'Hermanos(as)' ,
            'estado'=> $data->get('hermano_estado'),
            'enfermedad' => $data->get('select_hermanos')
        ];

        $hijos = array(
            'familiar' => 'Hijos',
            'estado'=> $data->get('hijos_estato'),
            'enfermedad' => $data->get('select_hijos')
        );

        $datos = json_encode(array($padre, $madre, $hermanos, $conyuge, $hijos));

        // $hf = new Heredofamiliar;
        // $hf->data = $datos;
        // $hf->save();

        return json_encode([
          'data'=>$datos
        ]);
    }

    public function noPatologicos($data) {
        $tetano = $data->input('tetano');
        $rubela = $data->input('rubeola');
        $bcg = $data->input('bcg');
        $hepatitis = $data->input('hepatitis');
        $influnza = $data->input('influenza');
        $neumococica = $data->input('neumococica');
        $vacunas = [
            'tetano' => $tetano,
            'rubeola' => $rubela,
            'bcg' => $bcg,
            'hepatitis' => $hepatitis,
            'influenza' => $influnza,
            'neumococica' => $neumococica,
            'otros' => $data->input('otrasvacunas')
        ];

        if ($data->get('cigarro') == 'si') {
            $cigarro = [
                'Cigarro' => 'Si',
                'edad' => $data->input('cigarro_edad'),
                'frecuencia' => $data->input('cigarropromedio'),
                'noConsumir' => $data->input('dejarcigarro')
            ];

        } elseif ($data->get('cigarro') == 'no') {
            $cigarro =[
                'Cigarro' => 'No',
                'edad' => NULL,
                'frecuencia' => NULL,
                'noConsumir' => NULL
            ];
        }
        ////////////////////////////////////////////

        if ($data->get('alcohol') == 'si') {
            $alcohol = [
                'Alcohol' => 'Si',
                'edad' => $data->input('alcohol_edad'),
                'frecuencia' => $data->input('alcohol_frecuencia'),
            ];
        } elseif ($data->get('alcohol') == 'no') {
            $alcohol = [
                'Alcohol' => 'No',
                'edad' => NULL,
                'frecuencia' => NULL,
            ];
        }
        ////////////////////////////////////////////

        if ($data->get('drogas') == 'si') {
            $drogas=[
                'Drogas' => 'Si',
                'edad' => $data->input('drogas_edad'),
                'frecuencia' => $data->input('drogas_frecuencia'),
            ];
        } elseif ($data->get('drogas') == 'no') {
            $drogas = [
                'Drogas' => 'No',
                'edad' =>NULL,
                'frecuencia' => NULL,
            ];
        }
        /////////////////////////////////////////

        if ($data->get('dieta') == 'si') {
            $dieta = [
                'Dieta' => 'Si',
                'descripcion' => $data->input('dieta_descripcion'),
            ];
        } elseif ($data->get('dieta') == 'no') {
            $dieta = [
                'Dieta' => 'No',
                'descripcion' => NULL,
            ];
        }
        /////////////////////////////////////////

        if ($data->get('deporte') == 'si') {
            $deporte = [
                'Deporte'=> 'Si',
                'frecuencia'=> $data->input('deporte_frecuencia'),
            ];
        } elseif ($data->get('deporte') == 'no') {
            $deporte =[
                'Deporte'=> 'No',
                'frecuencia'=> NULL,
            ];
        }

        // /////////////////////////////////////////
        return  json_encode([
        'vacunas'=>$vacunas,
        'g_sanguineo' => $data->input('tiposangre'),
        'transfusion' => $data->get('transfusion'),
        'medica_ingeridos' => $data->get('mediIngeridos'),
        'cigarro'=>$cigarro,
        'alcohol'=>$alcohol,
        'drogas'=>$drogas,
        'drogas_usadas'=>$data->get('drogas_usadas'),
        'dieta'=>$dieta,
        'deporte'=>$deporte,
        'pasatiempo'=>$data->get('pasatiempo')
      ]);
    }

    public function patologicos($data) {
        $dental = [
            'Cirugía dental' => $data->get('c_dental'),
            'Descripcion' => $data->get('dental')
        ];

        $estomago = [
            'Cirugía de estomago/ úlceras gastricas' => $data->get('c_estomago'),
            'Descripcion' => $data->get('estomago')
        ];

        $cuello = [
            'Lesion en cuello/ espalda / rodillas, manos, codos' => $data->get('lesiones'),
            'Descripcion' => $data->get('cuello')
        ];

        $recto = [
            'Cirugía de colon/ recto' => $data->get('c_colon'),
            'Descripcion' => $data->get('colon')
        ];

        $intestinos = [
            'Cirugía de intestinos' => $data->get('c_intestino'),
            'Descripcion' => $data->get('intestino')
        ];

        $oofo = [
            'Ooforectomia/ Histerectomia' => $data->get('ooforectomia'),
            'Descripcion' => $data->get('Histerectomia')
        ];

        $vasectomia = [
            'Salpingoclasia/ Vasectomia' => $data->get('sal_vas'),
            'Descripcion' => $data->get('Vasectomia')
        ];

        $hernias = [
            'Cirugia de hernias' => $data->get('c_hernia'),
            'Descripcion' => $data->get('hernias')
        ];

        $tiroides = [
            'Cirugia de tiroides' => $data->get('c_tiroides'),
            'Descripcion' => $data->get('tiroides')
        ];

        $colecis = [
            'Colecistectomia' => $data->get('colecistectomia'),
            'Descripcion' => $data->get('Colecistectomia')
        ];

        $apendi = [
            'Apéndicectomia' => $data->get('apendicectomia'),
            'Descripcion' => $data->get('apendicectomias')
        ];

        $fracturas = [
            'Fracturas o esguinces' => $data->get('fracturas'),
            'Descripcion' => $data->get('esguinces')
        ];

        $cirugiasLesion = [
            $dental,
            $estomago,
            $cuello,
            $recto,
            $intestinos,
            $oofo,
            $vasectomia,
            $vasectomia,
            $hernias,
            $tiroides,
            $colecis,
            $apendi,
            $fracturas
        ];

        return json_encode([
        'enfermPadecidas' => $data->get('enfermedad_padecida'),
        'transtornos' => $data->get('transtornos_ep'),
        'alergiaMedica'  => $data->get('alergiasMedicas'),
        'alergiaPiel' => $data->get('alergia_piel'),
        'alergiaOtro' => $data->get('alergia_otro'),
        'tumorCancer' => $data->get('tumo_cancer'),
        'probVista' => $data->get('problema_vista'),
        'enfOido' => $data->get('enfer_oido'),
        'probCulumVert' => $data->get('columna_vertebral'),
        'huesoArticulacion' => $data->get('hueso_articulacion'),
        'otroProbMedico' => $data->get('otro_problema'),
        'cirugiasLesion'=>$cirugiasLesion,
        'otra_cirugia'=>$data->get('otra_cirugia')
        ]);
    }

    public function genicoobstetricos($data) {
        return json_encode([
          'inicioRegla' => $data->get('inicio_regla'),
          'fRegla' => $data->get('frecuencia_regla'),
          'duraRegla' => $data->get('duracion_regla'),
          'relacionSexInicio' => $data->get('inicio_re_sexual'),
          'metodoUsado' => $data->get('metodo_antic'),
          'ultimaMestruacion' => $data->get('ult_mestruacion'),
          'numEmbarazos' => $data->get('embarazos'),
          'numPartos' => $data->get('partos'),
          'numCesareas' => $data->get('cesareas'),
          'numAbortos' => $data->get('abortos'),
          'fecha_examenCancer' => $data->get('examen_matriz'),
          'fecha_examenMamas' => $data->get('examen_mamas'),
          'edadMenopausia' => $data->get('menopausia')
        ]);
    }

    public function historia_laboral($data) {

        $emple_uno = [
            'nombre' => $data->get('nombreEmpresa_actual'),
            'puesto' => $data->get('actual_puesto'),
            'actividad' => $data->get('actual_empleo_actividad'),
            'duracion' => $data->get('actual_empleo_duracion'),
            'seguridad' => $data->get('actual_equipoS'),
            'danos' => $data->get('actual_empleo_danos'),
            'tipo' => '1'
        ];

        $emple_dos = [
            'nombre' => $data->get('nomEmpresa_anterior'),
            'puesto' => $data->get('anterior_puesto'),
            'actividad' => $data->get('anterior_empleo_actividad'),
            'duracion' => $data->get('anterior_empleo_duracion'),
            'seguridad' => $data->get('anteriro_equipoS'),
            'danos' => $data->get('anterior_empleo_danos'),
            'tipo' => '2'
        ];

        $emple_tres = [
            'nombre' => $data->get('nomEmpresa_anterior_two'),
            'puesto' => $data->get('two_anterior_puesto'),
            'actividad' => $data->get('two_anterior_empleo_actividad'),
            'duracion' => $data->get('two_anterior_empleo_duracion'),
            'seguridad' => $data->get('two_anteriro_equipoS'),
            'danos' => $data->get('two_anterior_empleo_danos'),
            'tipo' => '3'
        ];

        $empleos =[
            $emple_uno,
            $emple_dos,
            $emple_tres
        ];

        return json_encode([
          'musculoesqueletico'=>$data->get('musculoesqueletico'),
          'neurologico' =>  $data->get('neurologico_psicologico'),
          'gastroIntestinal' =>  $data->get('gastro_intestinal'),
          'genitourinario' =>  $data->get('genitourinario'),
          'cardiovasucular' =>  $data->get('cardiovascular'),
          'pulmunar' =>  $data->get('pulmunar'),
          'endocrino' =>  $data->get('endocrino'),
          'inmunologico' =>  $data->get('inmunologico'),
          'dermatologico' =>  $data->get('dermatologico'),
          'hematologico' => $data->get('hematologico'),
          'alergiaMedicamentos' => $data->get('reaccion_alergicas_medicamentos'),
          'problemaSalud' => $data->get('problema_salud'),
          'problemaMujer' =>$data->get('padecimientos_mujer'),
          'abortos' => $data->get('abortos_ano'),
          'fechaParto' => $data->get('embarazada_parto'),
          'problemaHombre' =>$data->get('padecimientos_hombre'),
          'padecimientoHombre' =>$data->get('otros_padecimientos_hombre'),
          'trabajoRealizados' => $data->get('trabajos_realizados'),
          'otrosTrabajos' => $data->get('otrosTrabajos'),
          'trabajoMateriales' => $data->get('materiales_expuesto'),
          'otrasExposiciones' => $data->get('otras_exposiciones'),
          'empleos' => $empleos,
          'equipoSegActual' => $data->get('equipo_seguridad'),
          'examenVista' => $data->get('examen_vista'),
          'lentesContacto' => $data->get('uso_lentes_graduados'),
          'otras_exposiciones'  => $data->get('otrasexpPuesto'),
          'famMaterialPeligroso' => $data->get('ma_peligroso_fam'),
          'lugaresVividos' => $data->get('lugar_vivido'),
          'expMaterialPeligroso' => $data->get('exp_mat_peligrosos'),

        ]);
    }

    public function examen_fisico($data) {

      return json_encode([
        'apariencia' => $data->get('apariencia'),
        'temperatura' => $data->get('temperatura'),
        'fc' => $data->get('fc'),
        'fr' => $data->get('fr'),
        'altura' => $data->get('altura'),
        'peso' => $data->get('peso'),
        'imc' => $data->get('imc'),
        'psBrazoDerecho' => $data->get('presion_derecho'),
        'psBrazoIzquierdo' => $data->get('presion_izquierdo'),
        'cabezaCueroOjos' => $data->get('observaciones_cabeza'),
        'oidoNarizBoca' => $data->get('observaciones_oidos'),
        'dientesFaringe' => $data->get('observaciones_dientes'),
        'cuello' => $data->get('observaciones_cuello'),
        'tiroides' => $data->get('observaciones_tiroides'),
        'nodoLinfatico' => $data->get('observaciones_nodo'),
        'toraxPulmones' => $data->get('observaciones_torax'),
        'pecho' => $data->get('observaciones_pecho'),
        'corazon' => $data->get('observaciones_corazon'),
        'abdomen' => $data->get('observaciones_abdomen'),
        'rectalDigital' => $data->get('observaciones_rectal'),
        'genitales' => $data->get('observaciones_genitales'),
        'columnaVertebral' => $data->get('observaciones_columna'),
        'piel' => $data->get('observaciones_piel'),
        'pulsoArterial' => $data->get('observaciones_pulso'),
        'extremidades' => $data->get('observaciones_extremidades'),
        'musculoesqueleto' => $data->get('observaciones_musculoesque'),
        'reflejosNeurologicos' => $data->get('observaciones_relejos'),
        'estadoMental' => $data->get('observaciones_mental'),
        'otrosComentarios' => $data->get('observaciones_comentarios'),
        'ojoIzquierdo' => $data->get('agudeza_izquierdo'),
        'ojoDerecho' => $data->get('agudeza_derecho'),
        'uso_epp' => $data->get('uso_epp'),
        'audiometria' => $data->get('audiometria'),
        'respiradores' => $data->get('respiradores_r'),
        'reentrenar' => $data->get('reentrenar'),
        'otrasRecomend' => $data->get('otra_recomendacion'),
        'recomEstiloVida' => $data->get('recomendaciones_vida'),
        'diagnosticoAudio' => $data->get('diagnostico_audiologico'),
      ]);
    }

    public function resultadosH($data) {
      return json_encode([
        'estadoSalud' => $data->get('estado_salud'),
        'aptitud' => $data->get('resultado_aptitud'),
        'comentarios' => $data->get('comentarios_finales')
      ]);
    }

    public function asignacion($idHistorial,$idPaciente) {
        try {
            $estudioProgramado = EstudioProgramado::where([['empleado_id', $idPaciente], ['historialClinico', NULL]])->first();
            $estudioProgramado->historialClinico = $idHistorial;
            $estudioProgramado->save();
            return 'Exito';
        } catch (\Exception $e) {
            return 'Error';
        }
    }


    public function regresaformato(Request $request){
            $formato=$request->formatos;
                return compact('formato');
        
    }
}
