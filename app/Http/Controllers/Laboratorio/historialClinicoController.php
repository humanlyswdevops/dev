<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Empleado;
use App\RegistroEntrada;
use App\EstudioProgramado;
use App\Empresa;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use mysqli;

class historialClinicoController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Controlador de historiales clínicos
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos de un historial clínico
    |
    */

    /**
     * Revisar si el permiso de usuario es de médico
     *
     * Revisa que el permiso guardado en la sesión sea la de
     * médico, sino manda un error 403 Prohibido
     *
     * @return error El error de Prohibido
     */
    private function permiso() {
        if (Session::get('permiso') != 'medico') {
            return abort(403);
        }
    }

    /**
     * Obtener la vista del historial clínico
     *
     * Devuelva la vista que contiene la información del paciente
     * incluyendo sus estudios.
     *
     * @return Laboratorio\Medicina\historial Vista con información del paciente
     */
    public function view($empresa_id) {
        $empresa = Empresa::find($empresa_id);
        if($empresa->dominio == null || $empresa->dominio == "")
        {
            abort(404);
        }
        $data_insert["fecha"] = now();
        $data_insert["nombre"] = 'Alvaro Arellano';
        $data_insert["puesto"] = 'Ventas';
        $data_insert["sucursal"] = 'Matriz';
        $data_insert["privilegio"] = 'medico';
        $data_insert["id_empleado"] = 'A005';
        $data_insert["token"] = now()->timestamp.rand(0,100);
        $data_db["host_db"] = $empresa->host_db;
        $data_db["user_db"] = $empresa->user_db;
        $data_db["password_db"] = $empresa->password_db;
        $data_db["name_db"] = $empresa->name_db;

        $this->insertarDatosHTDS($data_insert,$data_db);

        $token = $data_insert["token"];

        // return redirect($empresa->dominio.'auth/lab-login/'.$token);

        Session::put('empresa_select',$empresa_id);
        $this->permiso();
        $pacientes = $this->getPaciente($empresa_id);
        $tiene_pendientes = false;
        foreach ($pacientes as $paciente) {
            $paciente->estudios = RegistroEntrada::select('estudio.nombre', 'status', 'empleado_id')
                ->join('estudios as estudio', 'registrosentradas.estudios_id', 'estudio.id')
                ->whereIn('status', [0, 1, 2])
                ->where('empleado_id', $paciente->empleado_id)
                ->get();

            $estudio_pendiente = RegistroEntrada::where('empleado_id', $paciente->empleado_id)
                ->whereIn('status', [0, 1])
                ->first();

            if ($estudio_pendiente) {
                $tiene_pendientes = true;
            }
            else {
                $tiene_pendientes = false;
            }

            $paciente->tiene_pendientes = $tiene_pendientes;
        }

        return view('Laboratorio/Medicina/historialEmp')->with([
            'pacientes' => $pacientes
        ]);
    }
    public function estudiossass(){
        return view('Empresa.ResultadosEmpresa');
    }
    public function insertEmpleadoEmpresa(Request $request){
       $empresa=$request->id_empresa;
       $clave = 'HAS-'.time();
       $curp=$request->curp;
       $empleado = Empleado::find($curp);
       if (empty($empleado)){
           
       }
       if (empty($curp)) {$curp=Str::upper(Str::random(18));}    
       $apellidos=$request->apellidos;
       $empleado = new Empleado;
       $empleado->clave = $clave;
       $empleado->nombre = ucfirst(strtolower($request->nombre));
       $empleado->CURP = $curp;
       $apellido= explode(" ", $apellidos);
        // porción1
       $empleado->apellido_paterno = ucfirst(strtolower($apellido[0]));
       $empleado->apellido_materno = ucfirst(strtolower($apellido[1]));
       // $empleado->apellido_materno = ucfirst(strtolower($request->apm));
       $empleado->genero = $request->sexo;
       //$empleado->genero = $request->edad;
       //$empleado->fecha_nacimiento = null;
       //$empleado->lugar_nacimiento = null;
       //$empleado->nss =null;
       $empleado->empresa_id = $empresa;
       $empleado->status_id = 1;
       $empleado->grupo_id = 8;
        //    $empleado->colonia = null;
        //    $empleado->cp =null;
        //    $empleado->municipio = null;
        //    $empleado->estado = null;
        //    $empleado->email =null;
        //    $empleado->telefono = null;
       $empleado->save();
       $id=$empleado->id;
       $elements=$request->estudiosempleado;
       $estudios_por_programar = [];
         foreach ($elements as $element)
        {
        array_push($estudios_por_programar, (int)$element);
         }
       $token = Hash::make(Str::random(4));
       $estudios_JSON = json_encode($estudios_por_programar);
       $estudio_programado = new EstudioProgramado;
       $estudio_programado->estudios = $estudios_JSON;
       $estudio_programado->folio = $request->folio;
       $estudio_programado->empleado_id =  $id;
       $estudio_programado->status = 0;
       $estudio_programado->empresa_id = 6;
       $estudio_programado->token = $token;
       // $estudio_programado->folio = 'HS-'.time();
       $estudio_programado->save();

       //   return json_encode($empleado);
       return json_encode($empleado);
   }
   public function buscaempleado($curp){
    return $empleado = Empleado::find($curp);

   }
   
    
   public function programarEstudiosEmpleado(Request $request){
    $empleado_id = decrypt($request->encryptedID);
    $CURP = $request->input('CURP');
    $elements = $request->input('estudios_programar');
    $empleado = Empleado::find($empleado_id);

    $estudios_por_programar = [];
    foreach ($elements as $element)
    {
        array_push($estudios_por_programar, (int)$element);
    }

    $estudios_detalle = Estudios::whereIn('id', $estudios_por_programar)->get();

    $estudios_JSON = json_encode($estudios_por_programar);

    $token = Hash::make(Str::random(4));


    $estudio_programado = new EstudioProgramado;
    $estudio_programado->estudios = $estudios_JSON;
    $estudio_programado->fecha_inicial = $request->input('fechaInicio');
    $estudio_programado->fecha_final = $request->input('fechaFinal');
    $estudio_programado->status = 0;
    $estudio_programado->empleado_id = $empleado->id;
    $estudio_programado->empresa_id = Session::get('empresa')->id;
    $estudio_programado->token = $token;
    // $estudio_programado->folio = 'HS-'.time();
    $estudio_programado->save();

    $empresa = Empresa::find($empleado->empresa_id);

    RegistroEntrada::where([['status', 2], ['empleado_id', $empleado
        ->id]])
        ->update(['status' => 3]);

    $data = array(
        'estudios' => $estudio_programado,
        'paciente' => $empleado,
        'detalle' => $estudios_detalle,
        'empresa' => $empresa
    );

    return response()->json($data, 200);
    }

    // NOTE: Mostrar empresas que estan registradas

    public function view_empresas() {
        $this->permiso();
        $empresas = Empresa::all();

        return view('Laboratorio/Medicina/historial')->with([
            'empresas' => $empresas
        ]);
    }
    public function edit_empresa(Request $request)
    {
            $id = $request->id_e;
            $empresa = Empresa::find($id);
            if (!empty($empresa)) {
                $empresa->nombre = $request->nombre_e;            
                $empresa->direccion = $request->direccion_e;
                $image = $request->file('logo_e');
                if($image!=null){
                $antiguo_documento = $empresa->logo;
                if ($antiguo_documento) Storage::disk('empresa')->delete($antiguo_documento);
                $empresa->logo = $this->upload_image($image);
                }
                $empresa->user_id =  2;
                $empresa->save();
                return json_encode($empresa);
            }
           
    }
    public function getEmp($id)
    {
        $medic = Empresa::find($id);

        // Si ningún empleado coincide con el ID, manda un error 404.
        return compact("medic");
    }
    
    public function add_empresa(Request $request)
    {
        
            $empresa = new Empresa();
            $admin = new User;
            $empresa->nombre = $request->nombre;  
            $empresa->direccion = $request->direccion;  
            $image = $request->logo;
            $empresa->logo = $this->upload_image($image);                
            $empresa->user_id =  2;
            $empresa->save();
            return json_encode($empresa);
       
    }
    
    public function upload_image($image) {
        $image_nombre = '';
        if ($image) {
            $image_nombre = time() .  $image->getClientOriginalName();
            Storage::disk('empresa')->put($image_nombre, File::get($image));
        } else {
            $image_nombre  = 'null';
        }
        return $image_nombre;
    }
    public function delete_empresa($id)
    {
        
            
            $empresa = Empresa::find($id);
            $empresa->delete();
            
            return compact("empresa");
       
    }
    /**
     * Obtener información de un paciente
     *
     * Devuelve los datos del paciente.
     *
     * @return App\RegistroEntrada[] Arreglo que contiene solo un paciente con su información de estudios
     */
    public function getPaciente($id_empresa) {
      $empresa = Empresa::find($id_empresa);
      if ($empresa->id == 6) {
        return Empleado::where('empresa_id',$id_empresa)->get();
      }else {
        $this->permiso();
        return RegistroEntrada::select('ep.empleado_id', 'emp.nombre', 'emp.apellido_paterno as app', 'emp.apellido_materno as apm', 'emp.fecha_nacimiento', 'emp.CURP', 'emp.genero')
            ->distinct('empleado_id')
            ->join('empleados as emp', 'empleado_id', 'emp.id')
            ->join('estudiosProgramados as ep','ep.empleado_id', 'emp.id')
            ->where('emp.empresa_id',$id_empresa)
            ->where('ep.historialClinico', NULL)
            ->whereIn('registrosentradas.status', [0, 1, 2])
            ->get();
      }
    }

    /**
     * Obtener los estudios con registro de entrada
     *
     * Devuelve los estudios con registro de entrada.
     *
     * @return App\RegistroEntrada[] Arreglo de estudios con registro de entrada
     */
    public function getEstudios() {
        $this->permiso();
        return $estudios = RegistroEntrada::select('estudio.nombre', 'status', 'empleado_id')
            ->join('estudios as estudio', 'registrosentradas.estudios_id', 'estudio.id')
            ->where('status', 2)
            ->get();
    }

    /**
     * Obtener la cantidad de pacientes por género
     *
     * Devuelve la cantidad de pacientes agrupados
     * por género.
     *
     * @return json Datos sobre la cantidad de pacientes
     */
    public function numPaciente() {
        $this->permiso();
        if (Session::get('empresa_select')==6) {
          $sql =  \DB::select('select emp.genero, COUNT(*) as cantidad from empleados AS emp WHERE emp.empresa_id = 6 group by emp.genero');
        }else {
          $sql =  \DB::select('select emp.genero,COUNT(*) as cantidad from(SELECT ep.empleado_id FROM (select empleado_id from `registrosentradas` WHERE status in(0,1,2) GROUP BY empleado_id) as re JOIN estudiosProgramados as ep ON ep.empleado_id = re.empleado_id WHERE ep.historialClinico IS NULL) as regi join `empleados` as `emp` on regi.empleado_id = `emp`.`id` WHERE emp.empresa_id = '.Session::get('empresa_select').' group by emp.genero');
        }

        return json_encode($sql);
    }

    public function insertarDatosHTDS($data,$data_db)
    {
        $servername = $data_db["host_db"];
        $username = $data_db["user_db"];
        $password = $data_db["password_db"];
        $dbname = $data_db["name_db"];
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = 'INSERT INTO datosHtds (nombre, puesto, sucursal, privilegio, ID_empleado, token) VALUES ("'.$data["nombre"].'", "'.$data["puesto"].'", "'.$data["sucursal"].'", "'.$data["privilegio"].'", "'.$data["id_empleado"].'","'.$data["token"].'")';

        if ($conn->query($sql) === TRUE) {
            $conn->close();
            return true;
        } else {
            $conn->close();
            return false;
        }
    }
}
