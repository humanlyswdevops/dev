<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Session;

use App\RegistroEntrada;
use App\Categoria;
use App\EstudioProgramado;
use App\Imagenologia;
use App\Empleado;

class IngresosController extends Controller
{
    public function __construct($value = '') {

    }

    public function permiso() {
        if (Session::get('permiso') != 2) {
            return abort(403);
        }
    }

    public function view() {
        $this->permiso();
        $empleados = RegistroEntrada::select('empleado_id', 'empleados.nombre', 'empresas.nombre as empresa', 'empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno', 'empleados.CURP', 'empleados.genero', 'empleados.fecha_nacimiento')
            ->distinct('empleado.id')
            ->where([
                ['categoria_id', \Session::get('permiso')],
                ['status', 0]
            ])
            ->join('empleados', 'empleados.id', '=', 'registrosentradas.empleado_id')
            ->join('empresas', 'empresas.id', '=', 'empleados.empresa_id')
            ->get();

        $empleado = (object) [
            'nombre' => session('nombreEmpleado'),
            'puesto' => session('puestoEmpleado'),
            'sucursal' => session('sucursalEmpleado'),
            'privilegio' => session('privilegioEmpleado'),
            'id' => session('IdEmpleado')
        ];

        return view('Laboratorio/TecnicoRadiologo/dashboardEstudios', [
            'empleado' => $empleado,
            'ingresos' => $empleados
        ]);
    }

    public function viewEstudios($id) {
        $this->permiso();
        try {
            $idempleado = decrypt($id);
        } catch (DecryptException $e) {
            return abort(404);
        }

        $empleados = RegistroEntrada::select('estudios.id as estudios_id', 'estudios.nombre as estudio', 'registrosentradas.id as ingreso', 'empleados.id', 'empresas.nombre as empresa', 'empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno', 'empleados.CURP', 'empleados.genero', 'empleados.fecha_nacimiento')
            ->where([
                ['registrosentradas.categoria_id', \Session::get('permiso')],
                ['status', 0],
                ['empleado_id', $idempleado]
            ])
            ->join('empleados', 'empleados.id', '=', 'registrosentradas.empleado_id')
            ->join('empresas', 'empresas.id', '=', 'empleados.empresa_id')
            ->join('estudios',  'estudios.id', '=', 'registrosentradas.estudios_id')
            ->orderBy('registrosentradas.created_at', 'ASC')
            ->get();

        $empleado = Empleado::find($idempleado);

        return view('Laboratorio/TecnicoRadiologo/pacienteEstudios', [
            'empleado' => $empleado,
            'ingresos' => $empleados
        ]);
    }

    public function estudioRealizado(Request $request) {
        $this->permiso();
        $this->validate($request, [
            'id' => 'required',
            'ingreso' => 'required',
            'curp' => 'required'
        ]);

        try {
            $id = decrypt($request->get('id'));
            $ingreso = decrypt($request->get('ingreso'));
        } catch (DecryptException $e) {
            return abort(404);
        }

        $realizado =RegistroEntrada::where('id', $ingreso)->first();
        $existsDicom = Imagenologia::where([
                ['empleado_id', $realizado->empleado_id],
                ['estudio_id', $realizado->estudios_id]
            ])->first();

        if ($existsDicom != null) {
            $realizado->status = 1;
            $realizado->save();
            return json_encode($request->get('curp'));

        } else {
            return abort(401);
        }
    }

    public function dicom(Request $request) {
        $this->permiso();
        $this->validate($request, [
            'nim' => 'required',
            'dicom' => 'required',
            'id' =>'required',
            'estudio_id' => 'required'
        ]);

        try {
            $study = decrypt($request->get('estudio_id'));
            $id = decrypt($request->get('id'));
        } catch (DecryptException $e) {
            return abort(404);
        }

        //$path_name = $this->upload_image($request->file('dicom'));

        $imagen = new Imagenologia;
        $imagen->nim = $request->get('nim');
        $imagen->ruta = $request->get('dicom');
        $imagen->empleado_id = $id;
        $imagen->estudio_id = $study;
        $imagen->save();

        return json_encode($imagen->id);
        // return back()->with('success', 'La imagen dicom a sido registrada');
    }

    public function upload_image($image) {
        $this->permiso();

        $image_nombre;
        if ($image) {
            $image_nombre = time().$image->getClientOriginalName();
            Storage::disk('imagenologia')->put($image_nombre, File::get($image));
        } else {
            $image_nombre = 'null';
        }

        return $image_nombre;
    }
}
