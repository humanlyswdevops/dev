<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;

use App\RegistroEntrada;
use App\Empleado;
use App\Archivo;
use App\Expediente;
use App\EstudioProgramado;
use App\Resultados;

class LaboratorioController extends Controller
{
    public function view() {
        $this->permiso();
        $pacientes = $this->getPacientes();
        return view('Laboratorio/Laboratorio/Pacient', compact('pacientes'));
    }

    public function permiso() {
        if (Session::get('permiso') != 'laboratorio') {
            return abort(403);
        }
    }

    public function getPacientes() {
        $this->permiso();
        return RegistroEntrada::select('emp.id', 'emp.nombre', 'emp.CURP', 'emp.apellido_materno as apm', 'emp.apellido_paterno as app', 'emp.genero', 'emp.fecha_nacimiento')
        ->distinct('registrosentradas.empleado_id')
        ->where('status',0)
        ->whereIn('categoria_id', [1])
        ->join ('empleados as emp', 'emp.id', '=', 'registrosentradas.empleado_id')
        ->get();
    }

    public function pacienteEstudios($id_encrypt) {
        $this->permiso();
            try {
                $idempleado = decrypt($id_encrypt);
            } catch (DecryptException $e) {
                return abort(404);
            }

            $empleados = RegistroEntrada::select('estudios.id as estudios_id', 'estudios.nombre as estudio', 'registrosentradas.id as ingreso', 'empleados.id', 'empresas.nombre as empresa', 'empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno', 'empleados.CURP', 'empleados.genero', 'empleados.fecha_nacimiento')
                ->where([
                    ['status', 0],
                    ['empleado_id', $idempleado]
                ])
                ->whereIn('registrosentradas.categoria_id', [1])
                ->join('empleados', 'empleados.id', '=', 'registrosentradas.empleado_id')
                ->join('empresas', 'empresas.id', '=', 'empleados.empresa_id')
                ->join('estudios',  'estudios.id', '=', 'registrosentradas.estudios_id')
                ->orderBy('registrosentradas.created_at', 'ASC')
                ->get();

            $empleado=Empleado::find($idempleado);

        return view('Laboratorio/Laboratorio/EstudioPaciente', [
            'empleado' => $empleado,
            'ingresos' => $empleados
        ]);
    }

    public function saveResult(Request $request) {
        $this->permiso();
        $this->validate($request, [
            'empleado_id' => 'required',
            'estudio_id' => 'required',
            'registro_id' => 'required'
        ]);

        try {
            $empleado_id = decrypt($request->get('empleado_id'));
            $estudio_id = decrypt($request->get('estudio_id'));
            $entrada_id = decrypt($request->get('registro_id'));
        } catch (DecryptException $e) {
            return abort(404);
        }

        // Confirmamos que se ha tomado la muestra del estudio y ahora está en proceso,
        // en espera de finalizarse por el médico.
        $registro_entrada_update= RegistroEntrada::find($entrada_id);
        $registro_entrada_update->status = 1;
        $registro_entrada_update->save();

        $estudiosCompletados = false;
        
        return json_encode($estudiosCompletados);
    }
}
