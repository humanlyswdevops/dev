<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Imports\EmpleadosImport;
use Maatwebsite\Excel\Excel;


use App\Empleado;
use App\Grupo;
use App\EstudioEmpresa;
use App\EstudioGrupo;

class ImportExcelController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $grupos = Grupo::select('id', 'nombre')
            ->where('empresa_id', Session::get('empresa')->id)
            ->get();

        return view('Empresa/AltaEmGrup', compact('grupos'));
    }

    function Editar($curp) {
        $data = Empleado::select('id', 'nombre', 'fecha_nacimiento', 'CURP', 'apellido_paterno', 'apellido_materno',  'genero', 'direccion', 'email', 'clave', 'telefono', 'identity_excel')
            ->where('CURP',$curp)
            ->first();

        $data->id_ = encrypt($data->id);
        $data->id = 0;
        return $data;
    }

    public function download() {

    }

    function prev(Request $request) {
        $this->validate($request, [
            'excel' => 'required|mimes:xls,xlsx'
        ]);

        //DEBUG
        var_dump($request);
        die();

        $element_excel = (new EmpleadosImport)->toArray($request->file('excel'));
        return json_encode($request);
    }

    public function updateEmpleado(Request $request) {
        $this->validate($request, [
            'clave' => 'required|string',
            'nombre' => 'required|string',
            'app' => 'required|string',
            'apm' => 'required|string',
            'curp' => 'required|string|max:18',
            'nacimiento' => 'required|string',
            'genero' => 'required|string',
            'direccion' => 'required|string',
            'email' => 'required|string|email',
            'telefono' => 'required|string',
        ]);

        try {
            $id = decrypt($request->input('encryptedID'));
        } catch (DecryptException $e) {
            return abort(404);
        }

        $clave = Empleado::where([['id', "!=", $id], ['empresa_id', Session::get('empresa')->id], ['clave','=',$request->input('clave')]])->first();
        $curp = Empleado::where([['id', "!=", $id], ['empresa_id', Session::get('empresa')->id], ['CURP','=',$request->input('curp')]])->first();
        $email = Empleado::where([['id', "!=", $id], ['empresa_id', Session::get('empresa')->id], ['email','=',$request->input('email')]])->first();

        if ($curp != null) {
            return abort(400);
        }
        if ($email != null) {
            return abort(424);
        }
        if ($clave != null) {
            return abort(423);
        }

        $empleado = Empleado::where('id', $id)->first();

        $empleado->clave = $request->input('clave');
        $empleado->nombre = $request->input('nombre');
        $empleado->apellido_paterno = $request->input('app');
        $empleado->apellido_materno = $request->input('apm');
        $empleado->CURP = $request->input('curp');
        $empleado->fecha_nacimiento = $request->input('nacimiento');
        $empleado->email = $request->input('email');
        $empleado->direccion = $request->input('direccion');
        $empleado->telefono = $request->input('telefono');
        $empleado->save();

        return json_encode($empleado);
    }

    function Empleado_excel($excel) {
        $empleados = Empleado::where('identity_excel',$excel)->get();
        return json_encode($empleados);
    }

    function import(Request $request) {

        $this->validate($request, [
            'excel' => 'required|mimes:xls,xlsx',
            'grupo' => 'required'
        ]);


        $element_excel = (new EmpleadosImport)->toArray($request->file('excel'));
        $errors = [];
        $curps = [];
        $indice = 2;
        $encontrado=-1;



        foreach ($element_excel[0] as $key => $row) {
            try {
              $encontrado=array_count_values($curps)[$row['curp']];
            } catch (\Exception $e) {
              $encontrado=-1;
            }
            if ($encontrado==-1) {

              if ($row['clave'] == null) {
                  array_push($errors, ['Se requiere el campo Clave en la fila: ' . $indice]);
              }

              if ($row['nombre'] == null) {
                  array_push($errors, ['Se requiere el campo Nombre en la fila: ' . $indice]);
              }

              if ($row['apellido_paterno'] == null) {
                  array_push($errors, ['Se requiere el campo Apellido Paterno en la fila: ' . $indice]);
              }

              if ($row['apellido_materno'] == null) {
                  array_push($errors, ['Se requiere el campo Apellido Materno en la fila: ' . $indice]);
              }

              if ($row['curp'] == null) {
                  array_push($errors, ['Se requiere el campo Curp en la fila: ' . $indice]);
              }else {
                  array_push($curps, $row['curp']);
              }

              if (strlen($row['curp']) <= 17 || strlen($row['curp']) >= 19) {
                  array_push($errors, ['La curp es invalida en la fila: ' . $indice]);
              }

              if ($row['genero_masculinofemenino'] == null) {
                  array_push($errors, ['Se requiere el campo Genero en la fila: ' . $indice]);
              }

              if ($row['fecha_de_nacimiento_ano_mes_dia'] == null) {
                  array_push($errors, ['Se requiere el campo Fecha de nacimiento en la fila: ' . $indice]);
              }

              if ($row['direccion'] == null) {
                  array_push($errors, ['Se requiere el campo Dirección en la fila: ' . $indice]);
              }

              if ($row['telefono'] == null) {
                  array_push($errors, ['Se requiere el campo Telefono en la fila: ' . $indice]);
              }

              $empleado_curp = Empleado::where([
                  ['CURP', $row['curp']],
                  ['empresa_id', Session::get('empresa')->id]
              ])->first();

              if ($empleado_curp != null) {
                  array_push($errors, ['Curp ya registrada de: ' . $row['nombre'] . ' ' . $row['apellido_paterno'] . ' ' . $row['apellido_materno'] . ' en la fila:' . $indice]);
              }

              $empleado_clave = Empleado::where([
                  ['clave', $row['clave']],
                  ['empresa_id', Session::get('empresa')->id]
              ])->first();

              if ($empleado_clave != null) {
                  array_push($errors, ['Clave ya registrada de: ' . $row['nombre'] . ' ' . $row['apellido_paterno'] . ' ' . $row['apellido_materno'] . ' en la fila:' . $indice]);
              }

              $indice++;
            }else {
              array_push($errors, ['El curp ' . $row['curp'] . ' esta duplicado porfavor corrige antes de proseguir']);

            }
        }

        if (sizeof($errors) > 0) {
            return back()
                ->with('error',$errors);
        }

        $grupo_id = $request->get('grupo');
        if ($grupo_id == 'Otro') {

          $request->validate([
            'grupo_name' => 'required'
          ]);

          $grupo_identity = Grupo::where([
              ['nombre', $request->get('grupo_name')],
              ['empresa_id', Session::get('empresa')->id]
          ])->get();


          if (count($grupo_identity) > 0) {
              return abort(418);
          }
            else {
              $estudios =  EstudioEmpresa::where('empresa_id',Session::get('empresa')->id)
                              ->get();
              $grupo = new Grupo;
              $grupo->nombre = $request->get('grupo_name');
              $grupo->empresa_id = Session::get('empresa')->id;
              $grupo->save();
              $grupo_id = $grupo->id;

              foreach ($estudios as $estudio) {
                $grupo = new EstudioGrupo;
                $grupo->estudio_id = $estudio->estudio_id;
                $grupo->grupo_id = $grupo_id;
                $grupo->save();
            }
          }

        }

        Session::put('grupo', $grupo_id);

        if (Session::has('excel')) {
            Session::forget('excel');
        }

        Session::put('excel', Str::random(10));

        (new EmpleadosImport)->import($request->file('excel'));

        return back()
            ->with([
                'success' => 'Empleados registrados correctamente',
                'data' =>  $element_excel[0]
            ]);
    }
}
