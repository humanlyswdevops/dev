<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\EstudioProgramado;
use App\Categoria;
use App\Estudio;
use App\Empresa;
use App\Giro;
use App\User;
use App\Antecedente;
use App\AntecedenteForm;
use App\AntecedenteAnswer;
use App\ExpedienteMedico;
use App\ArchivoMedico;
use App\Especialidad;
use App\Medicamentos;
use Illuminate\Support\Facades\Storage;
use App\Configuracion;
use DB;
use Auth;

use Illuminate\Support\Facades\Session;
use URL;

class ConfiguracionController extends Controller
{

    public function index()
    {
        $config = $this->getConfig();
        $empresa = Empresa::find(\Session::get('empresa')->id);
        $giros = Giro::all();
        $antecedentes_form = AntecedenteForm::where('user_id', 0)->where('empresa_id', 0)->get();
        $mis_antecedentes_form = AntecedenteForm::where('user_id', Auth::user()->id)->where('empresa_id', Session::get('empresa')->id)->get();
        $mi_expediente = "";
        $medicamentos = Medicamentos::all();
        $especialidades = Especialidad::where('empresa_id', 0)->orWhere('empresa_id', Session::get('empresa')->id)->get();
        if (Auth::user()->role_id != 1) {
            $mi_expediente = ExpedienteMedico::where('user_id', Auth::user()->id)->first();
        }
        return view('Empresa/configuracion/configuracion', compact('config', 'empresa', 'giros', 'antecedentes_form', 'mis_antecedentes_form', 'mi_expediente', 'especialidades', 'medicamentos'));
    }

    public function configSave(Request $request)
    {
        //  return $request->all();
        $request->validate([
            'cedula' => 'required',
            'especialidad' => 'required',
            'institucion' => 'required',
            'platilla' => 'required',
            'aceptado' => 'required'
        ]);

        if ($request->get('aceptado') ==  'on') {
            $aceptado =  1;
        } else {
            $aceptado = 0;
        }

        $find = Configuracion::where('id_user', \Auth::user()->id)->first();
        if ($find) {

            $find->id_user = \Auth::user()->id;
            $find->cedula = $request->cedula;

            $especialidad_validador = Especialidad::find($request->especialidad);
            if (empty($especialidad_validador)) {
                $new_especialidad = new Especialidad();
                $new_especialidad->nombre = $request->especialidad;
                $new_especialidad->empresa_id = Session::get('empresa')->id;
                $new_especialidad->save();

                $find->especialidad_id = $new_especialidad->id;
            } else {
                $find->especialidad_id = $request->especialidad;
            }

            $find->institucion = $request->institucion;
            $find->receta_id = 1;
            $find->aceptado = $aceptado;
            $find->save();

            return $find;
        } else {
            $config = new Configuracion;
            $config->id_user = \Auth::user()->id;
            $config->cedula = $request->cedula;

            $especialidad_validador = Especialidad::find($request->especialidad);
            if (empty($especialidad_validador)) {
                $new_especialidad = new Especialidad();
                $new_especialidad->nombre = $request->especialidad;
                $new_especialidad->empresa_id = Session::get('empresa')->id;
                $new_especialidad->save();

                $config->especialidad_id = $new_especialidad->id;
            } else {
                $config->especialidad_id = $request->especialidad;
            }

            $config->institucion = $request->institucion;
            $config->receta_id = 1;
            $config->aceptado = $aceptado;
            $config->save();

            return $config;
        }
    }

    public function getConfig()
    {
        return Configuracion::where('id_user', \Auth::user()->id)->first();
    }

    public function update_empresa(Request $request)
    {
        try {
            DB::beginTransaction();
            $empresa = Empresa::find(\Session::get('empresa')->id);
            $empresa->nombre = $request->nombre;
            $empresa->direccion = $request->direccion;
            $empresa->telefono = $request->telefono;
            $empresa->pagina = $request->pagina;
            $empresa->giro_id = $request->giro;
            $empresa->check_aceptado = $request->check_aceptado;

            if ($request->file('logo') != null) {
                $file = $request->file('logo');

                $file_name = time() . $file->getClientOriginalName();
                Storage::disk('empresa')->put($file_name, \File::get($file));

                $antigua = $empresa->logo;
                if ($antigua) Storage::disk('empresa')->delete($antigua);
                $empresa->logo = $file_name;
                $empresa->save();
            }
            $empresa->save();
            DB::commit();
            $datos_sesion = collect(["id" => $empresa->id, "nombre" => $empresa->nombre, "logo" => $empresa->logo]);
            Session::put('empresa', json_decode($datos_sesion));
            return json_encode($empresa);
        } catch (\Throwable $th) {
            DB::rollback();
            return json_encode(['Ocurrio un error']);
            throw $th;
        }
    }

    public function update_perfil(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = Auth::user();
            if ($user->email != $request->email_user) {
                $validacion = User::where('email', $request->email_user)->first();
                if (!empty($validacion)) {
                    return response()->json("email", 200);
                }
            }
            $user->nombre = $request->nombre_user;
            $user->email = $request->email_user;
            $user->telefono = $request->telefono_user;
            $user->puesto = $request->puesto_user;
            $user->area = $request->area_user;
            $user->save();
            DB::commit();
            return json_encode(['Exito!']);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return response()->json("Error!", 500);
        }
    }

    public function update_password(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = Auth::user();
            $user->password = bcrypt($request->new_password);
            $user->save();
            DB::commit();
            return json_encode(['Success']);
        } catch (\Throwable $th) {
            DB::rollback();
            return json_encode([$th]);
            //throw $th;
        }
    }
    public function add_medicamento(Request $request)
    {
        try {
            DB::beginTransaction();
            $medicamento = new Medicamentos();
            $medicamento->clave = $request->clave;
            if (empty($medicamento->clave)) $medicamento->clave = time() . "XCB";
            $medicamento->nombre = $request->nombre;
            $medicamento->detalle = $request->detalle;
            $medicamento->id_user =  Auth::user()->id;
            $medicamento->id_empresa = Session::get('empresa')->id;
            $medicamento->save();
            return json_encode($medicamento);
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th);
            return json_encode(['Error']);
        }
    }
    public function edit_medicamento(Request $request)
    {
            $id = $request->input('id');
            $medic = Medicamentos::find($id);
            if (!empty(  $medic)) {
                $medic->clave = $request->input('clave_e');            
                $medic->nombre = $request->input('nombre_e');
                $medic->detalle = $request->input('detalle_e');
                $medic->save();
                return json_encode($medic);
            }
           
    }
    public function getMedicamento($id)
    {
        $medic = Medicamentos::find($id);

        // Si ningún empleado coincide con el ID, manda un error 404.
        return compact("medic");
    }
    
    public function delete_medicamento($id)
    {
        try {
            DB::beginTransaction();
            $medicamento = Medicamentos::find($id);
            $medicamento->delete();
            DB::commit();
            return compact("medicamento");
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
    // public function edit_medicamento(Request $request, $id)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $medicamento = Medicamentos::find($id);
    //         if(!empty($medicamento))
    //         {
    //         $medicamento->clave = $request->clave;
    //         if (empty($medicamento->clave)) $medicamento->clave = time()."XCB";
    //         $medicamento->nombre = $request->nombre;
    //         $medicamento->detalle = $request->detalle;
    //         $medicamento->save();
    //         }
    //         DB::commit();
    //         return response()->json(['mensaje'=>'Actualizado Correctamente']);
    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         DB::rollback();
    //         dd($th);
    //         return response()->json(['mensaje'=>'Error']);
    //     }

    // }

    public function add_antecedenteForm(Request $request)
    {
        try {
            DB::beginTransaction();
            $new_antecedente_form = new AntecedenteForm();
            $new_antecedente_form->nombre = $request->nombre_antecedente;
            $new_antecedente_form->empresa_id = Session::get('empresa')->id;
            $new_antecedente_form->user_id = Auth::user()->id;
            $new_antecedente_form->save();
            DB::commit();
            return json_encode($new_antecedente_form);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            dd($th);
            return json_encode(['Error']);
        }
    }

    public function edit_antecedenteForm(Request $request)
    {
        try {
            DB::beginTransaction();
            $antecedente_form = AntecedenteForm::find($request->antecedente_form_edit_id);
            if (!empty($antecedente_form)) {
                $antecedente_form->nombre = $request->nombre_antecedente_edit;
                $antecedente_form->save();
            }
            DB::commit();
            return json_encode(['Success']);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            dd($th);
            return json_encode(['Error']);
        }
    }

    public function delete_antecedenteForm(Request $request)
    {
        try {
            DB::beginTransaction();
            $antecedente_form = AntecedenteForm::find($request->antecedente_form_delete_id);
            $antecedentes = Antecedente::where('antecedenteF_id', $request->antecedente_form_delete_id)->get();
            foreach ($antecedentes as $key => $value) {
                $value->delete();
            }
            $antecedente_form->antecedentesAnswer()->delete();
            $antecedente_form->delete();
            DB::commit();
            return json_encode($request->antecedente_form_delete_id);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            dd($th);
            return json_encode(['Error']);
        }
    }

    public function add_antecedenteAnswer(Request $request)
    {
        try {
            DB::beginTransaction();
            $new_antecedente_answer = new AntecedenteAnswer();
            $new_antecedente_answer->pregunta = $request->nombre_answer;
            $new_antecedente_answer->user_id = Auth::user()->id;
            $new_antecedente_answer->antecedente_id = $request->antecedente_form_id;
            $new_antecedente_answer->campo = $request->tipo_campo;
            $new_antecedente_answer->save();
            DB::commit();
            return json_encode($new_antecedente_answer);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            dd($th);
            return json_encode(['Error']);
        }
    }

    public function edit_antecedenteAnswer(Request $request)
    {
        try {
            DB::beginTransaction();
            $antecedente_answer = AntecedenteAnswer::find($request->answer_id);
            $antecedente_answer->pregunta = $request->nombre_answer_edit;
            $antecedente_answer->campo = $request->tipo_campo_edit;
            $antecedente_answer->save();
            DB::commit();
            return json_encode($antecedente_answer);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            dd($th);
            return json_encode(['Error']);
        }
    }

    public function delete_antecedenteAnswer(Request $request)
    {
        try {
            DB::beginTransaction();
            $antecedente_answer = AntecedenteAnswer::find($request->answer_delete_id);

            $antecedentes = Antecedente::where('answerA_id', $request->answer_delete_id)->get();
            foreach ($antecedentes as $key => $value) {
                $value->delete();
            }

            $antecedente_answer->delete();
            DB::commit();
            return json_encode($request->answer_delete_id);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            dd($th);
            return json_encode(['Error']);
        }
    }

    public function update_expediente_medico(Request $request)
    {
        try {
            DB::beginTransaction();
            $mi_expediente = ExpedienteMedico::where('user_id', Auth::user()->id)->first();
            if (empty($mi_expediente)) {
                $mi_expediente = new ExpedienteMedico();
                $mi_expediente->user_id = Auth::user()->id;
                $mi_expediente->save();
            }

            if ($request->file('file_cv') != null) {
                $file = $request->file('file_cv');

                $file_name = time() . $file->getClientOriginalName();
                Storage::disk('medicos')->put($file_name, \File::get($file));

                $antigua = $mi_expediente->curriculum;
                if ($antigua) Storage::disk('medicos')->delete($antigua);
                $mi_expediente->curriculum = $file_name;
                $mi_expediente->curriculum_extension = $file->getClientOriginalExtension();
                $mi_expediente->save();
            }

            if ($request->file('file_cedula_main') != null) {
                $file = $request->file('file_cedula_main');

                $file_name = time() . $file->getClientOriginalName();
                Storage::disk('medicos')->put($file_name, \File::get($file));

                $antigua = $mi_expediente->archivos_medico()->where('check_principal', 'si')->first();

                if (empty($antigua)) {
                    $new_archivo_medico = new ArchivoMedico();
                    $new_archivo_medico->expediente_medico_id = $mi_expediente->id;
                    $new_archivo_medico->tipo_archivo = 'cedula';
                    $new_archivo_medico->nombre = 'Cédula profesional';
                    $new_archivo_medico->archivo = $file_name;
                    $new_archivo_medico->check_principal = 'si';
                    $new_archivo_medico->extension = $file->getClientOriginalExtension();
                    $new_archivo_medico->save();
                } else {
                    Storage::disk('medicos')->delete($antigua->archivo);
                    $antigua->nombre = 'Cédula profesional';
                    $antigua->archivo = $file_name;
                    $antigua->extension = $file->getClientOriginalExtension();
                    $antigua->save();
                }
            }

            if (isset($request->nuevos)) {
                foreach ($request->nuevos as $key => $value) {
                    $nombre_campo = "new_file_datos_" . $value;
                    $nombre_archivo = "new_file_" . $value;
                    $nuevo_archivo = $request->$nombre_campo;
                    $new_archivo_medico = new ArchivoMedico();

                    if ($nuevo_archivo != null) {

                        $new_archivo_medico->expediente_medico_id = $mi_expediente->id;
                        $new_archivo_medico->tipo_archivo = $nuevo_archivo["tipo"];
                        $new_archivo_medico->nombre = $nuevo_archivo["nombre"];
                        $new_archivo_medico->check_principal = 'no';
                    }

                    if ($request->file($nombre_archivo) != null) {

                        $file = $request->file($nombre_archivo);

                        $file_name = time() . $file->getClientOriginalName();
                        Storage::disk('medicos')->put($file_name, \File::get($file));

                        $new_archivo_medico->archivo = $file_name;
                        $new_archivo_medico->extension = $file->getClientOriginalExtension();
                        $new_archivo_medico->save();
                    }
                }
            }

            if (isset($request->archivos_existentes)) {
                foreach ($request->archivos_existentes as $key => $value) {
                    $archivo_existente = ArchivoMedico::find($value);
                    if (!empty($archivo_existente)) {
                        if ($request->file('archivo_custom_' . $value) != null) {

                            $file = $request->file('archivo_custom_' . $value);

                            $file_name = time() . $file->getClientOriginalName();

                            if ($archivo_existente->archivo != "" && $archivo_existente->archivo != null) {
                                Storage::disk('medicos')->delete($archivo_existente->archivo);
                            }

                            Storage::disk('medicos')->put($file_name, \File::get($file));

                            $archivo_existente->archivo = $file_name;
                            $archivo_existente->extension = $file->getClientOriginalExtension();
                            $archivo_existente->save();
                        }
                    }
                }
            }

            DB::commit();
            return redirect('Configuracion#miexpediente');
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json($th, 500);
        }
    }

    public function delete_archivo_medico(Request $request)
    {
        try {
            DB::beginTransaction();
            $archivo_medico = ArchivoMedico::find($request->archivo_medico_id);
            $archivo_medico->delete();
            DB::commit();
            return json_encode($request->archivo_medico_id);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function edit_expediente_medico(Request $request)
    {
        try {
            DB::beginTransaction();
            $archivo_medico = ArchivoMedico::find($request->edit_id_file);
            $archivo_medico->nombre = $request->edit_name_file;
            $archivo_medico->tipo_archivo = $request->edit_type_file;
            $archivo_medico->save();
            if ($request->edit_type_file == "cedula") {
                $tipo = "Cédula";
            } elseif ($request->edit_type_file == "certificacion") {
                $tipo = "Certificación";
            } else {
                $tipo = "Diplomado";
            }
            $data = collect(["id" => $request->edit_id_file, "tipo" => $tipo, "nombre" => $request->edit_name_file]);
            DB::commit();
            return json_encode($data);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function prueba_xml()
    {
        $url = url('/storage/app/ecg/example_ecg.xml');
        $xmldata = simplexml_load_file($url) or die("Failed to load");
        // echo('Inciando');
        // echo('<br />');
        // dd($xmldata);
        // dd($xmldata->component->series->component->sequenceSet,'1Hola');
        // dd($xmldata->component->series->subjectOf->annotationSet);
        echo 'Hola';
        echo ('<br />');
        dd($xmldata->component->series->subjectOf->annotationSet);
        foreach ($xmldata->component->series->subjectOf->annotationSet->component as $componente) {
            try {
                echo $componente->annotation->value->attributes()->unit;
                echo ('<br />');
                echo ('---??--------');
            } catch (\Throwable $th) {
                //throw $th;
                echo 'Error!';
                echo ('<br />');

                echo ('---??--------');
            }
        }
    }
}
