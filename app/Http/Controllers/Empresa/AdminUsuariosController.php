<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use Mail;

use App\Mail\MailUsuarioSecundario;
use App\User;
use App\Empresa;
use App\Roles;

class AdminUsuariosController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $roles = Roles::where('empresa_id',0)->where('id','!=',1)->where('id','!=',2)->orWhere('empresa_id',Session::get('empresa')->id)->get();
        return view('Empresa/ConfigUsuarios')
        ->with('roles', $roles);
    }

    public function getUsuariosSecundarios() {
        $empresa = Session::get('empresa');
        $users = User::where([['empresa_id', $empresa->id], ['role_id','!=' ,1],['id','!=',Auth::id()]])->get();
        
        // dd($users);
        return json_encode($users);
    }

    public function getUsuarioSecundario($user_id) {
        $empresa = Session::get('empresa');
        $user = User::find($user_id);
        return json_encode($user);
    }

    public function actualizarUsuarioSecundario(Request $request) {
        $this->validate ($request, [
            'userID' => 'required',
            'nombre' => 'required|string',
            'email' => 'required|email',
            'telefono' => 'required|string',
            'puesto' => 'required|string',
            'area' => 'required|string'
        ]);

        $user_id = $request->input('userID');
        $nombre = $request->input('nombre');
        $email = $request->input('email');
        $telefono = $request->input('telefono');
        $puesto = $request->input('puesto');
        $area = $request->input('area');
        $rol_id = $request->input('rol');

        $user = User::where('id', $user_id)->first();
        if ($user->id != $user_id) {
            return abort(400);
        }
        
        if ($user) {
            $user->nombre = $nombre;
            $user->email = $email;
            $user->telefono = $telefono;
            $user->puesto = $puesto;
            $user->area = $area;
            $user->role_id = $rol_id;
            $user->save();

            return json_encode($user);
        }
        else {
            return abort(403);
        }
        
    }

    public function agregarUsuarioSecundario(Request $request) {
      //return $request->all();
        $this->validate ($request, [
            'nombre' => 'required|string',
            'email' => 'required|email',
            'telefono' => 'required|string',
            'puesto' => 'required|string',
            'area' => 'required|string'
        ]);

        $nombre = $request->input('nombre');
        $email = $request->input('email');
        $telefono = $request->input('telefono');
        $puesto = $request->input('puesto');
        $area = $request->input('area');
        $rol_id = $request->input('rol');

        $password = Str::random(10);
        $password_encrypted = Hash::make($password);

        $temp_user = User::where('email', $email)->first();
        if ($temp_user) {
            return abort(400);
        }

        $empresa = Session::get('empresa');

        $user = new User;
        $user->nombre = $nombre;
        $user->email = $email;
        $user->telefono = $telefono;
        $user->puesto = $puesto;
        $user->area = $area;
        $user->password = $password_encrypted;
        $user->role_id = 3;
        $user->empresa_id = $empresa->id;
        $user->role_id = $rol_id;
        $user->save();

        $this->mail($request, $password, $empresa);

        return json_encode($user);
    }

    public function eliminarUsuarioSecundario($user_id) {
        $user = User::where([['id', $user_id], ['empresa_id', Session::get('empresa')->id]])->first();
        $nombre_usuario = $user->nombre;
        $user->delete();

        return json_encode("Se ha eliminado el usuario");
    }

    public function mail(Request $request, $password, $empresa) {
        $correo = $request->input('email');
        Mail::to($correo)->send(new MailUsuarioSecundario($request, $password, $empresa));
    }

    public function detalle_usuario($user_id)
    {
        dd($user_id);
        $contents = Storage::disk('medicos')->url('prueba.txt2');
    }
}
