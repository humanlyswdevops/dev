<?php
namespace App\Http\Controllers\Empresa;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;
use App\Empresa;
use App\Empleado;
use App\EstudioProgramado;
use App\Grupo;
use App\Categoria;
use App\Estudios;
use App\EstudioGrupo;
use App\Expediente;
use App\HistorialClinico;
use App\Heredofamiliar;
use App\NoPatologico;
use App\Patologico;
use App\Genicoobstetrico;
use App\HistoriaLaboral;
use App\HistoriaLaboral2;
use App\ExamenFisico;
use App\ResultadoHistorial;
use App\Resultados;
use App\Nota;
use App\RegistroEntrada;
use App\SassOrden;
use App\EstudioEmpresa;
use App\Antecedente;
use App\Agenda;
use App\Consulta;
use App\ConsultaExamenFisico;
use App\ConsultaMedicamento;
use App\AntecedenteForm;
use App\ConsultaIncapacidad;
use App\MedicamentosActivos;
use App\ArchivosPaciente;
use Dotenv\Validator;

class EmpleadosController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Controlador de todas las operaciones del modelo Empleado
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos de los empleados de las empresas.
    |
    */

    /**
     * Añade el middleware de autenticación a la clase
     *
     * Las funciones de esta clase solo serán ejecutadas si el middleware
     * verifica que el usuario se ha autenticado.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Obtiene todos los empleados
     * @return [type] [vista de empleados]
     */
    public function empleados()
    {
        $empresa = Session::get('empresa');
        $grupos = $this->getGrupos();
        $empleados = Empleado::select('empleados.id', 'empleados.nombre', 'clave', 'apellido_materno', 'apellido_paterno', 'fecha_nacimiento', 'CURP', 'grupos.nombre as grupo')->where('empleados.empresa_id', $empresa->id)
            ->where('status_id', '1')
            ->join('grupos', 'grupos.id', 'empleados.grupo_id')
            ->get();
        return view('Empresa/Empleados', ['empleados' => $empleados, 'grupos' => $grupos]);
    }
    public function buscarempleados()
    {
        // $term = $request->get('term');
        // $empresa = Session::get('empresa');
        // $grupos = $this->getGrupos();
        // return Empleado::select('empleados.id', 'empleados.nombre', 'clave', 'apellido_materno', 'apellido_paterno', 'fecha_nacimiento', 'CURP', 'grupos.nombre as grupo')
        // ->where('empleados.empresa_id', $empresa->id)
        // ->where('status_id', '1')
        // ->join('grupos', 'grupos.id', 'empleados.grupo_id')
        // ->where('empleados.nombre','like', '%'.$term.'%')
        // ->get();
        $empresa = Session::get('empresa');
        $grupos = $this->getGrupos();
        $empleados = Empleado::select('empleados.id', 'empleados.nombre', 'clave', 'apellido_materno', 'apellido_paterno', 'fecha_nacimiento', 'CURP', 'grupos.nombre as grupo')->where('empleados.empresa_id', $empresa->id)
            ->where('status_id', '1')
            ->join('grupos', 'grupos.id', 'empleados.grupo_id')
            ->orderBy('empleados.nombre', 'asc')
            ->get();
        return  (['empleados' => $empleados, 'grupos' => $grupos]);
    }
    /**
     * Retorna a los grupos creados
     * @return [type] [retorna grupos]
     */
    public function getGrupos()
    {
        $empresaId = Session::get('empresa')->id;
        return Grupo::where('empresa_id', $empresaId)->get();
    }

    /**
     * Devuelve la vista de alta de empleados
     *
     * La función mostrará la vista Empresa/AltaEmpleado en donde
     * se muestran las opciones para subir empleados de forma individual
     * o mediante una hoja de cálculo.
     *
     * @return Empresa\AltaEmpleado Vista de opciones de alta de empleados
     */
    public function AltaEmpleado()
    {
        $grupos = Grupo::select('nombre', 'id')->where('empresa_id', Session::get('empresa')
            ->id)
            ->get();

        return view('Empresa/AltaEmpleado', compact('grupos'));
    }

    /**
     * Obtener a un empleado por su ID
     *
     * Devuelve todos los datos del empleado que coincida con el ID encriptado.
     *
     * @param string $encryptedID ID encriptado del empleado por devolver
     * @return json Datos del empleado que coincide con el ID
     */
    public function getEmpleado($encryptedID)
    {
        try
        {
            $id = decrypt($encryptedID);
        }
        catch(DecryptException $e)
        {
            return abort(404);
        }

        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleado = Empleado::where([['empresa_id', $empresa->id], ['id', $id]])->first();

        if ($empleado != null)
        {
            return json_encode($empleado);
        }

        // Si ningún empleado coincide con el ID, manda un error 404.
        return abort(404);
    }

    /**
     * Obtener un empleado por la curp
     * @param  [type] $curp string de la curp
     * @return [type]       empleado
     */
    public function getEmpleadoCurp($curp)
    {
        $empresa = Session::get('empresa');
        $empleado = Empleado::where([['empresa_id', $empresa->id], ['CURP', $curp]])->first();

        if ($empleado != null)
        {
            return $empleado;
        }

        // Si ningún empleado coincide con el ID, manda un error 404.
        return abort(404);
    }

    /**
     * Obtener empleados de la empresa que inició sesión
     *
     * Devuelve todos los empleados de la empresa a la que pertenece
     * el usuario que inició sesióngetHistorialesClinicos.
     *
     * @return \App\Empleado[] Arreglo de todos los empleados de la empresa
     */
    public function getEmpleados()
    {
        $user = Auth::user();

        $empresa = Session::get('empresa');
        return Empleado::where('empresa_id', $empresa->id)
            ->where('status_id', '1')
            ->get();
    }

    /**
     * Obtener empleados de un grupo de la empresa que inició sesión
     *
     * Devuelve todos los empleados de la empresa que pertenezcan
     * a un grupo específico.
     *
     * @param string $encryptedID ID encriptado del grupo de la empresa
     * @return \App\Empleado[] Arreglo  de todos los empleados del grupo de la empresa
     */
    public function getEmpleadosDeGrupo($encryptedID)
    {

        try
        {
            $grupo_id = decrypt($encryptedID);
        }
        catch(DecryptException $e)
        {
            return abort(404);
        }

        $user = Auth::user();
        $empresa = Session::get('empresa');
        return Empleado::where([['empresa_id', $empresa->id], ['status_id', 1], ['grupo_id', $grupo_id]])->get();
    }

    /**
     * Obtener empleados con nombre de su grupo
     *
     * Devuelve la lista de empleados que recibe como parámetro sustituyendo
     * el ID de su grupo por el nombre de su grupo.
     *
     * @param \App\Empleado[] Arreglo de empleados por convertir su grupo_id
     * @param \App\Grupo[] Arreglo de los grupos de la empresa
     * @return \App\Empleado[] Arreglo de empleados con el nombre de su grupo en grupo_id
     */
    public function getEmpleadosGrupos($empleados, $grupos)
    {
        foreach ($empleados as $empleado)
        {
            $mygrupo_id = $empleado->grupo_id;

            foreach ($grupos as $grupo)
            {
                if ($grupo->id == $mygrupo_id)
                {
                    $nombre_grupo = $grupo->nombre;
                    $empleado->grupo_id = $nombre_grupo;
                    break;
                }
                else
                {
                    $empleado->grupo_id = "Sin grupo";
                }
            }
        }
        return $empleados;
    }

    /**
     * Mostrar la página de información de un empleado
     *
     * Carga toda la información del empleado cuya CURP coincide con
     * el parámetro de la función y luego devuelve la vista encargada
     * de desplegar dicha información.
     *
     * @param string $CURP CURP del empleado que se desea mostrar
     * @return Empresa\infoEmpleado Vista con la información del empleado (Empresa/infoEmpleado)
     */
    public function show($CURP)
    {
        
        
        
        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleado = $this->getEmpleadoCurp($CURP);
        $valoraciones=Consulta::where('empleado_id', $empleado->id)
        ->where('motivo', 'like', '%valoracion%')
        ->get();
        $sesiones=Consulta::where('empleado_id', $empleado->id)
        ->where('motivo', 'like', '%sesion%')
        ->get();
        $conta_se=count($sesiones);
        $conta_val=count($valoraciones);
        $estudios_programados = EstudioProgramado::where('empleado_id', $empleado->id)
            ->orderBy('fecha_inicial', 'desc')
            ->take(15)
            ->get();



        $grupos = Grupo::where('empresa_id', $empresa->id)
            ->orderBy('nombre', 'ASC')
            ->get();


        // NOTE: Agenda
        $agenda = $this->agenda($empleado->id);
        // NOTE: Expediente
        $expediente = $this->expedienteVerify($empleado->CURP);
        $consulta = Consulta::where('expediente_id', $expediente->id)
            ->where('estado', 'finalizado')
            ->latest()
            ->first();
        $consultas = Consulta::where('expediente_id', $expediente->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $antecedente = Antecedente::where('id_expediente', $expediente->id)
            ->first();

        $formAntecedente = AntecedenteForm::whereIn('empresa_id', [$empresa->id, 0])
            ->get();

        $medicamento = $this->medicamActivos($expediente->id);
        $documentos = $this->documentPatient($expediente->id);

        if ($consulta)
        {
            $examenFisico = ConsultaExamenFisico::where('consulta_id', $consulta->id)
            ->first();
            $consulta_id = $consulta->id;
            $incapacidad = $this->incapacidad($consulta->id);
            $expediente_id = $consulta->expediente_id;
        }
        else
        {
            $incapacidad = NULL;
            $examenFisico = NULL;
            $consulta_id = NULL;
            $expediente_id = NULL;
        }

        $estudios = $this->getEstudios($empleado->id);

        $notas = Nota::where('expediente_id',$expediente->id)->where('user_id',Auth::id())->get();
        // dd($puedeProgramar);
        return view('Empresa/infoEmpleado', [
          'empleado' => $empleado,
          'incapacidad' => $incapacidad,
          'encryptedID' => encrypt($empleado->id) ,
          'grupos' => $grupos,
          'historial_id' => $consulta_id,
          'expediente_id' => $expediente->id,
          'estudios' => $estudios,
          'medicamentosActivos' =>$medicamento,
          'ef' => $examenFisico,
          'curp' => $CURP,
          'consultas' => $consultas,
          'hc' => $consulta,
          'antecedentes' => $formAntecedente,
          'agenda' => $agenda,
          'documentos'=>$documentos,
          'notas'=>$notas,
          'conta_se'=>$conta_se,
          'conta_val'=> $conta_val
        //  'expediente_id' => $expediente_id
        ]);
    }

    /**
     * Ver documentos de los pacientes
     * @param  [type] $expedienteId [return data post]
     * @return [type]               [description]
     */
    public function documentPatient($expedienteId){
      return  ArchivosPaciente::where('expediente_id',$expedienteId)
      ->get();
    }

    /** Eliminar documentos del paciente */
    public function deleteArchivo(Request $request){
      $file = ArchivosPaciente::find($request->id);
      $file->delete();
      Storage::disk('documentos')->delete($file->storage);
      return $file;
    }

    /**
     * Documentos de los pacientes
     * @param  Request $request [data post]
     * @return [type]           [description]
     */
    public function docuementos(Request $request){
      $request->validate(['file' => 'required']);
      $file = $request->file('file');
      $archivo = $this->uploadDocuement($file);

      $archivos = new ArchivosPaciente();
      $archivos->expediente_id = $request->expId;
      $archivos->extencion = $archivo['extencion'];
      $archivos->nombre = $archivo['name'];
      $archivos->storage = $archivo['name'];
      $archivos->save();

      $data = array(
          'status' => 200,
          'archivo' => $archivos
      );
      return response()->json($data, $data['status']);
    }

    public function uploadDocuement($file){
      $name = time().'_'.$file->getClientOriginalName();
      $ext = pathinfo($name, PATHINFO_EXTENSION);
      Storage::disk('documentos')->put($name,\File::get($file));
      $data = array(
        'extencion' => $ext,
        'name'=> $name
      );
      return $data;
    }

    /**
     * Verificar si hay incapacidad del paciente
     * @param  [type] $consultaId [consulta id]
     * @return [type]             [null - incapacidad]
     */
    public function incapacidad($consultaId){
     $incapacidad =  ConsultaIncapacidad::where([
        ['consulta_id',$consultaId],
        ['estado',1]
      ])
      ->first();
      if ($incapacidad) {
         $inicial = \Carbon::parse($incapacidad->fechaInicial);
         $final = \Carbon::parse($incapacidad->fechaFinal)->format('d-m-Y');
          $days = $inicial->diffInDays($final);

        $inicial->format('d-m-Y') <= $final ? '' :  ($incapacidad = NULL);
        $days > 0  ? '' : ($incapacidad = NULL);
        return $incapacidad;
      }else {
        return $incapacidad;
      }

    }


    public function medicamActivos($expedienteId){
      return MedicamentosActivos::where('expediente_id',$expedienteId)
      ->get();
    }

    /**
     * Verifica que tenga un expediente, si no tiene, crea uno
     * @param  [type] $curp curp paciente
     * @return [type]       datos del expediente
     */
    public function expedienteVerify($curp){
        $expediente = Expediente::where('curp', $curp)->first();
        if ($expediente)
        {
            return $expediente;
        }
        else
        {
            $expediente = new Expediente;
            $expediente->curp = $curp;
            $expediente->save();
            return $expediente;
        }
    }

    public function getEstudios($empleadoId){

      return EstudioProgramado::where('empleado_id',$empleadoId)
      ->get();

      return Resultados::select('estudios.id', 'nombre', 'resultados.id as resultados_id', 'resultados.created_at')
      ->where('expediente_id', $expediente_id)
      ->join('estudios', 'estudios.id', 'resultados.estudio_id')
      ->get();
    }
    /**
     * Obtener datos de un cita agendada
     * @param  [type] $empleado_id empleado_id
     * @return [type]              [agendas]
     */
    public function agenda($empleado_id){
        return Agenda::where('user_id', \Auth::id())
        ->where('statu', 1)
        ->where('empleado_id', $empleado_id)
        ->get();
    }

    /**
     * Actualizar los datos de un empleado
     *
     * Actualiza los datos de un empleado con la información ingresada
     * en el formulario del modal de edición del empleado.
     *
     * @param \Illuminate\Http\Request $request Los datos ingresados en el formulario de edición de empleado
     * @return json Nuevos datos del empleado registrados en la base de datos
     */
    public function updateEmpleado(Request $request){
        $this->validate($request,[
          'clave' => 'required|string',
          'nombre' => 'required|string',
          'primerApellido' => 'required|string',
          'segundoApellido' => 'required|string',
          'curp' => 'required|string|max:18',
          'grupo' => 'required|string',
          'fechaNacimiento' => 'required|date',
        ]);

        try
        {
            $id = decrypt($request->input('empleado_id'));
        }
        catch(DecryptException $e)
        {
            return abort(404);
        }

        $clave = Empleado::where([
            ['id', "!=", $id],
            ['empresa_id', Session::get('empresa')->id],
            ['clave', '=', $request->input('clave') ],
            ['status_id', 1]
        ])
        ->first();

        $curp = Empleado::where([
            ['id', "!=", $id],
            ['empresa_id', Session::get('empresa')->id],
            ['CURP', '=', $request->input('curp') ],
            ['status_id', 1]
        ])
        ->first();

        $email = Empleado::where([
            ['id', "!=", $id],
            ['empresa_id', Session::get('empresa')->id],
            ['email', '=', $request->input('email') ],
            ['status_id', 1]
        ])
        ->first();
        if ($curp != null)
        {
            return abort(400, 'Error en CURP');
        }
        if ($clave != null)
        {
            return abort(400, 'Error en clave');
        }

        $empleado = Empleado::where('id', $id)->with('grupo')
            ->first();

        $expediente = Expediente::where('curp', $empleado->CURP)
            ->first();
        $expediente->curp = $request->input('curp');
        $expediente->save();

        $empresa = Session::get('empresa');
        $grupo = Grupo::where([['nombre', $request->input('grupo') ], ['empresa_id', $empresa
            ->id]])
            ->first();

        $empleado->clave = $request->input('clave');
        $empleado->nombre = ucfirst(strtolower($request->input('nombre')));
        $empleado->apellido_paterno = ucfirst(strtolower($request->input('primerApellido')));
        $empleado->apellido_materno = ucfirst(strtolower($request->input('segundoApellido')));
        $empleado->CURP = $request->input('curp');
        $empleado->grupo_id = $grupo->id;
        $empleado->fecha_nacimiento = $request->input('fechaNacimiento');
        $empleado->email = $request->input('email', '');
        $empleado->direccion = $request->input('direccion');
        $empleado->telefono = $request->input('telefono','');
        $empleado->save();

        return json_encode($empleado);
    }

    /**
     * Cambiar y eliminar imgen del paciente
     * @param  Request $request
     * @return [type]           [respuesta de estado de la peticion]
     */
    public function updateImage(Request $request){
        $request->validate(['file' => 'required|mimes:jpg,png,jpeg']);
        $file = $request->file('file');
        $name = $this->uploadImage($file);
        $empleado = Empleado::find($request->get('key'));
        $antigua = $empleado->imagen;
        if ($antigua) Storage::disk('paciente')->delete($antigua);
        $empleado->update(['imagen' => $name]);

        $data = array(
            'status' => 200,
            'img' => $name
        );
        return response()->json($data, $data['status']);
    }

    /**
     * caraga y almacena la imgen del paciente
     * @param  [type] $file [imagen]
     * @return [type]       [nombre de la imagen]
     */
    public function uploadImage($file){
        $file_name = time() . $file->getClientOriginalName();
        Storage::disk('paciente')
            ->put($file_name, \File::get($file));
        return $file_name;
    }

    /**
     * Registrar un nuevo empleado para una empresa
     *
     * Agrega un nuevo empleado a la base de datos asociándolo
     * a la empresa a la que pertenece el usuario que inició sesión.
     *
     * @param \Illuminate\Http\Request $request Los datos ingresados en el formulario de registro de empleado
     * @return json Datos con los que se registró al empleado
     */
    
    public function insertEmpleado(Request $request){

        $this->validate($request, [
            'grupo' => 'required',
            'nombre' => 'required|string',
            'app' => 'required|string',
            'apm' => 'required|string',
            // 'curp' => 'required|string|max:18',
            'nacimiento' => 'required|date',
            'genero' => 'required|string',
        ]);



        $clave = Empleado::where([['empresa_id', Session::get('empresa')->id], ['clave', '=', $request->input('clave') ],['status_id','!=',2]])
            ->first();

        $curp = Empleado::where([['empresa_id', Session::get('empresa')->id], ['CURP', '=', $request->input('curp') ],['status_id','!=',2]])
            ->first();

        // $email = Empleado::where([['empresa_id', Session::get('empresa')->id], ['email', '=', $request->input('email') ],['status_id','!=',2]])
        //     ->first();

        if ($curp != null)  return abort(400);
        // if ($email != null) return abort(424);
        if ($clave != null)  return abort(423);
        if (empty($request->clave)) $request->clave = 'HAS-'.time();
        if (empty($request->curp)) $request->curp = Str::upper(Str::random(18));

        // dd($request->curp);


        $empleado = new Empleado;
        $empleado->clave = $request->clave;
        $empleado->nombre = ucfirst(strtolower($request->input('nombre')));
        $empleado->CURP = $request->curp;
        $empleado->apellido_paterno = ucfirst(strtolower($request->input('app')));
        $empleado->apellido_materno = ucfirst(strtolower($request->input('apm')));
        $empleado->genero = $request->input('genero');
        $empleado->fecha_nacimiento = $request->input('nacimiento');
        $empleado->lugar_nacimiento = $request->input('lugarNacimiento');
        $empleado->nss = $request->input('nss');
        $empleado->empresa_id = Session::get('empresa')->id;
        $empleado->status_id = 1;
        $empleado->direccion = $request->input('direccion');
        $empleado->colonia = $request->input('colonia');
        $empleado->cp = $request->input('cp');
        $empleado->municipio = $request->input('municipio');
        $empleado->estado = $request->input('estado');
        $empleado->email = $request->input('email');
        $empleado->telefono = $request->input('telefono');

        $response = (object)[];

        $grupo_id = $request->input('grupo');
        if ($grupo_id == "Otro")
        {
            // Agregar nuevo grupo y asignarle el grupo al nuevo empleado
            $this->validate($request, ['nuevoGrupo' => 'required']);

            $nuevo_grupo = $request->input('nuevoGrupo');

            $grupo_identity = Grupo::where([['nombre', $nuevo_grupo], ['empresa_id', Session::get('empresa')
                ->id]])
                ->get();

            if (count($grupo_identity) > 0)
            {
                return abort(418);
            }
            else
            {
                $grupo = new Grupo;
                $grupo->nombre = $nuevo_grupo;
                $grupo->empresa_id = Session::get('empresa')->id;
                $grupo->save();
                $grupo_id = $grupo->id;

                $empleado->grupo_id = $grupo->id;

                $response->grupo_id = $grupo->id;
                $response->nuevo_grupo = $grupo->nombre;

                $estudios = EstudioEmpresa::where('empresa_id', Session::get('empresa')->id)
                    ->get();
                foreach ($estudios as $estudio)
                {
                    $grupo = new EstudioGrupo;
                    $grupo->estudio_id = $estudio->estudio_id;
                    $grupo->grupo_id = $grupo_id;
                    $grupo->save();
                }

            }
        }
        else
        {
            $empleado->grupo_id = $grupo_id;
            $response = $empleado;
        }

        $empleado->save();

        //   return json_encode($empleado);
        return json_encode($response);
    }

    /**
     * Programar estudios para un empleado desde un grupo
     *
     * Programa los estudios seleccionados en el formulario del modal
     * para programar estudios a un empleado desde la vista de un grupo.
     *
     * @param \Illuminate\Http\Request $request Datos sobre los estudios por programar
     * @return Empresa\infoGrupo Vista de la informacion de un grupo
     */
    // public function programarEstudiosEmpleadoGrupo(Request $request)
    // {

    //     $this->validate($request, ['fechaInicio' => 'required|date', 'fechaFinal' => 'required|date', ]);

    //     $grupo_id = decrypt($request->input('encryptedID'));
    //     $grupo = Grupo::find($grupo_id);

    //     $CURP = $request->input('CURP');
    //     $elements = $request->input('estudios_programar');

    //     $user = Auth::user();
    //     $empresa = Session::get('empresa');
    //     $empleado = Empleado::where([['empresa_id', $empresa->id], ['CURP', $CURP]])->first();

    //     if (empty($elements))
    //     {
    //         $estudios_previos = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 0]])
    //             ->get();

    //         foreach ($estudios_previos as $estudio_previo)
    //         {
    //             $estudio_previo->delete();
    //         }

    //         return redirect()
    //             ->route('grupos.show', $grupo->nombre)
    //             ->with(['message' => "Se han eliminado todos los estudios programados para este " . $empleado->nombre . " correctamente"]);
    //     }

    //     $estudios_por_programar = [];
    //     foreach ($elements as $element)
    //     {
    //         array_push($estudios_por_programar, (int)$element);
    //     }

    //     $estudios_JSON = json_encode($estudios_por_programar);
    //     $token = Hash::make(Str::random(4));

    //     $estudios_previos = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 0]])
    //         ->get();
    //     foreach ($estudios_previos as $estudio_previo)
    //     {
    //         $estudio_previo->delete();
    //     }

    //     $estudio_programado = new EstudioProgramado;
    //     $estudio_programado->estudios = $estudios_JSON;
    //     $estudio_programado->fecha_inicial = $request->input('fechaInicio');
    //     $estudio_programado->fecha_final = $request->input('fechaFinal');
    //     $estudio_programado->status = 0;
    //     $estudio_programado->empleado_id = $empleado->id;
    //     $estudio_programado->empresa_id = Session::get('empresa')->id;
    //     $estudio_programado->token = $token;
    //     $estudio_programado->save();

    //     RegistroEntrada::where([['status', 2], ['empleado_id', $empleado
    //         ->id]])
    //         ->update(['status' => 3]);

    //     return redirect()
    //         ->route('grupos.show', $grupo->nombre)
    //         ->with(['message' => "Se han programado los estudios para " . $empleado->nombre . " " . $empleado->apellido_paterno . " " . $empleado->apellido_materno . " " . " correctamente"]);
    // }

    /**
     * Programar estudios para un empleado
     *
     * Programa los estudios seleccionados en el formulario del modal
     * para programar estudios a un empleado desde la vista de información del empleado.
     *
     * @param \Illuminate\Http\Request $request Datos sobre los estudios por programar
     * @return Empresa\infoEmpleado Vista de la informacion del empleado
     */
    
    public function programarEstudiosEmpleado(Request $request){
        $this->validate($request, [
            'fechaInicio' => 'required|date',
            'fechaFinal' => 'required|date',
        ]);
        $empleado_id = decrypt($request->input('encryptedID'));
        $CURP = $request->input('CURP');
        $elements = $request->input('estudios_programar');
        $empleado = Empleado::find($empleado_id);

        $estudios_por_programar = [];
        foreach ($elements as $element)
        {
            array_push($estudios_por_programar, (int)$element);
        }

        $estudios_detalle = Estudios::whereIn('id', $estudios_por_programar)->get();

        $estudios_JSON = json_encode($estudios_por_programar);

        $token = Hash::make(Str::random(4));


        $estudio_programado = new EstudioProgramado;
        $estudio_programado->estudios = $estudios_JSON;
        $estudio_programado->fecha_inicial = $request->input('fechaInicio');
        $estudio_programado->fecha_final = $request->input('fechaFinal');
        $estudio_programado->status = 0;
        $estudio_programado->empleado_id = $empleado->id;
        $estudio_programado->empresa_id = Session::get('empresa')->id;
        $estudio_programado->token = $token;
        // $estudio_programado->folio = 'HS-'.time();
        $estudio_programado->save();

       $empresa = Empresa::find($empleado->empresa_id);

        RegistroEntrada::where([['status', 2], ['empleado_id', $empleado
            ->id]])
            ->update(['status' => 3]);

        $data = array(
            'estudios' => $estudio_programado,
            'paciente' => $empleado,
            'detalle' => $estudios_detalle,
            'empresa' => $empresa
        );

        return response()->json($data, 200);
    }
    /**
     * [guarda el nim de la orden creada]
     * @param  Request $request [data {json}]
     * @return [type]           [estatus de la peticion ]
     */
    public function sass_orden(Request $request){
        $request->validate([
            'nim' => 'required',
            'empresa_id' => 'required',
            'paciente_id' => 'required',
            'programacion' => 'required'
        ]);

        $sass = new SassOrden;
        $sass->empresa_id = $request->input('empresa_id');
        $sass->paciente_id = $request->input('paciente_id');
        $sass->nim_sass = $request->input('nim');
        $sass->programacion = $request->input('programacion');
        $sass->save();

        $sass->programacionEstudio->folio =  $request->input('nim');
        $sass->programacionEstudio->save();

        $empleado = Empleado::find($request->paciente_id);
        $empleado->id_sass = $request->idPatientSass;
        $empleado->save();

        return response()
            ->json($empleado, 200);
    }
    public function daicom()
    {
        return view('Empresa.daicom');
    }

    /** Elimina el estudio programado */
    public function deleteOrden(Request $data)
    {
        $estudios_programados = EstudioProgramado::find($data->id);
        $estudios_programados->delete();


        $sass = SassOrden::where('programacion',$data->id)->first();
        if ($sass) {
            $sass->delete();
        }

        return $estudios_programados;
    }

    /**
     * Eliminar un estudio de un empleado
     *
     * Elimina el estudio cuya información coincida con la regitrada
     * en el formulario recibido.
     *
     * @param \Illuminate\Http\Request $request Datos sobre el estudio por eliminar
     * @return Empresa\infoEmpleado Vista de la informacion del empleado
     */
    public function eliminarEstudioEmpleado(Request $request)
    {
        $empleado_id = decrypt($request->input('encryptedID'));
        $token = $request->input('token');
        $nombre_estudio = $request->input('nombreEstudio');
        $estudio_id = $request->input('idEstudio');
        $fechaInicial = $request->input('fechaInicial');
        $fechaFinal = $request->input('fechaFinal');

        $empleado = Empleado::find($empleado_id);

        $estudios_programados = EstudioProgramado::where([['token', $token], ['empleado_id', $empleado_id], ['status', 0]])->orWhere([['token', $token], ['empleado_id', $empleado_id], ['status', 3]])->get();

        foreach ($estudios_programados as $estudio_programado)
        {
            $lista_estudios = json_decode($estudio_programado->estudios);
            foreach ($lista_estudios as $key => $estudio)
            {
                if ($estudio == $estudio_id)
                {
                    unset($lista_estudios[$key]);
                }
            }

            $lista_actualizada = [];
            foreach ($lista_estudios as $estudio)
            {
                array_push($lista_actualizada, $estudio);
            }

            if (empty($lista_actualizada))
            {
                $estudio_programado->delete();
            }
            else
            {
                $estudio_programado->estudios = json_encode($lista_actualizada);
                $estudio_programado->save();
            }
        }

        return redirect()
            ->route('empleados.show', $empleado->CURP)
            ->with(['message' => "El estudio se ha eliminado correctamente para " . $empleado->nombre . " " . $empleado->apellido_paterno . " " . $empleado->apellido_materno]);
    }


    public function getNotaExpediente($curp)
    {
        $exp = Expediente::where('curp', $curp)->first();
        if ($exp)
        {
            $nota = Nota::where('expediente_id', $exp->id)->where('user_id',Auth::id())
                ->get();
            return json_encode($nota);
        }
        else
        {
            return '';
        }

    }
    public function updateNotaExpediente(Request $request)
    {

        $curp = $request->input('curp');
        $nota_id = $request->input('nota_id');
        $textoNota = $request->input('text');

        $expediente = Expediente::where('curp', $curp)->first();
        if (!$expediente)
        {
            $expediente = new Expediente;
            $expediente->curp = $curp;
            $expediente->save();
        }

        // $nota = Nota::where('expediente_id', $expediente->id)
        //     ->first();
        $nota = Nota::find($nota_id);

        if ($nota)
        {
            $nota->contenido = $textoNota;
            $nota->save();
        }
        else
        {
            $nota = new Nota;
            $nota->contenido = $textoNota;
            $nota->expediente_id = $expediente->id;
            $nota->save();
        }

        return json_encode($nota);
    }

    public function addNewNota(Request $request)
    {
        try {
            DB::beginTransaction();
            $user_id = Auth::id();
            $new_nota = new Nota();
            $new_nota->expediente_id = $request->expediente_id;
            $new_nota->titulo = '';
            $new_nota->contenido = '';
            $new_nota->user_id = $user_id;
            $new_nota->save();
            DB::commit();
            return response()->json($new_nota, 200);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function updateTitleNota(Request $request)
    {
        try {
            DB::beginTransaction();
            $nota = Nota::find($request->nota_id);
            $nota->titulo = $request->titulo;
            $nota->save();
            DB::commit();
            return response()->json($nota, 200);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function deleteNota(Request $request)
    {
        try {
            DB::beginTransaction();
            $nota = Nota::find($request->nota_id);
            $nota->delete();
            DB::commit();
            return response()->json($request->nota_id, 200);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }


    public function getHistorialesClinicos($encryptedID)
    {
        try
        {
            $empleado_id = decrypt($encryptedID);
        }
        catch(DecryptException $e)
        {
            return abort(404);
        }

        $empleado = Empleado::find($empleado_id);
        $expediente = Expediente::where('curp', $empleado->CURP)
            ->first();
        return Consulta::select('altura', 'peso', 'imc', 'fr', 'fc', 'temperatura','oxigeno', 'consultas.created_at')
            ->where('expediente_id', $expediente->id)
            ->where('estado', 'finalizado')
            ->join('consultaExamenFisico as ef', 'ef.consulta_id', 'consultas.id')
            ->orderBy('created_at', 'asc')
            ->take(20)
            ->get();
    }
    public function getHistorialClinicoCompleto($id)
    {
        $historialClinico = HistorialClinico::find($id);
        $historialClinico->identificacion = json_decode($historialClinico->identificacion);
        $historialClinico->heredofamiliar = json_decode($historialClinico->heredofamiliar);
        $historialClinico->noPatologico = json_decode($historialClinico->noPatologico);
        $historialClinico->patologico = json_decode($historialClinico->patologico);
        $historialClinico->genicoobstetrico = json_decode($historialClinico->genicoobstetrico);
        $historialClinico->historialLaboral = json_decode($historialClinico->historialLaboral);
        $historialClinico->examenFisico = json_decode($historialClinico->examenFisico);
        $historialClinico->resultado = json_decode($historialClinico->resultado);

        $historialClinico
            ->heredofamiliar->data = json_decode($historialClinico
            ->heredofamiliar
            ->data);
        return $historialClinico;
    }

    public function getDatosEnfermedades()
    {
        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where('empresa_id', $empresa->id)
            ->where('status_id', 1)
            ->get();

        // Arreglo de enfermedades por género y edad [<18, 18-28, 29-39, 40-59, 60+].
        $hipertension_hombres = [0, 0, 0, 0, 0];
        $hipertension_mujeres = [0, 0, 0, 0, 0];
        $diabetes_hombres = [0, 0, 0, 0, 0];
        $diabetes_mujeres = [0, 0, 0, 0, 0];

        foreach ($empleados as $empleado)
        {
            $expediente = Expediente::where('curp', $empleado->CURP)
                ->first();
            if (!$expediente)
            {
                continue;
            }

            $historialClinico = HistorialClinico::where('expediente_id', $expediente->id)
                ->where('origen', 1)
                ->where('estado', 'finalizado')
                ->latest()
                ->first();
            //return $historialClinico;
            // Decodificar contenido JSON del historial clínico.
            if (!$historialClinico)
            {
                continue;
            }
            $historialClinico->patologico = json_decode($historialClinico->patologico);

            $genero_empleado = strtolower($empleado->genero);
            $age = date_diff(date_create($empleado->fecha_nacimiento) , date_create('now'))->y;

            $enfermedades = $historialClinico
                ->patologico->enfermPadecidas;
            if ($enfermedades == null)
            {
                continue;
            }

            if ($genero_empleado == "masculino")
            {

                if (in_array("Alta Presión", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $hipertension_hombres[0] = $hipertension_hombres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $hipertension_hombres[1] = $hipertension_hombres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $hipertension_hombres[2] = $hipertension_hombres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $hipertension_hombres[3] = $hipertension_hombres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $hipertension_hombres[4] = $hipertension_hombres[4] + 1;
                    }
                }

                if (in_array("Diabetes", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $diabetes_hombres[0] = $diabetes_hombres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $diabetes_hombres[1] = $diabetes_hombres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $diabetes_hombres[2] = $diabetes_hombres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $diabetes_hombres[3] = $diabetes_hombres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $diabetes_hombres[4] = $diabetes_hombres[4] + 1;
                    }
                }
            }
            else if ($genero_empleado == "femenino")
            {

                if (in_array("Alta Presión", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $hipertension_mujeres[0] = $hipertension_mujeres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $hipertension_mujeres[1] = $hipertension_mujeres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $hipertension_mujeres[2] = $hipertension_mujeres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $hipertension_mujeres[3] = $hipertension_mujeres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $hipertension_mujeres[4] = $hipertension_mujeres[4] + 1;
                    }
                }

                if (in_array("Diabetes", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $diabetes_mujeres[0] = $diabetes_mujeres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $diabetes_mujeres[1] = $diabetes_mujeres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $diabetes_mujeres[2] = $diabetes_mujeres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $diabetes_mujeres[3] = $diabetes_mujeres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $diabetes_mujeres[4] = $diabetes_mujeres[4] + 1;
                    }
                }
            }
        }

        $datos_enfermedades = (object)[];
        $datos_enfermedades->hipertension = (object)[];
        $datos_enfermedades->diabetes = (object)[];

        $datos_enfermedades
            ->hipertension->hombres = $hipertension_hombres;
        $datos_enfermedades
            ->diabetes->hombres = $diabetes_hombres;
        $datos_enfermedades
            ->hipertension->mujeres = $hipertension_mujeres;
        $datos_enfermedades
            ->diabetes->mujeres = $diabetes_mujeres;

        return json_encode($datos_enfermedades);
    }

    public function getDatosEnfermedadesGrupo($idGrupo)
    {
        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where('empresa_id', $empresa->id)
            ->where('status_id', 1)
            ->where('grupo_id', $idGrupo)->get();

        // Arreglo de enfermedades por género y edad [<18, 18-28, 29-39, 40-59, 60+].
        $hipertension_hombres = [0, 0, 0, 0, 0];
        $hipertension_mujeres = [0, 0, 0, 0, 0];
        $diabetes_hombres = [0, 0, 0, 0, 0];
        $diabetes_mujeres = [0, 0, 0, 0, 0];

        foreach ($empleados as $empleado)
        {
            $historialClinico = HistorialClinico::where('empleado_id', $empleado->id)
                ->where('origen', 1)
                ->whereNull('origen')
                ->latest()
                ->first();

            if ($historialClinico == null)
            {
                continue;
            }

            // Decodificar contenido JSON del historial clínico.
            $historialClinico->patologico = json_decode($historialClinico->patologico);

            $genero_empleado = strtolower($empleado->genero);
            $age = date_diff(date_create($empleado->fecha_nacimiento) , date_create('now'))->y;

            $enfermedades = $historialClinico
                ->patologico->enfermPadecidas;
            if ($enfermedades == null)
            {
                continue;
            }

            if ($genero_empleado == "masculino")
            {

                if (in_array("Alta Presión", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $hipertension_hombres[0] = $hipertension_hombres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $hipertension_hombres[1] = $hipertension_hombres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $hipertension_hombres[2] = $hipertension_hombres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $hipertension_hombres[3] = $hipertension_hombres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $hipertension_hombres[4] = $hipertension_hombres[4] + 1;
                    }
                }

                if (in_array("Diabetes", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $diabetes_hombres[0] = $diabetes_hombres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $diabetes_hombres[1] = $diabetes_hombres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $diabetes_hombres[2] = $diabetes_hombres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $diabetes_hombres[3] = $diabetes_hombres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $diabetes_hombres[4] = $diabetes_hombres[4] + 1;
                    }
                }
            }
            else if ($genero_empleado == "femenino")
            {

                if (in_array("Alta Presión", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $hipertension_mujeres[0] = $hipertension_mujeres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $hipertension_mujeres[1] = $hipertension_mujeres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $hipertension_mujeres[2] = $hipertension_mujeres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $hipertension_mujeres[3] = $hipertension_mujeres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $hipertension_mujeres[4] = $hipertension_mujeres[4] + 1;
                    }
                }

                if (in_array("Diabetes", $enfermedades))
                {

                    if ($age < 18)
                    {
                        $diabetes_mujeres[0] = $diabetes_mujeres[0] + 1;
                    }
                    else if ($age >= 18 && $age <= 28)
                    {
                        $diabetes_mujeres[1] = $diabetes_mujeres[1] + 1;
                    }
                    else if ($age >= 29 && $age <= 39)
                    {
                        $diabetes_mujeres[2] = $diabetes_mujeres[2] + 1;
                    }
                    else if ($age >= 40 && $age <= 59)
                    {
                        $diabetes_mujeres[3] = $diabetes_mujeres[3] + 1;
                    }
                    else if ($age >= 60)
                    {
                        $diabetes_mujeres[4] = $diabetes_mujeres[4] + 1;
                    }
                }
            }
        }

        $datos_enfermedades = (object)[];
        $datos_enfermedades->hipertension = (object)[];
        $datos_enfermedades->diabetes = (object)[];

        $datos_enfermedades
            ->hipertension->hombres = $hipertension_hombres;
        $datos_enfermedades
            ->diabetes->hombres = $diabetes_hombres;
        $datos_enfermedades
            ->hipertension->mujeres = $hipertension_mujeres;
        $datos_enfermedades
            ->diabetes->mujeres = $diabetes_mujeres;

        return json_encode($datos_enfermedades);
    }
    public function getDatosIMCGrupo($idGrupo)
    {
        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where('empresa_id', $empresa->id)
            ->where('status_id', 1)
            ->where('grupo_id', $idGrupo)->get();

        // Arreglo de valores de IMC por género para personas con edad entre 13 y 59 [<18.5, 18.5-24.9, 25-29.9, 30-39.9, 40+].
        $IMC_hombres = [0, 0, 0, 0, 0];
        $IMC_mujeres = [0, 0, 0, 0, 0];

        foreach ($empleados as $empleado)
        {

            $expediente = Expediente::where('curp ', $empleado->CURP)
                ->first();

            $historialClinico = HistorialClinico::where('empleado_id', $expediente->id)
                ->where('origen', 1)
                ->whereNull('origen')
                ->latest()
                ->first();

            if ($historialClinico == null)
            {
                continue;
            }

            // Decodificar contenido JSON del historial clínico.
            $historialClinico->examenFisico = json_decode($historialClinico->examenFisico);

            $genero_empleado = strtolower($empleado->genero);
            $age = date_diff(date_create($empleado->fecha_nacimiento) , date_create('now'))->y;

            $imc = $historialClinico
                ->examenFisico->imc;
            if ($imc == null || $age < 13 || $age > 59)
            {
                continue;
            }

            if ($genero_empleado == "masculino")
            {
                if ($imc < 18.5)
                {
                    $IMC_hombres[0] = $IMC_hombres[0] + 1;
                }
                else if ($imc >= 18.5 && $imc < 25)
                {
                    $IMC_hombres[1] = $IMC_hombres[1] + 1;
                }
                else if ($imc >= 25 && $imc < 30)
                {
                    $IMC_hombres[2] = $IMC_hombres[2] + 1;
                }
                else if ($imc >= 30 && $imc < 40)
                {
                    $IMC_hombres[3] = $IMC_hombres[3] + 1;
                }
                else if ($imc >= 40)
                {
                    $IMC_hombres[4] = $IMC_hombres[4] + 1;
                }
            }
            else if ($genero_empleado == "femenino")
            {
                if ($imc < 18.5)
                {
                    $IMC_mujeres[0] = $IMC_mujeres[0] + 1;
                }
                else if ($imc >= 18.5 && $imc < 25)
                {
                    $IMC_mujeres[1] = $IMC_mujeres[1] + 1;
                }
                else if ($imc >= 25 && $imc < 30)
                {
                    $IMC_mujeres[2] = $IMC_mujeres[2] + 1;
                }
                else if ($imc >= 30 && $imc < 40)
                {
                    $IMC_mujeres[3] = $IMC_mujeres[3] + 1;
                }
                else if ($imc >= 40)
                {
                    $IMC_mujeres[4] = $IMC_mujeres[4] + 1;
                }
            }
        }

        $datos_imc = (object)[];
        $datos_imc->hombres = $IMC_hombres;
        $datos_imc->mujeres = $IMC_mujeres;

        return json_encode($datos_imc);
    }

    public function getDatosIMC()
    {
        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where('empresa_id', $empresa->id)
            ->where('status_id', 1)
            ->get();

        // Arreglo de valores de IMC por género para personas con edad entre 13 y 59 [<18.5, 18.5-24.9, 25-29.9, 30-39.9, 40+].
        $IMC_hombres = [0, 0, 0, 0, 0];
        $IMC_mujeres = [0, 0, 0, 0, 0];

        foreach ($empleados as $empleado)
        {
            $expediente = Expediente::where('curp', $empleado->CURP)
                ->first();
            if (!$expediente)
            {
                continue;
            }
            $historialClinico = HistorialClinico::where('expediente_id', $expediente->id)
                ->where('origen', 1)
                ->whereNull('origen')
                ->latest()
                ->first();

            if ($historialClinico == null)
            {
                continue;
            }

            // Decodificar contenido JSON del historial clínico.
            $historialClinico->examenFisico = json_decode($historialClinico->examenFisico);

            $genero_empleado = strtolower($empleado->genero);
            $age = date_diff(date_create($empleado->fecha_nacimiento) , date_create('now'))->y;

            $imc = $historialClinico
                ->examenFisico->imc;
            if ($imc == null || $age < 13 || $age > 59)
            {
                continue;
            }

            if ($genero_empleado == "masculino")
            {
                if ($imc < 18.5)
                {
                    $IMC_hombres[0] = $IMC_hombres[0] + 1;
                }
                else if ($imc >= 18.5 && $imc < 25)
                {
                    $IMC_hombres[1] = $IMC_hombres[1] + 1;
                }
                else if ($imc >= 25 && $imc < 30)
                {
                    $IMC_hombres[2] = $IMC_hombres[2] + 1;
                }
                else if ($imc >= 30 && $imc < 40)
                {
                    $IMC_hombres[3] = $IMC_hombres[3] + 1;
                }
                else if ($imc >= 40)
                {
                    $IMC_hombres[4] = $IMC_hombres[4] + 1;
                }
            }
            else if ($genero_empleado == "femenino")
            {
                if ($imc < 18.5)
                {
                    $IMC_mujeres[0] = $IMC_mujeres[0] + 1;
                }
                else if ($imc >= 18.5 && $imc < 25)
                {
                    $IMC_mujeres[1] = $IMC_mujeres[1] + 1;
                }
                else if ($imc >= 25 && $imc < 30)
                {
                    $IMC_mujeres[2] = $IMC_mujeres[2] + 1;
                }
                else if ($imc >= 30 && $imc < 40)
                {
                    $IMC_mujeres[3] = $IMC_mujeres[3] + 1;
                }
                else if ($imc >= 40)
                {
                    $IMC_mujeres[4] = $IMC_mujeres[4] + 1;
                }
            }
        }

        $datos_imc = (object)[];
        $datos_imc->hombres = $IMC_hombres;
        $datos_imc->mujeres = $IMC_mujeres;

        return json_encode($datos_imc);
    }
    public function getDatosGeneros()
    {
        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where('empresa_id', $empresa->id)
            ->where('status_id', 1)
            ->get();

        // Contadores de mujeres y hombres.
        $hombres = 0;
        $mujeres = 0;

        foreach ($empleados as $empleado)
        {

            $genero_empleado = strtolower($empleado->genero);
            if ($genero_empleado == "masculino" || $genero_empleado == "MASCULINO" || $genero_empleado == "Masculino")
            {
                $hombres++;
            }
            else if ($genero_empleado == "femenino" || $genero_empleado == "FEMENINO")
            {
                $mujeres++;
            }
        }

        $generos = (object)[];
        $generos->hombres = $hombres;
        $generos->mujeres = $mujeres;

        return json_encode($generos);
    }
    public function getDatosGenerosGrupo($idGrupo)
    {
        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where('empresa_id', $empresa->id)
            ->where('status_id', 1)
            ->where('grupo_id', $idGrupo)->get();

        // Contadores de mujeres y hombres.
        $hombres = 0;
        $mujeres = 0;

        foreach ($empleados as $empleado)
        {

            $genero_empleado = strtolower($empleado->genero);
            if ($genero_empleado == "masculino")
            {
                $hombres++;
            }
            else if ($genero_empleado == "femenino")
            {
                $mujeres++;
            }
        }

        $generos = (object)[];
        $generos->hombres = $hombres;
        $generos->mujeres = $mujeres;

        return json_encode($generos);
    }
    public function eliminarEmpleado(Request $request)
    {
        $paciente = Empleado::find($request->id);
        $paciente->status_id = 2;
        $paciente->save();
        $empresaId = Session::get('empresa')->id;
        return Empleado::select('empleados.id', 'empleados.nombre', 'clave', 'apellido_materno', 'apellido_paterno', 'fecha_nacimiento', 'CURP', 'grupos.nombre as grupo')
            ->where('empleados.empresa_id', $empresaId)->where('status_id', '1')
            ->join('grupos', 'grupos.id', 'empleados.grupo_id')
            ->get();
    }

    // public function estudios()
    // {
    //   return Estudios::all();
    // }
    //
    // public function estudios_update(Request $request)
    // {
    //   $estudio = Estudios::find($request->input('id'));
    //   if ($estudio->sass_code!= NULL) {
    //     $estudio->sass_code = $request->input('code');
    //     $estudio->costo = $request->input('costo',0);
    //     $estudio->save();
    //   }
    //   return $estudio;
    //
    // }

}
