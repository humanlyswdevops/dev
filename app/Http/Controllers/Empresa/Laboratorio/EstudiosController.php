<?php

namespace App\Http\Controllers\Empresa\Laboratorio;

use App\EstudioProgramado;
use App\Estudios;
use App\Empleado;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class EstudiosController extends Controller {

    public function getEstudiosToma($id){
        $toma =  EstudioProgramado::find($id);
        $estudios = \json_decode($toma->estudios);
        $estudios = Estudios::whereIn('id',$estudios)->get();
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios
        );

        return response()->json($toma, 200);
    }
    public function Daicoms($id,$id_estudio){
         $toma =  EstudioProgramado::find($id);
         $estudios=Estudios::find($id_estudio);
         $empleado=Empleado::find($toma->empleado_id);
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios,
            'empleado'=>$empleado
        );
        
       
       //dd($toma);
       return view('Empresa.daicom')->with('toma',$toma,200);
       
    }
    public function tablaestudios($id,$id_estudio){
        $toma =  EstudioProgramado::find($id);
         $estudios=Estudios::find($id_estudio);
         $empleado=Empleado::find($toma->empleado_id);
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios,
            'empleado'=>$empleado
        );
        return view('Empresa/Estudios')->with('toma',$toma,200);
       
    }
    
    /*public function estudios_id_folio($id)
    {
        $estudios=Estudios::findOrFail($id);
        return view('Empresa.daicom',compact('estudios'));
        /*return view('Empresa.daicom')
        ->with('id',$id)
        ->with('folio',$folio);
        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $personas = Estudios::buscarpor($id,$buscar)->Paginate(5);

        return view ('persona.index',compact('personas'));
       
    }*/

}
