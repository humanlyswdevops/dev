<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Empresa;
use App\HistorialClinico;
use App\Consulta;
use App\ConsultaDiagnostico;



class EmpresasController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Controlador de todas las operaciones del modelo Empresa
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos de una empresa.
    |
    */

    /**
     * Obtener datos de una empresa
     *
     * Devuelve datos específicos de la empresa cuyo nombre coincida
     * con el texto recibido como parámetro.
     *
     * @access public
     * @param string $nombreEmpresa Nombre de la empresa por devolver
     * @return json Arreglo con el nombre, giro, página web, teléfono, dirección y logo de la empresa
     */
    public function getEmpresa($nombreEmpresa) {
        $empresa = Empresa::select('nombre', 'giro', 'pagina', 'telefono', 'direccion', 'logo')
            ->where('nombre', $nombreEmpresa)
            ->first();

        return json_encode($empresa);
    }

    public function getConsultasStats($year = 2020){
      return Consulta::where('id_user',\Auth::id())
      ->select(\DB::raw('Month(created_at) as month, count(*) as stats'))
      ->where('estado','finalizado')
      ->whereYear('created_at',$year)
      ->groupBy('month')
      ->get();
    }

    public function indicadores()
    {

        $inicial = \Carbon::now()->subDays(30)->format('Y-m-d');
        $final = \Carbon::now()->addDay(1)->format('Y-m-d');
       return Consulta::select(\DB::raw('cd.nombre, count(*) as stats'))
       ->where([
        ['empresa_id',\Session::get('empresa')->id],
        ['estado','finalizado']
       ])
       ->whereBetween('consultas.created_at',[$inicial,$final])
       ->join('consultaDiagnostico as cd','cd.consulta_id','consultas.id')
       ->groupBy('cd.nombre')
       ->orderBy('stats','desc')
       ->take(10)
       ->get();

    }

    public function indicadoresDate(Request $data){

        $final = \Carbon::parse($data->final)->addDay(1)->format('Y-m-d');
        return Consulta::select(\DB::raw('cd.nombre, count(*) as stats'))
       ->where([
        ['empresa_id',\Session::get('empresa')->id],
        ['estado','finalizado']
       ])
       ->whereBetween('consultas.created_at',[$data->inicial,$final])
       ->join('consultaDiagnostico as cd','cd.consulta_id','consultas.id')
       ->groupBy('cd.nombre')
       ->orderBy('stats','desc')
       ->take(10)
       ->get();
    }

}
