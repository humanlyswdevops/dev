<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\EstudioProgramado;
use App\Categoria;
use App\Estudio;
use App\Empleado;

class EstudiosProgramadosController extends Controller
{   
    /*
    |--------------------------------------------------------------------------
    | Controlador de todas las operaciones del modelo EstudioProgramado
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos de un estudio programado.
    |
    */

    /**
     * Obtener un estudio programado
     * 
     * Devuelve toda la información del estudio programado cuyo ID
     * coincida con el ID recibido como parámetro
     * 
     * @param Integer ID del estudio programado por devolver
     * @return json Datos del estudio programado
     */
    function getEstudioProgramado($id) {
        $estudio_programado = EstudioProgramado::where('id', $id)->first();
        return json_encode($estudio_programado);
    }
    function estudiospro(){
        $estudio_programado = EstudioProgramado::all();    
        return view('Empresa.Resultados')->with(json_encode($estudio_programado));    
    }
 
      
       
   
}
