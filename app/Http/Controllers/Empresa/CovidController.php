<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Mail;

use App\Mail\MailUsuarioSecundario;
use App\User;
use App\Empresa;
use App\Empleado;
use App\Covid;
use App\SassOrden;

class CovidController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function index($curp,Request $request)
    {
      $fecha = $request->get('version');
      if ($fecha) {
        $empleado= $this->empleado($curp);
        $covid = $this->covid_encuesta($empleado->id,$request->get('version'));
        $version = $this->version_covid($empleado->id);
      }else {
        $empleado= $this->empleado($curp);
        $covid = $this->covid_encuesta($empleado->id);
        $version = $this->version_covid($empleado->id);
      }
      return view('Empresa/covid')->with([
        'empleado' =>$empleado,
        'estudioCaso'=>$covid,
        'version'=>$version
      ]);

    }
    public function version_covid($id_empleado){
      $empleado = Empleado::find($id_empleado);
      $estudio =  \DB::connection('htds')
      ->table('estudiosCaso')
      ->select('fechaInsert')
      ->where('curp',$empleado->CURP)
      ->orderBy('fechaInsert','Desc')
      ->get();
      return $estudio;
    }

    public function empleado($curp){
      $empleado = Empleado::where('CURP',$curp)->first();
      if ($empleado ==null) {
        return abort(404);
      }
      return $empleado;
    }

    public function covid_encuesta($id_empleado,$fecha = null){
      if ($fecha == null) {
        $empleado = Empleado::find($id_empleado);
        $estudio =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->where('curp',$empleado->CURP)
        ->latest('fechaInsert')
        ->first();
      }else {
        $empleado = Empleado::find($id_empleado);
        $estudio =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->where('curp',$empleado->CURP)
        ->where('fechaInsert',$fecha)
        ->first();
      }

        return $estudio;
    }

    public function covid_indicadores(){

      // $sass=SassOrden::where('empresa_id',\Session::get('empresa')->id)->get();
      //   $nim=[];
      //
      //   foreach ($sass as $value) {
      //       if($value->nim_sass != ''){
      //           array_push($nim,$value->nim_sass);
      //       }
      //   }
      //   $sexo =  \DB::connection('sass')->table('PACtrnIngresos')->select('Sexo', \DB::raw('count(*) as total'))->whereIn('NumeroToma',$nim)->groupBy('Sexo')->get();
      //
      // $paciente_sass = \DB::connection('sass')->table('PACtrnIngresos')->whereIn('NumeroToma',$nim)->get();
      //
      // $estudio_id = [];
      // $numero_toma = [];
      // foreach ($paciente_sass as  $value) {
      //   array_push($estudio_id,$value->Id);
      //   array_push($numero_toma,$value->NumeroToma);
      //
      // }
      //
      // $resultados_procentaje = \DB::connection('sass')->table('PACtrnResultados')
      // ->select('ResultadoAlfaNumerico',\DB::raw('count(*) as total'))
      // ->where('NombreResultado','COV2 SAR')
      // ->whereIn('IdEstudio',$estudio_id)
      // ->groupBy('ResultadoAlfaNumerico')
      // ->get();
      //
      //  $edades = \DB::connection('sass')->table('PACtrnIngresos')
      // ->select('Edad',\DB::raw('count(*) as total'))
      // ->whereIn('NumeroToma',$numero_toma)
      // ->groupBy('Edad')
      // ->get();






      //
      // return \DB::connection('sass')->select(,"SELECT  FROM PACtrnResultados WHERE NombreResultado = 'COV2 SAR' AND IdEstudio IN (SELECT Id FROM PACtrnIngresos WHERE NumeroToma IN ($nim_string)) GROUP BY ResultadoAlfaNumerico");

      //  return view('Empresa/covid_indicadores',compact('sexo','resultados_procentaje','edades'));
      $covid = $this->get_empleados_covid_Table();
      return view('Empresa/covid_indicadores',compact('covid'));
    }

    public function get_empleados_covid(){
        $id_empresa = Session::get('empresa')->id;
        $empleados = Empleado::select('CURP')->where('empresa_id',$id_empresa)->get();
        $empleados_array=[];
        foreach ($empleados as $item) {
           array_push($empleados_array,$item->CURP);
        }

        $positivo =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->whereIn('curp',$empleados_array)
        ->where('resultados','POSITIVO')
        ->count();


        $negativo =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->whereIn('curp',$empleados_array)
        ->where('resultados','NEGATIVO')
        ->count();

        $hombre =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->whereIn('curp',$empleados_array)
        ->where('resultados','POSITIVO')
        ->where('sexo','Masculino')
        ->count();

        $mujer =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->whereIn('curp',$empleados_array)
        ->where('resultados','POSITIVO')
        ->where('sexo','Femenino')
        ->count();

        $empleado_hombre_edad =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->select(\DB::raw('CONCAT(anioFechaNac,"-", mesFechaNac,"-",diaFechaNac) AS anioNacimiento'))
        ->whereIn('curp',$empleados_array)
        ->where('resultados','POSITIVO')
        ->where('sexo','Masculino')->get();

        $empleado_mujer_edad =  \DB::connection('htds')
        ->table('estudiosCaso')
        ->select(\DB::raw('CONCAT(anioFechaNac,"-", mesFechaNac,"-",diaFechaNac) AS anioNacimiento'))
        ->whereIn('curp',$empleados_array)
        ->where('resultados','POSITIVO')
        ->where('sexo','Femenino')->get();

        $edad_Hombre18_20=0;
        $edad_Hombre20_24=0;
        $edad_Hombre25_44=0;
        $edad_Hombre45_49=0;
        $edad_Hombre50_59=0;
        $edad_Hombre60_64=0;
        $edad_Hombre65=0;
        foreach ($empleado_hombre_edad as $item) {
          $fecha_nacimiento  = $item->anioNacimiento;
          $edad = \Carbon\Carbon::parse($fecha_nacimiento )->age;

          if ($edad>=18 && $edad<=19) {
             $edad_Hombre18_20++;
           }
           else if ($edad>=20 && $edad<=24) {
             $edad_Hombre20_24++;
           }
           else if ($edad>=25 && $edad<=44) {
             $edad_Hombre25_44++;
           }
           else if ($edad>=45 && $edad<=49) {
             $edad_Hombre45_49++;
           }
           else if ($edad>=50 && $edad<=59) {
             $edad_Hombre50_59++;
           }
           else if ($edad>=60 && $edad<=64) {
             $edad_Hombre60_64++;
           }
           else if ($edad>=65) {
             $edad_Hombre65++;
           }

        }


        $edad_Mujer18_20=0;
        $edad_Mujer20_24=0;
        $edad_Mujer25_44=0;
        $edad_Mujer45_49=0;
        $edad_Mujer50_59=0;
        $edad_Mujer60_64=0;
        $edad_Mujer65=0;
        foreach ($empleado_mujer_edad as $item) {
          $fecha_nacimiento  = $item->anioNacimiento;
          $edad = \Carbon\Carbon::parse($fecha_nacimiento )->age;

          if ($edad>=18 && $edad<=19) {
             $edad_Mujer18_20++;
           }
           else if ($edad>=20 && $edad<=24) {
             $edad_Mujer20_24++;
           }
           else if ($edad>=25 && $edad<=44) {
             $edad_Mujer25_44++;
           }
           else if ($edad>=45 && $edad<=49) {
             $edad_Mujer45_49++;
           }
           else if ($edad>=50 && $edad<=59) {
             $edad_Mujer50_59++;
           }
           else if ($edad>=60 && $edad<=64) {
             $edad_Mujer60_64++;
           }
           else if ($edad>=65) {
             $edad_Mujer65++;
           }

        }
        $resultados = array(
            'positivo' => $positivo,
            'negativo' => $negativo
        );

        $generos = array(
            'hombre' => $hombre ,
            'mujer'=>$mujer
         );
         $ragoedadHombre = array(
           'edad_Hombre18_20' => $edad_Hombre18_20,
           'edad_Hombre20_24' => $edad_Hombre20_24,
           'edad_Hombre25_44' => $edad_Hombre25_44,
           'edad_Hombre45_49' => $edad_Hombre45_49,
           'edad_Hombre50_59' => $edad_Hombre50_59,
           'edad_Hombre60_64' => $edad_Hombre60_64,
           'edad_Hombre65' => $edad_Hombre65,
         );
         $ragoedadMujer = array(
           'edad_Mujer18_20' => $edad_Mujer18_20,
           'edad_Mujer20_24' => $edad_Mujer20_24,
           'edad_Mujer25_44' => $edad_Mujer25_44,
           'edad_Mujer45_49' => $edad_Mujer45_49,
           'edad_Mujer50_59' => $edad_Mujer50_59,
           'edad_Mujer60_64' => $edad_Mujer60_64,
           'edad_Mujer65' => $edad_Mujer65,
         );
         $datos = array(
             'resultados' => $resultados,
             'generos'=>$generos,
             'ragoedadHombre' => $ragoedadHombre,
             'ragoedadMujer' => $ragoedadMujer
         );

         // $fecha_nacimiento  = "1990-10-23";
         // $edad = \Carbon\Carbon::parse($fecha_nacimiento )->age;
         return json_encode($datos);
    }

    public function get_empleados_covid_Table(){
        $id_empresa = Session::get('empresa')->id;
        $empleados = Empleado::select('CURP')->where('empresa_id',$id_empresa)->get();
        $empleados_array=[];
        foreach ($empleados as $item) {
           array_push($empleados_array,$item->CURP);
        }
        $covid =   \DB::connection('htds')

        ->table('estudiosCaso')
        ->select('nombre','curp','aMaterno','aPaterno','resultados')
        ->whereIn('curp',$empleados_array)
        ->get();
        return $covid;

    }
}
