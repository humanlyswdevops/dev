<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Imports\ReclutamientoImport;
use Maatwebsite\Excel\Excel;

use App\User;
use App\Empresa;
use App\EstudioEmpresa;
use App\Empleado;
use App\Covid;
use App\SassOrden;

class ReclutamientoController extends Controller {

  public function index(){
    $reclutamiento = $this->reclutamiento();
    $estudios = $this->estudios();
    return view('Empresa/reclutamiento',compact('reclutamiento','estudios'));
  }

  public function reclutamiento(){
    $empresaId = Session::get('empresa')->id;

    return Empleado::where('empresa_id',$empresaId)
    ->where('status_id',3)
    ->get();
  }

  public function estudios(){
    $empresaId = Session::get('empresa')->id;
    return EstudioEmpresa::where('empresa_id',$empresaId)
    ->join('estudios as e','e.id','estudiosEmpresa.estudio_id')
    ->get();
  }

  function import(Request $request) {

      $this->validate($request, [
          'excel' => 'required|mimes:xls,xlsx',
          'grupo' => 'required'
      ]);

      $element_excel = (new ReclutamientoImport)->toArray($request->file('excel'));

      $errors = [];
      $curps = [];
      $indice = 2;
      $encontrado=-1;

      foreach ($element_excel[0] as $key => $row) {
          try {
            $encontrado=array_count_values($curps)[$row['curp']];
          } catch (\Exception $e) {
            $encontrado=-1;
          }
          if ($encontrado==-1) {

            if ($row['clave'] == null) {
                array_push($errors, ['Se requiere el campo Clave en la fila: ' . $indice]);
            }

            if ($row['nombre'] == null) {
                array_push($errors, ['Se requiere el campo Nombre en la fila: ' . $indice]);
            }

            if ($row['apellido_paterno'] == null) {
                array_push($errors, ['Se requiere el campo Apellido Paterno en la fila: ' . $indice]);
            }

            if ($row['apellido_materno'] == null) {
                array_push($errors, ['Se requiere el campo Apellido Materno en la fila: ' . $indice]);
            }

            if ($row['curp'] == null) {
                array_push($errors, ['Se requiere el campo Curp en la fila: ' . $indice]);
            }else {
                array_push($curps, $row['curp']);
            }

            if (strlen($row['curp']) <= 17 || strlen($row['curp']) >= 19) {
                array_push($errors, ['La curp es invalida en la fila: ' . $indice]);
            }

            if ($row['genero_masculinofemenino'] == null) {
                array_push($errors, ['Se requiere el campo Genero en la fila: ' . $indice]);
            }

            if ($row['fecha_de_nacimiento_ano_mes_dia'] == null) {
                array_push($errors, ['Se requiere el campo Fecha de nacimiento en la fila: ' . $indice]);
            }

            if ($row['direccion'] == null) {
                array_push($errors, ['Se requiere el campo Direcci贸n en la fila: ' . $indice]);
            }

            if ($row['e_mail'] == null) {
                array_push($errors, ['Se requiere el campo Email en la fila: ' . $indice]);
            }

            if ($row['telefono'] == null) {
                array_push($errors, ['Se requiere el campo Telefono en la fila: ' . $indice]);
            }

            $empleado_curp = Empleado::where([
                ['CURP', $row['curp']],
                ['empresa_id', Session::get('empresa')->id]
            ])->first();

            if ($empleado_curp != null) {
                array_push($errors, ['Curp ya registrada de: ' . $row['nombre'] . ' ' . $row['apellido_paterno'] . ' ' . $row['apellido_materno'] . ' en la fila:' . $indice]);
            }

            $empleado_clave = Empleado::where([
                ['clave', $row['clave']],
                ['empresa_id', Session::get('empresa')->id]
            ])->first();

            if ($empleado_clave != null) {
                array_push($errors, ['Clave ya registrada de: ' . $row['nombre'] . ' ' . $row['apellido_paterno'] . ' ' . $row['apellido_materno'] . ' en la fila:' . $indice]);
            }

            $indice++;
          }else {
            array_push($errors, ['El curp ' . $row['curp'] . ' esta duplicado porfavor corrige antes de proseguir']);

          }
      }


      if (sizeof($errors) > 0) {
          return response()->json($errors,500);
      }


      if (Session::has('excel')) {
          Session::forget('excel');
      }

      Session::put('excel', Str::random(10));

      (new ReclutamientoImport)->import($request->file('excel'));

      return response()->json($element_excel[0],200);
  }

}
