<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Empresa;
use App\Grupo;
use App\EstudioEmpresa;
use App\EstudioGrupo;
use App\Categoria;
use App\Estudios;
use App\Empleado;
use App\EstudioProgramado;

class GruposController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Controlador de todas las operaciones del modelo Grupo
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos de un grupo.
    |
    */

    /**
     * Añade el middleware de autenticación a la clase
     *
     * Las funciones de esta clase solo serán ejecutadas si el middleware
     * verifica que el usuario se ha autenticado.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
      $grupos = $this->getGrupos();
      return view('Empresa/Grupo',compact('grupos'));
    }

    /**
     * Obtener todos los grupos de una empresa
     *
     * Devuelve la lista de grupos de la empresa del usuario
     * que inició sesión.
     *
     * @return \App\Grupo[] Arreglo con los grupos de la empresa
     */
    public function getGrupos() {
        return Grupo::where('empresa_id', Session::get('empresa')->id)->get();
    }

    /**
     * Crear un nuevo grupo para una empresa
     *
     * Crea un nuevo grupo asociado a la empresa del usuario
     * que inició sesión.
     *
     * @param \Illuminate\Http\Request $request Datos ingresados en el formulario de creación de grupo
     * @return json Lista de todos los grupos de la empresa
     */
    public function insertGrupo(Request $request){
        $this->validate($request ,[
            'grupo' => 'required'
        ]);

        $grupo_identity = Grupo::where([
            ['nombre', $request->get('grupo')],
            ['empresa_id', Session::get('empresa')->id]
        ])->get();

        if (count($grupo_identity) > 0) {
            return abort(400);
        } else {
            $grupo = new Grupo;
            $grupo->nombre = $request->input('grupo');
            $grupo->empresa_id = Session::get('empresa')->id;
            $grupo->save();
            $this->selectorEstudiosGrupo($grupo->id);
            $grupos_empresa = Grupo::where('empresa_id',Session::get('empresa')->id)->get();
            return json_encode($grupos_empresa);
        }
    }

    /**
     * Eliminar un grupo
     *
     * Elimina un grupo si no tiene estudios programados y todos
     * los empleados que pertenecían a ese grupo se quedan sin grupo asignado.
     *
     * @param string $nombre Nombre del grupo por eliminar
     * @return json Lista actualizada de todos los grupos de la empresa
     */
    public function deleteGrupo($nombre) {
        $grupo = Grupo::where([['empresa_id', Session::get('empresa')->id], ['nombre', $nombre]])->first();
        $estudios_grupo = EstudioProgramado::where('grupo_id',$grupo->id)->get();

        if(count($estudios_grupo) == 0) {
            Empleado::where('grupo_id', $grupo->id)
                ->update(['grupo_id' => 0]);

            $grupo->delete();
            $grupos_empresa = $this->getGrupos();
            return json_encode($grupos_empresa);

        } else {
            return json_encode(null);
        }
    }

    /**
     * Actualizar el nombre de un grupo
     *
     * Actualiza el nombre de un grupo y devuelve la lista de grupos
     * de la empresa actualizada.
     *
     * @param \Illuminate\Http\Request $request Datos del formulario que actualiza el nombre del grupo
     * @return json Lista de grupos de la empresa
     */
    public function updateGrupo(Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'nombre_update'=> 'required'
        ]);

        $grupo = Grupo::where([
            ['empresa_id', Session::get('empresa')->id],
            ['nombre', $request->input('nombre')]
            ])->first();
        $grupo->nombre = $request->input('nombre_update');
        $grupo->save();

        $grupos_empresa = $this->getGrupos();
        return json_encode($grupos_empresa);
    }

    /**
     * Muestra la vista con información de un grupo de la empresa
     *
     * Carga todos los datos del grupo y devuelve la vista con su información.
     *
     * @param string $nombre Nombre del grupo por mostrar
     * @return Empresa\infoGrupo Vista que muestra la información del grupo
     */
    public function show($nombre) {
        $user = Auth::user();
        $empresa = Session::get('empresa');

        $grupo = Grupo::where([['nombre', $nombre], ['empresa_id', $empresa->id]])->first();

        $estudios_empresa = EstudioEmpresa::where('empresa_id', $empresa->id)
            ->with('estudio')
            ->get();

        $estudios_grupo = EstudioGrupo::where('grupo_id', $grupo->id)
            ->with('estudio')
            ->get();

        $estudios_programados = EstudioProgramado::where([['grupo_id', $grupo->id], ['status', 0]])
            ->orderBy('fecha_inicial', 'desc')
            ->take(15)
            ->get();

        $last_token = "";
        $estudios_unicos = [];
        foreach ($estudios_programados as $estudio_programado) {
            if ($estudio_programado->token != $last_token) {
                array_push($estudios_unicos, $estudio_programado);
                $last_token = $estudio_programado->token;
            }
        }

        $nombres = [];
        foreach ($estudios_unicos as $estudio_unico) {
            $lista_estudios = json_decode($estudio_unico->estudios);
            foreach($lista_estudios as $estudio) {
                array_push($nombres, Estudios::find($estudio)->nombre);
            }
            $estudio_unico->estudios = json_encode($nombres);
            $estudio_unico->estudios_id = json_encode($lista_estudios);
            $nombres = [];
        }

        $categorias = Categoria::all();

        return view ('Empresa/infoGrupo', [
            'grupo' => $grupo,
            'encryptedID' => encrypt($grupo->id),
            'estudios_empresa' => $estudios_empresa,
            'estudios_grupo' => $estudios_grupo,
            'categorias' => $categorias,
            'estudios_programados' => $estudios_unicos
        ]);
    }

    /**
     * Asigna los estudios autorizados para un grupo
     *
     * Determina qué estudios estarán disponibles para programarse a un grupo
     * ingresándolos en la tabla estudiosGrupo de la base de datos.
     *
     * @param \Illuminate\Http\Request $request Datos del formulario que selecciona los estudios
     * @return Empresa\infogrupo Vista con la información del grupo
     */
    public function selectorEstudiosGrupo($id) {
        $empresaId = Session::get('empresa')->id;
        $estudios = EstudioEmpresa::where('empresa_id',$empresaId)->get();
        foreach ($estudios as $value) {
          $estudioGrupo = new EstudioGrupo;
          $estudioGrupo->estudio_id = $value->estudio_id;
          $estudioGrupo->grupo_id = $id;
          $estudioGrupo->save();
        }
        return $empresaId;
    }

    /**
     * Programar estudios para un grupo completo
     *
     * Programa los estudios seleccionados en el formulario a todo el grupo.
     *
     * @param \Illuminate\Http\Request $request Datos del formulario con lista de estudios por programar
     * @return Empresa\infoGrupo Vista con la información del grupo
     */
    public function programarEstudiosGrupo(Request $request) {
        $grupo_id = decrypt($request->input('encryptedID'));
        $grupo = Grupo::find($grupo_id);
        $elements = $request->input('estudios_programar');

        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where([['empresa_id', $empresa->id],['status_id', 1], ['grupo_id', $grupo_id]])->get();

        $cantidadNoProgramados = 0;
        $seProgramaronEstudios = 0;

        if (empty($elements)) {
            foreach($empleados as $empleado) {
                $estudios_en_proceso = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 1]])
                    ->orWhere([['empleado_id', $empleado->id], ['status', 3]])
                    ->get();

                if (sizeof($estudios_en_proceso) < 1) {
                    $seProgramaronEstudios++;
                } else {
                    $cantidadNoProgramados++;
                    continue;
                }

                $estudios_previos = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 0]])->get();
                foreach($estudios_previos as $estudio_previo) {
                    $estudio_previo->delete();
                }
            }

            if ($seProgramaronEstudios > 0) {
                return redirect()->route('grupos.show', $grupo->nombre)
                    ->with(['message'=>"Se han eliminado todos los estudios programados para este grupo correctamente"]);
            } else {
                return redirect()->route('grupos.show', $grupo->nombre)
                    ->withErrors(["No se han realizado cambios porque todos los miembros de este grupo tiene estudios en proceso"]);
            }
        }

        $estudios_por_programar = [];
        foreach($elements as $element) {
            array_push($estudios_por_programar, (int)$element);
        }

        $user = Auth::user();
        $empresa = Session::get('empresa');
        $empleados = Empleado::where([['empresa_id', $empresa->id], ['grupo_id', $grupo_id]])->get();

        $estudios_JSON = json_encode($estudios_por_programar);

        $token = Hash::make(Str::random(4));

        foreach($empleados as $empleado) {
            $estudios_en_proceso = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 1]])
                ->orWhere([['empleado_id', $empleado->id], ['status', 3]])
                ->get();

            if (sizeof($estudios_en_proceso) < 1) {
                $seProgramaronEstudios++;
            } else {
                $cantidadNoProgramados++;
                continue;
            }

            $estudios_previos = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 0]])->get();
            foreach($estudios_previos as $estudio_previo) {
                $estudio_previo->delete();
            }

            $estudio_programado = new EstudioProgramado;
            $estudio_programado->estudios = $estudios_JSON;
            $estudio_programado->fecha_inicial = $request->input('fechaInicio');
            $estudio_programado->fecha_final = $request->input('fechaFinal');
            $estudio_programado->status = 0;
            $estudio_programado->empleado_id = $empleado->id;
            $estudio_programado->empresa_id = $empresa->id;
            $estudio_programado->token = $token;
            $estudio_programado->grupo_id = $grupo_id;
            $estudio_programado->save();
        }

        if (($seProgramaronEstudios > 0) && ($cantidadNoProgramados == 0)) {
            return redirect()->route('grupos.show', $grupo->nombre)
                ->with(['message'=>"Se han programado estudios correctamente para todo el grupo"]);

        } else if ($seProgramaronEstudios > 0) {
            return redirect()->route('grupos.show', $grupo->nombre)
                ->with(['message'=>"Se han programado estudios correctamente para los miembros del grupo que no tienen estudios en proceso"]);

        } else {
            return redirect()->route('grupos.show', $grupo->nombre)
                ->withErrors(["No se han realizado cambios porque todos los miembros de este grupo tiene estudios en proceso"]);
        }
    }

    /**
     * Eliminar un estudio al grupo completo
     *
     * Elimina un estudio a todos los miembros de un grupo si este fue
     * programado desde la vista de información de su grupo.
     *
     * @param \Illuminate\Http\Request $request Datos del formulario que identifica al estudio por eliminar
     * @return Empresa\infogrupo Vista con la información del grupo
     */
    public function eliminarEstudioGrupo(Request $request) {
        $grupo_id = decrypt($request->input('encryptedID'));
        $token = $request->input('token');
        $nombre_estudio = $request->input('nombreEstudio');
        $estudio_id =  $request->input('idEstudio');
        $fechaInicial =  $request->input('fechaInicial');
        $fechaFinal =  $request->input('fechaFinal');

        $grupo = Grupo::find($grupo_id);

        $estudios_programados = EstudioProgramado::where([['token', $token], ['status', 0]])
                    ->orWhere([['fecha_inicial', $fechaInicial], ['fecha_final', $fechaFinal], ['status', 0]])
                    ->get();

        foreach ($estudios_programados as $estudio_programado) {
            $lista_estudios = json_decode($estudio_programado->estudios);
            foreach ($lista_estudios as $key => $estudio) {
                if ($estudio == $estudio_id) {
                    unset($lista_estudios[$key]);
                }
            }

            $lista_actualizada = [];
            foreach ($lista_estudios as $estudio) {
                array_push($lista_actualizada, $estudio);
            }

            if (empty($lista_actualizada)) {
                $estudio_programado->delete();
            } else {
                $estudio_programado->estudios = json_encode($lista_actualizada);
                $estudio_programado->save();
            }
        }

        return redirect()->route('grupos.show', $grupo->nombre)
            ->with(['message' => "El estudio se ha eliminado correctamente"]);
    }

    /**
     * Obtener los estudios programados para un grupo
     *
     * Devuelve la lista de estudios programados para el grupo cuyo nombre
     * corresponde al parámetro recibido.
     *
     * @param string $nombre Nombre del grupo
     * @return json Lista de estudios programados para el grupo
     */
    public function getEstudiosProgramadosGrupo($nombre) {
        $grupo = Grupo::where([['empresa_id', Session::get('empresa')->id], ['nombre', $nombre]])->first();
        $estudios_programados = EstudioProgramado::where([['grupo_id', $grupo->id], ['status', 0] ])->first();

        return json_encode($estudios_programados);
    }

    /**
     * Obtener lista de empleados sin grupo
     *
     * Devuelva una lista en formado json con los empleados que no tienen
     * un grupo asignado (grupo_id = 0).
     *
     * @return json Lista de empleados sin grupo
     */
    public function getEmpleadosSinGrupo() {
        $empleados_sin_grupo = Empleado::where([['grupo_id', 0],['status_id', 1], ['empresa_id', Session::get('empresa')->id]])->get();
        return json_encode($empleados_sin_grupo);
    }

    /**
     * Agregar empleados a un grupo
     *
     * Asigna con el grupo que corresponda al ID encriptado a los empleados
     * enlistados en el campo empleados_agregar.
     *
     * @param \Illuminate\Http\Request $request Datos del formulario para agregar empleados a un grupo
     * @return Empresa\infoGrupo Vista con la información del grupo
     */
    public function agregarEmpleadosGrupo(Request $request) {
        $grupo_id = decrypt($request->input('encryptedID'));
        $grupo = Grupo::find($grupo_id);
        $elements = [];

        foreach ($request->input('empleados_agregar') as $CURP) {
            $empleado = Empleado::where('CURP', $CURP)->first();
            $empleado->grupo_id = $grupo->id;
            $empleado->save();
        }

        return redirect()->route('grupos.show', $grupo->nombre)
            ->with(['message' => "Los empleados se han agregado correctamente"]);
    }
}
