<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Resultados;
use App\Expediente;
use App\Archivo;
use App\SassOrden;
use App\Estudios;
use App\Empleado;
use App\Imagenologia;
use App\EstudioProgramado;

class ResultadosController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Controlador de todas las operaciones del modelo Resultados
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos de un resultado para usuarios con permisos de empresa.
    |
    */

    /**
     * Añade el middleware de autenticación a la clase
     *
     * Las funciones de esta clase solo serán ejecutadas si el middleware
     * verifica que el usuario se ha autenticado.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     *
     */
    public function index() {
        $resultados = $this->getResultados();
        return view('Empresa/Resultados')
            ->with('resultados', $resultados);
    }
    public function estudios_fech($fecha_inicial,$fecha_final){
    
      $resultados = $this->getResultadosFecha($fecha_inicial,$fecha_final);
      return compact('resultados', $resultados);

    }

    /**
     *
     */
    public function archivo($id) {
        try {
            $idDecrypt = decrypt($id);
        } catch (DecryptException $e) {
            return abort(404);
        }

        $resultado = Resultados::find($idDecrypt);
        $estudio = Estudios::find($resultado->estudio_id);
        $nim = SassOrden::where('programacion',$resultado->estudiosProgramados_id)
        ->first();
        if ($nim) {
          $paciente = Empleado::find($nim->paciente_id);
       }else {
         return back();
       }

      //
      //   $resultado_sass = \DB::connection('sass')
      //   ->table('PACtrnIngresos')->where('NumeroToma',$nim->nim_sass)
      //   ->get();
      //
      //   $resultado_sass_first = \DB::connection('sass')
      //   ->table('PACtrnIngresos')->where('NumeroToma',$nim->nim_sass)
      //   ->first();
      //
      //
      //
      //   if ($paciente->id_sass == NULL) {
      //     $paciente->id_sass = $resultado_sass_first->IdPaciente;
      //     $paciente->save();
      //   }
      //
      // $estudio_select = null;
      // $estudios_detail = null;
      //
      // foreach ($resultado_sass as  $value) {
      //     $estudio_sass = \DB::connection('sass')->table('catEstudios')->where('id',$value->IdCatEstudio)->first();
      //     if ($estudio_sass->Clave == $codigo_estudios->codigo) {
      //       $estudio_select = $value;
      //       $estudios_detail = $estudio_sass;
      //     }
      // }
      //
      // if ($estudio_select == null) {
      //   $resultado_sass=[];
      //   $estudios_detail=[];
      // }else {
      //   $resultado_sass = \DB::connection('sass')
      //   ->table('PACtrnResultados')
      //   ->where('idEstudio',$estudio_select->Id)
      //   ->get();
      // }
      //
      if ($resultado->categoria_id == 2) {
        $imagenologia = Imagenologia::where('resultado_id',$idDecrypt)->get();
      }else{
        $imagenologia = null;
      }

      return view('Empresa/Archivos', compact('archivos','estudio','estudios_detail','paciente','nim','imagenologia'));
    }

    /**
     *
     */
    /*public function getResultados() {
        $response =  Resultados::select('empleados.nombre as nombre', 'empleados.apellido_paterno as app', 'empleados.apellido_materno as apm', 'estudios.nombre as estudio', 'resultados.created_at', 'resultados.id as id')
            ->join('expedientes', 'resultados.expediente_id', '=', 'expedientes.id')
            ->join('empleados', 'empleados.CURP', 'expedientes.curp')
            ->join('estudios', 'estudios.id', 'resultados.estudio_id')
            ->where('resultados.empresa_id', Session::get('empresa')->id)
            ->get();

        return $response;
    }*/
    public function getResultados() {

      $response =  EstudioProgramado::select('estudiosProgramados.id','estudiosProgramados.estudios','estudiosProgramados.folio','estudiosProgramados.empleado_id','estudiosProgramados.fecha_inicial','estudiosProgramados.fecha_final','empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno')
      ->join('empleados','estudiosProgramados.empleado_id','empleados.id')
    
      ->get();
      

      return $response;
  }
  public function getResultadosFecha($fecha_inicial,$fecha_final) {

    $response =  EstudioProgramado::select('estudiosProgramados.id','estudiosProgramados.estudios','estudiosProgramados.folio','estudiosProgramados.empleado_id','estudiosProgramados.fecha_inicial','estudiosProgramados.fecha_final','empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno')
    ->join('empleados','estudiosProgramados.empleado_id','empleados.id')
    ->where('estudiosProgramados.fecha_inicial','>=',$fecha_inicial)
    ->where('estudiosProgramados.fecha_final','<=',$fecha_final)
    ->get();
  
    

    return $response;
}
}
