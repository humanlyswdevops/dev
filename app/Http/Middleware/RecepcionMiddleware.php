<?php

namespace App\Http\Middleware;

use Closure;

class RecepcionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->role_id;
        if($role == 3){
            return $next($request);
        } else {
            return redirect()->route('admin');
        }
    }
}
