<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Grupo;

class EstudioGrupo extends Model {
    protected $table = 'estudiosGrupo';

    public function grupo() {
        return $this->belongsTo(Grupo::class, 'grupo_id', 'id');
    }

    public function estudio() {
        return $this->belongsTo(Estudios::class, 'estudio_id', 'id');
    }
}
