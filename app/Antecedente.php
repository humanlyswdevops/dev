<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antecedente extends Model {
    protected $table = 'antecedentes';
 
    public function answer()
    {
        return $this->hasOne(AntecedenteAnswer::class, 'id', 'answerA_id');
    }

    public function expediente()
    {
        return $this->hasOne(Expediente::class, 'id', 'id_expediente');
    }
}
