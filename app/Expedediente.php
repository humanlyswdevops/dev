<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Nota;

class Expediente extends Model
{
    protected $table = "expedientes";

    public function nota() {
        return $this->hasOne(Nota::class);
    }

    public function estudiosCompletados()
    {
        try {
            return $this->hasMany(Resultados::class, 'expediente_id', 'id');
        } catch (\Throwable $th) {
            return $this->hasMany(Resultados::class, 'expediente_id', 0);
        }
    }

    public function empleado()
    {
        return $this->hasOne(Empleado::class, 'CURP', 'curp');
    }

    public function estudioscompletadosMedicina()
    {
        try {
            return $this->hasMany(Resultados::class, 'expediente_id', 'id')->where('categoria_id',3);
        } catch (\Throwable $th) {
            return $this->hasMany(Resultados::class, 'expediente_id', '-1');
        }
    }
}
