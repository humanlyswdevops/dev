<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EstudioEmpresa;
use App\Empleado;
use App\Giro;
use Illuminate\Support\Facades\Storage;

class Empresa extends Model {
    protected $table = 'empresas';

    public function Usuario() {
        return $this->hasOne('App\User');
    }

    public function giro() {
        return $this->belongsTo(Giro::class, 'giro_id', 'id');
    }

    public function estudiosEmpresa() {
        return $this->hasMany(EstudioEmpresa::class);
    }

    public function empleados() {
        return $this->hasMany(Empleado::class);
    }

    public function foto()
    {
        $exists = Storage::disk('empresa')->exists($this->logo);
        if(!$exists)
        {
            $this->logo = "noLogo.png";
        }
        return $this->hasOne(Empresa::class,'id');
    }
}
