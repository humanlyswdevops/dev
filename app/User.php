<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;

use App\Accesos;
use App\Especialidad;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
        /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }


    public function role() {
        return $this->belongsTo('App\Roles');
    }

    public function accesos(){
      return $this->hasMany(Accesos::class, 'rol_id','role_id');
    }

    public function admin() {
        if ($this->role->tipo == "administrador") {
            return true;
        } else {
            return false;
        }
    }

    public function empresa() {
        if ($this->role->tipo == "usuario" || $this->role->tipo == "usuario-secundario") {
            return true;
        } else {
            return false;
        }
    }

    public function usuarioPrincipal() {
        if ($this->role->tipo == "usuario") {
            return true;
        } else {
            return false;
        }
    }

    public function usuarioSecundario() {
        if ($this->role->tipo == "usuario-secundario") {
            return true;
        } else {
            return false;
        }
    }

    public function espec(){
      return $this->hasOne(Especialidad::class,'id','especialidad_id');
    }
}
