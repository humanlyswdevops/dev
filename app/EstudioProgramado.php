<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Estudios;
use App\Empleado;
use App\Grupo;

class EstudioProgramado extends Model {
    protected $table = "estudiosProgramados";

    public function empleado() {
        return $this->belongsTo(Empleado::class, 'empleado_id', 'id');            
    }

    public function objetivo() {
        if ($this->empleado != null) {
            return ($this->empleado->nombre . " " . $this->empleado->apellido_paterno . " " . $this->empleado->apellido_materno);
        } else {
            return $this->grupo->nombre;
        }
    }
}
