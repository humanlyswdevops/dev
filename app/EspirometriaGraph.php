<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspirometriaGraph extends Model {
    protected $table = 'espirometria_man_graph';
}
