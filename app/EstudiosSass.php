<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudiosSass extends Model {
    protected $table = 'estudiosSass';

    public function toma()
    {
       return  $this->hasOne(Toma::class,'id','toma_id');
    }
}
