<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcgMedidas extends Model {
    protected $table = 'ecgs_medidas';
}
