<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspirometriaMan extends Model {
    protected $table = 'espirometria_man';


    public function graph()
    {
        return $this->hasOne(EspirometriaGraph::class,'maneobra','maneobra');
    }


}
