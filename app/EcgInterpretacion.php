<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcgInterpretacion extends Model {
    protected $table = 'ecgs_interpretacion';
}
