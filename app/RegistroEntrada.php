<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroEntrada extends Model {
    protected $table = 'registrosentradas';

    // public function empleado() {
    //     return $this->hasMany(Empleado::class, 'id');
    // }

    public function empleado()
    {
        return $this->hasOne(Empleado::class, 'id', 'empleado_id');
    }
}
