<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntaFormulario extends Model {
    protected $table = 'pregunta_formulario';

    public function respuestas()
    {
        return $this->hasMany(RespuestaPregunta::class, 'pregunta_formulario_id', 'id');
    }

    public function respuesta($historial_clinico,$pregunta_id)
    {
        return $this->hasOne(RespuestaPaciente::class)
        ->where('pregunta_formulario_id',$pregunta_id)
        ->where('historial_clinico_id',$historial_clinico)
        ->first();
    }
}
