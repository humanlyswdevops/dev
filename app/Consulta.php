<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model {
    protected $table = 'consultas';

    public function ecgs()
    {
        return $this->hasMany(Ecg::class, 'consulta_id', 'id');
    }

    public function signos()
    {
        return $this->hasOne(ConsultaExamenFisico::class,'consulta_id', 'id');
    }

    public function medicamentos()
    {
        return $this->hasMany(ConsultaMedicamento::class, 'consulta_id', 'id');
    }
}
