<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcgSerie extends Model {
    protected $table = 'ecgs_series';
}
