<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivoMedico extends Model {
    protected $table = 'archivos_medicos';
}
