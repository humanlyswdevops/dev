<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Empresa;
use App\Estudios;

class EstudioEmpresa extends Model {
    protected $table = 'estudiosEmpresa';
  
    public function empresa() {
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }

    public function estudio() {
        return $this->belongsTo(Estudios::class, 'estudio_id', 'id');
    }

}
