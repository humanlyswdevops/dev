<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EstudioProgramado;
use App\Categoria;
use App\EstudioEmpresa;
use App\EstudioGrupo;

class Estudios extends Model {
    protected $table = "estudios";

    public function estudiosEmpresa() {
        return $this->hasMany(EstudioEmpresa::class);
    }

    public function estudiosGrupo() {
        return $this->hasMany(EstudioGrupo::class);
    }

    public function categoria() {
        return $this->belongsTo(Categoria::class, 'categoria_id', 'id');
    }
}
