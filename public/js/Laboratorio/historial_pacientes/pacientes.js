var userList = new List('admin_list', {
    valueNames: [ 'search_nombre','search_edad','search_genero','search_curp','search_numero_estudios'],
    page: 6,
    pagination: {
        innerWindow: 1,
        left: 1,
        right: 1,
        paginationClass: "pagination",
        item:"<li class='page-item'><a class='page-link page' href='#'></a></li>",
    }
});
var length_lis = $('.list .list_custom').length;

if(length_lis == 0){
    $('.jPaginateNext').css('display','none');
    $('.jPaginateBack').css('display','none');
}
else{
    $("#anterior").addClass('pagination').html('<li class="page-item prev-item jPaginateBack"><button type="button" style="border-radius: 50%;" class="page-link navegadores"></button></li>');
    $("#siguiente").addClass('pagination').html('<li class="page-item next-item jPaginateNext"><button type="button" style="border-radius: 50%;" class="page-link navegadores"></button></li>');
    $('.jPaginateNext').css('display','block');
    $('.jPaginateBack').css('display','block');

    $('.jPaginateNext').on('click', function(e){
        var list = $('.pagination').find('li');
        $.each(list, function(position, element){
            if($(element).is('.active')){
                $(list[position+1]).find('a')[0].click();
            }
        })
    });

    $('.jPaginateBack').on('click', function(e){
        var list = $('.pagination').find('li');
        $.each(list, function(position, element){
            if($(element).is('.active')){
                $(list[position-1]).find('a')[0].click();
            }
        })
    });
}

$(".input_buscador").keyup(function(){
    if($('.list .list_custom').length == 0)
    {
        $("#msg-result").removeClass('d-none');
    }else{
        $("#msg-result").addClass('d-none');
    }
});

setInterval(() => {
    $(".page-link.page").click(function(e){
        e.preventDefault();
    });
    $(".a_modal_no_estudios").click(function(){
        $("#name_paciente").text($(this).data('name'));
        $("#modal_no_estudios").modal();
    });
}, 500);

$(".a_modal_no_estudios").click(function(){
    $("#name_paciente").text($(this).data('name'));
    $("#modal_no_estudios").modal();
});

$(".btn_sort").click(function(){
    $(".btn_sort .text-dark .feather").removeClass('text-white');
    if($(this).hasClass("asc"))
    {
        $(this).find('.text-dark .icon-chevron-up').addClass('text-white');
    }else{
        $(this).find('.text-dark .icon-chevron-down').addClass('text-white');
    }
});
