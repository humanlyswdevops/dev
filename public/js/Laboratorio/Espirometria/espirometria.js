// function initToma() {
//     obj = new Object();
//     obj.empresaId = $('.empresaId').val();
//     obj.empleadoId = $('.empleadoId').val();
//     $.ajax({
//         type:'post',
//         dataType:'json',
//         data:obj,
//         url:'../solicitud',
//     }).done(res=>{
//         console.log(res);
//     }).fail(err=>{

//     })
// }

// $(window).ready(()=>{
//     for (var i = 1; i <= 3; i++) {
//         var data = $('#maneobra_'+i).val();
//         var fvc = $('#fvc_'+i).val();
//         electrocardiograma(data,i,fvc);
//     }
// })

// function electrocardiograma(res,number,fvc) {

//   $('#content').append(
//     `
//     <div class="col-md-4">
//     <div id="ecg${number}" style="height: 400px; width: 100%;"></div>
//     </div>
//     `
//   )

//   data = res.split(' ');
//   dataEcg = [];
//   data.forEach((item, i) => {
//     dataEcg.push(parseInt(item) * 0.001);
//   });

//   console.log(dataEcg);

//   var xAxisStripLinesArray = [];
//   var yAxisStripLinesArray = [];
//   var dps = [];
//   var dataPointsArray =dataEcg;

//   var chart = new CanvasJS.Chart(`ecg${number}`,{
//       title:{
//         text:"Maneobra "+number,
//       },
//       axisY:{
//         stripLines:yAxisStripLinesArray,
//         gridThickness: 1,
//         gridColor:"#002b46",
//         lineColor:"#002b46",
//         tickColor:"#002b46",
//         labelFontColor:"#002b46",
//       },
//       axisX:{
//         stripLines:xAxisStripLinesArray,
//         gridThickness: 1,
//         gridColor:"#002b46",
//         lineColor:"#002b46",
//         tickColor:"#002b46",
//         labelFontColor:"#002b46",
//       },
//       credits: {
//         enabled: false
//     },
//       data: [
//           {
//             type: "spline",
//             color:"black",
//             dataPoints: dps
//           }
//       ]
//   });

//   var volumen = fvc/dataEcg.length;
//   var volumenIncrement = volumen;
//   for(var i=0; i<dataEcg.length;i++){
//     dps.push({y: dataPointsArray[i],x: volumenIncrement});
//     volumenIncrement += volumen;
//   }

//   chart.render();

// }

//////////////////////////////////////////////////////////////////////

$(document).on('change', '#fv_input', function(){

    if ($(this).val()) {
        var formData = new FormData(document.getElementById("fv"));
        $.ajax({
            url: "../fv_file",
            type: "post",
            dataType: "json",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend:()=>{
                $('.fv-load').show();

            }
        }).done(function(res){
            $('.fv-load').hide();

            $('.fv_img').attr('src','../storage/app/espirometria/'+res.data.fv);
        }).fail(err=>{
            swal('Error','Hubo algún problema al subir el archivo, intentalo más tarde','error')
        });
    }
});

$(document).on('change', '#vt_input', function(){
    console.log($(this).val());
    if ($(this).val()) {
        var formData = new FormData(document.getElementById("vt"));

        $.ajax({
            url: "../vt_file",
            type: "post",
            dataType: "json",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend:()=>{
                $('.vt-load').show();
            }
        }).done(function(res){
            $('.vt-load').hide();
            $('.vt_img').attr('src','../storage/app/espirometria/'+res.data.vt);

        }).fail(err=>{
            swal('Error','Hubo algún problema al subir el archivo, intentalo más tarde','error')
        })
    }

});



$('.updateMedico').click(function (e) {
    $('#datosMedico').modal('show');
});


$('#updateFormData').on('submit',function(e){

    e.preventDefault();
    var formData = new FormData(document.getElementById("updateFormData"));
    $.ajax({
        type: "post",
        url: "../Audiometria/guardarFirma",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        beforeSend:()=>{
          $('.loadFormUpdate').show();
          $('#updateFormData').hide();

        }
    }).done(res=>{

        $('.firmaMedico').attr('src','../storage/app/datosHtds/'+res.firma);
        $('.titulo').text(res.titulo_profesional);
        $('.nombre').text(res.nombre_completo);
        $('.cedula').text(res.cedula);
        $('#datosMedico').modal('hide');
        $('.loadFormUpdate').hide();
        $('#updateFormData').show();
        swal('Modificado','Los datos se modificaron correctamente','success')

    }).fail(err=>{
        $('.loadFormUpdate').hide();
        $('#updateFormData').show();
        swal('Error','Hubo algún problema, intentalo más tarde','error');
    });

})



$('#saveForm').on('submit',function(e){
    e.preventDefault();
    swal({
        title: "Atención",
        text: "Estas a punto de terminar, ¿Estás seguro de continuar?",
        icon: "warning",
        buttons: {
            cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
            },
            confirm: {
                    text: "Confirmar",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
            }
        }
    })
    .then((isConfirm) => {
        if (isConfirm) {
            saveEspirometria();
        } else {
            swal("Cancelado", "la operación fue cancelada", "error");
        }
    });
})





function saveEspirometria() {
    let interpretacion = $('#interpretacion').val();
    let medico = $('#medicoToken').val();

    if (interpretacion == ''){
        swal('Espera...','Falta por interpretar el estudio.','info');
        return
    }else if(medico == ""){
        swal('Espera...','Falta la firma de medico.','info');
        return
    }
    data = new Object();
    data.ingreso = $('#ingreso').val();
    data.interpretacion = interpretacion;
    data.espiroId = $('#espirometria').val();

    $.ajax({
        type: "post",
        url: "../save_espirometria",
        data: data,
        dataType: "json",
        beforeSend:()=>{
          $('.loadFormUpdate').show();
          $('#updateFormData').hide();

        }
    }).done(res=>{

        swal({
			position:'top-end',
			icon:'success',
			text:'El estudio fue terminado correctamente',
			showConfirmButton:true
		}).then(isConfirm=>{
			location.href = "../Medicina-Pacientes";
		})

    }).fail(err=>{

        swal('Error','Hubo algún problema, intentalo más tarde','error');
    });
}
