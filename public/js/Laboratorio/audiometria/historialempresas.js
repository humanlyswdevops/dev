// $(document).ready(function(){
//     window.location.replace("Medicina-Pacientes");
// })
var options = {
    valueNames: ['nombre','direccion'],
    page: 3,
    pagination: true,
    pagination: {
        innerWindow: 1,
        left: 1,
        right: 1,
        paginationClass: "pagination",
        item: "<li class='page-item'><a class='page-link page' ></a></li>",
    }
};

var consultas = new List('empresas', options);
//Agregar Empresa
$("#btnagregar").click(function () {
    $("#agregarmodal").modal();
})
// $("#form_empresas").submit(function (e) {
//     e.preventDefault();
//     form = $("#form_empresas").serialize();
//     $.ajax({
//         type: 'post',
//         dataType: 'json',
//         url: 'add_empresa',
//         data: form,
//     }).done(function (response) {
//         $("#btnopenmedicamento").prop("disabled", false);
//         swal({
//             position: 'top-end',
//             icon: 'success',
//             text: 'Los datos fueron almacenados de manera correcta',
//             title: 'Empresa Registrada',
//             showConfirmButton: true
//         }).then(isConfirm => {
//             location.reload();
//         });
        
//     }).fail(function (error) {
//         var message;
//         message = "Algo ocurrió, inténtalo más tarde...";
//         swal('Error', message, 'error')
//         // alert("Error");

//         $("#btnagregar").prop("disabled", false);
//     });
// });
$("#form_empresas").submit(function(e){
    e.preventDefault();
    var formData = new FormData();
    var input_file = document.getElementById("logo");
    formData.append('logo',input_file.files[0]);
    formData.append('nombre',$("#nombre").val());
    formData.append('direccion',$("#direccion").val());
    alert(input_file);
    $.ajax({
        url:'add_empresa',
        type:'post',
        data:formData,
        dataType:'json',
        contentType:false,
        processData:false,
        beforeSend:()=>{
        }
    }).done(res=>{
        swal({
            position: 'top-end',
            icon: 'success',
            text: 'Los datos fueron almacenados de manera correcta',
            title: 'Empresa Registrada',
            showConfirmButton: true
        }).then(isConfirm => {
            location.reload();
        });         
    }).fail(err=>{
        swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
    });
})
//Agregar Empresa

// $(document).on('click', '.edit_emp', function () {
//     cargadatos($(this).data('id'));

// });

// function cargadatos(id) {
//     $.ajax({
//         type: "get",
//         url: "getEmp/" + id,
//         dataType: "json",
//         beforeSend: () => {

//         }
//     }).done(res => {
//         console.log(res.medic);
//         document.getElementById("id_e").value = res.medic.id;
//         document.getElementById("nombre_e").value = res.medic.nombre;
//         document.getElementById("direccion_e").value = res.medic.direccion;
//         // document.getElementById("logo_e").value = res.medic.logo;
//         $('#editar').modal('show');
//     }).fail(err => {

//         console.log(err);
//         swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
//     })
// }
// $('#form_edit_empresa').on('submit', function (e) {
//     e.preventDefault();
//     swal({
//         title: "Modifiación",
//         text: "¿Estas seguro de modificar los datos?",
//         icon: "warning",
//         buttons: {
//             cancel: {
//                 text: "No",
//                 value: null,
//                 visible: true,
//                 className: "",
//                 closeModal: false,
//             },
//             confirm: {
//                 text: "Si",
//                 value: true,
//                 visible: true,
//                 className: "",
//                 closeModal: false
//             }
//         }
//     }).then(isConfirm => {
//         if (isConfirm) {
//             modificacion();
//         } else {
//             swal("Cancelado", "La modificación fue cancelada", "error");
//         }
//     });

// });
// function modificacion() {
//     // e.preventDefault();
//     var formData = new FormData();
//     // var input_file = document.getElementById("logo");
//     // formData.append('logo',input_file.files[0]);
//     alert($("#nombre_e").val());
//     alert($("#direccion_e").val());
//     alert($("#id_e").val());
//     formData.append('nombre_e',$("#nombre_e").val());
//     formData.append('direccion_e',$("#direccion_e").val());   
//     formData.append('id_e',$("#id_e").val());
//     $.ajax({
//         url:'edit_empresa',
//         type:'post',
//         data:formData,
//         dataType:'json',
//         contentType:false,
//         processData:false,
//         beforeSend:()=>{
//         }
//     }).done(res=>{
//         swal("Exito","La información se ha actualizado correctamente.","success"); 
//         location.reload();          
//     }).fail(err=>{
//         swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
//     });
// }


// $(document).on('click', '.edit_emp', function () {
//     cargadatos($(this).data('id'));

// });
function cargadatos(id) {
    $.ajax({
        type: "get",
        url: "getEmp/" + id,
        dataType: "json",
        beforeSend: () => {

        }
    }).done(res => {
        console.log(res.medic);
        document.getElementById("id_e").value = res.medic.id;
        document.getElementById("nombre_e").value = res.medic.nombre;
        document.getElementById("direccion_e").value = res.medic.direccion;       
        // document.getElementById("logo_e").value = res.medic.logo;
        // document.getElementById("logo_e").files[0].value=res.medic.logo;
        $('#editar').modal('show');
    }).fail(err => {

        console.log(err);
        swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
    })
}
$('#form_edit_empresa').on('submit', function (e) {
    e.preventDefault();
    swal({
        title: "Modificación",
        text: "¿Estas seguro de modificar los datos?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            modificacion();
        } else {
            swal("Cancelado", "La modificación fue cancelada", "error");
        }
    });

});
function modificacion() {

    var formData = new FormData();
    var input_file = document.getElementById("logo_e");
    
    formData.append('logo_e',input_file.files[0]);
    formData.append('nombre_e',$("#nombre_e").val());
    formData.append('direccion_e',$("#direccion_e").val());
    formData.append('id_e',$("#id_e").val());
    console.log(input_file.files[0]);
    $.ajax({
        url:'editar_empresa',
        type:'post',
        data:formData,
        dataType:'json',
        contentType:false,
        processData:false,
        beforeSend:()=>{
        }
    }).done(res=>{
        swal({
            position: 'top-end',
            icon: 'success',
            text: 'Los datos fueron actualizados de manera correcta',
            title: 'Empresa Actualizada',
            showConfirmButton: true
        }).then(isConfirm => {
            location.reload();
        });         
    }).fail(err=>{
        swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
    });
}


//Eliminar Medicamento
function eliminar(id) {
    swal({
        title: "Eliminación",
        text: "¿Estas seguro de eliminar los datos?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            eliminacion(id);
        } else {
            swal("Cancelado", "La operación ha sido cancelada", "error");
        }
    });

}

function eliminacion(id) {
    $.ajax({
        type: 'POST',
        url: 'delete_empresa/'+id,
        dataType: 'json',
        data: $('#form_edit_empresa').serialize(),
        beforeSend: () => {
        },
        success: function (response) {          
            swal({
                position: 'top-end',
                icon: 'success',
                text: 'Los datos se han eliminado correctamente',
                title: 'Empresa Eliminada',
                showConfirmButton: true
            }).then(isConfirm => {
                location.reload();
            })

        }
    }).fail(function (error) {
        $('#btnedit').removeAttr('disabled');
        
            swal('Error','Ocurrio algo inesperado, intentalo más tarde','error');
        
    });
}

