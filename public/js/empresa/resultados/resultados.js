function showModal(url) {
    $('#embed').attr('src', url);
    $('#link').attr('href', url);
    $('#modal').modal('show');
}
cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
var loaded = false;
cornerstone.events.addEventListener('cornerstoneimageloadprogress', function(event) {
    const eventData = event.detail;
    if(eventData.percentComplete==100){
      $('#loadersDicom').hide();
      $('#dicomview').show();
    }else {
      $('#dicomview').hide();
      $('#loadersDicom').show();

    }
});
function loadAndViewImage(imageId) {
    var element = document.getElementById('dicomImage');

    try {
        var start = new Date().getTime();
        cornerstone.loadAndCacheImage(imageId).then(function(image) {
            console.log(image);
            var viewport = cornerstone.getDefaultViewportForImage(element, image);
            cornerstone.displayImage(element, image, viewport);
            if(loaded === false) {
                cornerstoneTools.mouseInput.enable(element);
                cornerstoneTools.mouseWheelInput.enable(element);
                cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
                cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
                cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
                cornerstoneTools.zoomWheel.activate(element); // zoom is the default tool for middle mouse wheel
                loaded = true;
            }
      ///INFORMACION DEL PACIENTE
        function getnamepaciente() {
          return value = image.data.string('x00100010');
        }
        function getIdpaciente() {
          return value = image.data.string('x00100020');
        }
        function nacimiento() {
          return value = image.data.string('x00100030');
        }
        function sexo() {
          return value = image.data.string('x00100040');
        }

        ///Información de estudio
          function desestudio() {
            return value = image.data.string('x00081030');
          }
          function nomprotocol() {
            return value = image.data.string('x00181030');
          }
          function numacceso() {
            return value = image.data.string('x00080050');
          }
          function idestudio() {
            return value = image.data.string('x00200010');
          }
          function fechaestudio() {
            return value = image.data.string('x00080020');
          }
          function timestudio() {
            return value = image.data.string('x00080030');
          }

          ///Información de la serie
            function descserie() {
              return value = image.data.string('x0008103e');
            }
            function numserie() {
              return value = image.data.string('x00200011');
            }
            function modalidad() {
              return value = image.data.string('x00080060');
            }
            function partecuerpo() {
              return value = image.data.string('x00180015');
            }
            function fechaestudio() {
              return value = image.data.string('x00080020');
            }
            function timserie() {
              return value = image.data.string('x00080031');
            }

              function fabricante() {
                return value = image.data.string('x00080070');
              }
              function modelo() {
                return value = image.data.string('x00081090');
              }
              function estacion() {
                return value = image.data.string('x00081010');
              }
              function tituloae() {
                return value = image.data.string('x00020016');
              }
              function istitucion() {
                return value = image.data.string('x00080080');
              }
              function software() {
                return value = image.data.string('x00181020');
              }
              function implementacion() {
                return value = image.data.string('x00020013');
              }
        }, function(err) {
            alert(err);
        });
    }
    catch(err) {
        alert(err);
    }
}
function downloadAndView(name) {
    let url = name;
    url = "wadouri:" + url;
    loadAndViewImage(url);
}
var element = document.getElementById('dicomImage');
cornerstone.enable(element);
$('#dicomview').hide();
$('#loadersDicom').hide();


$(document).ready(function() {
  sass = $('#idSass').val();
  examen = $('#idExamen').val();
  sass = sass.split('/');
  toma = sass[0];

  consecutivo = sass[1];
  data = new Object();
  data.toma = toma;
  data.consecutivo = consecutivo
  data.metodo = 'estudios';
  data.examen = examen;
  $.ajax({
    type:'post',
    data:data,
    dataType:'json',
    url:'https://asesores.ac-labs.com.mx/sass/index.php'
  }).done(res=>{
    if (res.length>0) {
      result(res);
    }else {
      swal('Estudios Pendientes','Los resultados del examen aún no son liberados','info')
    }
  }).fail(err=>{

  })
});


function result(res) {
  dataSet = [];
  res.forEach((item, i) => {
    $('.seccion').text(item.seccion);
    dataSet.push([
      item.seccion,
      item.examen,
      item.abrev,
      item.resultado_capturado,
      // item.fecha_valida,
      item.metodo,
    ]);

  });

  $('#example').DataTable({
    data:dataSet,
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
  });
}
