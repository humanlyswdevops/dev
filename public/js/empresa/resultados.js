$(".table_estudios").DataTable({
  responsive: !1,
  language: {
    "decimal": "",
    "emptyTable": "No hay información",
    "info": "",
    "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "_MENU_",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "Buscar",
    "zeroRecords": "Sin resultados encontrados",
    "paginate": {
      "first": "Primero",
      "last": "Ultimo",
      "next": "Siguiente",
      "previous": "Anterior"
    }
  }

});
 
$(document).on('click','.showEstudio', function(){
  console.log();
  $('#estudiosShow').modal('show');
  showEstudios($(this).data('id'));
});

function daicom(tomaid, id_estudio) {
  url = "./dai/" + tomaid + "/" + id_estudio;
  window.location.href = url;
}

function showEstudios(tomaId) {

  $.ajax({
    type: "get",
    url: "./getEstudiosPacienteToma/" + tomaId,
    dataType: "json",
    beforeSend: () => {
      $('.loadEstudios').show();
    }
  }).done(res => {

    $('.loadEstudios').hide();
    $('.contEstudios').empty();
    toma = res.toma;
    estudios = res.estudios;
    console.log(toma);
    console.log(estudios);
    estudios.forEach((item, i) => {
      if (item.categoria_id == 2) {
        e_clase = '<a onclick="daicom(' + toma.id + "," + item.id + ')" class="btn-sm btn-secondary btn text-white"><i class="feather icon-airplay"></i></a>';
      }
      else {
        e_clase = '<a type="button" target="_blank" href="./Resultados/' + toma.id + '/' + item.id + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-external-link"></i></a>';
      }
      $('.contEstudios').append(`
          <div class="col-md-4 mb-1">
              <div class="text-center shadow bg-white p-1  rounded">
                  <h5 class='secondary'>${item.nombre}</h5>
                 
                  <hr>
                  <small class="d-block">${toma.created_at}</small>
                  <small class="d-block">${item.seccion == null ? '' : item.seccion}</small>             
                
                  <hr>
                  ${e_clase}

              </div>
          </div>
      `)

    });
  }).fail(err => {
    $('.loadEstudios').hide();
    console.log(err);
    swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
  })
}

/*$("#buscar_estudios").submit(function( e) {
  e.preventDefault();
 
  var formData = new FormData($(this)[0]);
 alert(fecha_inicial,fecha_inicial);
  $.ajax({
    type: 'POST',
    url:"../Resultados1/"+fecha_inicial+"/"+fecha_final,
    data: {fecha_inicial:fecha_inicial,
    fecha_final:fecha_final},
    contentType: 'multipart/form-data',
    cache: false,
    contentType: false,
    processData: false,
    beforeSend:()=>{
      alert("BeforeSend");
  }
}).done(res=>{
alert(fecha_inicial);
alert(fecha_final);
}).fail(err=>{
  console.log(formData);
})
  
});*/

$(document).on('click', '.buscar_fecha', function () {
  fecha_inicial = $("input[name=fecha_inicial]").val();
  fecha_final = $("input[name=fecha_final]").val();
  buscarestudio(fecha_inicial, fecha_final);

});
function buscarestudio(fecha_inicial, fecha_final) {
  // $('.table_estudios').dataTable().fnClearTable();

  //$('.table_estudios').DataTable().fnClearTable();
  //$(".table_estudios").dataTable().fnDestroy();
  //$('.table_estudios').dataTable().fnClearTable();
  $('#tabla_dinamica').empty();
  $.ajax({
    type: "get",
    url: "./Resultados1/" + fecha_inicial + "/" + fecha_final,
    dataType: "json",
    beforeSend: () => {
      $(".cargar").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
          <span class="sr-only">Loading...</span>
          </div>`);
    }
  }).done(res => {
    $(".cargar").empty();
    $('#tabla_dinamica').append('<table class="table_estudios1 table" style="width:100%"><thead><tr>' +
      '<th>Empleado</th><th>Toma</th><th>Fecha Inicial</th> <th>Fecha Final</th>  <th>Acciones</th></tr></thead>' +
      ' <tbody></tbody></table>');

    res.resultados.forEach((item, i) => {

      fecha = item.fecha_inicial + "T00:00:00";

      fechaf = item.fecha_final + "T00:00:00";
      if (item.folio == null) {
        folio_sass = ' ';
      }
      else {
        folio_sass = item.folio;
      }

      const monthNames = ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
        "Jul", "Ago", "Sep", "Oct", "Nov", "Dec"];

      const f = new Date(fecha);
      const fm = monthNames[f.getMonth()];
      fd = f.getDate();
      const fy = f.getFullYear();
      const ff = new Date(fechaf);
      const fmf = monthNames[ff.getMonth()];
      const fdf = ff.getDate();


      const fyf = ff.getFullYear();


      $('tbody').append(`<tr><td>${item.nombre} ${item.apellido_paterno} ${item.apellido_materno}</td>
                                         <td>${folio_sass}</td>
                                         <td>${fm} / ${fd} / ${fy}</td>
                                         <td>${fmf} / ${fdf} / ${fyf}</td>
                                         <td><a data-id="${item.id}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>    
                                         </td>     
                                     </tr>`)
      console.log(" <td>" + item.nombre + "</td> ")
    });


    nueva_tabla();


  }).fail(err => {

    console.log(err);
    console.log("./Resultados1/" + fecha_inicial + "/" + fecha_final);

  })
}

function nueva_tabla() {
  $(".table_estudios1").DataTable({
    responsive: !1,
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }

  });


  function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }
}