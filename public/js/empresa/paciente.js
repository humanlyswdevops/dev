
"use strict";
var validacion = $.trim($("#validacion").data('validacion'));
var botones = [];
console.log(validacion);
if(validacion == "true")
{
    botones = [
        {
            text: "<i class='feather icon-plus'></i> Nuevo Paciente",
            action: function() {
                $(this).removeClass("btn-secondary"), $(".add-new-data").addClass("show"), $(".overlay-bg").addClass("show"), $("#data-name, #data-price").val(""), $("#data-category, #data-status").prop("selectedIndex", 0)
            },
            className: "btn-primary btn-sm"
        },
        {
            text: "Carga Masiva",
            action: function() {
                window.location.href ='AltaEmpleadoGrps';
            },
            className: "btn-outline-secondary text-white bg-secondary btn-sm"
        }
    ];
}

$(".data-list-view").DataTable({
    responsive: !1,
    dom: '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    },

    order: [
        [1, "asc"]
    ],
    buttons:botones,
    initComplete: function(t, e) {
        $(".dt-buttons .btn").removeClass("btn-secondary")
    }
}), 0 < $(".data-items").length && new PerfectScrollbar(".data-items", {
    wheelPropagation: !1
}), $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
    $(".add-new-data").removeClass("show"), $(".overlay-bg").removeClass("show"), $("#data-name, #data-price").val(""), $("#data-category, #data-status").prop("selectedIndex", 0)
})

$('#alta').on('submit',function(e) {
    e.preventDefault();
    $("#btn_enviar").prop("disabled", true);

    $.ajax({
        type:'post',
        dataType:'json',
        url:'altaEmpleados',
        data:$('#alta').serialize()
    }).done( function(response){
      $("#btn_enviar").prop("disabled", false);
      swal({
        position:'top-end',
        icon:'success',
        text:'Los datos fueron almacenados de manera correcta',
        title:'Paciente Registrado',
        showConfirmButton:true
      }).then(isConfirm=>{
       location.reload();
      })
    }).fail(function(error) {
        var message;
        if(error.status == 422){
            message = "Algo ha ocurrido, verifica que todos los campos sean correctos, y que haya seleccionado un grupo";
            swal('Error',message,'error');
        }
        else if (error.status == 400){
            message = "La curp ingresada ya se encuentra registrado, intenta con otra";
            swal('Error',message,'error')
        }
        else if(error.status == 424){
            message = "El correo ingresado ya se encuentra registrado, intenta con otra";
            swal('Error',message,'error')
        }
        else if(error.status == 423){
            message = "La clave ingresada ya se encuentra registrada, intenta con otra";
            swal('Error',message,'error')
        }
        else if(error.status == 418){
            message = "El grupo que ya existe. Por favor selecciónelo de la lista de grupos o elija otro nombre";
            swal('Error',message,'error')
        }
        else{
            message = "Algo ocurrió, inténtalo más tarde...";
            swal('Error',message,'error')
        }
        $("#btn_enviar").prop("disabled", false);
    });

});


$("").on("click", function(t) {
   t.stopPropagation(), $(this).closest("td").parent("tr").fadeOut()
})

$(document).on('click', '.action-delete',function(){
  const id = $(this).data('id');
  swal({
      title: "Eliminación",
      text: "¿Estas seguro de eliminar los datos del paciente?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  }).then(isConfirm => {
      if (isConfirm) {
          eliminacion(id);
      } else {
          swal("Cancelado", "La eliminación fue cancelada", "error");
      }
  });
})


/**
 * Eliminacion de un paciente
 * @param  {[type]} e [preventDefault]
 * @return {[type]}   []
 */

function eliminacion(id){
  $.ajax({
      data: {id:id}, //datos que se envian a traves de ajax
      url: 'eliminarEmpleado', //archivo que recibe la peticion
      type: 'post', //método de envio
      success: function(response) {

      }
  }).done(res=>{
        swal({
            position:'top-end',
            icon:'success',
            title:'Eliminación Correcta',
            text:'El paciente se elimino de manera correcta',
            showConfirmButton:true
        }).then(isConfirm=>{
            location.reload();
        })

  }).fail(err=>{

    swal({
        position:'top-end',
        icon:'success',
        title:'Upss...',
        text:'Hubo un problema, intentalo más tarde',
        showConfirmButton:true
    }).then(isConfirm=>{
        location.reload();
    })

  })
}

