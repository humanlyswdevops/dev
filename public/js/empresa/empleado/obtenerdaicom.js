var arr = [];
var datos = [];
$(document).ready(function () {
    var toma1 = $("input[name=toma]").val();
    var estudios1 = $("input[name=estudio]").val();
    var toma = JSON.parse(toma1);
    var estudios = JSON.parse(estudios1);
    console.log("ESTUDIOS:", estudios);
    let tomate = JSON.stringify(toma);
    tomaId = toma.folio;
    id_toma = toma.id;
    estudio = toma.estudios;
    nombre = estudios.nombre;
    codigo = estudios.codigo;
    console.log(toma);
    //codigo=80016;
  //codigo=80050;
  
    showDaicoms(tomaId, codigo);
});
function showDaicoms(tomaId, codigo) {
    let Idtoma = tomaId.replace("-", "/");
    console.log(Idtoma);
    $.ajax({
        type: "POST",
        url: "https://humanly-sw.com/imagenologia/Api/V1_regresainterpretacion",
        dataType: "json",
        data: {
            sass: Idtoma,
            //sass: "101210401/0023"
           
        },
        beforeSend: () => { },
    })
        .done((res) => {
            das = JSON.stringify(res);
            if (das == "[]" || tomaId == null) {
                $(".contImagenologia").append(`
                    <div class='col-md-12'>
                        <h5 class="mb-0">
                            <div class="alert alert-primary alert_32">
                                <p class="text-center">
                                    ¡No existen tomas!
                                </p>
                             </div>
        
                         </h5>
                    </div>
                `);
            }
            else {
                $(".form-check").hide();
                let i = 0;
                datos = JSON.stringify(res);
                arr1 = datos;
                j = 0;
                for (let item of res) {
                    arr = item.dicoms;
                    if (item.id_estudio == codigo) {
                        if (arr.length == 0) {
                            $("#estudios").append(` 
                                <div class="form-check form-check-inline">
                                     <div class="custom-control custom-radio">
                                         <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" value="" onclick='mostrar("${codigo}",this.value)'>
                                        <label class="custom-control-label secondary" for="customRadio1">Toma 1</label>
                                     </div> 
                                </div>                                 
                            `);
                        } else {
                            
                            while (j <= arr.length - 1) {
                                var ruta1 =
                                    "https://imagenologia.dynpbx.mx/Imagenologia2/" +
                                    arr[j];

                                var ruta = ruta1.replace("\\", "/");
                                
                                $("#estudios").append(` 
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio${j}" name="customRadio" class="custom-control-input" value="${ruta}" onclick='mostrar("${codigo}",this.value)'>
                                            <label class="custom-control-label secondary" for="customRadio${j}">Toma ${j}</label>
                                        </div> 
                                    </div>                                 
                                `);

                                j++;
                            }
                        }
                    }
                    i++;
                }
            }
        })
        .fail((err) => {
            $(".loadImagenologia").hide();
            console.log("Error");
            console.log(err);
        });
}

function mostrar(codigo, valor) {
    let sindaicom;
    let interurl;
    if (valor == null || valor == "") {
        sindaicom =
            "<div class='col-md-12'><h5 class=mb-0><div class='alert alert-primary alert_32'><p class='text-center'>¡No existen tomas!</p></div></h5></div>";
    } else {
        sindaicom = "";
    }
    console.log("Example:" + sindaicom);
    k = 0;
    var d = JSON.parse(datos);
    
    var inter;
    var encointer;
    var clase;
    var d = JSON.parse(datos);
    console.log(d);
    for (let item of d) {
        interurl=item.interpretacion;
        if (item.id_estudio == codigo) {
            inter = item.interpretacion;
            interpreta = inter.length;
            if (interpreta < 200) {
                clase =
                    '<embed src="http://humanly-sw.com/imagenologia/uploads/interpretacionpdf/' +
                    atob(item.interpretacion) +
                    '" type="application/pdf" width="100%;" height="702px">';
                console.log(clase);
            } else {
                encointer = atob(item.interpretacion);
                clase = decode_utf8(encointer);
                
                clase="<img src='http://has-humanly.com/Paginas/appprep-3/images/cahitoredondo.svg' class='membretada'>"+ clase+"<img src='http://has-humanly.com/Paginas/appprep-3/images/logo_30.svg' class='imgleft'>"+
                '<a href="https://humanly-sw.com/imagenologia/Pdf/GenerarPdf_Interpretacion/'+item.id_pdf+'" class="btn task-cat primario ml-2 mt-1 ">Descargar <img src="https://humanly-sw.com/imagenologiadev/uploads/assets/cloud_download.svg" alt="icono de nube"></a>';      

            }
            console.log(inter.length);
            $(".contImagenologia").empty();
            
            $(".contImagenologia").append(`
            <div class='col-md-12'>
                 <div class="card">
                         <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                
                            </h5>
                         </div>

                 <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class='row'>
                      <div class='col-md-6'>
                        <div class="card-body">
                         
                         <div class="row" id="dicomview">                        
                            <div class="col-md-12">
                            ${sindaicom}
                                <div style="width:100%;height:700px;position:relative;color: white;display:inline-block;border-style: dashed; border-width: 1px; border-color:gray;"
                                    oncontextmenu="return false" class='disable-selection noIbar' unselectable='on' onselectstart='return false;'
                                        onmousedown='return false;'>
                                        <div id="dicomImage" style="width:100% ;height:700px;top:0px;left:0px; position:absolute"></div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
              
                     <div class='col-md-6'>
                        <div class="card-content">              
                        ${clase}   
                                   
                        </div>  
                                     
                     </div>
                     
                 </div>
                
              </div>`);
              

        }
        k++;
    }
    downloadAndView("dicomImage", valor);
    function decode_utf8(s) {
        return decodeURIComponent(escape(s));
    }
}

 /* function imprSelec(iurl){ 
  
    url = "../PDF/imagenologia/interpretacion/"+iurl;
    window.open( url, 'Print', 'left=200, top=150,bottom=100, width=950, height=500, toolbar=0, resizable=0,status=0');
};*/
