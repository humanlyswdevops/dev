/*$('.estudiosAll').select2({
    ajax: {
      url: "../estudios_get_name",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          term:params.term
         };

      },
      processResults: function (data) {
          return {
              results: $.map(data, function (item) {

                  return {
                      text:  item.nombre ,
                      id: item.id,
                      name:item.nombre
                  }
              })
          };
      }
    },
    minimumInputLength: 2,
    language: "es",
    placeholder: "Estudios - Buscar nombre o codigo",
    searching:"Buscando",
    language: {
       noResults: function() {
         return "No hay resultados";
       },
       searching: function() {
        return "Buscando..";
       },
       inputTooShort: function () {
       return "Por favor ingresa 2 o mas letras"
       }
    },
});
function dai(id){
    $.ajax({
        url: 'permisos/'+id, //archivo que recibe la peticion
        type: 'get', //método de envio
        dataType:'json'
    }).done(res=>{
      permisosTable(res);
    }).fail(err=>{
      swal('Error','Hubo algún problema, intentalo más tarde','error');
    })
  }

$(document).on('click','.showEstudio', function(){
    $('#estudiosShow').modal('show');
    showEstudios($(this).data('id'));
});

function daicom(id_estudio) {
url="../dai/"+id_estudio;
window.location.href=url;
     

}

function showEstudios(tomaId) {
    $.ajax({
        type: "get",
        url: "../ShowEstudio/"+tomaId,
        dataType: "json",
        beforeSend:()=>{
            $('.loadEstudios').show();
        }
      
    }).done(res=>{
      
      $('.loadEstudios').hide();
      $('.contEstudios').empty();
      res.forEach((item, i) => {
        if(item.seccion=="IMAGENOLOGIA")
            {
                e_clase='<a onclick="daicom('+tomaId+')" class="btn-sm btn-secondary btn text-white"><i class="feather icon-airplay"></i></a>';
            }
            else{
                e_clase='<a type="button" target="_blank" href="../Resultados/'+item.id+'" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-external-link"></i></a>';
            }
        $('.contEstudios').append(`
          <div class="col-md-4 mb-1">
            <div class="text-center shadow bg-white p-1  rounded">
              <h5>${item.examen}</h5>
              <hr>
              <small class="d-block">${item.created_at}</small>
              <small class="d-block">${item.seccion}</small>
              <hr>
              ${e_clase}
            </div>
          </div>
        `)
      });
    }).fail(err=>{
      console.log("../ShowEstudio/"+tomaId);
        $('.loadEstudios').hide();
        console.log(err);
        swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
    })
  }

var options = {
	valueNames: ['toma','fecha'],
	page: 4,
	pagination: true,
	pagination: {
        innerWindow: 1,
        left: 1,
        right: 1,
        paginationClass: "pagination",
        item:"<li class='page-item'><a class='page-link page' ></a></li>",
    }
  };

var consultas = new List('tomas', options);*/


$('.estudiosAll').select2({
  ajax: {
    url: "../estudios_get_name",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        term:params.term
       };

    },
    processResults: function (data) {
        return {
            results: $.map(data, function (item) {

                return {
                    text:  item.nombre ,
                    id: item.id,
                    name:item.nombre
                }
            })
        };
    }
  },
  minimumInputLength: 2,
  language: "es",
  placeholder: "Estudios - Buscar nombre o codigo",
  searching:"Buscando",
  language: {
     noResults: function() {
       return "No hay resultados";
     },
     searching: function() {
      return "Buscando..";
     },
     inputTooShort: function () {
     return "Por favor ingresa 2 o mas letras"
     }
  },
});
function dai(id){
  $.ajax({
      url: 'permisos/'+id, //archivo que recibe la peticion
      type: 'get', //método de envio
      dataType:'json'
  }).done(res=>{
    permisosTable(res);
  }).fail(err=>{
    swal('Error','Hubo algún problema, intentalo más tarde','error');
  })
}

$(document).on('click','.showEstudio', function(){
  $('#estudiosShow').modal('show');
  showEstudios($(this).data('id'));
});

function daicom(tomaid,id_estudio) {
  
url="../dai/"+tomaid+"/"+id_estudio;
window.location.href=url;
   

}
function showEstudios(tomaId) {

  $.ajax({
      type: "get",
      url: "../getEstudiosPacienteToma/"+tomaId,
      dataType: "json",
      beforeSend:()=>{
          $('.loadEstudios').show();
      }
  }).done(res=>{
      
      $('.loadEstudios').hide();
      $('.contEstudios').empty();
      toma = res.toma;
      estudios =  res.estudios;
      console.log(toma);
      console.log(estudios);
      estudios.forEach((item, i) => {
          if(item.categoria_id==2)
          {
            e_clase='<a onclick="daicom('+toma.id+","+item.id+')" class="btn-sm btn-secondary btn text-white"><i class="feather icon-airplay"></i></a>';
          }
          else{
            e_clase='<a type="button" target="_blank" href="../Resultados/'+toma.id+'/'+item.id+'" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-external-link"></i></a>';
          }
      $('.contEstudios').append(`
          <div class="col-md-4 mb-1">
              <div class="text-center shadow bg-white p-1  rounded">
                  <h5 class='secondary'>${item.nombre}</h5>
                 
                  <hr>
                  <small class="d-block">${toma.created_at}</small>
                  <small class="d-block">${item.seccion == null ? '' : item.seccion}</small>             
                
                  <hr>
                  ${e_clase}

              </div>
          </div>
      `)

      });
  }).fail(err=>{
      $('.loadEstudios').hide();
      console.log(err);
      swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
  })
}





var options = {
valueNames: ['toma','fecha'],
page: 4,
pagination: true,
pagination: {
      innerWindow: 1,
      left: 1,
      right: 1,
      paginationClass: "pagination",
      item:"<li class='page-item'><a class='page-link page' ></a></li>",
  }
};

var consultas = new List('tomas', options);
