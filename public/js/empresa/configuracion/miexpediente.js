var contador_new_files = 0;
$("#btnAddFile").click(function(){
    let tipo_archivo = $("#tipo_archivo");
    let nombre_archivo = $("#nombre_archivo");
    let validador = true;
    if(tipo_archivo.val() <= 0)
    {
        $("#error_tipo_archivo").addClass('d-block').html('Debes de seleccionar un tipo de archivo');
        validador = false;
    }

    if($.trim(nombre_archivo.val()).length <= 0)
    {
        $("#error_nombre_archivo").addClass('d-block').html('Debes de ingresar un nombre a tu documento');
        validador = false;
    }

    if(!validador)
    {
        return false;
    }
    
    contador_new_files++;
    $("#containerNewFiles").append('<fieldset class="form-group" id="nuevo_archivo_'+contador_new_files+'">'+
        '<label for="new_file_'+contador_new_files+'">Documento: '+$("#tipo_archivo option:selected").text()+'/'+nombre_archivo.val()+'</label>'+
        '<div class="custom-file">'+
            '<input type="file" class="custom-file-input" required id="new_file_'+contador_new_files+'" name="new_file_'+contador_new_files+'">'+
            '<label class="custom-file-label" for="new_file_'+contador_new_files+'">Elija el archivo</label>'+
        '</div>'+
        '<input type="hidden" name="nuevos[]" value="'+contador_new_files+'">'+
        '<input type="hidden" name="new_file_datos_'+contador_new_files+'['+"tipo"+']" value="'+tipo_archivo.val()+'">'+
        '<input type="hidden" name="new_file_datos_'+contador_new_files+'['+"nombre"+']" value="'+nombre_archivo.val()+'">'+
        '<div class="alert alert-primary mb-2" role="alert">'+
            '<div class="clearfix">'+
                '<button data-id="'+contador_new_files+'" class="delete_archivo_nuevo float-right btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light" type="button">'+
                    '<i class="feather icon-trash"></i>'+
                '</button>'+
            '</div>'+
        '</div>'+
    '</fieldset>');
    tipo_archivo.val('-1');
    nombre_archivo.val('');
    $(".custom-file input").change(function(e){
        $(this).next(".custom-file-label").html(e.target.files[0].name)
    });
    delete_new_file();
});

$(".delete_archivo_custom").click(function(){
    let id = $(this).data('id');
    let nombre = $(this).data('nombre');
    $("#input_delete_archivo_medico_id").val(id);
    $("#form_delete_archivo_medico_texto").text(nombre);
    $("#confirmEliminacionArchivoModal").modal();
});

$("#btn_delete_archivo_medico_modal").click(function(e){
    e.preventDefault();
    let data = $("#form_delete_archivo_medico").serialize();
    $.ajax({
        url:'delete_archivo_medico', // place your controller's method
        type:'post',
        data:data,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $("#form_delete_archivo_medico").find('.spinner').show();
        }
    }).done(res=>{
        $("#div_archivo_custom_"+res).remove();
        $("#form_delete_archivo_medico").find('.spinner').hide();
        $("#confirmEliminacionArchivoModal").modal('hide');
        swal('Exito','Documento Eliminado Exitosamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
})

$("#form_update_expediente").submit(function(e){
    $("#btnAddFile").attr('disabled',true);
    $("#btnUpdateExpediente").attr('disabled',true);
    $("#btnUpdateExpediente").find('.spinner').show();
});

function delete_new_file() {
    $(".delete_archivo_nuevo").click(function() {
        let id = $(this).data('id');
        $("#nuevo_archivo_"+id).remove();
    })
}


$(".edit_archivo_custom").click(function(e){
    e.preventDefault();
    let id = $(this).data('id');
    let tipo = $(this).data('tipo');
    let nombre = $(this).data('nombre');
    console.log(id,tipo,nombre);
    $("#edit_id_file").val(id);
    $("#edit_name_file").val(nombre);
    $("#edit_type_file").val(tipo);
    $("#editArchivoCustomModal").modal();
});

$("#form_edit_archivo").submit(function(e){
    e.preventDefault();
    let form = $("#form_edit_archivo").serialize();
    console.log(form);
    $.ajax({
        url:'edit_expediente_medico', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $("#btn_edit_archivoCustom_modal").attr('disabled',true);
            $("#btn_edit_archivoCustom_modal").find('.spinner').show();
        }
    }).done(res=>{
        console.log(res);
        $("#div_archivo_custom_"+res.id).find('label.nombre_doc').text('Actualiza tu documento: '+res.tipo+"/"+res.nombre);
        $("#editArchivoCustomModal").modal('hide');
        $("#btn_edit_archivoCustom_modal").attr('disabled',false);
        $("#btn_edit_archivoCustom_modal").find('.spinner').hide();
        swal('Exito','El documento se ha actualizado correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
});