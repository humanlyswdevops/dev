$("#btnModalPassword").click(function(){
    $("#passwordModal").modal();
});

$("#form_update_password").submit(function(e){
    e.preventDefault();
    console.log('Entro');
    let password = $("#new_password");
    let password_confirm = $("#new_password_confirm");
    if(password.val() === password_confirm.val())
    {
        password.removeClass('is-invalid');
        password_confirm.removeClass('is-invalid');
        $("#error_password").removeClass('d-block').addClass('d-none');
        $("#error_password").text('');
        enviarDatos();
        password.val('');
        password_confirm.val('');
    }else{
        password.addClass('is-invalid');
        password_confirm.addClass('is-invalid');
        $("#error_password").text('Las contraseñas no coinciden');
        $("#error_password").removeClass('d-none').addClass('d-block');
    }
});

function enviarDatos() {
    let form = $("#form_update_password").serialize();
    console.log(form);
    $.ajax({
        url:'update_password', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
        }
    }).done(res=>{
        $("#passwordModal").modal('hide');
        swal('Exito','La contraseña se actualizo correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
}

$("#form_update_misDatos").submit(function(e){
    e.preventDefault();
    let form = $(this).serialize();
    $.ajax({
        url:'update_perfil', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
        }
    }).done(res=>{
        if(res == "email"){
            swal('Error','El correo electrónico ya esta registrado, intente con otro','error');
        }else{
            $(".user-status").text($("#puesto_user").val());
            $(".user-name").text($("#nombre_user").val());
            swal('Exito','Perfil actualizado correctamente','success');
        }
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
});