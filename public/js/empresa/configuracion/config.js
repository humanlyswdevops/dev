$('.spinner').hide();

$('#config').on('submit', function(event) {
  event.preventDefault();
  $.ajax({
      type:'post',
      dataType:'json',
      data:$('#config').serialize(),
      url:'save_configuracion',
      beforeSend: function(){
        $('.spinner').show();
          $('.submit').attr('disabled',true);
      },
  }).done(function(res){
    $('.spinner').hide();
    $('.submit').attr('disabled',false);
    swal('Exito','Cambios Guardados','success');
  }).fail(function(error){
    $('.spinner').hide();
    $('.submit').attr('disabled',false);
    swal('Error','Hubo algún problema intentalo más tarde','error');
  });
})

var hash_url = window.location.hash;
console.log(hash_url);
if(hash_url == "#datos"){
    $("#datos-tab").addClass("active");
    $("#datos").addClass("active");
}else if(hash_url == "#misdatos"){
    $("#misdatos-tab").addClass("active");
    $("#misdatos").addClass("active");
}else if(hash_url == "#miexpediente"){
    $("#miexpediente-tab").addClass("active");
    $("#miexpediente").addClass("active");
}else if(hash_url == "#historialClinico"){
    $("#historialClinico-tab").addClass("active");
    $("#historialClinico").addClass("active");
}else{
    $("#account-tab").addClass("active");
    $("#account").addClass("active");
}

$("#especilidad_select").select2({
  tags:true,
  tokenSeparators: [","],
  placeholder: 'Selecciona una especialidad'
});