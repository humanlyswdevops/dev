if (window.FileList && window.File && window.FileReader) {
    validador_imagen = false;
    document.getElementById('input_imagen_logo_empresa').addEventListener('change', event => {
    
    $(".span-file").show();
    $('#img_logo_empresa').attr('src','--');
    $("#status_img_logo_empresa").text('');
    $("#status_img_logo_empresa").hide();
    const file = event.target.files[0];
    if (!file.type) {
        console.log(file);
        $("#status_img_logo_empresa").text('Error: el archivo '+file.name+' no es compatible con este navegador, intente con un archivo con extensión JPG o PNG.');
        $("#status_img_logo_empresa").show();
        texto_imagen = '.';
        return;
    }
    if (!file.type.match('image.*')) {
        $("#status_img_logo_empresa").text('Error: el archivo seleccionado no parece ser una imagen, intente con un archivo con extensión JPG o PNG.');
        $("#status_img_logo_empresa").show();
        texto_imagen = '.';
        return;
    }
    const reader = new FileReader();
    reader.addEventListener('load', event => {
        $(".span-file").hide();
        $('#img_logo_empresa').attr('src',event.target.result);
        validador_imagen = true;
        texto_imagen = '';
        console.log('Entro');
        console.log(event.target.result);
    });
    reader.readAsDataURL(file);
    });
}

$("#btn-imagen-destacada").click(function(){
    $("#input_imagen_logo_empresa").click();
});

$("#form_datos").on("submit",function(e){
    e.preventDefault();
    var formData = new FormData();
    var input_file = document.getElementById("input_imagen_logo_empresa");
    if(input_file.files.length > 0)
    {
        formData.append('logo',input_file.files[0] );
    }
    formData.append("nombre",  $("#form_datos input[name='nombre_empresa']").val());
    formData.append("direccion", $("#form_datos input[name='direccion_empresa']").val());
    formData.append("telefono", $("#form_datos input[name='telefono']").val());
    formData.append("pagina", $("#form_datos input[name='pagina']").val());
    formData.append("giro", $("#giro").val());
    formData.append("check_aceptado", $("#terminos_condiciones").val());
    
   
    $.ajax({
        url:'update_empresa', // place your controller's method
        type:'post',
        data:formData,
        dataType:'json', // text, json etc
        contentType:false,
        processData:false,
        beforeSend:()=>{
            $('.spinner').show();
        }
    }).done(res=>{
        $('.spinner').hide();
        $("#logo_principal_layaout").attr('src',$("#img_logo_empresa").attr('src'));
        swal('Exito','Cambios Guardados','success');
        $("#logo_receta_medica").attr('src',$("#img_logo_empresa").attr('src'));
        $("#nombre_receta_medica").text(res.nombre);
    }).fail(err=>{
        $('.spinner').hide();
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
});