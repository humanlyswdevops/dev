// // const { data } = require("jquery");


// $(".search").keyup( function() {
// 		var contador = 0;
// 		var rex = new RegExp($(this).val(), 'i');
// 		$(".cards_item").hide();
// 		$(".cards_item").filter( function() {
// 				if (rex.test($(this).text()))
// 				contador ++;
// 				return rex.test($(this).text());
// 		}).show();
// 		if (contador > 0) {
// 				$(".aviso-vacio").hide();
// 		} else {
// 				$(".aviso-vacio").show();
// 		}
// });


$(document).on('change', '.inputImage', function(){
	if ($(this).val()) {
		var formData = new FormData(document.getElementById("imageChange"));
		$.ajax({
				url:'../imgeChange', // place your controller's method
				type:'post',
				data:formData,
				dataType:'json', // text, json etc
				contentType:false,
				processData:false,
				beforeSend:()=>{
					$('.imgLoad').show();
					$('.editIcon').hide();
				}
		}).done(res=>{
			$('.perfilImge').attr('src','../storage/app/pacientes/'+res.img);
			$('.imgLoad').hide();
			$('.editIcon').show();
		}).fail(err=>{
            $('.imgLoad').show();
			$('.editIcon').hide();
		});
	}
});

$(document).on('change', '#documentosPaciente', function(){
	if ($(this).val()) {
		var formData = new FormData(document.getElementById("docuementPatient"));

		$.ajax({
				url:'../docPatient', // place your controller's method
				type:'post',
				data:formData,
				dataType:'json', // text, json etc
				contentType:false,
				processData:false,
				beforeSend:()=>{
					$('.spinnerDoc').show();
					$('.addDoc').hide();
				}
		}).done(res=>{
			$('.spinnerDoc').hide();
			$('.addDoc').show();
			$('#docuementPatient')[0].reset();
			res = res.archivo;
			let validacion = $.trim($("#validador_documentosGenerales").data('validacion'));
			let button = '';
			if(validacion == "true")
			{
				button = '<button type="button" data-key="'+res.id+'" class="btn deleteD btn-danger btn-icon ml-auto waves-effect waves-light text-white">'+
						'<i class="feather icon-trash"></i>'+
				'</button>';
			}
			$('.content_document').append(`
				<div  id="document_${res.id}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
					<a class="mr-50" target="_blank" href="../storage/app/documentos/${res.storage}">
						<img src="https://img.icons8.com/color/48/000000/download-from-cloud.png"/>
					</a>
						<div class="user-page-info">
							<h6 class="mb-0">
								${res.nombre}
							</h6>
							<small class="block text-muted">Hace un momento</small>
						</div>
						${button}
				</div>
			`);
		}).fail(err=>{

		});
	}
});


$(document).on('click','.deleteD', function(e){
	swal({
	title: "Atención",
	text: "¿Seguro que deseas eliminar el archivo?",
	icon: "warning",
	buttons: {
		cancel: {
				text: "No",
				value: null,
				visible: true,
				className: "",
				closeModal: false,
		},
		confirm: {
				text: "Si",
				value: true,
				visible: true,
				className: "",
				closeModal: false
		}
	}
	})
	.then((isConfirm) => {
	if (isConfirm) {
		deleteD($(this).data('key'));
	} else {
		swal("Cancelado", "La operación se cancelo correctamente", "error");
	}
	});
})



function deleteD(key) {
	$.ajax({
		url: "../deleteDoc",
		type: "post",
		dataType: "json",
		data: {id:key},
	}).done(function(res){
		console.log($(this).parent());
		$('#document_'+res.id).remove();
		swal("Eliminado", "El archivo se elimino correctamente", "success");
	}).fail(err=>{
		swal("Error", "Error al eliminar la imagen, intentalo nuevamente.", "error");
	});
}


function showProgramarModal(curp) {
	$('#programarModal').modal('show');
}

var toolbarOptions = [
		['image'],
		//['Arial','bold', 'italic', 'underline', 'strike'],
		// ['blockquote', 'code-block'],
		// [{
		//   'header': 1
		// }, {
		//   'header': 2
		// }],
		[{
			'list': 'ordered'
		}, {
			'list': 'bullet'
		}],
		// [{
		//   'script': 'sub'
		// }, {
		//   'script': 'super'
		// }],
		[{
			'indent': '-1'
		}, {
			'indent': '+1'
		}],
		[{
			'direction': 'rtl'
		}],
		// [{
		//   'size': ['small', false, 'large', 'huge']
		// }],
		[{
			'header': [1, 2, 3, 4, 5, 6, false]
		}],
		[{
			'color': []
		}, {
			'background': []
		}],
		[{
			'font': []
		}],
		[{
			'align': []
		}],
		//['clean']
];


$(document).ready(function() {
	$('.spinner').hide();
	$("#btnAddNewNota").show();
	getNota();
});


function saveNota(text,id){
	data = new Object();
	data.curp = $('#curp').val();
	data.nota_id = id;
	data.text = text;
	$.ajax({
		type:'post',
		dataType:'json',
		data:data,
		url:'../updateNotaExpediente',
		beforeSend:()=>{
			$('.spinner').show();
			$("#btnAddNewNota").hide();
		}
	}).done(res=>{
		$('.spinner').hide();
		$("#btnAddNewNota").show();
	}).fail(err=>{
		$('.spinner').hide();
	})
}


function getNota(){
	curp = $('#curp').val();
	$.ajax({
		type:'get',
		dataType:'json',
		url:'../getNotaExpediente/'+curp
	}).done(res=>{
		if (res.length > 0) {
			res.forEach(element => {
				iniciarEditor(element.id);
				$('#editor'+element.id).children('.ql-editor').html(element.contenido);
			});
		}
	}).fail(err=>{

	})
}

function addNewNota(expediente_id) {
	let data = {
		"expediente_id":expediente_id
	}
	$.ajax({
		type:'post',
		dataType:'json',
		data: data,
		url:'../addNewNota'
	}).done(res=>{
		let sin_notas = $("#div_notas").find('.row');
		if(sin_notas.length > 0)
		{
			sin_notas.remove();
		}
		let element_html = '<div id="div_nota'+res.id+'">'+
			'<div class="btn-group w-100">'+
				'<input onkeyup="actualizarTitulo('+res.id+')" id="titulo_nota'+res.id+'" placeholder="Título de la nota" type="text" class="form-control" style="border-radius: 0px;" value="'+res.titulo+'">'+
				'<button onclick="deleteNota('+res.id+')" type="button" class="btn-outline-dark btn-danger">'+
					'<i class="feather float-right icon-trash-2"></i>'+
				'</button>'+
			'</div>'+
			'<div class="mb-2" id="editor'+res.id+'" data-id="'+res.id+'"></div>'+
		'</div>';

		$("#div_notas").append(element_html);
		iniciarEditor(res.id);
	}).fail(err=>{

	})
}

function iniciarEditor(id) {
	var quill = new Quill('#editor'+id, {
	modules: {
		toolbar: toolbarOptions
		},
	theme: 'snow',
	placeholder: 'Escribe tu nota...'
	});
	quill.on('text-change', function() {
		html = $('#editor'+id).children('.ql-editor').html();
		saveNota(html,id);
	});
}
function actualizarTitulo(nota_id) {
	data = {
		'titulo': $("#titulo_nota"+nota_id).val(),
		'nota_id': nota_id
	}
	$.ajax({
		type:'post',
		dataType:'json',
		data: data,
		url:'../updateTitleNota'
	}).done(res=>{
		// console.log(res);
	}).fail(err=>{

	})
}

function deleteNota(nota_id) {
	swal({
		title: "Atención",
		text: "¿Seguro que deseas eliminar la nota?",
		icon: "warning",
		buttons: {
			cancel: {
					text: "No",
					value: null,
					visible: true,
					className: "",
					closeModal: false,
			},
			confirm: {
					text: "Si",
					value: true,
					visible: true,
					className: "",
					closeModal: false
			}
		}
		})
	.then((isConfirm) => {
		console.log(isConfirm);
		if (isConfirm) {
			data = {
				'nota_id': nota_id
			}
			$.ajax({
				type:'post',
				dataType:'json',
				data: data,
				url:'../deleteNota'
			}).done(res=>{
				$("#div_nota"+nota_id).remove();
				let notas = $("#div_notas").find('div');
				if(notas.length == 0)
				{
					$("#div_notas").html('<div class="col-12 p-0" id="div_notas"><div class="row"> Sin notas </div></div>');
				}
				swal("Exito","Se elimino correctamente la nota","success");
			}).fail(err=>{

			});
		} else {
			swal("Cancelado", "La operación se cancelo correctamente", "error");
		}
	});

}

$(document).on('click','.deleteM', function(){
	var key = $(this).data('key');
	$.ajax({
	  url:'../deleteMedicaActive/'+key,
	  data: key,
	  dataType:'json',
	  type:'post',
	  beforeSend:()=>{
		$('.loadMedica').show();
	  }
	}).done(res=>{
	  $('.loadMedica').hide();
	  $('#medicamento_'+res.id).remove();
	}).fail(err=>{

	})
  })
// NOTE: Modifiación del empleados

/**
 * [$ description]
 * @param  {[type]} editar_empleado [llamar modal de edicion]
 * @return {[type]}                 [modal]
 */
$('#editar_empleado').on('click', (arguments) => {
	$('#empleadoModal').modal('show');
})
/**
 * [description]
 * @param  {[type]} e [evento de formulario]
 * @return {[type]}   []
 */
$('#empleado_form').on('submit', function(e) {
e.preventDefault();
    swal({
		title: "Modifiación",
		text: "¿Estas seguro de modificar los datos del paciente?",
		icon: "warning",
		buttons: {
            cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
            },
            confirm: {
                    text: "Si",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
            }
	}
}).then(isConfirm => {
    if (isConfirm) {
            modificacion();
    } else {
            swal("Cancelado", "La modificación fue cancelada", "error");
    }
});

});

/**
 * ajax de modificacion del paciente
 * @return {[type]} [description]
 */
function modificacion(){
	$.ajax({
			type:'POST',
			url:'updateEmpleado',
			dataType:'json',
			data: $('#empleado_form').serialize(),
			beforeSend:()=>{

			},
			success: function(response){
					$('#btn_enviar').removeAttr('disabled');
					document.getElementById("campoClave").innerHTML = response.clave;
					document.getElementById("campoNombreCompleto").innerHTML = response.nombre + " " + response.apellido_paterno + " " + response.apellido_materno;
					document.getElementById("campoCurp").innerHTML = response.CURP;
					document.getElementById("campoGrupo").innerHTML = response.grupo.nombre;
					document.getElementById("campoFechaNacimiento").innerHTML = response.fecha_nacimiento;
					//document.getElementById("campoEdad").innerHTML = response[0].edad; //FALTA RECIBIR EDAD DESDE EMPLEADOS CONTROLLER
					document.getElementById("campoSexo").innerHTML = response.genero;
					document.getElementById("campoEmail").innerHTML = response.email;
					document.getElementById("campoDireccion").innerHTML = response.direccion;
					document.getElementById("campoTelefono").innerHTML = response.telefono;
					$('#empleadoModal').modal('hide');
					swal('Modificación Finalizada','Los datos fueron modificados correctamente','success');

					// dominio = document.domain;
					// if (curpOriginal != $('#curp').val()) {
					//   window.location.replace("https://"+dominio+'/Clinica/public/Empleados');
					// }
			}
	}).fail(function(error) {
			$('#btn_enviar').removeAttr('disabled');

			if(error.status == 422) {
                swal('Error','Todos los campos son obligatorios. Revise que haya ingresado correctamente la información.','info');
			} else if (error.status == 400 && error.responseJSON.message == "Error en CURP") {
                swal('Error','La CURP ingresada ya se encuentra registrada, intente con otra','info');
			} else if (error.status == 400 && error.responseJSON.message == "Error en correo") {
                swal('Error','El correo ingresado ya se encuentra registrado, intente con otro','info');

			} else if (error.status == 400 && error.responseJSON.message == "Error en clave") {
                swal('Error','La clave del empleado ya se encuentra registrada, intente con otra','info');
			} else {
                swal('Error','Ocurrio algo inesperado, intentalo más tarde','error');
			}

	});
}

// NOTE: Eliminar al paciente


$('#eliminar_empleado').on('click', function(){
	const id = $(this).data('id');
	swal({
			title: "Eliminación",
			text: "¿Estas seguro de eliminar los datos del paciente?",
			icon: "warning",
			buttons: {
                cancel: {
                        text: "No",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                },
                confirm: {
                        text: "Si",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                }
			}
	}).then(isConfirm => {
			if (isConfirm) {
					eliminacion(id);
			} else {
					swal("Cancelado", "La eliminación fue cancelada", "error");
			}
	});
})


/**
 * Eliminacion de un paciente
 * @param  {[type]} e [preventDefault]
 * @return {[type]}   []
 */

function eliminacion(id){
	$.ajax({
        data: {id:id}, //datos que se envian a traves de ajax
        url: '../eliminarEmpleado', //archivo que recibe la peticion
        type: 'post', //método de envio
        success: function(response) { //una vez que el archivo recibe el request lo procesa y lo
            window.location.href = "../Empleados";
        }
	});
}


$('#motivo_consulta').on('click', (arguments) => {
	$('#consulta').modal('show');
});


$('#consulta_cerrar').on('click', (arguments) => {
	$('#consulta').modal('hide');
})

/**
 * [Peticion para programar un estudio y dar de alta en sass]
 * @type {Peticion}
 */
$('#programacion_estudios').on('submit',(e)=>{
	e.preventDefault();
	swal({
		title: "Atención",
		text: "Seguro que deseas actualizar programar el estudio",
		icon: "warning",
		buttons: {
			cancel: {
					text: "Cancelar",
					value: null,
					visible: true,
					className: "",
					closeModal: false,
			},
			confirm: {
					text: "Confirmar",
					value: true,
					visible: true,
					className: "",
					closeModal: false
			}
		}
	})
	.then((isConfirm) => {
		if (isConfirm) {

			progrmacionEstudio($('#programacion_estudios').serialize());
		} else {
				swal("Cancelado", "la operación fue cancelada", "error");
		}
	});
})



function progrmacionEstudio(data){
	$.ajax({
		type:'post',
		dataType:'json',
		data:data,
		url:'../programarEstudiosEmpleado',
		beforeSend:()=>{
			$('#programarModal').modal('hide');
			$('#loading').modal('show');

		}
	}).done((res)=>{
		sass(res);
	}).fail(err=>{
			console.log(err);
			swal("Error", "Hubo algún problema, intentalo más tarde", "error");
	})
}

/**
 * Registrar a un nuevo paciente a sass
 * @param  {[json]} res [respuesta del paciente que recien fue registrado
 * en la base de datos]
 * @return {[json]}     [retorna el codigo id (nim) del paciente]
 */
function sass(responseJSON){
		estudios=[];

		responseJSON.detalle.forEach((item, i) => {
			code_sass =parseInt(item.codigo);
			estudios.push(code_sass);
		});

		var res = responseJSON.paciente;

		if (res.genero == 'Masculino') {
			res.genero = 'M'
		}else {
			res.genero = 'F';
		}
		var id_paciente;
		if (res.id_sass!=null) {
			 id_paciente = parseInt(res.id_sass);
		}else {
			id_paciente = null;
		}
		var json_sass = new Object();
		json_sass.patient = {
				id:id_paciente ,
				first_name:res.nombre,
				last_name:res.apellido_paterno+' '+res.apellido_materno,
				birth_date:res.fecha_nacimiento,
				email:res.email,
				phone:res.telefono,
				gender:res.genero
		};
		json_sass.company=responseJSON.empresa.cliente_id,
        json_sass.doctor=responseJSON.empresa.medico_id,
 //       json_sass.date_off_appointment = '23/12/2020 12:54:00',
		json_sass.diagnose="",
		json_sass.observations="",
		json_sass.client="ael",
        json_sass.user_id="1",
		json_sass.branch_id=responseJSON.empresa.sucursal_id,
		json_sass.studies=estudios,
		json_sass.payment={
				amount:0,
				payment_type:0,
				reference:0
		};
		console.log(json_sass);
		data = JSON.stringify(json_sass);
		$.ajax({
				async:true,
				cache:false,
				type: 'POST',
				url  : 'http://sassdev.dyndns.org/ws/',
				data : data,
				dataType: 'json'
		}).done(function(recupera){
            if (recupera.data.codigo_toma == "ERR-ERR") {
                deleteProgramacion(responseJSON.estudios.id);
            }else{
                var data = new Object();
                data.empresa_id = responseJSON.estudios.empresa_id;
                data.paciente_id = responseJSON.estudios.empleado_id;
                data.programacion = responseJSON.estudios.id;
                data.idPatientSass = recupera.data.id_paciente;
                data.nim = recupera.data.codigo_toma;
				nim=data.nim;
				nim = nim.split('-');				
  				nim1 = nim[0];			
  				nim2 = nim[1];
				if(nim2.length!=4){
					n="0"+nim2;
					data.nim = nim1+"-"+n;
					
				}
				  
                publish_orden(data);
				
            }
		}).fail((err)=>{
            deleteProgramacion(responseJSON.estudios.id);
			$('#loading').modal('hide');
		});
}

/**
 *
 * La funcion tiene que eliminar el estudi programado si es que dio un erro el api de sass
 *
 * @param {*} id
 */
function deleteProgramacion(id) {
    $.ajax({
		type:'post',
		dataType:'json',
		data:{id:id},
		url:'../sass_orden_delete',
	}).done(res=>{
        console.log(res);
        swal({
			position:'top-end',
			icon:'error',
            title:'Error',
            text:'Hubo un problema al dar de pre alta en el laboratorio, intentalo más tarde o comunicate con el area de soporte',
            showConfirmButton:true
		}).then(isConfirm=>{
			location.reload();
		})
	}).fail(err=>{
       console.log(err);
       swal({
            position:'top-end',
            icon:'error',
            title:'Error',
            text:'Hubo un problema al dar de pre alta en el laboratorio, intentalo más tarde o comunicate con el area de soporte',
            showConfirmButton:true
        }).then(isConfirm=>{
            location.reload();
        })
	})
}

/**
 * Guardar el nim que se obtiene de la orden emitida por has
 * @param  {[json]} data [form data]
 * @return {[type]}      []
 */
function publish_orden(data){

	$.ajax({
		type:'post',
		dataType:'json',
		data:data,
		url:'../sass_orden',
	}).done(res=>{
		console.log(res);
		$('#loading').modal('hide');
		swal({
			position:'top-end',
            icon:'success',
            title:'Estudio Programado',
			text:'Los estudios fueron programados de manera correcta',
			showConfirmButton:true
		}).then(isConfirm=>{
			location.reload();
		})
	}).fail(err=>{
        deleteProgramacion(data.programacion);
	})
}


// NOTE: Indicadores de signos Vitales
// NOTE: Agendar
//
//
//
function agenda(empleadoId,reason,agendaId){
	swal({
		title: "Atención",
		text: "Seguro que deseas continuar con la consulta",
		icon: "warning",
		buttons: {
			cancel: {
					text: "Cancelar",
					value: null,
					visible: true,
					className: "",
					closeModal: false,
			},
			confirm: {
					text: "Confirmar",
					value: true,
					visible: true,
					className: "",
					closeModal: false
			}
		}
	})
	.then((isConfirm) => {
		if (isConfirm) {
			data = new Object();
			data.id = empleadoId;
			data.motivo = reason;
			data.idAgenda = agendaId;
			agendaConsulta(data);
		} else {
				swal("Cancelado", "la operación fue cancelada", "error");
			}
	});
}

function agendaConsulta(data){
	console.log(data);
	$.ajax({
		type:'post',
		dataType:'json',
		data:data,
		url:'../consultasAgenda'
	}).done(res=>{
			console.log(res);
		location.href="../consultas/"+res.encrypt;

	}).fail(err=>{
		swal("Error", "Intentalo más tarde", "error");
	})
}


// NOTE: estudios select2
//
$('.estudiosSelect').select2();


// Note Area de consultas


var options = {
	valueNames: [ 'name', 'fecha_consulta','medicamentos_consulta','estado_consulta','diagnostico_consulta'],
	page: 3,
	pagination: true,
	pagination: {
        innerWindow: 1,
        left: 1,
        right: 1,
        paginationClass: "pagination",
        item:"<li class='page-item'><a class='page-link page' ></a></li>",
    }
  };

var consultas = new List('consultas-list', options);
