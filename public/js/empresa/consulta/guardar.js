'use strict'

$('#save_consulta').on('click', function(){

  swal({
      title: "Atención",
      text: "¿Seguro que deseas finalizar la consulta?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  })
  .then((isConfirm) => {
      if (isConfirm) {
          consultaDiagnostico();
          examenSave();
          padecimientos();
          identificacion();
          consultaProcedimiento();
          consultaMedicamento();
          consultaTratamiento();
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
  });

});


function identificacion(){
  var id = $('.historial_id').val();
  $.ajax({
    type:'post',
    dataType:'json',
    url:'../finalizarConsulta/'+id
  }).done(res=>{
    swal({
      position:'top-end',
      icon:'success',
  //    text:'El recorrido fue asigando correctamente',
      title:'Consulta terminada',
      showConfirmButton:true
    }).then(isConfirm=>{
     location.href = "../Agenda";
   });
    console.log(res);
  }).fail(err=>{
    swal("Error", "Hubo algún problema, intentalo más tarde ", "error");
  })

}


$('#deleteConsulta').on('click', function(){

  swal({
      title: "Atención",
      text: "¿Seguro que deseas eliminar la consulta?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  })
  .then((isConfirm) => {
      if (isConfirm) {
          deleteConsulta();
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
  });

})


function deleteConsulta() {
  var id = $('.historial_id').val();
  $.ajax({
    type:'get',
    dataType:'json',
    url:'../deleteConsulta/'+id
  }).done(res=>{
    swal({
      position:'top-end',
      icon:'success',
  //    text:'El recorrido fue asigando correctamente',
      title:'Consulta eliminada',
      showConfirmButton:true
    }).then(isConfirm=>{
      location.href = "../empleados/"+res.curp;
   });
    console.log(res);
  }).fail(err=>{
    swal("Error", "Hubo algún problema, intentalo más tarde ", "error");
  })
}
