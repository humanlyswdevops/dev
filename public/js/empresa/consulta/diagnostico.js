
$('.save_alert').hide();
// NOTE: selects de Diagnostico
$('.cie').select2({
   ajax: {
     url: "../cie_10",
     dataType: 'json',
     delay: 250,
     data: function (params) {
       return {
         term:params.term
        };

     },
     processResults: function (data) {
         return {
             results: $.map(data, function (item) {
                 return {
                     text: item.catalog_key+' - '+ item.nombre,
                     id: item.catalog_key,
                     name: item.nombre,
                 }
             })
         };
     }
   },
   minimumInputLength: 2,
   language: "es",
   placeholder: "CIE 10 - Buscar nombre o codigo",
   searching:"Buscando",
   language: {
      noResults: function() {
        return "No hay resultados";
      },
      searching: function() {
       return "Buscando..";
      },
      inputTooShort: function () {
      return "Por favor ingresa 2 o mas letras"
      }
   },
});

$('.cat_proc').select2({
   ajax: {
     url: "../procedimientos",
     dataType: 'json',
     delay: 250,
     data: function (params) {
       return {
         term:params.term
        };

     },
     processResults: function (data) {
         return {
             results: $.map(data, function (item) {
                 return {
                     text: item.clave+' - '+ item.nombre,
                     name:item.nombre,
                     id: item.clave
                 }
             })
         };
     }
   },
   minimumInputLength: 2,
   language: "es",
   placeholder: "Procediminetos - Buscar nombre o codigo",
   searching:"Buscando",
   language: {
      noResults: function() {
        return "No hay resultados";
      },
      searching: function() {
       return "Buscando..";
      },
      inputTooShort: function () {
      return "Por favor ingresa 2 o mas letras"
      }
   },
});

$('.cat_med').select2({
   ajax: {
     url: "../medicamentos",
     dataType: 'json',
     delay: 250,
     data: function (params) {
       return {
         term:params.term
        };

     },
     processResults: function (data) {
         return {
             results: $.map(data, function (item) {
                 return {
                     text: item.clave+' - '+ item.nombre +' - '+item.detalle,
                     id: item.clave,
                     name:item.nombre
                 }
             })
         };
     }
   },
   minimumInputLength: 2,
   language: "es",
   placeholder: "Medicamentos - Buscar nombre o codigo",
   searching:"Buscando",
   language: {
      noResults: function() {
        return "No hay resultados";
      },
      searching: function() {
       return "Buscando..";
      },
      inputTooShort: function () {
      return "Por favor ingresa 2 o mas letras"
      }
   },
});


$('.cat_disc').select2({
    ajax: {
      url: "../discapacidad",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          term:params.term
         };

      },
      processResults: function (data) {
          return {
              results: $.map(data, function (item) {
                  return {
                      text: item.clave+' - '+ item.nombre,
                      id: item.clave,
                      name:item.nombre
                  }
              })
          };
      }
    },
    minimumInputLength: 2,
    language: "es",
    placeholder: "Cif - Buscar nombre o codigo",
    searching:"Buscando",
    language: {
       noResults: function() {
         return "No hay resultados";
       },
       searching: function() {
        return "Buscando..";
       },
       inputTooShort: function () {
       return "Por favor ingresa 2 o mas letras"
       }
    },
 });



//////////////////////diagnostico/////////////////////////////////
$(document).on('select2:select','.cie', function(e){
  console.log(e);
  addDiagnostico(e)
  consultaDiagnostico();
})

function addDiagnostico(e){
  key = e.params.data.id
  name = e.params.data.name
  data = $('.'+key);
  if ( data.length > 0) {
    $('.cie').val(null).trigger('change');
    return
  }
  $('.cie_card').append(`
    <div class="col-md-12 mt-1 elemetDiagnostico ${key}" data-name="${name}" data-clave="${key}">
      <fieldset class="form-label-group mb-50">
          <h6 class="display-6">
          <div class="badge badge-secondary mr-1 mb-1">
            <i class="fa fa-flask"></i>
            <span>${key}</span>
          </div>
            ${name}
            <a class="deleteDiagostico float-right">
              <i class="float-rigth feather icon-trash-2"></i>
            </a>
          </h6>
          <div  id="comment_${key}" ></div>
       </fieldset>
     </div>
  `);
  $('.cie').val(null).trigger('change');
  quill_Diagnostico('comment_'+key);
}

$(document).on('click','.deleteDiagostico', function(){
  swal({
    title: "Atención",
    text: "¿Seguro que deseas eliminar el diagnostico?",
    icon: "warning",
    buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
    }
})
.then((isConfirm) => {
    if (isConfirm) {
        $(this).parent().parent().parent().remove();
        consultaDiagnostico();
        swal("Eliminado", "El diagnostico fue eliminado", "error");
    } else {
        swal("Cancelado", "La operación se cancelo correctamente", "error");
    }
});
})

function quill_Diagnostico(termino){
  termino = new Quill('#'+termino, {
    theme: 'snow',
    placeholder:'Descripción'
  });
  termino.on('text-change', () => {
     consultaDiagnostico();
  })
}
function diagnostico_(){
  arrayDiagnostico = [];
  $('.elemetDiagnostico').each(function(index) {
    clave = $(this).data('clave');
    nombre = $(this).data('name');
    comentario = $('#comment_'+clave).children('.ql-editor').html();
    diagnosticoJson = new Object();
    diagnosticoJson.clave = clave;
    diagnosticoJson.nombre = nombre;
    diagnosticoJson.comentario = comentario;
    arrayDiagnostico.push(diagnosticoJson);
  });
  return arrayDiagnostico;
}
/////////////////////////procedimientos//////////////////////////////
$(document).on('select2:select','.cat_proc', function(e){
  addProcedimiento(e);
  consultaProcedimiento();
})
function addProcedimiento(e){
  key = e.params.data.id
  name = e.params.data.name
  data = $('.proc_'+key);
  if ( data.length > 0) {
    $('.cat_proc').val(null).trigger('change');
    return
  }
  $('.proc_card').append(`
    <div class="col-md-12 mt-1 elemetProcedimietno proc_${key}" data-name="${name}" data-clave="${key}">
      <fieldset class="form-label-group mb-50">
          <h6 class="display-6">
          <div class="badge badge-secondary mr-1 mb-1">
            <i class="fa fa-flask"></i>
            <span>${key}</span>
          </div>
            ${name}
            <a class="deleteProcedimiento float-right">
              <i class="float-rigth feather icon-trash-2"></i>
            </a>
          </h6>
          <div  id="comment_proc_${key}" ></div>
       </fieldset>
     </div>
  `);
  $('.cat_proc').val(null).trigger('change');
  quill_Procedimiento('comment_proc_'+key);
}
function quill_Procedimiento(termino){
  termino = new Quill('#'+termino, {
    theme: 'snow',
    placeholder:'Descripción'
  });
  termino.on('text-change', () => {
     consultaProcedimiento();
  })
}
$(document).on('click','.deleteProcedimiento', function(){
  swal({
    title: "Atención",
    text: "¿Seguro que deseas eliminar el procedimiento?",
    icon: "warning",
    buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
    }
})
.then((isConfirm) => {
    if (isConfirm) {
        $(this).parent().parent().parent().remove();
        consultaProcedimiento();
        swal("Eliminado", "El procedimiento fue eliminado", "error");
    } else {
        swal("Cancelado", "La operación se cancelo correctamente", "error");
    }
});
})
function cat_proc(id,name,key){

  arrayProcedimiento = [];
  $('.elemetProcedimietno').each(function(index) {
    clave = $(this).data('clave');
    nombre = $(this).data('name');
    comentario = $('#comment_proc_'+clave).children('.ql-editor').html();
    procediemientoJson = new Object();
    procediemientoJson.clave = clave;
    procediemientoJson.nombre = nombre;
    procediemientoJson.comentario = comentario;
    arrayProcedimiento.push(procediemientoJson);
  });
  return arrayProcedimiento;

}

/////////////////////Medicamento//////////////////////////////////

$(document).on('select2:select','.cat_med', function(e){
  $('.save_alert').show();
  addMedicamento(e);
  consultaMedicamento();
})
$(document).on('select2:select','.selectMedicamento', function(e){
  consultaMedicamento();
})
function addMedicamento(e){
  key = e.params.data.id
  name = e.params.data.name
  data = $('.medi_'+key);
  if ( data.length > 0) {
    $('.cat_med').val(null).trigger('change');
    return
  }
  $('.medic_card').append(`
    <div class="col-md-4 mt-1 medi_${key} elementMedicamento " data-name="${name}" data-clave="${key}">
      <table class="table mb-0">
          <thead class="bg-secondary">
              <tr>
                  <th scope="col" class="text-white" colspan="2">
                    <div class="badge badge-primary mr-1">
                      <i class="fa fa-flask"></i>
                      <span>${key}</span>
                    </div>
                    ${name}
                    <a class="deleteMedicamento float-right">
                      <i class="float-rigth feather icon-trash-2"></i>
                    </a>
                  </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Tomar</th>
                <td>
                  <select class="selectMedicamento tomar_${key}" style="width:100%">
                    <option value=""></option>
                    <option value="1/2 tableta">1/2 tableta</option>
                    <option value="1 tableta">1 tableta</option>
                    <option value="2 tableta">2 tableta</option>
                    <option value="1 ampolleta">1 ampolleta</option>
                    <option value="1 pastilla">1 pastilla</option>
                    <option value="2 pastilla">2 pastilla</option>
                    <option value="1 cápsula">1 cápsula</option>
                    <option value="2 cápsula">2 cápsula</option>
                    <option value="1 cucharada">1 cucharada</option>
                    <option value="1 gota">1 gota</option>
                    <option value="2 gotas">2 gotas</option>
                    <option value="2.4 Ml">2.4 Ml</option>
                    <option value="5 ML">5 ML</option>
                    <option value="10 ML">10 ML</option>
                  </select>
                </td>
              </tr>
              <tr>
                <th>Frecuencia</th>
                <td>
                  <select class="selectMedicamento fr_${key}" style="width:100%">
                    <option value=""></option>
                    <option value="cada 4 horas">cada 4 horas</option>
                    <option value="cada 6 horas">cada 6 horas</option>
                    <option value="cada 8 horas">cada 8 horas</option>
                    <option value="cada 12 horas">cada 12 horas</option>
                    <option value="cada 24 horas">cada 24 horas</option>
                  </select>
                </td>

              </tr>
              <tr>
                <th>Duración</th>
                <td>
                  <select class="selectMedicamento dia_${key}" style="width:100%">
                    <option value=""></option>
                    <option value="3 días">3 días</option>
                    <option value="5 días">5 días</option>
                    <option value="7 días">7 días</option>
                    <option value="10 días">3 días</option>
                    <option value="15 días">15 días</option>
                    <option value="30 días">30 días</option>
                  </select>
                </td>
              </tr>
              <tr>
                <th colspan="2">
                    <div class="form-control comentario_${key}" id="comentario_med_${key}"></div>
                </th>
              </tr>
          </tbody>
      </table>
    </div>
  `);
  styleMedicamento();
  $('.cat_med').val(null).trigger('change');
  quill_Medicamento('comentario_med_'+key);
}
function quill_Medicamento(termino){
  termino = new Quill('#'+termino, {
    theme: 'snow',
    placeholder:'Descripción'
  });
  termino.on('text-change', () => {
     consultaMedicamento();
  })
}
$(document).on('click','.deleteMedicamento', function(){
  swal({
    title: "Atención",
    text: "¿Seguro que deseas eliminar el medicamento?",
    icon: "warning",
    buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
    }
})
.then((isConfirm) => {
    if (isConfirm) {
        $(this).parent().parent().parent().parent().parent().remove();
        consultaMedicamento();
        swal("Eliminado", "El medicamento fue eliminado", "success");
    } else {
        swal("Cancelado", "La operación se cancelo correctamente", "error");
    }
});
})
function cat_medi(id,name,key){

  arrayMedicamento = [];
  $('.elementMedicamento').each(function(index) {

    clave = $(this).data('clave');
    nombre = $(this).data('name');
    tomar = $('.tomar_'+clave).val();
    fr = $('.fr_'+clave).val();
    duracion = $('.dia_'+clave).val();
    comentario = $('#comentario_med_'+clave).children('.ql-editor').text();

    medicamentoJson = new Object();
    medicamentoJson.clave = clave;
    medicamentoJson.nombre = nombre;
    medicamentoJson.tomar = tomar;
    medicamentoJson.frecuencia = fr;
    medicamentoJson.duracion = duracion;
    medicamentoJson.comentario = comentario;
    arrayMedicamento.push(medicamentoJson);

  });
  return arrayMedicamento;
}


///////////////////////////////////////////////////////

function consultaDiagnostico() {
  diagnostico = diagnostico_();
  data = new Object();
  data.diagnostico = diagnostico;
  data.tipo = 'diagnostico'
  data.id = $('#historial_id').val();
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'../diagnostico',
    beforeSend:()=>{
      $('.pdf').attr('disabled',true);
    }
  }).done(res=>{
    $('.medicamentoSave').hide();
    $('.pdf').attr('disabled',false);
  }).fail(err=>{
    $('.pdf').attr('disabled',false);
    console.log(err);
  })
}

function consultaProcedimiento() {

  procedimiento = cat_proc();
  data = new Object();
  data.procedimiento = procedimiento;
  data.tipo = 'procedimiento'
  data.id = $('#historial_id').val();
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'../diagnostico',
    beforeSend:()=>{
      $('.pdf').attr('disabled',true);
    }
  }).done(res=>{
    $('.medicamentoSave').hide();
    $('.pdf').attr('disabled',false);
  }).fail(err=>{
    $('.pdf').attr('disabled',false);
    console.log(err);
  })

}

function consultaMedicamento() {
  medicamento = cat_medi();
  data = new Object();
  data.medicamento = medicamento;
  data.tipo = 'medicamento'
  data.id = $('#historial_id').val();
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'../diagnostico',
    beforeSend:()=>{
      $('.pdf').attr('disabled',true);
    }
  }).done(res=>{
    $('.medicamentoSave').hide();
    $('.pdf').attr('disabled',false);
  }).fail(err=>{
    $('.pdf').attr('disabled',false);
    console.log(err);
  })
}

function consultaTratamiento() {

  diagnostico = diagnostico_();
  data = new Object();
  data.tratamiento = $('.tratamiento').children('.ql-editor').html();
  data.notas = $('.notas').children('.ql-editor').html();
  data.tipo = 'tratamiento'
  data.id = $('#historial_id').val();
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'../diagnostico',
    beforeSend:()=>{
      $('.pdf').attr('disabled',true);
    }
  }).done(res=>{
    $('.medicamentoSave').hide();
    $('.pdf').attr('disabled',false);
  }).fail(err=>{
    $('.pdf').attr('disabled',false);
    console.log(err);
  })
}

///////////////////////////////////////////////////////



$('.pdf').on('click', function(arguments){
id = $('#historial_id').val();
//window.print();
 url = '../Receta/'+id
 var printWindow = window.open( url, 'Print', 'left=200, top=150,bottom=100, width=950, height=500, toolbar=0, resizable=0');
// printWindow.print(url);
// printWindow.close();
});



$('.email').on('click', function(arguments){
  swal({
    title: "Atención",
    text: "¿Seguro que deseas enviar la receta medica por email?",
    icon: "warning",
    buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
    }
  })
  .then((isConfirm) => {
    if (isConfirm) {
        emailReceta();
    } else {
        swal("Cancelado", "La operación se cancelo correctamente", "error");
    }
  });

});
function emailReceta(){
  id = $('#historial_id').val();
  $.ajax({
    type:'get',
    dataType:'json',
    url:'../RecetaEmail/'+id,

  }).done(res=>{

    if(res == "Correcto")
    {
        swal("Enviado", "Se ha enviado el correo con la receta médica", "success");
    }else{
        swal("Error", "Algo ocurrio intentalo más tarde", "error");
    }

  }).fail(err=>{
      swal("Error", "Algo ocurrio intentalo más tarde", "error");
  })
}

function sendEmail(res) {
  $.ajax({
    type:'post',
    data:res,
    dataType:'json',
    url:'https://humanly.javierviteramirez.com/Email/public/hasEmail',
  }).done(res=>{

    swal("Enviado", "Se ha enviado el correo con la receta médica", "success");

  }).fail(err=>{
    swal("Error", "Algo ocurrio intentalo más tarde", "error");
  })
}
$('.alertCedula').on('click', function(arguments){
swal('Configura tu cuenta','Para poder generar recetas médicas, es necesario completar algunos campos en la configuración de su cuenta','info')
});

///////////////////DISCAPACIDAD/////////////////////////////


$(document).on('select2:select','.cat_disc', function(e){
    console.log(e);
    addDiscapacidad(e);
    consultaDiscapacidad();
})

function addDiscapacidad(e){
key = e.params.data.id
name = e.params.data.name
data = $('.disc_'+key);
if ( data.length > 0) {
    $('.cat_disc').val(null).trigger('change');
    return
}
$('.disc_card').append(`
    <div class="col-md-12 mt-1 elementDiscapaciad disc_${key}" data-name="${name}" data-clave="${key}">
    <fieldset class="form-label-group mb-50">
        <h6 class="display-6">
        <div class="badge badge-secondary mr-1 mb-1">
            <i class="fa fa-flask"></i>
            <span>${key}</span>
        </div>
            ${name}
            <a class="deleteDiagostico float-right">
            <i class="float-rigth feather icon-trash-2"></i>
            </a>
        </h6>
        <div  id="disc_coment_${key}" ></div>
        </fieldset>
    </div>
`);
$('.cat_disc').val(null).trigger('change');
quill_Discapacidad('disc_coment_'+key);
}

$(document).on('click','.deleteDiagostico', function(){
swal({
    title: "Atención",
    text: "¿Seguro que deseas eliminar la discapacidad?",
    icon: "warning",
    buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
    }
})
.then((isConfirm) => {
    if (isConfirm) {
        $(this).parent().parent().parent().remove();
        consultaDiscapacidad();
        swal("Eliminado", "La discapaciad fue eliminada", "error");
    } else {
        swal("Cancelado", "La operación se cancelo correctamente", "error");
    }
});
})

function quill_Discapacidad(termino){
console.log(termino);
termino = new Quill('#'+termino, {
    theme: 'snow',
    placeholder:'Descripción'
});
termino.on('text-change', () => {
    consultaDiscapacidad();
})
}

function cat_discapcidad(){
arrayDiagnostico = [];
$('.elementDiscapaciad').each(function(index) {
    clave = $(this).data('clave');
    nombre = $(this).data('name');
    comentario = $('#disc_coment_'+clave).children('.ql-editor').html();
    diagnosticoJson = new Object();
    diagnosticoJson.clave = clave;
    diagnosticoJson.nombre = nombre;
    diagnosticoJson.comentario = comentario;
    arrayDiagnostico.push(diagnosticoJson);
});
return arrayDiagnostico;
}



function consultaDiscapacidad() {
    data = new Object();
    discapacidades = cat_discapcidad();
    data.discapacidades = discapacidades;
    data.tipo = 'discapacidad'
    data.id = $('#historial_id').val();
    $.ajax({
      type:'post',
      dataType:'json',
      data:data,
      url:'../diagnostico',
      beforeSend:()=>{
        $('.pdf').attr('disabled',true);
      }
    }).done(res=>{
      $('.medicamentoSave').hide();
      $('.pdf').attr('disabled',false);
    }).fail(err=>{
      $('.pdf').attr('disabled',false);
      console.log(err);
    })
}

//NOtas email


$('.notasEmail').on('click', function(){
    $('#notas').modal('show');
})

$('.enviarEmailNota').on('click',function(){
    swal({
      title: "Atención",
      text: "¿Seguro que deseas enviar las notas por email?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
    })
    .then((isConfirm) => {
      if (isConfirm) {
        NotasEmail();
      } else {
          $('#notas').modal('hide');
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
    });
});

  function NotasEmail(){
    id = $('#historial_id').val();
    email = $('#notaEmailPatient').val();

    obj = new Object();
    obj.id = id;
    obj.email = email;

    $.ajax({
      type:'post',
      dataType:'json',
      url:'../NotasEmail',
      data:obj
    }).done(res=>{
      $('#notas').modal('hide');
      if(res == "Correcto")
      {
          swal("Enviado", "Se ha enviado el correo con la receta médica", "success");
      }else{
          swal("Error", "Algo ocurrio intentalo más tarde", "error");
      }

    }).fail(err=>{
        swal("Error", "Algo ocurrio intentalo más tarde", "error");
    })
  }
