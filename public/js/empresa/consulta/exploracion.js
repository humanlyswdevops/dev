$(document).ready(function() {
    $('.exploracion_select').select2();
});

$(document).on('select2:select','.exploracion_select', function(e){
  exploracion(e.params.data.id,e.params.data.text);
})


$(document).on('select2:unselecting','.exploracion_select',function(e) {
    console.log(e);
    swal({
        title: "Atención",
        text: "¿Seguro de eliminar el siguiente examen?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    })
    .then((isConfirm) => {
        if (isConfirm) {

            let id_eliminado = 0;
            if(e.params.args.data !== undefined)
            {
                 id_eliminado = e.params.args.data.id;
            }
            let seleccionados  = $(".exploracion_select").val();
            seleccionados = jQuery.grep(seleccionados, function(value) {
                console.log(value,'----****');
                return value != id_eliminado;
            });

            $('.exploracion_select').val(seleccionados).trigger('change');
            deletes(e.params.args.data.id,e.params.args.data.text);
            swal("Correcto", "el examen se elimino correctamente", "success");
        } else {
            swal("Cancelado", "La operación se cancelo correctamente", "error");
        }
    });
    return false;
});

// $(document).on('select2:unselect','.exploracion_select', function(e){
//   deletes(e.params.data.id,e.params.data.text);
// });

function exploracion(id,text){
    $('#exploracion_section').append(`
       <div class="col-md-4 ${id}">
         <div class="card examen_fisico_card">
            <div class="card-header">
              <div class=" display-5" >${text}</div>
            </div>
            <div class="card-body">
              <div class="col-md-12">
                <div class=" form-control col-mfd-12 examen_fisico" id="${id}">
                </div>
              </div>
            </div>
          </div>
        </div>
      `);
     quill(id);
}

function deletes(id) {
  $('.'+id).remove();
  examenSave();
}

function quill(termino){
  termino = new Quill('#'+termino, {
    theme: 'snow',
    placeholder:'Descripción'
  });
  termino.on('text-change', () => {
     examenSave();
  })
}


function examenSave(){
  var data = new Object();
  $('.examen_fisico').each(function(index) {
    prueba=$(this).attr('id');
    text = $(this).children('.ql-editor').html();
    data[prueba] = text;
  });

  data.estatura = $('#altura').val();
  data.peso = $('#peso').val();
  data.imc = $('#imc_label').val();
  data.temperatura = $('#temperatura').val();
  data.fr = $('#fr').val();
  data.fc = $('#fc').val();
  data.so = $('#saturacion').val();
  data.pa = $('#presion').val();
  data.idHistorial = $('#historial_id').val();
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'../examen_fisico'
  }).done(res=>{
    //console.log(res);
  }).fail(err=>{

  })

}
