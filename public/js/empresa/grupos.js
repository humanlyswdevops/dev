// NOTE: Ocultar $element
$('.spinnerAdd').hide();
var validacion = $.trim($("#data-list-view").data('insert'));
var botones = [];
console.log(validacion);
if(validacion == "true")
{
    botones = [
      {
          text: "<i class='feather icon-plus'></i> Nuevo Grupo",
          action: function() {
              $(this).removeClass("btn-secondary"),$(".add-new-data").addClass("show") , $(".overlay-bg").addClass("show"), $("#data-name, #data-price").val(""), $("#data-category, #data-status").prop("selectedIndex", 0)
          },
          className: "btn-outline-primary btn-sm"
      }
    ];
}

$(".data-list-view").DataTable({
    responsive: !1,
    dom: '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    },

    order: [
        [1, "asc"]
    ],

    buttons:botones,
    initComplete: function(t, e) {
        $(".dt-buttons .btn").removeClass("btn-secondary")
    }
}), 0 < $(".data-items").length && new PerfectScrollbar(".data-items", {
    wheelPropagation: !1
}), $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
    $(".add-new-data").removeClass("show"), $(".overlay-bg").removeClass("show"), $("#data-name, #data-price").val(""), $("#data-category, #data-status").prop("selectedIndex", 0)
})


$('#formGrupo').on('submit',function(e){
    $('#alert').hide();
    e.preventDefault();
        $.ajax({
            type:'post',
            dataType:'json',
            data:$('#formGrupo').serialize(),
            url:'new_grupo',
            beforeSend: function(){
                $('.spinnerAdd').show();
                $('.spinnerAdd').parent().attr('disabled',true);
              },
            complete: function() {
            }

        }).done(function(response){
          $('.spinnerAdd').hide();
          $('.spinnerAdd').parent().attr('disabled',false);
            document.querySelector('#formGrupo').reset();

            swal({
              position:'top-end',
              icon:'success',
              text:'El grupo se ha creado correctamente',
              title:'Grupo Creado',
              showConfirmButton:true
            }).then(isConfirm=>{
             location.reload();
            })


        }).fail(function(error){
            $('.spinnerAdd').hide();
            $('.spinnerAdd').parent().attr('disabled',false);
            if (error.status==400) {
              swal('Atención','El nombre del grupo ya existe','info');
            }
            else {
              swal('Error','Algo ha ocurrido, verifica que los campos sean correctos o intentalo mas tarde...','error')
            }
        });
    })


$(document).on("click",'.action-delete' ,function(t) {
  nombre = $(this).data('nombre');
  swal({
      title: "Atención",
      text: "¿Seguro que deseas eliminar el grupo?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  })
  .then((isConfirm) => {
      if (isConfirm) {
          delete_grupo(nombre);
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
  })
})

function delete_grupo(nombre){
    $('#alert').hide();
    $.ajax({
        type:'get',
        dataType:'json',
        url:'delete_grupo/'+nombre,
        beforeSend: function(){
            $('#load_delete').show();
            $('#present_alert').hide();
            $('#btn_delete').hide();
          },
        complete: function() {
        }

    }).done(function(response){
        if(response!=null){
            swal({
              position:'top-end',
              icon:'success',
              text:'El grupo se ha eliminado de manera correcta',
              title:'Eliminación Exitosa',
              showConfirmButton:true
            }).then(isConfirm=>{
             location.reload();
            })

        }else{
          swal('Atención','El grupo no se puede eliminar debido a que tiene estudios programados','info');
        }

    }).fail(function(error){
        swal("Error", "Algo a ocurrido, intentalo mas tarde...", "error");
    })
}

// NOTE: Modificar
//
//

$(document).on("click",'.action-edit' ,function(t) {
    nombre = $(this).data('nombre');
    $('#load_update').hide();
    $('#title_update').text('Modificació del grupo '+ nombre);
    $('#grupo_update').val(nombre);
    $('#nombreGrupo').val(nombre);
    $('#updategrupos').modal('show');

})

function update(data) {
  $.ajax({
    type:'POST',
    dataType:'json',
    data:data,
    url:'updateGrupos',
    beforeSend:function(){

    },
    complete:function(){
  // $('#updategrupos').modal({backdrop: 'static', keyboard: false})
    }
  }).done(function(response){


      swal({
        position:'top-end',
        icon:'success',
        text:'El grupo se ha modificado de manera correcta',
        title:'Modificación Exitosa',
        showConfirmButton:true
      }).then(isConfirm=>{
       location.reload();
      })


  }).fail(function(){
      swal('Error','El grupo no se pudo modificar intentalo más tarde','error');
 });

}


$('#formGrupoUpdate').on('submit',function(e){
  e.preventDefault();
  swal({
      title: "Atención",
      text: "¿Seguro que deseas modificar el grupo?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  })
  .then((isConfirm) => {
      if (isConfirm) {
            update($('#formGrupoUpdate').serialize());
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
  })

});
