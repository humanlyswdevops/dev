
"use strict";
$(".data-list-view").DataTable({
    responsive: !1,
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }
})

var accesos_insert = $.trim($("#validacion_accesos").data('insert'));
var accesos_delete = $.trim($("#validacion_accesos").data('delete'));
var accesos_update = $.trim($("#validacion_accesos").data('update'));

$(document).on('click','.permisos', function(){
  console.log('Esta entrando aqui?');
  var id = $(this).data('rol');
  permisos(id);
});


function permisos(id){
  $.ajax({
      url: 'permisos/'+id, //archivo que recibe la peticion
      type: 'get', //método de envio
      dataType:'json'
  }).done(res=>{
    permisosTable(res);
  }).fail(err=>{
    swal('Error','Hubo algún problema, intentalo más tarde','error');
  })
}

function permisosTable(res){
  var dataSet=[];
  res.forEach((item, i) => {
    let button_delete = '';
    if(accesos_delete == "true")
    {
        button_delete = `<button type="button" data-access="${item.id}" class="btn delete btn-sm btn-icon btn-danger" name="button">
                <i class="feather icon-trash"></i>
            </button>`;
    }
    dataSet.push([
      item.tipo,
      item.modulo,
      item.crud,
      button_delete
    ]);
  });
  $('#permisosTable').dataTable().fnDestroy();
  $("#permisosTable").DataTable({
      data:dataSet,
      responsive: !1,
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "_MENU_",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
        }
      }
  })

}


$(document).on('click', '.delete',function(){
  const id = $(this).data('access');
  swal({
      title: "Eliminación",
      text: "¿Estas seguro de eliminar el permiso?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  }).then(isConfirm => {
      if (isConfirm) {
          eliminacion(id);
      } else {
          swal("Cancelado", "La eliminación fue cancelada", "error");
      }
  });
})


/**
 * Eliminacion de un Accesos
 * @param  {[type]} e [preventDefault]
 * @return {[type]}   []
 */

function eliminacion(id){
  $.ajax({
      data: {id:id}, //datos que se envian a traves de ajax
      url: 'deleteAccess', //archivo que recibe la peticion
      type: 'delete', //método de envio
      dataType:'json',
  }).done(res=>{

    permisos(res.data.rol_id);
    swal('Eliminado','El acceso se elimino correctamente','success');
    // NOTE: permiso
  }).fail(err=>{
      swal('Error','Hubo algún problema, intentalo más tarde','error');
  })
}


$('.addPermisos').on('click', ()=>{
  $('#addpermisos').modal('show');
});



$(document).ready(function() {
    $('.rolselect').select2();
    $('.select-modulo').select2();
    $('.select-multiple').select2({
        placeholder:"Selecciona una operación"
    });
});


$('#formPermisos').on('submit', function(e){
  e.preventDefault();
  let form = $(this).serialize();
  console.log(form);
  $.ajax({
    type:'post',
    dataType:'json',
    data:$(this).serialize(),
    url:'addPermiso',
    beforeSend:()=>{
      $('.loadAddp').show();
      $('.btnAddP').hide();
    }
  }).done(res=>{
    $('.loadAddp').hide();
    $('.btnAddP').show();
    if (res.data == 'repeat') {
      swal('Atención','El permiso ya esta registrado','info');
    }else {
      permisos(res.data.rol_id);
      swal('Agregado','El permiso fue agregado al rol','success');
    }
  }).fail(err=>{
    $('.loadAddp').hide();
    $('.btnAddP').show();
    swal('Error','Hubo algún problema, intentalo más tarde','error');
  })

});


$('.addRol').on('click', function(){
  $('#addroles').modal('show');
})


// "processing": true,
// "serverSide": true,
// "ajax": "scripts/server_processing.php",
// "deferLoading": 57

$('#formRol').on('submit', function(e){
  e.preventDefault();
  $.ajax({
    type:'post',
    dataType:'json',
    data:$(this).serialize(),
    url:'addRol ',
    beforeSend:()=>{
      $('.btnAddR').hide();
      $('.loadAddR').show();
    }
  }).done(res=>{
    $('#addroles').modal('hide');
    $('#formRol')[0].reset();
    $('.btnAddR').show();
    $('.loadAddR').hide();
    swal({
      position:'top-end',
      icon:'success',
      text:'El rol fue agregado exitosamente',
      title:'Agregado',
      showConfirmButton:true
    }).then(isConfirm=>{
     location.reload();
    })
  }).fail(err=>{
    $('.btnAddR').show();
    $('.loadAddR').hide();
    swal('Error','Hubo algún problema, intentalo más tarde','error');
  })

});





$(document).on('click','.permisosdelete', function(){
  const id = $(this).data('delete');
  swal({
      title: "Eliminación",
      text: "¿Estas seguro de eliminar el permiso?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  }).then(isConfirm => {
      if (isConfirm) {
          eliminacionRol(id);
      } else {
          swal("Cancelado", "La eliminación fue cancelada", "error");
      }
  });
})


function eliminacionRol(id) {
  $.ajax({
      data: {id:id}, //datos que se envian a traves de ajax
      url: 'deleteRol', //archivo que recibe la peticion
      type: 'post', //método de envio
      dataType:'json',
  }).done(res=>{
    
    swal({
      position:'top-end',
      icon:'success',
      text:'El rol se elimino correctamente',
      title:'Eliminado',
      showConfirmButton:true
    }).then(isConfirm=>{
     location.reload();
    })
    // NOTE: permiso
  }).fail(err=>{
      swal('Error','Hubo algún problema, intentalo más tarde','error');
  })
}
