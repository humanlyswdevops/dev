var contador_nuevos_tags = 0;
var array_nuevos_tags = [];
var editores = [];

$('.patologico').on('click', function(arguments){
   $('#patologico').modal('show')
   editores = [];
   id = $(this).data('id');
   nombre = $(this).data('nombre');

   $('.antecedente_name').text(nombre);
   $('.content_detail').empty();
   antecedente(id);
})

/**
 * La funcion obtiene las preguntas del antecedente seleccionado
 *
 * @param {*} id
 */
function antecedente(id){
    $.ajax({
      type:'post',
      dataType:'json',
      data:{id:id},
      url:'../antecedentesGet',
      beforeSend:()=>{
        $('.second').show();
        $('.first').hide();

      }
    }).done(res=>{
      $('#keyForm').val(id);
      $('.antecedentes_select').empty();
      $('.content_detail').empty();
      antecedentes = [];
      res.forEach((item, i) => {
        object = new Object();
        object.id = item.id;
        object.text = item.pregunta;
        object.tipo = item.campo;
        antecedentes.push(object);
      });
      $(".antecedentes_select").select2({
          tags:antecedentes,
      });
      $(".antecedentes_select").select2({
         tags:false,
         createTag: function (tag) {
            return {
                id: contador_nuevos_tags++,
                text: tag.term,
                isNew : true
            };
        }
      });

      getFieldAntecedentes(id);

    }).fail(err=>{

    })
}
/**
 *  en caso de que el antecedente tenga resultados ya registrados
 *  Obtendra todos los resultados Resultados
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function getFieldAntecedentes(id){

    object = new Object();
    object.curp = curp = $('#curp').val();
    object.id = id
    $.ajax({
      type:'post',
      dataType:'json',
      data:object,
      url:'../fieldAntecedente'
    }).done(res=>{
      arrayFields = [];
      res.forEach((item, i) => {
        let checkEditor = "";
        if(item.status){
          checkEditor = "checked";
        }
        arrayFields.push(item.answerA_id);
        $('.content_detail').append(`
          <div class="col-md-4 mt-1  field_${item.answerA_id}" data-id="${item.answerA_id}">
            <fieldset class="form-label-group mb-50">
                  <h6 class="display-6">
                      <div class="badge badge-secondary mr-1 mb-1">
                          <i class="feather icon-user"></i>
                          <span>${item.pregunta}</span>
                      </div>
                  </h6>
                    <div class="custom-control custom-switch custom-control-inline">
                        <input ${checkEditor} type="checkbox" class="custom-control-input" onClick="handleQuill(${item.id},${item.answerA_id})" id=switch_quill_${item.answerA_id} data-id="${item.answerA_id}">
                        <label class="custom-control-label" for="switch_quill_${item.answerA_id}"></label>
                        <span class="switch-label">Activar/Desactivar</span>
                    </div>
                  <div data-id="${item.answerA_id}" id="antecedente_${item.answerA_id}" ></div>
             </fieldset>
           </div>
        `);
        if(item.status){
          quill('antecedente_'+item.answerA_id,item.answerA_id);
          $(`#antecedente_${item.answerA_id} .ql-editor`).html(item.respuesta);
        }
      });
      $('.antecedentes_select').val(arrayFields).trigger('change.select2');
      $('.second').hide();
      $('.first').show();
    }).fail(err=>{
    })
}

$(document).on('select2:unselecting','.antecedentes_select',function(e) {
    swal({
        title: "Atención",
        text: "¿Seguro de eliminar el siguiente antecedente?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    })
    .then((isConfirm) => {
        if (isConfirm) {

            let id_eliminado = 0;
            if(e.params.args.data.isNew !== undefined)
            {
                id_eliminado = array_nuevos_tags[parseInt(e.params.args.data.id)+1];
            }else{
                id_eliminado = e.params.args.data.id;
            }

            let seleccionados  = $(".antecedentes_select").val();
            seleccionados = jQuery.grep(seleccionados, function(value) {
                return value != id_eliminado;
            });
            $('.antecedentes_select').trigger({
                type: 'select2:unselect',
                params: {
                    data: e.params.args.data
                }
            });
            $('.antecedentes_select').val(seleccionados).trigger('change');

            swal("Correcto", "el antecedente se elimino correctamente", "success");
        } else {
            swal("Cancelado", "La operación se cancelo correctamente", "error");
        }
    });
    return false;
});


$(document).on('select2:select','.antecedentes_select', function(e){
    let nuevo = true;
    if(e.params.data.id > 0 && e.params.data.text != e.params.data.id && typeof e.params.data.title !== 'undefined'){
        nuevo = false;
        antecedente_fild(e.params,nuevo,true);
        // antecedente_fild(e.params.data.id,nuevo,true);
    }else{
        antecedente_fild(e,nuevo,false);
    }
    // if(!nuevo){
    //     let id = e.params.data.id;
    //     add_element(e.params.data,id);
    //     refreshAdd(e.params.data,id);
    // }
})

$(document).on('select2:unselect','.antecedentes_select', function(e){
//  actualica_patologico();
    let id_eliminado = 0;
    if(e.params.data.isNew !== undefined)
    {
        id_eliminado = array_nuevos_tags[parseInt(e.params.data.id)+1];
    }else{
        id_eliminado = e.params.data.id;
    }
    delete_element(id_eliminado);

})

/**
 * Agregar elementos de un antecedente
 * @param {[type]} e [description]
 */
//
function add_element(e,a_id,id = 0){
    $('.content_detail').append(`
        <div class="col-md-4 mt-1  field_${a_id}" data-text="${e.text}" data-id="${a_id}">
        <fieldset class="form-label-group mb-50">
            <h6 class="display-6">
            <div class="badge badge-secondary mr-1 mb-1">
                <i class="feather icon-user"></i>
                <span>${e.text}</span>
            </div>
            </h6>
            <div class="custom-control custom-switch custom-control-inline">
                <input checked type="checkbox" class="custom-control-input" onClick="handleQuill(${id},${a_id})" id=switch_quill_${a_id} data-id="${a_id}">
                <label class="custom-control-label" for="switch_quill_${a_id}"></label>
                <span class="switch-label">Activar/Desactivar</span>
            </div>
            <div data-id="${a_id}" id="antecedente_${a_id}" ></div>
        </fieldset>
        </div>
    `);
    
    quill('antecedente_'+a_id,a_id);
}
/**
 *
 * @param  {[type]} termino [id del quill]
 * @param  {[type]} id      [id del antecedente]
 * @return {[type]}         [eventos]
 */
function quill(termino,id){
  termino = new Quill('#'+termino, {
    theme: 'snow',
    id:id,
    placeholder:'Descripción'
  });
  termino.on('text-change', () => {
     antecedente_fild(termino.options.id,false,false);
  })
  let editor = {
      quill: termino,
      id: parseInt(id)
  }
  editores.push(editor);
}

function handleQuill(id,answer_id) {
    let inputSwitch = $(`#switch_quill_${answer_id}`);
    let editor = editores.find(e => e.id == answer_id);
    var status = true;
    if(!inputSwitch.prop("checked")){
        status = false
    }
    $.ajax({
        type: 'GET',
        url: '../changeStatusAntecedentes/'+id+"/"+status,
        success: function(res){
            if(inputSwitch.prop("checked")){
                $(`#antecedente_${answer_id}`).html(res.respuesta);
                quill('antecedente_'+answer_id,answer_id);
                $(`#antecedente_${answer_id}`).show();
                $(`.field_${answer_id} small`).text(res.respuesta);
                //alert('.field_'+answer_id+'.status).text((Positivo)');
                $(`.field_${answer_id} .status`).text("(Positivo)");
            }else{
                $(`.field_${answer_id} small`).text("");
                $(`.field_${answer_id} .status`).text("(Negativo)");
                editores = editores.filter(e => e.id !== answer_id);
                destory_editor(`#antecedente_${answer_id}`);
                $(`#antecedente_${answer_id}`).hide();
                status = false;
            }
        }
    });
}

function destory_editor(selector){
    if($(selector)[0])
    {
        var content = $(selector).find('.ql-editor').html();
        $(selector).html(content);

        $(selector).siblings('.ql-toolbar').remove();
        $(selector + " *[class*='ql-']").removeClass (function (index, css) {
           return (css.match (/(^|\s)ql-\S+/g) || []).join(' ');
        });

        $(selector + "[class*='ql-']").removeClass (function (index, css) {
           return (css.match (/(^|\s)ql-\S+/g) || []).join(' ');
        });
    }
    else
    {
        console.error('editor not exists');
    }
}


/**
 * cambio de texto en una de las actividades del antecedente
 * @param  {[type]} e [description]
 * @return {[type]}   [description]
 */
function antecedente_fild(id,validacion_nuevo,validacion_data){
    if(validacion_nuevo)
    {
        var lae = id;
        id = id.params.data.text;
    }else if(validacion_data){
        var lae = id;
        id = id.data.id;
    }

    html = $('#antecedente_'+id+' .ql-editor').text();
   
    object = new Object();
    object.html = html;
    object.curp = curp = $('#curp').val();
    object.answerId = id;
    let texto = id;
    object.formId = $('#keyForm').val();
    object.validacion = validacion_nuevo;
    $.ajax({
        type:'post',
        dataType:'json',
        data:object,
        url:'../antecedentesResult'
    }).done(res=>{
        object.answerId = res.antecedente.answerA_id;
        object.html = res.antecedente.respuesta;
        refreshText(object);
        if(validacion_nuevo)
        {
            lae.params.data.id = res.antecedente.answerA_id;
            params = new Object();
            params.id = res.antecedente.answerA_id;
            params.title = "";
            params.text = lae.params.data.text;
            params.disabled = false;
            params.selected = true;
            add_element(params,res.antecedente.answerA_id,res.antecedente.id);
            refreshAdd(params,res.antecedente.answerA_id);
            
            array_nuevos_tags[contador_nuevos_tags] = res.antecedente.answerA_id;
        }else if(validacion_data){
            add_element(lae.data,id,res.antecedente.id);
            refreshAdd(lae.data,id);
            actualiza_positivo(id);
           
            
        }

    }).fail(err=>{

    })
}
function actualiza_positivo(id){
  $(`.field_${id} .status`).text("(Positivo)");
}
function refreshAdd(obj,a_id) {

  formId = $('#keyForm').val();
  $('.alert_'+formId).hide();
  $('.form_'+formId+' .content_fields').append(
    `
    <div class="col-md-12 field_${a_id}">
      <h6 class="primary">
        <i class="feather icon-check"></i>
        ${obj.text}
        <span class="primary status"></span>
      </h6>
      <small class="secondary field_text"></small>
    </div>
    `);
}

function refreshText(obj) {
  //alert(obj.html);
  $('.field_'+obj.answerId+' .field_text').text(obj.html);
  $('.field_'+obj.answerId+' .status').text("(Positivo)");
  
}

/**
 * Eliminar una actividad del antecedente
 * @return {[type]} [description]
 */
function delete_element(id){
  object = new Object();
  object.curp = curp = $('#curp').val();
  object.answerId = id;
  object.formId = $('#keyForm').val();
  $.ajax({
    type:'post',
    dataType:'json',
    data:object,
    url:'../deleteAntecedente'
  }).done(res=>{
    $('.field_'+res.id).remove();
  }).fail(err=>{

  })
}


$(document).ready(function() {
  $('.cat_med').select2({
     ajax: {
       url: "../medicamentos",
       dataType: 'json',
       delay: 250,
       data: function (params) {
         return {
           term:params.term
          };

       },
       processResults: function (data) {
           return {
               results: $.map(data, function (item) {
                   return {
                       text: item.clave+' - '+ item.nombre +' - '+item.detalle,
                       id: item.clave,
                       name:item.nombre
                   }
               })
           };
       }
     },
     minimumInputLength: 2,
     language: "es",
     placeholder: "Medicamentos - Buscar nombre o codigo",
     searching:"Buscando",
     language: {
        noResults: function() {
          return "No hay resultados";
        },
        searching: function() {
         return "Buscando..";
        },
        inputTooShort: function () {
        return "Por favor ingresa 2 o mas letras"
        }
     },
  });

});


$(document).on('select2:select','.cat_med', function(e){
  $('.loadMedica').show();
   let data = e.params.data;
   medicamentoActivo(data);
})


function medicamentoActivo(data) {
  data.expediente_id = $('.expedienteId').val();
  $.ajax({
    url:'../medicamentoActivo',
    data: data,
    dataType:'json',
    type:'post',

  }).done(res=>{
    $('.loadMedica').hide();
    let validacion = $.trim($("#validacion_medicamentos").data('validacion'));
    let button = '';
    if(validacion == "true")
    {
      button =  '<a type="button" data-key="'+res.id+'" class="btn deleteM btn-danger btn-icon ml-auto waves-effect waves-light text-white">'+
          '<i class="feather icon-trash"></i>'+
        '</a>';
    }
    $('.content_medicamento').append(`

      <div id="medicamento_${res.id}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
        <div class="mr-50">
            <img src="https://img.icons8.com/cotton/48/000000/medical-history.png"/>
        </div>
        <div class="user-page-info">
          <h6 class="mb-0">
          ${res.nombre}
          </h6>
          <small class="block">${res.clave}</small>
          <span class="font-small-2">Ahora Mismo</span>
        </div>
        ${button}
      </div>
      `);
     $('.cat_med').val(null).trigger('change');
  }).fail(err=>{

  })
}


$(document).on('click','.deleteM', function(){
  var key = $(this).data('key');
  $.ajax({
    url:'../deleteMedicaActive/'+key,
    data: key,
    dataType:'json',
    type:'post',
    beforeSend:()=>{
      $('.loadMedica').show();
    }
  }).done(res=>{
    $('.loadMedica').hide();
    $('#medicamento_'+res.id).remove();
  }).fail(err=>{

  })
})

































