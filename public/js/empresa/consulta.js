$(document).ready(function(){
  id = $('#historial_id').val();
  $.ajax({
    type:'get',
    dataType:'json',
    url:'../getHistorial/'+id
  }).done(res=>{
    console.log(res);
    if (res.examenFisico != null && res.padecimiento != null ) {
      padecimientoFill(res.examenFisico,res.padecimiento);
    }else if (res.examenFisico == null && res.padecimiento != null) {
      padecimientoFill(null,res.padecimiento);
    }else if (res.examenFisico != null && res.padecimiento == null) {
      padecimientoFill(res.examenFisico,null);
    }else if (res.examenFisico == null && res.padecimiento == null) {
      padecimientoFill(null,null);
    }


    examenFill(res.examenFisico);
    diagnosticoFill(res);
    incapacidad(res.incapacidad);


  }).fail(err=>{
    console.log(err);
  })
});

function padecimientoFill(res,pad){
  if (res!=null) {
    $('#altura').val(res.altura);
    $('#peso').val(res.peso);
    $('#imc_label').val(res.imc);
    $('.imc_label').text(res.imc);
    $('#temperatura').val(res.temperatura);
    $('#fr').val(res.fr);
    $('#fc').val(res.fc);
    $('#saturacion').val(res.oxigeno);
  }
  if (pad!= null) {
    $('#razon .ql-editor').html(pad.razon);
    $('#sintomas .ql-editor').html(pad.sintomas);
    $('#exploracion .ql-editor').html(pad.exploracion);
  }

}

function examenFill(res) {
 console.log(res);
  if (res==null) {
    return
  }
  for(const item in res){
    if (res[item] != null || res[item] != undefined || res[item] != '' ) {
      if (item != 'updated_at' && item != 'created_at' &&  item != 'consulta_id' &&
      item != 'id' && item != 'altura' && item != 'peso' && item != 'imc' &&
      item != 'temperatura' && item != 'fr' && item != 'fc' && item != 'oxigeno') {
          examenFillElement(res[item],item);
      }
    }
  }

}
var  arraySelect = [];
function examenFillElement(value, index){
  if (value != '' && value != null && value != undefined) {
    arraySelect.push(index);
    $('.exploracion_select').val(arraySelect).trigger('change.select2');

    title = getTitle(index)
    exploracionTwo(index,title);
    $('#'+index).children('.ql-editor').html(value);
  }

}
function getTitle(name){
  switch (name) {
    case 'apariencia':
   return  'Apariencia general'
    case 'psBrazoDerecho':
  return  'Presión sanguínea - Brazo derecho'
    break
    case 'psBrazoIzquierdo':
  return  'Presión sanguínea - Brazo izquierdo'
    break
    case 'cabezaCueroOjos':
  return  'Cabeza y Ojos'
    break
    case 'oidoNarizBoca':
  return  'Oidos/Nariz/Boca'
    break
    case 'dientesFaringe':
  return  'Dientes/Faringe'
    break
    case 'cuello':
  return  'Cuello'
    break
    case 'tiroides':
  return  'Tiroides'
    break
    case 'nodoLinfatico':
  return  'Nodo Linfático'
    break
    case 'toraxPulmones':
  return  'Torax y Pulmones'
    break
    case 'pecho':
  return  'Pecho'
    break
    case 'corazon':
  return  'Corazón'
    break
    case 'abdomen':
  return  'Abdomen'
    break
    case 'rectalDigital':
  return  'Rectal Digital'
    break
    case 'genitales':
  return  'Genitales'
    break
    case 'columnaVertebral':
  return  'Columna vertebral'
    break
    case 'piel,Piel':
  return  'Piel'
    break
    case 'pulsoArterial':
  return  'Pulso Arteral'
    break
    case 'extremidades':
  return  'Extremidades'
    break
    case 'musculoesqueleto':
  return  'Musculoesqueletico'
    break
    case 'reflejosNeurologicos':
  return  'Reflejos y Neurológicos'
    break
    case 'estadoMental':
  return  'Estado Mental'
    break
    case 'ojoIzquierdo':
  return  'Ojo Izquierdo'
    break
    case 'ojoDerecho':
  return  'Ojo Derecho'
    break
    case 'diagnosticoAudio':
  return  'Diagnostico Audiologico'
    break
    case 'otrosComentarios':
  return  'Otro'
    break
    case 'fm':
  return  'Fuerza Muscular'
    break
    case 'rm':
  return  'Rango de Movimiento'
    break
    case 'dolor':
  return  'Dolor'
    break
  }
}
function exploracionTwo(id,text){
    $('#exploracion_section').append(`
       <div class="col-md-4 ${id}">
         <div class="card examen_fisico_card">
            <div class="card-header">
              <div class=" display-5" >${text}</div>
            </div>
            <div class="card-body">
              <div class="col-md-12">
                <div class=" form-control col-mfd-12 examen_fisico" id="${id}">
                </div>
              </div>
            </div>
          </div>
        </div>
      `);
     quill(id);
}
function quillTwo(termino){
  termino = new Quill('#'+termino, {
    theme: 'snow',
    placeholder:'Descripción'
  });
  termino.on('text-change', () => {
     examenSave();
  })
}
// NOTE: diagnostico
tratamiento = new Quill('.tratamiento', {
  theme: 'snow',
  placeholder: 'Tratamiento para el paciente...'
});
tratamiento.on('text-change', () => {
   consultaTratamiento();
})

tratamiento = new Quill('.notas', {
    theme: 'snow',
    placeholder: 'Notas para el paciente...'
});
tratamiento.on('text-change', () => {
    consultaTratamiento();
})
// diagnostico
function diagnosticoFill(res){
  if (res == null) {
    return
  }
  if (res.diagnostico) {
    arrayDiagnostico = [];
    res.diagnostico.forEach((item) => {
      $('.cie_card').append(`
        <div class="col-md-12 mt-1 elemetDiagnostico ${item.clave}" data-name="${item.nombre}" data-clave="${item.clave}">
          <fieldset class="form-label-group mb-50">
              <h6 class="display-6">
              <div class="badge badge-secondary mr-1 mb-1">
                <i class="fa fa-flask"></i>
                <span>${item.clave}</span>
              </div>
                ${item.nombre}
                <a class="deleteDiagostico float-right">
                  <i class="float-rigth feather icon-trash-2"></i>
                </a>
              </h6>
              <div  id="comment_${item.clave}" ></div>
           </fieldset>
         </div>
      `);
      quill_Diagnostico_two('comment_'+item.clave);
      $('#comment_'+item.clave).children('.ql-editor').html(item.comentario);
    });
  }
  if (res.medicamento) {
    console.log(res.medicamento);
    arrayMedicamento = [];
    res.medicamento.forEach((item) => {
      $('.medic_card').append(`
        <div class="col-md-4 mt-1 medi_${item.clave} elementMedicamento " data-name="${item.nombre}" data-clave="${item.clave}">
          <table class="table mb-0">
              <thead class="bg-secondary">
                  <tr>
                      <th scope="col" class="text-white" colspan="2">
                        <div class="badge badge-primary mr-1">
                          <i class="fa fa-flask"></i>
                          <span>${item.clave}</span>
                        </div>
                        ${item.nombre}
                        <a class="deleteMedicamento float-right">
                          <i class="float-rigth feather icon-trash-2"></i>
                        </a>
                      </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>Tomar</th>
                    <td>
                      <select class="selectMedicamento tomar_${item.clave}" style="width:100%">
                        <option value=""></option>
                        <option value="1/2 tableta">1/2 tableta</option>
                        <option value="1 tableta">1 tableta</option>
                        <option value="2 tableta">2 tableta</option>
                        <option value="1 ampolleta">1 ampolleta</option>
                        <option value="1 pastilla">1 pastilla</option>
                        <option value="2 pastilla">2 pastilla</option>
                        <option value="1 cápsula">1 cápsula</option>
                        <option value="2 cápsula">2 cápsula</option>
                        <option value="1 cucharada">1 cucharada</option>
                        <option value="1 gota">1 gota</option>
                        <option value="2 gotas">2 gotas</option>
                        <option value="2.4 Ml">2.4 Ml</option>
                        <option value="5 ML">5 ML</option>
                        <option value="10 ML">10 ML</option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <th>Frecuencia</th>
                    <td>
                      <select class="selectMedicamento fr_${item.clave}" style="width:100%">
                        <option value=""></option>
                        <option value="cada 4 horas">cada 4 horas</option>
                        <option value="cada 6 horas">cada 6 horas</option>
                        <option value="cada 8 horas">cada 8 horas</option>
                        <option value="cada 12 horas">cada 12 horas</option>
                        <option value="cada 24 horas">cada 24 horas</option>
                      </select>
                    </td>

                  </tr>
                  <tr>
                    <th>Duración</th>
                    <td>
                      <select class="selectMedicamento dia_${item.clave}" style="width:100%">
                        <option value=""></option>
                        <option value="3 días">3 días</option>
                        <option value="5 días">5 días</option>
                        <option value="7 días">7 días</option>
                        <option value="10 días">3 días</option>
                        <option value="15 días">15 días</option>
                        <option value="30 días">30 días</option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <th colspan="2">
                      <div class="form-control comentario_${item.clave}" id="comentario_med_${item.clave}"></div>
                    </th>
                  </tr>
              </tbody>
          </table>
        </div>
      `);
       $('.tomar_'+item.clave).val(item.tomar);
       $('.fr_'+item.clave).val(item.frecuencia);
       $('.dia_'+item.clave).val(item.duracion);
      styleMedicamento();
      quill_Diagnostico_two('comentario_med_'+item.clave);
      $('#comentario_med_'+item.clave).children('.ql-editor').html(item.comentario);
    });

  }
  if (res.procedimiento) {
    arrayProcedimiento = [];
    res.procedimiento.forEach((item) => {
      $('.proc_card').append(`
        <div class="col-md-12 mt-1 elemetProcedimietno proc_${item.clave}" data-name="${item.nombre}" data-clave="${item.clave}">
          <fieldset class="form-label-group mb-50">
              <h6 class="display-6">
              <div class="badge badge-secondary mr-1 mb-1">
                <i class="fa fa-flask"></i>
                <span>${item.clave}</span>
              </div>
                ${item.nombre}
                <a class="deleteProcedimiento float-right">
                  <i class="float-rigth feather icon-trash-2"></i>
                </a>
              </h6>
              <div  id="comment_proc_${item.clave}" ></div>
           </fieldset>
         </div>
      `);
      quill_Diagnostico_two('comment_proc_'+item.clave);
      $('#comment_proc_'+item.clave).children('.ql-editor').html(item.comentario);
    });
  }
  if (res.discapacidad) {
    arrayDiscapacidad = [];
    res.discapacidad.forEach((item) => {
      $('.disc_card').append(`
        <div class="col-md-12 mt-1 elementDiscapaciad disc_${item.clave}" data-name="${item.nombre}" data-clave="${item.clave}">
          <fieldset class="form-label-group mb-50">
              <h6 class="display-6">
              <div class="badge badge-secondary mr-1 mb-1">
                <i class="fa fa-flask"></i>
                <span>${item.clave}</span>
              </div>
                ${item.nombre}
                <a class="deleteDiagostico float-right">
                  <i class="float-rigth feather icon-trash-2"></i>
                </a>
              </h6>
              <div  id="disc_coment_${item.clave}" ></div>
           </fieldset>
         </div>
      `);
      quill_Discapacidad('disc_coment_'+item.clave);
      $('#disc_coment_'+item.clave).children('.ql-editor').html(item.comentario);
    });
  }
  if (res.tratamiento) {
    $('.tratamiento').children('.ql-editor').html(res.tratamiento.tratamiento);
    $('.notas').children('.ql-editor').html(res.tratamiento.notas);

  }

}

function styleMedicamento() {
  $('.selectMedicamento').select2({
    language: "es",
    searching:"Buscando",
    language: {
       noResults: function() {
         return "No hay resultados";
       },
       searching: function() {
        return "Buscando..";
       },
       inputTooShort: function () {
       return "Por favor ingresa 2 o mas letras"
       }
    },
  });
}

function quill_Diagnostico_two(termino){
  termino = new Quill('#'+termino, {
    theme: 'snow',
    placeholder:'Descripción'
  });

  termino.on('text-change', () => {
     consultaDiagnostico();
  })
}


/**
 * Agrega informacion de campos que ya hayan sido registrados
 * @param       {[type]} res [campo a verificar ]
 * @constructor
 */
function incapacidad(res) {
  if (!res) return false;
  console.log(res);
  $('#in_inicial').val(res.fechaInicial);
  $('#in_final').val(res.fechaFinal);
  $('#in_motivo').val(res.motivo);
  $('.quillIncapacidad .ql-editor').html(res.observacion);
  if (res.estado == 1)  $('#incapacidadCheck').prop('checked',true);
}

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
var razon = new Quill('#razon', {
  theme: 'snow',

  placeholder: 'Escribe la razón de la visita...'
});

$('#razon_btn').on('click', (arguments) => {
  var text = quill.getText(0, 1000);
  console.log(text);
})

var sintomas = new Quill('#sintomas', {
  theme: 'snow',
  placeholder: 'Escribe los sintomas del paciente...'
});

$('#sintomas_btn').on('click', (arguments) => {
  var text = quill.getText(0, 1000);
  console.log(text);
})

var exploracion = new Quill('#exploracion', {
  theme: 'snow',
  placeholder: 'Escribe la exploración física...'
});

$('#exp_btn').on('click', (arguments) => {
  var text = quill.getText(0, 1000);
  console.log(text);
})

$(".imc").keyup(function(event) {

  const altura = $('#altura').val();
  const peso = $('#peso').val();
  if (altura != '' && peso !='') {
    const imc = Math.round(peso * 10 / altura / altura) / 10;
    $('.imc_label').text(imc)
    $('#imc_label').val(imc)
  }


});

// NOTE: Ocultar sipinners
$('.spinner').hide();

razon.on('text-change', ()=>{
 padecimientos();
 $('.razon_spinn').show();
});

sintomas.on('text-change', () => {
   $('.sintomasSpinn').show();
   padecimientos();
})
exploracion.on('text-change', () => {
   $('.efSpinn').show();
   padecimientos();
})

function padecimientos(razon,sintomas,exploracion){
  padecimiento = new Object();
  padecimiento.razon = $('#razon .ql-editor').html();
  padecimiento.sintomas = $('#sintomas .ql-editor').html();
  padecimiento.exploracion = $('#exploracion .ql-editor').html();
  padecimiento.idHistorial = $('#historial_id').val();

  $.ajax({
    type:'post',
    dataType:'json',
    data:padecimiento,
    url:'../padecimiento',
  }).done((res)=>{
    $('.spinner').hide();
  }).fail(err=>{
    console.log(err);
  })
}


$('#sintomas').on('text-change', ()=>{
  console.log('holamundo');
});
