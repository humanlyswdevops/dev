events();

function events() {
 loadEstudio();
 dataTablesLoad();
}

function loadEstudio() {
    $('.links').on('click',function(e) {
        $('i').removeClass('active');
        $(this).parent().find('i').addClass('active');
        $('#selected').text($(this).text());
        e.preventDefault();
        var id = $(this).data('value');
        ajaxEstudio(id);
    });
}

function ajaxEstudio(id) {
    $.ajax({
        type:'get',
        dataType:'json',
        url:"Estudios_get/" + id,
        success: function(datas) {
            var dataSet = [];
            $.each(datas,function(index, value) {
                dataSet.push([
                    datas[index].nombre,
                    datas[index].duracion + ' días',
                    '$' + datas[index].costo,
                    '<label class="' + datas[index].estado + '">'+ datas[index].estado + '</label>',
                    '<button class="btn btn-secondary btn-sm" onclick="showModal(' + datas[index].id + ')">Editar <i class="fa fa-edit"></i></button>'
                ]);
            });

            $('#table_estudios').dataTable().fnDestroy();

            $('#table_estudios').dataTable({
                data: dataSet,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        }
    });
}

function dataTablesLoad() {
    var dataSets = [
        ["Nombre", "Duración", "Costo", "Estado", "Modificar"]
    ];

    $(document).ready(function() {
        $('#table_estudios').dataTable( {
            data:dataSets,
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
    });
}

function showContent() {
    var dv = document.getElementById('someSwitchOptionInfo');
    if (dv.checked) {
        $(".alta").css("display", "block");
        $(".lista").css("display", "none");
    }
    else {
        $(".lista").css("display", "block");
        $(".alta").css("display", "none");
    }
}

function showModal(id) {
    $.ajax({
        type:'get',
        dataType:'json',
        url:"Estudio/" + id,
        success: function(response) {
            document.querySelector('#inputId').value = response.id;
            document.querySelector('#inputEstudio').value = response.nombre;
            document.querySelector('#inputCodigo').value = response.codigo;
            document.querySelector('#inputDescripcion').value = response.descripcion;
            document.querySelector('#inputRequisitos').value = response.requisitos;
            document.querySelector('#inputDuracion').value = response.duracion;
            document.querySelector('#inputCosto').value = response.costo;

            var select = document.getElementById("inputState");

            for (var i = 1; i < select.length; i++) {
                if (select.options[i].value == response.statu_id) {
                    select.selectedIndex = i;
                }
            }
            $('#myModal').modal('show');
        }
    });
}

$('#estudio_form').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        type:'POST',
        url:'updateEstudio',
        dataType:'text',
        data: $('#estudio_form').serialize(),
        success: function(response) {
            setTimeout(() => {
                $("#myModal").modal('hide');
                $('#alerts').html('');
            }, 1500);
            tpl = ` <div class="alert alert-success" role="alert">
                La modificación se realizó correctamente
                </div>`;
            $('#alerts').html(tpl);
            var id = $('.active').parent().find('a').data('value');
            ajaxEstudio(id);
        }
    }).fail(function () {
        setTimeout(() => {
            $('#alerts').html('');
        }, 3500);
        tpl = ` <div class="alert alert-danger" role="alert">
            Revise que todos los campos estén llenos y utilice el tipo de dato correcto.
            </div>`;
        $('#alerts').html(tpl);
    });
});

$('#altaEstudio').on('submit', function(e) {
    e.preventDefault();
    // var categoria = document.querySelector('i.active');

    // if (categoria) {
        // var categoria_id = $('.active').parent().find('a').data('value');
        $.ajax({
            type:'post',
            url:'altaEstudio',
            dataType:'json',
            // data:$('#altaEstudio').serialize() + '&categoria_id=' + categoria_id,
            data:$('#altaEstudio').serialize(),
            success: function(response) {
                setTimeout(() => {
                    $("#crearEstudio").modal('hide');
                    $('#alert_alta').html('');
                }, 3500);
                tpl = ` <div class="alert alert-success" role="alert">
                El estudio se registró correctamente.
                </div>`;
                $('#alert_alta').html(tpl);
                ajaxEstudio(id);
                document.querySelector('#altaEstudio').reset();
            }
        })
        .fail(function () {
            setTimeout(() => {
                $('#alert_alta').html('');
            }, 3500);
            tpl = ` <div class="alert alert-danger" role="alert">
                Revise que todos los campos estén llenos y utilice el tipo de dato correcto.
                </div>`;
            $('#alert_alta').html(tpl);
        });
    // }
    // else {
    //     alert('Selecciona la categoría');
    // }
});
