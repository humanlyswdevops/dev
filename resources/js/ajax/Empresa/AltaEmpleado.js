$(document).ready( function() {
    $("#inputNuevoGrupo").hide();
});



function htds(res){
  data = new Object();
  data.nombre = res.nombre;
  data.a_paterno = res.apellido_paterno;
  data.a_materno = res.apellido_materno;
  data.correo = res.email;
  data.fec_nacimiento = res.fecha_nacimiento;
  data.telefono = res.telefono;
  data.sexo = res.genero;
  data.curp = res.CURP;
  data.estatus = 1;
  data.claveTrabajador = res.clave;


  $.ajax({
      type:'post',
      dataType:'json',
      url:'https://comerdis.ac-labs.com.mx/TalentoHumano/guardaEmpleado.php',
      data:data,
  }).done( function(response){
    console.log(response);
  });
}



$('#alta').on('submit',function(e) {
    e.preventDefault();
    $("#btn_enviar").prop("disabled", true);

    $.ajax({
        type:'post',
        dataType:'json',
        url:'altaEmpleados',
        data:$('#alta').serialize()
    }).done( function(response){

        if (response.empresa_id == 8) {
          htds(response);
        }




        document.querySelector('#alta').reset();
        $('.modal-body').animate({
            scrollTop: '0px'

        },100);

        tpl = ` <div class="alert alert-success" role="alert">
             El empleado se registró correctamente
            </div>`;

        $('#alerts').html(tpl);
        $("#btn_enviar").prop("disabled", false);

        setTimeout(() => {
            $('#alerts').html('');

        }, 4500);

        setTimeout(() => {
            $('#myModal').modal('hide');
        }, 4550);

        $("#inputNuevoGrupo").hide();
        $("#inputNuevoGrupo").prop("disabled", true);

        if (response.nuevo_grupo) {
            console.log(response)
            document.querySelector("#opciones-grupos").innerHTML += `<option value="${response.grupo_id}">${response.nuevo_grupo}</option>`
        }


    }).fail(function(error) {
        var message;
        if(error.status == 422){
            message = "Algo ha ocurrido, verifica que todos los campos sean correctos, y que haya seleccionado un grupo";
        }
        else if (error.status == 400){
            message = "La curp ingresada ya se encuentra registrado, intenta con otra";
        }
        else if(error.status == 424){
            message = "El correo ingresado ya se encuentra registrado, intenta con otra";
        }
        else if(error.status == 423){
            message = "La clave ingresada ya se encuentra registrada, intenta con otra";
        }
        else if(error.status == 418){
            message = "El grupo que ya existe. Por favor selecciónelo de la lista de grupos o elija otro nombre";
        }
        else{
            message = "Algo ocurrió, inténtalo más tarde...";
        }
        console.log(error.status );
        $('.modal-body').animate({
            scrollTop: '0px',
        }, 100);

        tpl = ` <div class="alert alert-danger" role="alert">
                ${message}
            </div>`;
        $('#alerts').html(tpl);
        $("#btn_enviar").prop("disabled", false);
    });

});

$("#grupo").change( function() {
    if ($(this).val() === "Otro") {
        $("#inputNuevoGrupo").show();
        $("#inputNuevoGrupo").prop("disabled", false);
    } else {
        $("#inputNuevoGrupo").hide();
        $("#inputNuevoGrupo").prop("disabled", true);
    }
});


/**
 * Registrar a un nuevo paciente a sass
 * @param  {[json]} res [respuesta del paciente que recien fue registrado
 * en la base de datos]
 * @return {[json]}     [retorna el codigo id (nim) del paciente]
 */
function sass(res) {
    if (res.genero == 'Masculino') {
      res.genero = 'M'
    }else {
      res.genero = 'F';
    }
    var json_sass = new Object();
    json_sass.patient = {
        id:null,
        first_name:res.nombre,
        last_name:res.apellido_paterno+' '+res.apellido_materno,
        birth_date:res.fecha_nacimiento,
        email:res.email,
        phone:res.telefono,
        gender:res.genero
    };
    json_sass.company="466",
    json_sass.doctor="299",
    json_sass.diagnose="Nutrición alterada",
    json_sass.observations="Paciente en ayuno",
    json_sass.client="ael",
    json_sass.user_id="1",
    json_sass.branch_id="1",
    json_sass.studies=[1,3],
    json_sass.payment={
        amount:0,
        payment_type:0,
        reference:0
    };
    data = JSON.stringify(json_sass);
    $.ajax({
        async:true,
        cache:false,
        type: 'POST',
        url  : 'http://sassdev.dyndns.org/ws/',
        data : data,
        dataType: 'json'
    }).done(function(recupera){
      console.log(recupera);
    }).fail(function(){
      console.log('Error al cargar.');
    });

}
//
// function estudios() {
//   var respuesta;
//   $.ajax({
//       async:true,
//       dataType:'json',
//       type: 'get',
//       url  : 'estudios_get_update',
//   }).done(function(recupera){
//     $.ajax({
//         async:true,
//         dataType:'json',
//         type: 'get',
//         url  : 'http://sassdev.dyndns.org/ws/tests.php?client=ael',
//     }).done(function(res){
//       sass_array=res;
//       has_array =recupera;
//       todo=[];
//       recupera.forEach((has, i) => {
//           res.forEach((sass, si) => {
//             if (has.nombre == sass.test) {
//               has_array[i].sass_code = sass.test_code;
//               has_array[i].sass_price = sass.price;
//               sass_array.splice(si,1);
//               todo.push(has_array[i]);
//               data = {
//                 id:has.id,
//                 code : sass.test_code,
//                 costo: sass.price
//               }
//
//             }
//
//           });
//
//       });
//       has_save(JSON.stringify(todo))
//       console.log(has_array);
//       console.log(sass_array);
//
//     })
//
//
//
//   }).fail(function(){
//     console.log('Error al cargar.');
//   });
//
// }
//
//
// function has_save(data) {
//   da = JSON.parse(data);
//   da.forEach((item, i) => {
//     data = {
//       id:item.id,
//       code,
//       costo,
//     }
//     $.ajax({
//         async:true,
//         dataType:'json',
//         type: 'post',
//         data:data,
//         url:'has'
//     }).done(function(res){
//         console.log(res);
//       }).fail(err=>{
//         console.log(err);
//       });
//   });
//
//
//
// }
