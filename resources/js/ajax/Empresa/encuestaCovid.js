
$(".number-tab-steps").steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  enableAllSteps: true,
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
    finish: 'Guardar',
    previous: 'Anterior',
    next: 'Siguiente'
  },
  onFinished: function(event, currentIndex) {
    swal({
      title: "Confirmación de envio",
      text: "¿Seguro que desea terminar con la encuesta?",
      icon: "warning",
      buttons: {
        cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
        },
        confirm: {
          text: "Guardar",
          value: null,
          visible: false,
          className: "",
          closeModal: false
        }
      }
    }).then(isConfirm => {
      if (isConfirm) {
        $('#form').submit();

      } else {
        swal("Operacion Cancelada", "Puede continuar...", "error");
      }
    });

  }

});
$(".number-tab-stepssVer").steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  enableAllSteps: true,
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
    finish: 'Modificar',
    previous: 'Anterior',
    next: 'Siguiente'
  },
  onFinished: function(event, currentIndex) {
    swal('Atención','Sin privilegios para modificar','info')
  }

});
function redireccionar()
{

}
$("#form").on('submit', (e) => {
  e.preventDefault();
  $.ajax({
    type: 'POST',
    dataType: 'json',
    data: $("#form").serialize(),
    url: '../GuardarEncuesta'
  }).done((res) => {
    $('#wizard').find('a[href="#finish"]').remove();
    swal({
      position: 'top-end',
      icon: 'success',
      title: 'Encuesta guardada correctamente',
      showConfirmButton: true
    }).then(isConfirm => {
      if (isConfirm) {
        redireccionar();
      }
    });
  }).fail((err) => {
    if (err.status == 0) {
      swal("Algo ha ocurrido", "Verifica tu conexión de internet", "info");
    } else {
      swal("Algo ha ocurrido", "Verifica que todos los campos sean correctos...", "error");
    }
  });
});
