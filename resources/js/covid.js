  $('#example').DataTable({
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "Mostrar _MENU_ Entradas",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar:",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }
});

function covid(curp)
{
  window.location.href="Covid/"+curp;
}
// Se obtienen los datos de cantidades de empleados por género.
$.ajax({
  type: 'get',
  dataType: 'json',
  url: "getDatosGeneros",
  success: function(generos) {
    var hombres = generos.hombres;
    var mujeres = generos.mujeres;

    Highcharts.chart('container-generos', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Personal por género'
      },
      credits: {
        enabled: false
      },
      subtitle: {
        text: 'Total: ' + (hombres + mujeres)
      },
      tooltip: {
        pointFormat: 'Porcentaje: <b>{point.y:.1f}%</b><br>Cantidad: <b>{point.y2}</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.y:.1f} %'
          }
        }
      },
      series: [{
        name: 'Género',
        colorByPoint: true,
        data: [{
          name: 'Hombres',
          y: hombres / (hombres + mujeres) * 100,
          y2: hombres,
          color: '#0000ff'
        }, {
          name: 'Mujeres',
          y: mujeres / (hombres + mujeres) * 100,
          y2: mujeres,
          color: '#ff0044'
        }]
      }]
    });
  }
});

$.ajax({
  type: 'get',
  dataType: 'json',
  url: "get_info_covid",
  success: function(response) {
    var res = response.resultados;

    var positivo = res.positivo;
    var negativo = res.negativo;

    Highcharts.chart('container-covid', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Resultados'
      },
      credits: {
        enabled: false
      },
      subtitle: {
        text: 'Total: ' + (positivo + negativo)
      },
      tooltip: {
        pointFormat: 'Porcentaje: <b>{point.y:.1f}%</b><br>Cantidad: <b>{point.y2}</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.y:.1f} %'
          }
        }
      },
      series: [{
        name: 'Resultados',
        colorByPoint: true,
        data: [{
          name: 'Negativos',
          y: negativo / (positivo + negativo) * 100,
          y2: negativo,
          color: '#0000ff'
        }, {
          name: 'Positivos',
          y: positivo / (positivo + negativo) * 100,
          y2: positivo,
          color: '#ff0044'
        }]
      }]
    });
    get_genero(response);
    get_genero_edad_hombre(response);
    get_genero_edad_mujer(response);
  }
});


function get_genero(response) {
  console.log(response);

  var res = response.generos;

  var mujer = res.mujer;
  var hombre = res.hombre;

  Highcharts.chart('container-covid-genero', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Positivo a Covid por genero'
    },
    credits: {
      enabled: false
    },
    subtitle: {
      text: 'Total: ' + (mujer + hombre)
    },
    tooltip: {
      pointFormat: 'Porcentaje: <b>{point.y:.1f}%</b><br>Cantidad: <b>{point.y2}</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.1f} %'
        }
      }
    },
    series: [{
      name: 'Resultados',
      colorByPoint: true,
      data: [{
        name: 'Hombres',
        y: hombre / (mujer + hombre) * 100,
        y2: hombre,
        color: '#0000ff'
      }, {
        name: 'Mujeres',
        y: mujer / (mujer + hombre) * 100,
        y2: mujer,
        color: '#ff0044'
      }]
    }]
  });
}

function get_genero_edad_hombre(response) {
  console.log(response);

  var res = response.ragoedadHombre;

  var edad_Hombre18_20 = res.edad_Hombre18_20;
  var edad_Hombre20_24 = res.edad_Hombre20_24;
  var edad_Hombre25_44 = res.edad_Hombre25_44;
  var edad_Hombre45_49 = res.edad_Hombre45_49;
  var edad_Hombre50_59 = res.edad_Hombre50_59;
  var edad_Hombre60_64 = res.edad_Hombre60_64;
  var edad_Hombre65 = res.edad_Hombre65;

  Highcharts.chart('container-covid-edad_hombre', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Positivo a Covid por rango de edad y genero masculino'
    },
    credits: {
      enabled: false
    },
    subtitle: {
      text: 'Total: ' + (
        edad_Hombre18_20 +
        edad_Hombre20_24 +
        edad_Hombre25_44 +
        edad_Hombre45_49 +
        edad_Hombre50_59 +
        edad_Hombre60_64 +
        edad_Hombre65)
    },
    tooltip: {
      pointFormat: 'Porcentaje: <b>{point.y:.1f}%</b><br>Cantidad: <b>{point.y2}</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.1f} %'
        }
      }
    },
    series: [{
      name: 'Resultados',
      colorByPoint: true,
      data: [
        {
          name: 'Edad 18-20',
          y: edad_Hombre18_20 / (
            edad_Hombre18_20 +
            edad_Hombre20_24 +
            edad_Hombre25_44 +
            edad_Hombre45_49 +
            edad_Hombre50_59 +
            edad_Hombre60_64 +
            edad_Hombre65) * 100,
          y2: edad_Hombre18_20,
          color: '#A69F2D'
        },
        {
          name: 'Edad 20-24',
          y: edad_Hombre20_24 / (
            edad_Hombre18_20 +
            edad_Hombre20_24 +
            edad_Hombre25_44 +
            edad_Hombre45_49 +
            edad_Hombre50_59 +
            edad_Hombre60_64 +
            edad_Hombre65) * 100,
          y2: edad_Hombre20_24,
          color: '#FF5733'
        },
        {
          name: 'Edad 25-44',
          y: edad_Hombre25_44 / (
            edad_Hombre18_20 +
            edad_Hombre20_24 +
            edad_Hombre25_44 +
            edad_Hombre45_49 +
            edad_Hombre50_59 +
            edad_Hombre60_64 +
            edad_Hombre65) * 100,
          y2: edad_Hombre25_44,
          color: '#642DA6'
        },
        {
          name: 'Edad 45-49',
          y: edad_Hombre45_49 / (
            edad_Hombre18_20 +
            edad_Hombre20_24 +
            edad_Hombre25_44 +
            edad_Hombre45_49 +
            edad_Hombre50_59 +
            edad_Hombre60_64 +
            edad_Hombre65) * 100,
          y2: edad_Hombre45_49,
          color: '#A62D43'
        },
        {
          name: 'Edad 50-59',
          y: edad_Hombre50_59 / (
            edad_Hombre18_20 +
            edad_Hombre20_24 +
            edad_Hombre25_44 +
            edad_Hombre45_49 +
            edad_Hombre50_59 +
            edad_Hombre60_64 +
            edad_Hombre65) * 100,
          y2: edad_Hombre50_59,
          color: '#A22DA6'
        },
        {
          name: 'Edad 60-64',
          y: edad_Hombre60_64 / (
            edad_Hombre18_20 +
            edad_Hombre20_24 +
            edad_Hombre25_44 +
            edad_Hombre45_49 +
            edad_Hombre50_59 +
            edad_Hombre60_64 +
            edad_Hombre65) * 100,
          y2: edad_Hombre60_64,
          color: '#A22DA6'
        },
        {
          name: 'Edad 65+',
          y: edad_Hombre65 / (
            edad_Hombre18_20 +
            edad_Hombre20_24 +
            edad_Hombre25_44 +
            edad_Hombre45_49 +
            edad_Hombre50_59 +
            edad_Hombre60_64 +
            edad_Hombre65) * 100,
          y2: edad_Hombre65,
          color: '#A6892D'
        }
      ]
    }]
  });
}

function get_genero_edad_mujer(response) {
  console.log(response);

  var res = response.ragoedadMujer;

  var edad_Mujer18_20 = res.edad_Mujer18_20;
  var edad_Mujer20_24 = res.edad_Mujer20_24;
  var edad_Mujer25_44 = res.edad_Mujer25_44;
  var edad_Mujer45_49 = res.edad_Mujer45_49;
  var edad_Mujer50_59 = res.edad_Mujer50_59;
  var edad_Mujer60_64 = res.edad_Mujer60_64;
  var edad_Mujer65 = res.edad_Mujer65;

  Highcharts.chart('container-covid-edad_mujer', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Positivo a Covid por rango de edad y genero femenino'
    },
    credits: {
      enabled: false
    },
    subtitle: {
      text: 'Total: ' + (
        edad_Mujer18_20 +
        edad_Mujer20_24 +
        edad_Mujer25_44 +
        edad_Mujer45_49 +
        edad_Mujer50_59 +
        edad_Mujer60_64 +
        edad_Mujer65)
    },
    tooltip: {
      pointFormat: 'Porcentaje: <b>{point.y:.1f}%</b><br>Cantidad: <b>{point.y2}</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y:.1f} %'
        }
      }
    },
    series: [{
      name: 'Resultados',
      colorByPoint: true,
      data: [
        {
          name: 'Edad 18-20',
          y: edad_Mujer18_20 / (
            edad_Mujer18_20 +
            edad_Mujer20_24 +
            edad_Mujer25_44 +
            edad_Mujer45_49 +
            edad_Mujer50_59 +
            edad_Mujer60_64 +
            edad_Mujer65) * 100,
          y2: edad_Mujer18_20,
          color: '#A69F2D'
        },
        {
          name: 'Edad 20-24',
          y: edad_Mujer20_24 / (
            edad_Mujer18_20 +
            edad_Mujer20_24 +
            edad_Mujer25_44 +
            edad_Mujer45_49 +
            edad_Mujer50_59 +
            edad_Mujer60_64 +
            edad_Mujer65) * 100,
          y2: edad_Mujer20_24,
          color: '#FF5733'
        },
        {
          name: 'Edad 25-44',
          y: edad_Mujer25_44 / (
            edad_Mujer18_20 +
            edad_Mujer20_24 +
            edad_Mujer25_44 +
            edad_Mujer45_49 +
            edad_Mujer50_59 +
            edad_Mujer60_64 +
            edad_Mujer65) * 100,
          y2: edad_Mujer25_44,
          color: '#642DA6'
        },
        {
          name: 'Edad 45-49',
          y: edad_Mujer45_49 / (
            edad_Mujer18_20 +
            edad_Mujer20_24 +
            edad_Mujer25_44 +
            edad_Mujer45_49 +
            edad_Mujer50_59 +
            edad_Mujer60_64 +
            edad_Mujer65) * 100,
          y2: edad_Mujer45_49,
          color: '#A62D43'
        },
        {
          name: 'Edad 50-59',
          y: edad_Mujer50_59 / (
            edad_Mujer18_20 +
            edad_Mujer20_24 +
            edad_Mujer25_44 +
            edad_Mujer45_49 +
            edad_Mujer50_59 +
            edad_Mujer60_64 +
            edad_Mujer65) * 100,
          y2: edad_Mujer50_59,
          color: '#A22DA6'
        },
        {
          name: 'Edad 60-64',
          y: edad_Mujer60_64 / (
            edad_Mujer18_20 +
            edad_Mujer20_24 +
            edad_Mujer25_44 +
            edad_Mujer45_49 +
            edad_Mujer50_59 +
            edad_Mujer60_64 +
            edad_Mujer65) * 100,
          y2: edad_Mujer60_64,
          color: '#A22DA6'
        },
        {
          name: 'Edad 65+',
          y: edad_Mujer65 / (
            edad_Mujer18_20 +
            edad_Mujer20_24 +
            edad_Mujer25_44 +
            edad_Mujer45_49 +
            edad_Mujer50_59 +
            edad_Mujer60_64 +
            edad_Mujer65) * 100,
          y2: edad_Mujer65,
          color: '#A6892D'
        }
      ]
    }]
  });

}
