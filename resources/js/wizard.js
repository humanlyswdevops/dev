$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
    // /****Cargar imagen */
    $(function() {
        $('#logo').change(function(e) {
            addImage(e); 
           });
      
           function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;
          
            if (!file.type.match(imageType))
             return;
        
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
           }
        
           function fileOnload(e) {
            var result=e.target.result;
            $('#load_image').attr("src",result);
           }
          });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
/*Selected del giro*/
$( function() {
    $("#giro").hide();
    $("#id_categoria").change( function() {
        if ($(this).val() === "Otro") {
            $("#giro").show();
            $("#giro").prop("disabled", false);
        } else {
            $("#giro").hide();
            $("#giro").prop("disabled", true);
        }
    });
});

function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}



