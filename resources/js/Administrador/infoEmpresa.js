setInterval(function() {
  $('.alert-success').hide(700, "swing");
}, 3000);

setInterval(function() {
  $('.alert-danger').hide(700, "swing");
}, 3000);

$('#logo').change(function(e) {
  addImage(e);
});

function addImage(e) {
  var file = e.target.files[0],
    imageType = /image.*/;

  if (!file.type.match(imageType))
    return;

  var reader = new FileReader();
  reader.onload = fileOnload;
  reader.readAsDataURL(file);
}

function fileOnload(e) {
  var result = e.target.result;
  $('#load_image').attr("src", result);
}

function showModalLogotipo() {
  $('#load_image').attr("src", $('#logo_actual').attr('src'));

  $('#modalLogotipo').modal('show');
}

function showEstudiosModal(encryptedID) {
  $.ajax({
    type: 'get',
    dataType: 'json',
    url: "../getEstudiosEmpresa/" + encryptedID,
    success: function(data) {
      var estudiosEmpresa = JSON.parse(JSON.stringify(data));
      console.log(estudiosEmpresa);

      for (estudio of estudiosEmpresa) {
        $('#estudio' + estudio.estudio_id).attr('checked', true);
      }
      $('#estudiosModal').modal('show');
    }
  });
}

$(document).ready(function() {
  $('.estudios_input').hide();
  $('.links').on('click', function(e) {
    $('.estudios_input').hide();
    $('i').removeClass('actives');
    $(this).parent().find('i').addClass('actives');
    $('#selected').text($(this).text());
    var id = $(this).data('nombre');
    ajaxEstudio(id);
  });
});


function ajaxEstudio(id) {
  var cont = '.' + id;
  $(cont).show();
}

function EmpresaAdmin(nombreEmpresa) {

  window.location.href = "../EmpresaAdmin/"+nombreEmpresa;
}
/*seleccionar todos*/
$(document).ready(function() {
  selected = true;
  $('#BtnSeleccionar').click(function() {
    if (selected) {
      $('.estudios_input input[type=checkbox]').prop("checked", true);
      $('#BtnSeleccionar').val('Deseleccionar');

    } else {
      $('.estudios_input input[type=checkbox]').prop("checked", false);
      $('#BtnSeleccionar').val('Seleccionar todo');
    }
    selected = !selected;
  });
});
