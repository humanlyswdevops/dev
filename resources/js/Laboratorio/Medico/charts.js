
$(window).on("load", function(){
    require.config({
        paths: {
            echarts: '../../../app-assets/vendors/js/charts/echarts'
        }
    });
    $(".search").keyup( function() {
        var contador = 0;
        var rex = new RegExp($(this).val(), 'i');
        $(".pacientes").hide();
        $(".pacientes").filter( function() {
            if (rex.test($(this).text()))
            contador ++;
            return rex.test($(this).text());
        }).show();
    });

});

$.ajax({
  type:'get',
  url:'../NumPacientes',
  dataType:'json'
}).done((res)=>{
  console.log(res);
  var mujeres = 0;
  var hombres = 0;

  res.forEach((item, i) => {
     if(item.genero.toLowerCase()=='femenino'){
       mujeres=item.cantidad;
     }else {
       hombres=item.cantidad;
     }
  });
  $('#mujeres').text(mujeres);
    $('#hombres').text(hombres);
      $('#total').text(mujeres+hombres);
  require(
      [
        'echarts',
        'echarts/chart/pie',
        'echarts/chart/funnel'
      ],
      function (ec) {
          var myChart = ec.init(document.getElementById('task-pie-chart'));
          chartOptions = {
              tooltip: {
                  trigger: 'item',
                  formatter: "{a} <br/>{b}: {c} ({d}%)"
              },
              legend: {
                  orient: 'horizontal',
                  x: 'left',
                  data: ['Mujeres', 'Hombres']
              },
              color: ['#434fa5', '#8666d7'],
              toolbox: {
                  show: true,
                  orient: 'horizontal',
                  //Enable if you need
                  feature: {
                      restore: {
                          show: true,
                          title: 'Restore'
                      }
                  }
              },
              calculable: true,
              series: [{
                  name: 'Encontrados',
                  type: 'pie',
                  radius: '70%',
                  center: ['50%', '57.5%'],
                  data: [
                      {value: mujeres, name: 'Mujeres'},
                      {value: hombres, name: 'Hombres'}
                  ]
              }]
          };
          myChart.setOption(chartOptions);
          $(function () {
              $(window).on('resize', resize);
              $(".menu-toggle").on('click', resize);
              function resize() {console.log('holamundo');
                  setTimeout(function() {
                      myChart.resize();
                  }, 200);
              }
          });
      }
  );
}).fail((err)=>{
  console.log(err);
    alert('Se ha detectado un problema con los pacientes');
});
