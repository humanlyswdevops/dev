$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
function Modal(curp,id_estudio,ingreso){
    $('#estudios').modal('show');
    $('#estudio').val(id_estudio);
    $('#ingreso').val(ingreso);
}
   var i=1;
function addUrl(){
  if(i==6){
    alert('limite de 5 links por estudio');
    return;
  }
   $(".addUrl").append('<input type="url" class="form-control mt-2" placeholder="Enlaces a documentos del estudio en SASS" name="documentos'+i+'[]" required>');
   $(".checkbox").append(`
     <label class="pt-1 pb-2">
     <input data-toggle="tooltip" data-placement="top" title="Compartir Documentos" name="documentos${i}[]"  type="checkbox" checked style="display: none">
     </label>
     `);
   i++;
}

$('#formuploadajax').on('submit',e=>{
  e.preventDefault();
  $.ajax({
    dataType:'json',
    data:$('#formuploadajax').serialize(),
    type:'post',
    url:'../MedicinaUrl',
    beforeSend:()=>{
      $('#load').show();
      $('#formuploadajax').hide();
    }
  }).done((res)=>{
    $('#load').hide();
    $('#alert_success').show();
    $('#formuploadajax')[0].reset();
    setTimeout(()=>{
      location.reload();
    //  window.location.href ='https://expedienteclinico.humanly-sw.com/Clinica/public/Medicina';
    },2000)
  }).fail((err)=>{
    $('#load').hide();
    $('#formuploadajax').show();
    if(err.status==0){
      toastr.error('Verifica tu conexion a internet', 'Hubo un problema', {"showDuration": 500});
    }else{
      // toastr.error('Verifica que los campos sean correctos, o intentalos más tarde ', 'Algo a Ocurrido', {positionClass: 'toast-top-full-width', "showDuration": 500,containerId: 'toast-top-full-width'});
    }
  })

});
