$(document).ready(function() {
    setInterval(function(){
        $('.alert-success').hide(700, "swing");
    },2000);

    setInterval(function(){
        $('.alert-danger').hide(700, "swing");
    },2000);
});

function loadEmpleados(encryptedID) {
    $('#tabla-empleados').DataTable({
        serverSide: true,
        ajax: "empleadosProgramados/" + encryptedID,
        language: {
            "decimal": "",
            "emptyTable": "No hay empleados con estudios programados durante este periodo para esta empresa.",
            "info": "",
            "infoEmpty": "",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        columns: [
            {data: 'nombre'},
            {data: 'apellido_paterno'},
            {data: 'apellido_materno'},
            {data: 'CURP'},
            {data: 'btn'},
        ]
    });
}