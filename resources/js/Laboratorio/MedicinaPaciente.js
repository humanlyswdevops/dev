



function showControll(clase, operation) {
  let element = '.' + clase;
  if (operation === 'disabled') {
    $(element).removeAttr('disabled', '');
  } else {
    $(element).removeAttr('disabled');
    $(element).show();
  }
}

function hideControll(clase, operation) {
  let element = '.' + clase;
  if (operation === 'disabled') {
    $(element).attr('disabled', '');
  } else {
    $(element).attr('disabled', '');
    $(element).hide();

  }
}

function iframTall(url) {
  $('#iframeBg').show();
  $('#iframeBg').attr('src', url);
}

function resultEstudios(empleado, estudio) {
  $('#iframeBg').attr('src', '');
  $.ajax({
    dataType: 'json',
    type: 'get',
    url: '../getArchivos/' + empleado + '/' + estudio
  }).done((res) => {
    $('#iframes').html('');
    res.forEach((item, index) => {
      if (item.tipo === 'url') {
        $('#iframes').append(`<iframe scrolling="no" class="col-12 " src="${item.archivo}" width="200" height="200"></iframe>`);
        $('#iframes').append(`<button type="button" class="col-12 mb-1 btn btn-success btn-min-width" onclick="iframTall('${item.archivo}')"><i class="icon-eye"></i> Ver</button>`);
      } else {
        $('#iframes').append(`<iframe scrolling="no" class="col-12 " src="https://expedienteclinico.humanly-sw.com/developing/public/PDF/${item.id}" width="200" height="200"></iframe>`);
        $('#iframes').append(`<button type="button" class="col-12 mb-1 btn btn-success btn-min-width" onclick="iframTall('https://expedienteclinico.humanly-sw.com/Clinica/public/PDF/${item.id}')"><i class="icon-eye"></i> Ver</button>`);
      }
    });

    $('#bounceInDown').modal();

    console.log(res);

  }).fail((err) => {

  });
}

$(".admision_contratista").steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  enableAllSteps: true,
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
    finish: 'Guardar',
    previous: 'Anterior',
    next: 'Siguiente'
  },
  onFinished: function (event, currentIndex) {
    swal({
      title: "Confirmación de envio",
      text: "¿Seguro que desea terminar el historial clinico?",
      icon: "warning",
      buttons: {
        cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
        },
        confirm: {
          text: "Guardar",
          value: true,
          visible: true,
          className: "",
          closeModal: false
        }
      }
    }).then(isConfirm => {
      if (isConfirm) {
        $('#formadmision_contratista').submit();
      } else {
        swal("Operacion Cancelada", "Puede continuar...", "error");
      }
    });

  }

});
$("#formadmision_contratista").on('submit', (e) => {
  e.preventDefault();
  varaible = $("#formadmision_contratista").serialize();
  $.ajax({
    type: 'POST',
    dataType: 'json',
    data: $("#formadmision_contratista").serialize(),
    url: '../historialForm'
  }).done((res) => {
    console.log(res);
    $('#wizard').find('a[href="#finish"]').remove();
    swal({
      position: 'top-end',
      icon: 'success',
      title: varaible,
      showConfirmButton: true
    }).then(isConfirm => {
      // window.location.href = '../Pacientes-Medico/'+res.empresa_id;
      window.location.reload();
    });
  }).fail((err) => {
    if (err.responseText.indexOf('estado_salud') > -1) {
      tpl = ` <div class="alert alert-danger" id="AlertaResultado" >
                  Faltan campos por llenar
              </div>`;
      $('#AlertaResultado').html(tpl);
      $(".alert").delay(4000).slideUp(300, function () {
        $(this).alert('close');
      });
      $('#estado_salud').css({
        'color': 'blank',
        'background': '#ff000060'
      });
    }

    if (err.status == 0) {
      swal(varaible);
    } else {
      swal(varaible);
    }
  });
});
$(".cadenadecustodia").steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  enableAllSteps: true,
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
    finish: 'Guardar',
    previous: 'Anterior',
    next: 'Siguiente'
  },
  onFinished: function (event, currentIndex) {
    swal({
      title: "Confirmación de envio",
      text: "¿Seguro que desea terminar el historial clinico?",
      icon: "warning",
      buttons: {
        cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
        },
        confirm: {
          text: "Guardar",
          value: true,
          visible: true,
          className: "",
          closeModal: false
        }
      }
    }).then(isConfirm => {
      if (isConfirm) {
        $('#formcustodia').submit();
      } else {
        swal("Operacion Cancelada", "Puede continuar...", "error");
      }
    });

  }

});

$("#formcustodia").on('submit', (e) => {
  e.preventDefault();
  varaible = $("#formcustodia").serialize();
  $.ajax({
    type: 'POST',
    dataType: 'json',
    data: $("#formcustodia").serialize(),
    url: '../historialForm'
  }).done((res) => {
    console.log(res);
    $('#wizard').find('a[href="#finish"]').remove();
    swal({
      position: 'top-end',
      icon: 'success',
      title: varaible,
      showConfirmButton: true
    }).then(isConfirm => {
      // window.location.href = '../Pacientes-Medico/'+res.empresa_id;
      window.location.reload();
    });
  }).fail((err) => {
    if (err.responseText.indexOf('estado_salud') > -1) {
      tpl = ` <div class="alert alert-danger" id="AlertaResultado" >
                  Faltan campos por llenar
              </div>`;
      $('#AlertaResultado').html(tpl);
      $(".alert").delay(4000).slideUp(300, function () {
        $(this).alert('close');
      });
      $('#estado_salud').css({
        'color': 'blank',
        'background': '#ff000060'
      });
    }

    if (err.status == 0) {
      swal(varaible);
    } else {
      swal(varaible);
    }
  });
});

$(".number-tab-steps").steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  enableAllSteps: true,
  titleTemplate: '<span class="step"><i class="ficon feather icon-list"></i></span> #title#',
  labels: {
    finish: 'Guardar',
    previous: 'Anterior',
    next: 'Siguiente'
  },
  onFinished: function (event, currentIndex) {
    swal({
      title: "Confirmación de envio",
      text: "¿Seguro que desea terminar el historial clinico?",
      icon: "warning",
      buttons: {
        cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
        },
        confirm: {
          text: "Guardar",
          value: true,
          visible: true,
          className: "",
          closeModal: false
        }
      }
    }).then(isConfirm => {
      if (isConfirm) {
        $('#form').submit();
      } else {
        swal("Operacion Cancelada", "Puede continuar...", "error");
      }
    });

  }

});

$("#form").on('submit', (e) => {
  e.preventDefault();
  var formulario = $("#form").serialize();
  $.ajax({
    type: 'POST',
    dataType: 'json',
    data: $("#form").serialize(),
    url: '../historialForm'
  }).done((res) => {
    console.log(formulario);
    console.log(res);
    $('#wizard').find('a[href="#finish"]').remove();
    swal({
      position: 'top-end',
      icon: 'success',
      title: formulario,
      showConfirmButton: true
    }).then(isConfirm => {
      // window.location.href = '../Pacientes-Medico/'+res.empresa_id;
      window.location.reload();
    });
  }).fail((err) => {
    if (err.responseText.indexOf('estado_salud') > -1) {
      tpl = ` <div class="alert alert-danger" id="AlertaResultado" >
                  Faltan campos por llenar
              </div>`;
      $('#AlertaResultado').html(tpl);
      $(".alert").delay(4000).slideUp(300, function () {
        $(this).alert('close');
      });
      $('#estado_salud').css({
        'color': 'blank',
        'background': '#ff000060'
      });
    }

    if (err.status == 0) {
      swal("Algo ha ocurrido", "Verifica tu conexión de internet", "info");
    } else {
      swal("Algo ha ocurrido", "Verifica que todos los campos sean correctos...", "error");
    }
  });
});

function validaNumericos(event) {
  if (event.charCode >= 48 && event.charCode <= 57) {
    return true;
  } else if (event.charCode == 13) {
    return true;
  }
  return false;
}

var identificacionLleno = document.getElementById("identificacion").innerHTML;
var heredofamiliar = document.getElementById("heredofamiliar").innerHTML;
var noPatologico = document.getElementById("noPatologico").innerHTML;
var patologico = document.getElementById("patologico").innerHTML;

if ($("#generoPaciente").val() == "FEMENINO") {
  var gine = document.getElementById("gine").innerHTML;
}
var HLaboral = document.getElementById("HLaboral").innerHTML;
var examen = document.getElementById("examen").innerHTML;
var resultado = document.getElementById("resultado").innerHTML;

// function checkAddress(genero) {
//   if ($('input:radio[name=FormVacio]:checked').val() == "Vacio") {
//     console.log(examen);
//      $("#identificacion").html(identificacionVacio);
//      $("#heredofamiliar").html(heredofamiliarVacio);
//     document.getElementById("noPatologico").innerHTML = nopatologicoVacio;
//     document.getElementById("patologico").innerHTML = patologicoVacio;
//     if (genero=="FEMENINO") {
//       document.getElementById("gine").innerHTML = gineVacio;
//       document.getElementById("HLaboral").innerHTML = HLHVacio;
//     }else {
//       document.getElementById("HLaboral").innerHTML = HLMVacio;
//     }
//     document.getElementById("examen").innerHTML = examenFVacio;
//     document.getElementById("resultado").innerHTML = resultadoVacio;
//   } else {
//     document.getElementById("identificacion").innerHTML = identificacionLleno;
//     document.getElementById("heredofamiliar").innerHTML = heredofamiliar;
//     document.getElementById("noPatologico").innerHTML = noPatologico;
//     document.getElementById("patologico").innerHTML = patologico;
//     if (genero=="FEMENINO") {
//     document.getElementById("gine").innerHTML = gine;
//   }
//     document.getElementById("HLaboral").innerHTML = HLaboral;
//     document.getElementById("examen").innerHTML = examen;
//     document.getElementById("resultado").innerHTML = resultado;
//   }
// }
$(document).ready(function () {
  $(".imc").keyup(function (event) {
    const altura = $('#altura').val();
    const peso = $('#peso').val();

    const imc = Math.round(peso * 10 / altura / altura) / 10;
    $('#imc').val(imc)
  });
});
var identificacionVacio = `<div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="fechahoy">Fecha:</label>
          <input type="date" class="form-control" id="fechahoy" name="fecha_estudio" value="">
        </div>
      </div>
      <div class="col-md-2">
                <div class="form-group">
          <label for="noEmpleado">No de paciente:</label>
          <input type="text" class="form-control" name="noEmpleado" id="noEmpleado" value="">
        </div>

      </div>
      <div class="col-md-3">
                <div class="form-group">
          <label for="departamento">Departamento:</label>
          <input type="text" class="form-control" name="departamento" id="departamento" value="">
        </div>
              </div>
      <div class="col-md-2">
        <div class="form-group">
          <label for="edad">Edad:</label>
          <input type="text" class="form-control" name="edad" id="edad" value="">
        </div>
      </div>

      <div class="col-md-2">
        <div class="form-group">
          <label for="sexo">Sexo :</label>
          <input type="text" class="form-control" name="sexo" value="Femenino" id="">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="app">Apellido Paterno :</label>
          <input type="text" class="form-control" name="app" id="app" value="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="apm">Apellido Materno :</label>
          <input type="text" class="form-control" id="apm" name="apm" value="">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="nombre">Nombre :</label>
          <input type="text" class="form-control" id="nombre" name="nombre" value="">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8">
        <div class="form-group">
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="estadoCivil" value="Casado" id="casado">
            <label class="custom-control-label" for="casado">Casado</label>
          </div>

                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="estadoCivil" value="Divorciado" id="Divorciado">
            <label class="custom-control-label" for="Divorciado">Divorciado</label>
          </div>

                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" checked="" class="custom-control-input bg-success" name="estadoCivil" checked value="Soltero" id="Soltero">
            <label class="custom-control-label" for="Soltero">Soltero</label>
          </div>

                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="estadoCivil" value="Viudo" id="Viudo">
            <label class="custom-control-label" for="Viudo">Viudo</label>
          </div>

                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="estadoCivil" value="Unión libre" id="libre">
            <label class="custom-control-label" for="libre">Unión Libre</label>
          </div>


                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="estadoCivil" value="Separado" id="Separado">
            <label class="custom-control-label" for="Separado">Separado</label>
          </div>

        </div>
      </div>
      <div class="col-md-4">
                <div class="form-group">
          <label for="lastName1">Escolaridad :</label>
          <input type="text" class="form-control" name="escolaridad" id="escolaridad" value="">
        </div>
              </div>
    </div>

    <div class="row">
      <div class="col-md-4">
                <div class="form-group">
          <label for="domicilio">Domicilio :</label>
          <input type="text" class="form-control" id="domicilio" name="domicilio" placeholder="Calle/No/Colonia/Municipio/Estado" value="">
        </div>
              </div>
      <div class="col-md-4">
                <div class="form-group">
          <label for="lugarnacimiento">Lugar de Nacimiento :</label>
          <input type="text" class="form-control" id="lugarnacimiento" name="lugarnacimiento" value="">
        </div>

      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="nacimiento">Fecha de Nacimiento :</label>
          <input type="date" class="form-control" id="nacimiento" name="nacimiento" value="">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
                <fieldset class="form-group">
          <label for="ciudad">Ciudad o Ejido</label>
          <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="" value="">
        </fieldset>
              </div>
      <div class="col-md-4">
                <fieldset class="form-group">
          <label for="municipio">Municipio</label>
          <input type="text" class="form-control" name="municipio" id="municipio" placeholder="" value="">
        </fieldset>
              </div>
      <div class="col-md-4">
                <fieldset class="form-group">
          <label for="estado">Estado</label>
          <input type="text" class="form-control" name="estado" id="estado" placeholder="" value="">
        </fieldset>
              </div>

    </div>
    <div class="row">
      <div class="col-md-4">
                <div class="form-group">
          <label for="emergencia">En caso de emergencia llamar a:</label>
          <input type="text" class="form-control" id="emergencia" name="emergencia" value="">
        </div>
              </div>
      <div class="col-md-4">
                <div class="form-group">
          <label for="parentesco">Parentesco :</label>
          <input type="text" class="form-control" name="parentesco" id="parentesco" value="">
        </div>
              </div>
      <div class="col-md-4">
                <div class="form-group">
          <label for="telefonoParenteso">Telefono :</label>
          <input type="text" maxlength="10" class="form-control" name="telefonoParenteso" onkeypress="return validaNumericos(event)" id="telefonoParenteso" value="">
        </div>
              </div>
    </div>
    <div class="row">
      <div class="col-md-4">
                <div class="form-group">
          <label for="domicilioEm">Domicilio :</label>
          <input type="text" class="form-control" id="domicilioEm" name="domicilioEm" value="">
        </div>
              </div>
      <div class="col-md-4">
                <div class="form-group">
          <label for="lugtrabajo">Lugar de Trabajo :</label>
          <input type="text" class="form-control" name="lugtrabajo" id="lugtrabajo" value="">
        </div>
              </div>
      <div class="col-md-4">
                <div class="form-group">
          <label for="telefonoParenteso2">Telefono :</label>
          <input type="text" maxlength="10" class="form-control" name="telefonoParenteso2" onkeypress="return validaNumericos(event)" id="telefonoParenteso2" value="">
        </div>
              </div>

    </div>`;
var heredofamiliarVacio = `<hr>
    <p class="text-center">Por favor indique si su familia (Padre, Madre, Hermanos(as)) tuvo alguna de las enfermedades que se mencionan</p>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Padre:</label>
        </div>
      </div>
      <div class="col-md-3">
                <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Vivo" checked="" name="padre_estado" id="vivo_padre">
          <label class="custom-control-label" for="vivo_padre">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Finado" name="padre_estado" id="finado_padre">
          <label class="custom-control-label" for="finado_padre">Finado</label>
        </div>
              </div>

      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" name="select_padre[]" id="select_padre" placeholder="Enfermedades" multiple="">

                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Cancer">Cancer</option>

                        <option value="Asma">Asma</option>

                        <option value="Colesterol Alto">Colesterol Alto</option>

                        <option value="Evc">Evc</option>

                        <option value="Diabetes">Diabetes</option>

                        <option value="Otros">Otros</option>

                        <option value="Obesidad">Obesidad</option>

                        <option value="Sida">Sida</option>

                        <option value="Tuberculosis">Tuberculosis</option>

                        <option value="Sangrado">Sangrado</option>

                        <option value="Anemia">Anemia</option>

                        <option value="Alcoholismo">Alcoholismo</option>

                        <option value="Alta presión sanguinea">Alta presión sanguinea</option>

                        <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>

                        <option value="Defectos Congénitos">Defectos Congénitos</option>

                        <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
                      </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Madre:</label>
        </div>
      </div>
      <div class="col-md-3">
                <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Vivo" checked="" name="madre_estado" id="madre_viva">
          <label class="custom-control-label" for="madre_viva">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Finado" name="madre_estado" id="madre_finada">
          <label class="custom-control-label" for="madre_finada">Finado</label>
        </div>

      </div>
      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" name="select_madre[]" placeholder="Enfermedades" multiple="">

                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Asma">Asma</option>

                        <option value="Colesterol Alto">Colesterol Alto</option>

                        <option value="Evc">Evc</option>

                        <option value="Diabetes">Diabetes</option>

                        <option value="Otros">Otros</option>

                        <option value="Obesidad">Obesidad</option>

                        <option value="Sida">Sida</option>

                        <option value="Tuberculosis">Tuberculosis</option>

                        <option value="Sangrado">Sangrado</option>

                        <option value="Anemia">Anemia</option>

                        <option value="Alcoholismo">Alcoholismo</option>

                        <option value="Alta presión sanguinea">Alta presión sanguinea</option>

                        <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>

                        <option value="Defectos Congénitos">Defectos Congénitos</option>

                        <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
                      </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Conyuge:</label>
        </div>
      </div>
      <div class="col-md-3">
                <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Vivo" checked="" name="conyuge_estado" id="conyuge_vivo">
          <label class="custom-control-label" for="conyuge_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Finado" name="conyuge_estado" id="conyuge_finado">
          <label class="custom-control-label" for="conyuge_finado">Finado</label>
        </div>
              </div>
      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" name="select_conyuge[]" placeholder="Enfermedades" multiple="">
                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Asma">Asma</option>

                        <option value="Colesterol Alto">Colesterol Alto</option>

                        <option value="Evc">Evc</option>

                        <option value="Diabetes">Diabetes</option>

                        <option value="Otros">Otros</option>

                        <option value="Obesidad">Obesidad</option>

                        <option value="Sida">Sida</option>

                        <option value="Tuberculosis">Tuberculosis</option>

                        <option value="Sangrado">Sangrado</option>

                        <option value="Anemia">Anemia</option>

                        <option value="Alcoholismo">Alcoholismo</option>

                        <option value="Alta presión sanguinea">Alta presión sanguinea</option>

                        <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>

                        <option value="Defectos Congénitos">Defectos Congénitos</option>

                        <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
                      </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">hermanos(as):</label>
        </div>
      </div>
            <div class="col-md-3">
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Vivo" checked="" name="hermano_estado" id="hermano_vivo">
          <label class="custom-control-label" for="hermano_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Finado" name="hermano_estado" id="hermano_finado">
          <label class="custom-control-label" for="hermano_finado">Finado</label>
        </div>
      </div>
            <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" name="select_hermanos[]" placeholder="Enfermedades" multiple="">
                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Asma">Asma</option>

                        <option value="Colesterol Alto">Colesterol Alto</option>

                        <option value="Evc">Evc</option>

                        <option value="Diabetes">Diabetes</option>

                        <option value="Otros">Otros</option>

                        <option value="Obesidad">Obesidad</option>

                        <option value="Sida">Sida</option>

                        <option value="Tuberculosis">Tuberculosis</option>

                        <option value="Sangrado">Sangrado</option>

                        <option value="Anemia">Anemia</option>

                        <option value="Alcoholismo">Alcoholismo</option>

                        <option value="Alta presión sanguinea">Alta presión sanguinea</option>

                        <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>

                        <option value="Defectos Congénitos">Defectos Congénitos</option>

                        <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
                      </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Hijos:</label>
        </div>
      </div>
      <div class="col-md-3">
                <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Vivo" checked="" name="hijos_estato" id="hijo_vivo">
          <label class="custom-control-label" for="hijo_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" value="Finado" name="hijos_estato" id="hijo_finado">
          <label class="custom-control-label" for="hijo_finado">Finado</label>
        </div>
              </div>
      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" name="select_hijos[]" placeholder="Enfermedades" multiple="">
                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Hepatitis">Hepatitis</option>

                        <option value="Asma">Asma</option>

                        <option value="Colesterol Alto">Colesterol Alto</option>

                        <option value="Evc">Evc</option>

                        <option value="Diabetes">Diabetes</option>

                        <option value="Otros">Otros</option>

                        <option value="Obesidad">Obesidad</option>

                        <option value="Sida">Sida</option>

                        <option value="Tuberculosis">Tuberculosis</option>

                        <option value="Sangrado">Sangrado</option>

                        <option value="Anemia">Anemia</option>

                        <option value="Alcoholismo">Alcoholismo</option>

                        <option value="Alta presión sanguinea">Alta presión sanguinea</option>

                        <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>

                        <option value="Defectos Congénitos">Defectos Congénitos</option>

                        <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
                      </select>
        </div>
      </div>

    </div>`;
var nopatologicoVacio = `<hr>
 <p class="text-center">Vacunas Recibidas</p>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label>Tetanos: </label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="tetano[]" value="Tetano" id="tetano_si">
            <label class="custom-control-label" for="tetano_si" onclick="showControll('tetano','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="tetano[]" value="No" id="tetano_no">
            <label class="custom-control-label" for="tetano_no" onclick="hideControll('tetano','show')">no</label>
          </div>
          <div class="tetano" style="display:none">
            <input type="text" class="form-control tetano" name="tetano[]" placeholder="Nota">
          </div>
                  </div>

      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Rubeloa: </label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="rubeola[]" value="Rubeola" id="rubeola_si">
            <label class="custom-control-label" for="rubeola_si" onclick="showControll('rubeola','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="rubeola[]" value="no" id="rubeola_no">
            <label class="custom-control-label" for="rubeola_no" onclick="hideControll('rubeola','show')">no</label>
          </div>

                  </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>(BCG): </label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="bcg[]" value="(BCG)" id="bcg_si">
            <label class="custom-control-label" for="bcg_si" onclick="showControll('bcg','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="bcg[]" value="no" id="bcg_no">
            <label class="custom-control-label" for="bcg_no" onclick="hideControll('bcg','show')">no</label>
          </div>
          <div class="bcg" style="display:none">
            <input type="text" class="form-control bcg" name="bcg[]" placeholder="Nota">
          </div>
                  </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Hepatitis: </label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="hepatitis[]" value="Hepatitis" id="hepatitis_si">
            <label class="custom-control-label" for="hepatitis_si" onclick="showControll('hepatitis','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="hepatitis[]" value="no" id="hepatitis_no">
            <label class="custom-control-label" for="hepatitis_no" onclick="hideControll('hepatitis','show')">no</label>
          </div>
          <div class="hepatitis" style="display:none">
            <input type="text" class="form-control hepatitis" name="hepatitis[]" placeholder="Nota">
          </div>
                  </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Influenza: </label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="influenza[]" value="Influenza" id="influenza_si">
            <label class="custom-control-label" for="influenza_si" onclick="showControll('influenza','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="influenza[]" value="no" id="influenza_no">
            <label class="custom-control-label" for="influenza_no" onclick="hideControll('influenza','show')">no</label>
          </div>
          <div class="influenza" style="display:none">
            <input type="text" class="form-control influenza" name="influenza[]" placeholder="Nota">
          </div>
                  </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Neumococica: </label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="neumococica[]" value="Neumococica" id="neumococica_si">
            <label class="custom-control-label" for="neumococica_si" onclick="showControll('neumococica','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="neumococica[]" value="no" id="neumococica_no">
            <label class="custom-control-label" for="neumococica_no" onclick="hideControll('neumococica','show')">no</label>
          </div>
          <div class="neumococica" style="display:none">
            <input type="text" class="form-control neumococica" name="neumococica[]" placeholder="Nota">
          </div>
                  </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
                <div class="form-group">
          <label for="otrasvacunas">Otras Vacunas :</label>
          <input type="text" class="form-control" name="otrasvacunas" id="otrasvacunas">
        </div>
              </div>
      <div class="col-md-4">
                <div class="form-group">
          <label for="tiposangre">Grupo Sanguinio :</label>
          <input type="text" class="form-control" name="tiposangre" id="tiposangre">
        </div>
              </div>
      <div class="col-md-4">
        <div class="form-group">
          <label class="d-block">¿Alguna vez ha recibido usted alguna transfusión sanguinea? </label>
                    <div class="d-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="transfusion" value="si" id="transfusion_si">
            <label class="custom-control-label" for="transfusion_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="transfusion" value="no" id="transfusion_no">
            <label class="custom-control-label" for="transfusion_no">no</label>
          </div>
                  </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="medicamentos_ingeridos">Mencione los medicamentos que haya tomado en las últimas dos semanas</label>
                    <input type="text" class="form-control" id="medicamentos_ingeridos" name="mediIngeridos" placeholder="">

                  </fieldset>
      </div>
    </div>
    <p class="text-center">Uso del Cigarro</p>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label class="d-block">¿Fuma Ustes? </label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="cigarro" onclick="showControll('cigarro','disabled')" value="si" id="cigarro_si">
            <label class="custom-control-label" for="cigarro_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="cigarro" checked="" onclick="hideControll('cigarro','disabled')" value="no" id="cigarro_no">
            <label class="custom-control-label" for="cigarro_no">no</label>
          </div>
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="cigarro_edad">¿Desde que edad?</label>
                    <input type="text" class="form-control cigarro" disabled="" id="cigarro_edad" name="cigarro_edad" placeholder="">

                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="cigarropromedio">¿Número promedio de cigarro que fuma?</label>
                    <input type="text" class="form-control cigarro" disabled="" id="cigarropromedio" name="cigarropromedio" placeholder="">

                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="dejarcigarro">¿A que edad dejo de fumar?</label>
                    <input type="text" class="form-control cigarro" disabled="" id="dejarcigarro" name="dejarcigarro" placeholder="">
                  </fieldset>
      </div>
    </div>
    <p class="text-center">Uso de Alcohol</p>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label class="d-block">¿Toma bebidas alcoholicas?</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="alcohol" value="si" id="alcohol_si">
            <label class="custom-control-label" for="alcohol_si" onclick="showControll('alcohol','disabled')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="alcohol" value="no" id="alcohol_no">
            <label class="custom-control-label" for="alcohol_no" onclick="hideControll('alcohol','disabled')">no</label>
          </div>
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="alcohol_edad">¿Desde que edad?</label>
                    <input type="text" class="form-control alcohol" disabled="" id="alcohol_edad" name="alcohol_edad" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="alcohol_frecuencia">¿Con que frecuencia y cantidad?</label>
                    <input type="text" class="form-control alcohol" disabled="" id="alcohol_frecuencia" name="alcohol_frecuencia" placeholder="">
                  </fieldset>
      </div>
    </div>
    <p class="text-center">Uso de Drogas</p>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label class="d-block">¿Alguna vez usó drogas?</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="drogas" value="si" id="dogras_si">
            <label class="custom-control-label" for="dogras_si" onclick="showControll('drogas','disabled')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="drogas" value="no" id="dogras_no">
            <label class="custom-control-label" for="dogras_no" onclick="hideControll('drogas','disabled')">no</label>
          </div>
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="alcohol_edad">¿Desde que edad?</label>
                    <input type="text" class="form-control drogas" disabled="" id="drogas_edad" name="drogas_edad" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="alcohol_frecuencia">¿Con que frecuencia y cantidad?</label>
                    <input type="text" class="form-control drogas" disabled="" id="drogas_frecuencia" name="drogas_frecuencia" placeholder="">
                  </fieldset>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group drogas">
          <label>Mencione que drogas ha usado</label>
          <select class="selectize-multiple" name="drogas_usadas[]" placeholder="Drogas" multiple="">
                        <option value="Resistol">Resistol</option>

                        <option value="Sarolo">Sarolo</option>

                        <option value="Marihuana">Marihuana</option>

                        <option value="Cocaina">Cocaina</option>

                        <option value="Pastillas">Pastillas</option>

                        <option value="Thinner">Thinner</option>

                        <option value="Otras">Otras</option>
                      </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label class="d-block">¿Se le ha indicado alguna dieta?</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="dieta" value="si" id="dieta_si">
            <label class="custom-control-label" for="dieta_si" onclick="showControll('dieta','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="dieta" value="no" id="dieta_no">
            <label class="custom-control-label" for="dieta_no" onclick="hideControll('dieta','show')">no</label>
          </div>
                  </div>
        <div class="form-group dieta" style="display:none">
                    <input type="text" class="form-control dieta" name="dieta_descripcion" placeholder="Descripción">

                  </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label class="d-block">¿Practica algun deporte?</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="deporte" value="si" id="deporte_si">
            <label class="custom-control-label" for="deporte_si" onclick="showControll('deporte_frecuencia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="deporte" value="no" id="deporte_no">
            <label class="custom-control-label" for="deporte_no" onclick="hideControll('deporte_frecuencia','show')">no</label>
          </div>
                  </div>
        <div class="form-group deporte_frecuencia" style="display:none">
                    <input type="text" class="form-control deporte_frecuencia" name="deporte_frecuencia" placeholder="Especifique cual y frecuencia">

                  </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="pasatiempo">Indique cuál es su pasatiempo favorito:</label>
                    <input type="text" class="form-control" name="pasatiempo" id="pasatiempo" placeholder="">
                  </fieldset>
      </div>

    </div>`;
var patologicoVacio = `<hr>
<div class="row">
        <div class="col-md-8">
          <fieldset class="form-group">
            <label class="">Mencione si ha padecido alguna vez las siguientes enfermedades</label>
            <select class="selectize-multiple" name="enfermedad_padecida[]" placeholder="Enfermedades" multiple="">
                            <option value="Alta Presión">Alta Presión</option>

                            <option value="Soplo del corazón">Soplo del corazón</option>

                            <option value="Asma bronquial">Asma bronquial</option>

                            <option value="Enfisema Pulmunar">Enfisema Pulmunar</option>

                            <option value="Bronquitis">Bronquitis</option>

                            <option selected="" value="Diabetes">Diabetes</option>

                            <option value="Migraña">Migraña</option>

                            <option value="Tuberculosis">Tuberculosis</option>

                            <option value="Tuberculosis">Tuberculosis</option>

                            <option value="Gastritis">Gastritis</option>

                            <option value="Úlceras">Úlceras</option>

                            <option value="Piedras en la vesícula">Piedras en la vesícula</option>

                            <option value="Hepatitis">Hepatitis</option>

                            <option value="Infección urinaria">Infección urinaria</option>

                            <option value="Convulsiones">Convulsiones</option>

                            <option value="Piedras en el riñon">Piedras en el riñon</option>

                            <option value="Enf. de transmisión sexual">Enf. de transmisión sexual</option>

                            <option value="Anemia">Anemia</option>

                            <option value="Enfermedades de la sangre">Enfermedades de la sangre</option>

                            <option value="Fiebre tifoidea">Fiebre tifoidea</option>

                            <option value="Fiebre reumática">Fiebre reumática</option>

                            <option value="Enfermedad de la tiroides">Enfermedad de la tiroides</option>

            </select>
          </fieldset>
        </div>
        <div class="col-md-4">
          <fieldset class="form-group">
            <label for="transtornos">Trantornos emocionales o psiquiatricos</label>
                        <input type="text" class="form-control" id="transtornos" name="transtornos_ep">
                      </fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="alergias">Alergias a medicamentos</label>
                        <input type="text" class="form-control" id="alergias" name="alergiasMedicas" placeholder="">
                      </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="alergia_piel">Alergias en la piel o sensiblidad</label>
                        <input type="text" class="form-control" id="alergia_piel" name="alergia_piel" placeholder="">
                      </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="alergia_otro">Otro tipo de alergia</label>
                        <input type="text" class="form-control" id="alergia_otro" name="alergia_otro" placeholder="">
                      </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="tumo_cancer">Tumores o cancer</label>
                        <input type="text" class="form-control" id="tumo_cancer" name="tumo_cancer" placeholder="">
                      </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="problema_vista">Problemas de la vista</label>
                        <input type="text" class="form-control" id="problema_vista" name="problema_vista" placeholder="">
                      </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="enfer_oido">Enfermedades del oido</label>
                        <input type="text" class="form-control" id="enfer_oido" name="enfer_oido" placeholder="">
                      </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="columna_vertebral">Problema en la columna vertebral</label>
                        <input type="text" class="form-control" id="columna_vertebral" name="columna_vertebral" placeholder="">
                      </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="hueso_articulacion">Husos y articulaciones</label>
                        <input type="text" class="form-control" id="hueso_articulacion" name="hueso_articulacion" placeholder="">
                      </fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="otro_problema">Otros problemas médicos no enlistados:</label>
                        <input type="text" class="form-control" id="otro_problema" name="otro_problema">
                      </fieldset>
        </div>
      </div>
      <hr>
      <p class="text-center">Indique si ha tenido alguna de las lesiones o cirugias siguientes:</p>
      <hr>
      <div class="row">
        <div class="col-md-3">
          <label class="d-block">Cirugía dental</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="c_dental" value="si" id="c_dental_si">
            <label class="custom-control-label" for="c_dental_si" onclick="showControll('dental','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="c_dental" value="no" id="c_dental_no">
            <label class="custom-control-label" for="c_dental_no" onclick="hideControll('dental','show')">no</label>
          </div>
                    <div class="dental" style="display:none">
                        <input type="text" class="form-control dental" name="dental" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugía de estomago/úlceras gastricas</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="c_estomago" value="si" id="c_estomago_si">
            <label class="custom-control-label" for="c_estomago_si" onclick="showControll('estomago','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="c_estomago" value="no" id="c_estomago_no">
            <label class="custom-control-label" for="c_estomago_no" onclick="hideControll('estomago','show')">no</label>
          </div>
                    <div class="estomago" style="display:none">
                        <input type="text" class="form-control estomago" name="estomago" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Lesion en cuello/ espalda / rodillas, manos, codos</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="lesiones" value="si" id="lesion_cer_si">
            <label class="custom-control-label" for="lesion_cer_si" onclick="showControll('cuello','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="lesiones" value="no" id="lesion_cer_no">
            <label class="custom-control-label" for="lesion_cer_no" onclick="hideControll('cuello','show')">no</label>
          </div>
                    <div class="cuello" style="display:none">
                        <input type="text" class="form-control cuello" name="cuello" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugía de colon/recto</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="c_colon" value="si" id="c_colon_si">
            <label class="custom-control-label" for="c_colon_si" onclick="showControll('colon','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="c_colon" value="no" id="c_colon_no">
            <label class="custom-control-label" for="c_colon_no" onclick="hideControll('colon','show')">no</label>
          </div>
                    <div class="colon" style="display:none">
                        <input type="text" class="form-control colon" name="colon" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugía de intestinos</label>
                      <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-success" name="c_intestino" value="si" id="c_intestino_si">
              <label class="custom-control-label" for="c_intestino_si" onclick="showControll('intestino','show')">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-success" checked="" name="c_intestino" value="no" id="c_intestino_no">
              <label class="custom-control-label" for="c_intestino_no" onclick="hideControll('intestino','show')">no</label>
            </div>
                    <div class="intestino" style="display:none">
                        <input type="text" class="form-control intestino" name="intestino" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Oforectomia/Histerectomia</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="ooforectomia" value="si" id="ooforectomia_si">
            <label class="custom-control-label" for="ooforectomia_si" onclick="showControll('Histerectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="ooforectomia" value="no" id="ooforectomia_no">
            <label class="custom-control-label" for="ooforectomia_no" onclick="hideControll('Histerectomia','show')">no</label>
          </div>
                    <div class="Histerectomia" style="display:none">
                        <input type="text" class="form-control Histerectomia" name="Histerectomia" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Salpingoclasia/Vasectomia</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="sal_vas" value="si" id="sal_vas_si">
            <label class="custom-control-label" for="sal_vas_si" onclick="showControll('Vasectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="sal_vas" value="no" id="sal_vas_no">
            <label class="custom-control-label" for="sal_vas_no" onclick="hideControll('Vasectomia','show')">no</label>
          </div>
                    <div class="Vasectomia" style="display:none">
                        <input type="text" class="form-control Vasectomia" name="Vasectomia" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugia de hernias</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="c_hernia" value="si" id="c_hernia_si">
            <label class="custom-control-label" for="c_hernia_si" onclick="showControll('hernias','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="c_hernia" value="no" id="c_hernia_no">
            <label class="custom-control-label" for="c_hernia_no" onclick="hideControll('hernias','show')">no</label>
          </div>
                    <div class="hernias" style="display:none">
                        <input type="text" class="form-control hernias" name="hernias" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugia de tiroides</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="c_tiroides" value="si" id="c_tiroides_si">
            <label class="custom-control-label" for="c_tiroides_si" onclick="showControll('tiroides','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="c_tiroides" value="no" id="c_tiroides_no">
            <label class="custom-control-label" for="c_tiroides_no" onclick="hideControll('tiroides','show')">no</label>
          </div>
                    <div class="tiroides" style="display:none">
                        <input type="text" class="form-control tiroides" name="tiroides" placeholder="Descripción" value="no">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Colecistectomia</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="colecistectomia" value="si" id="colecistectomia_si">
            <label class="custom-control-label" for="colecistectomia_si" onclick="showControll('Colecistectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="colecistectomia" value="no" id="colecistectomia_no">
            <label class="custom-control-label" for="colecistectomia_no" onclick="hideControll('Colecistectomia','show')">no</label>
          </div>
                    <div class="Colecistectomia" style="display:none">
                        <input type="text" class="form-control Colecistectomia" name="Colecistectomia" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Apéndicectomia</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="apendicectomia" value="si" id="apendicectomia_si">
            <label class="custom-control-label" for="apendicectomia_si" onclick="showControll('apendicectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="apendicectomia" value="no" id="apendicectomia_no">
            <label class="custom-control-label" for="apendicectomia_no" onclick="hideControll('apendicectomia','show')">no</label>
          </div>
                    <div class="apendicectomia" style="display:none">
                        <input type="text" class="form-control apendicectomia" name="apendicectomias" placeholder="Descripción">
                      </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Fracturas o esguinces</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="fracturas" value="si" id="fracturas_si">
            <label class="custom-control-label" for="fracturas_si" onclick="showControll('esguinces','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="fracturas" value="no" id="fracturas_no">
            <label class="custom-control-label" for="fracturas_no" onclick="hideControll('esguinces','show')">no</label>
          </div>
                    <div class="esguinces" style="display:none">
                        <input type="text" class="form-control esguinces" name="esguinces" placeholder="Descripción">

                      </div>
        </div>
      </div>
      <div class="row mt-1">
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="otra_cirugia">Otras cirugías incluyendo accidentes automovilísticos o del trabajo:</label>
                        <input type="text" class="form-control" name="otra_cirugia" id="otra_cirugia" placeholder="">

                      </fieldset>
        </div>
      </div>`;
var gineVacio = `<div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="inicio_regla">¿A qué edad ínicio su regla?</label>
                    <input type="text" class="form-control" id="inicio_regla" name="inicio_regla" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="frecuencia_regla">¿Frecuencia de regla?</label>
                    <input type="text" class="form-control" id="frecuencia_regla" name="frecuencia_regla" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="duracion_regla">¿Cuántos días dura su regla?</label>
                    <input type="text" class="form-control" id="duracion_regla" name="duracion_regla" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="inicio_re_sexual">¿Edad en que inicío sus relaciones sexuales?</label>
                    <input type="text" class="form-control" id="" name="inicio_re_sexual" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="metodo_antic">¿Cuál metodo de planifcación ha usado?</label>
                    <input type="text" class="form-control" id="metodo_antic" name="metodo_antic" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="ult_mestruacion">¿Cuál fue la fecha de su ultima mestruación?</label>
                    <input type="text" class="form-control" name="ult_mestruacion" id="ult_mestruacion" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="embarazos">¿Cuántos embarazos ha tenido?</label>
                    <input type="text" class="form-control" id="embarazos" name="embarazos" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="partos">¿Cuántos partos ha tenido?</label>
                    <input type="text" class="form-control" id="partos" name="partos" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="cesareas">¿Cuántas cesáreas ha tenido?</label>
                    <input type="text" class="form-control" id="cesareas" name="cesareas" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="abortos">¿Cuántos abortos ha tenido?</label>
                    <input type="text" class="form-control" id="abortos" name="abortos" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="examen_matriz">¿Cuándo se realizó el último exámen del cáncer de matriz?</label>
                    <input type="text" class="form-control" id="examen_matriz" name="examen_matriz" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="examen_mamas">¿Cuándo se realizó el último exámen de mamas?</label>
                    <input type="text" class="form-control" id="examen_mamas" name="examen_mamas" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="menopausia">¿A qué edad tuvo su menopausia?</label>
                    <input type="text" class="form-control" id="menopausia" name="menopausia" placeholder="">
                  </fieldset>
      </div>
    </div>`;
var HLHVacio = `<div class="row">
    <div class="col-md-6">
      <label>
        Ha realizado unos de los siguientes trabajos:
      </label>
      <select class="selectize-multiple" name="trabajos_realizados[]" placeholder="Trabajos" multiple="">

                <option value="Granjero">Granjero</option>

                <option value="Colocando aisalantes">Colocando aisalantes</option>

                <option value="Textiles">Textiles</option>

                <option value="Canteras">Canteras</option>|

                <option value="Militares">Militares</option>

                <option value="Minas">Minas</option>

                <option value="Fundación">Fundación</option>

                <option value="Petroquimicos">Petroquimicos</option>

                <option value="Construcción">Construcción</option>

                <option value="Mecanicos">Mecanicos</option>

                <option value="Leñador">Leñador</option>

                <option value="Embarques">Embarques</option>

                <option value="Pulidor de arena">Pulidor de arena</option>

                <option value="Pintor">Pintor</option>

                <option value="Manuf. de papel">Manuf. de papel</option>

                <option value="Desengrasador">Desengrasador</option>

      </select>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="otros_trabajos_">Otros Trabajos</label>
                <input type="text" class="form-control" id="otros_trabajos_" name="otrosTrabajos" placeholder="">
              </fieldset>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <label>
        ¿Se ha expuesto o ha trabajado con alguno de estos materiales?
      </label>
      <select class="selectize-multiple" name="materiales_expuesto[]" placeholder="Materiales" multiple="">
                <option value="Berilio">Berilio</option>

                <option value="Humo de soldadura">Humo de soldadura</option>

                <option value="Plastico/Hule">Plastico/Hule</option>

                <option value="Ruido">Ruido</option>

                <option value="Asbesto">Asbesto</option>

                <option value="Dioxido de azufre">Dioxido de azufre</option>

                <option value="Radiación">Radiación</option>

                <option value="Plomo">Plomo</option>

                <option value="Caustico(Acido,Base)">Caustico(Acido,Base)</option>

                <option value="Silice">Silice</option>

                <option value="Gases de cloro">Gases de cloro</option>

                <option value="Traunma fis. Repetido">Traunma fis. Repetido</option>

                <option value="Arsenico">Arsenico</option>

                <option value="Talco">Talco</option>

                <option value="Gases/Humo irritante">Gases/Humo irritante</option>

                <option value="Fluoruro">Fluoruro</option>

                <option value="Insecticidas/Otros">Insecticidas/Otros</option>

                <option value="Otros materiales">Otros materiales</option>

                <option value="Otros aceites">Otros aceites</option>

                <option value="Otros polvos">Otros polvos</option>

                <option value="Otros solventes">Otros solventes</option>

                <option value="Otras fibras sinteticas">Otras fibras sinteticas</option>

                <option value="Otro argon">Otro argon</option>

      </select>
    </div>

  </div>
  <div class="row">
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="otras_exposiciones">Otras exposiciones</label>
                <input type="text" class="form-control" id="otras_exposiciones" name="otras_exposiciones">
              </fieldset>
    </div>
  </div>
  <hr>
  <p class="text-center">Empleo Actual</p>
  <hr>
  <div class="row">
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="nomEmpresa_actual">Nombre de la empresa:</label>
                <input type="text" class="form-control" id="nomEmpresa_actual" name="nombreEmpresa_actual" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="actul_puesto">Puesto:</label>
                <input type="text" class="form-control" id="actul_puesto" name="actual_puesto" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="actual_empleo_actividad">Descripción de su actividad:</label>
                <input type="text" class="form-control" id="actual_empleo_actividad" name="actual_empleo_actividad" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="actual_empleo_duracion">Duración del empleo:</label>
                <input type="text" class="form-control" id="actual_empleo_duracion" name="actual_empleo_duracion" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="actual_equipoS">Equipo de seguridad usado:</label>
                <input type="text" class="form-control" id="actual_equipoS" name="actual_equipoS" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="actual_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                <input type="text" class="form-control" id="actual_empleo_danos" name="actual_empleo_danos" placeholder="">
              </fieldset>
    </div>
  </div>
  <hr>
  <p class="text-center">Empleo Anterior</p>
  <hr>
  <div class="row">
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="nomEmpresa_anterior">Nombre de la empresa:</label>
                <input type="text" class="form-control" id="nomEmpresa_anterior" name="nomEmpresa_anterior" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="anterior_puesto">Puesto:</label>
                <input type="text" class="form-control" id="anterior_puesto" name="anterior_puesto" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="anterior_empleo_actividad">Descripción de su actividad:</label>
                <input type="text" class="form-control" id="anterior_empleo_actividad" name="anterior_empleo_actividad" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="anterior_empleo_duracion">Duración del empleo:</label>
                <input type="text" class="form-control" id="anterior_empleo_duracion" name="anterior_empleo_duracion" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="anteriro_equipoS">Equipo de seguridad usado:</label>
                <input type="text" class="form-control" id="anteriro_equipoS" name="anteriro_equipoS" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="anterior_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                <input type="text" class="form-control" id="anterior_empleo_danos" name="anterior_empleo_danos" placeholder="">
              </fieldset>
    </div>
  </div>
  <hr>
  <p class="text-center">Empleo Anterior</p>
  <hr>
  <div class="row">
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="nomEmpresa_anterior_two">Nombre de la empresa:</label>
                <input type="text" class="form-control" id="nomEmpresa_anterior_two" name="nomEmpresa_anterior_two" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="two_anterior_puesto">Puesto:</label>
                <input type="text" class="form-control" id="two_anterior_puesto" name="two_anterior_puesto" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="two_anterior_empleo_actividad">Descripción de su actividad:</label>
                <input type="text" class="form-control" id="two_anterior_empleo_actividad" name="two_anterior_empleo_actividad" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="two_anterior_empleo_duracion">Duración del empleo:</label>
                <input type="text" class="form-control" id="two_anterior_empleo_duracion" name="two_anterior_empleo_duracion" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="two_anteriro_equipoS">Equipo de seguridad usado:</label>
                <input type="text" class="form-control" id="two_anteriro_equipoS" name="two_anteriro_equipoS" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="two_anterior_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                <input type="text" class="form-control" id="two_anterior_empleo_danos" name="two_anterior_empleo_danos" placeholder="">
              </fieldset>
    </div>
  </div>
  <hr>
    <p class="text-center">Equipo de seguridad usando en el puesto actual</p>
  <hr>
  <div class="row">
    <div class="col-md-6">
      <fieldset class="form-group">
        <select class="selectize-multiple" name="equipo_seguridad[]" placeholder="Equipo de seguridad" multiple="">

                              <option value="R">Respiradores</option>
                    <option value="CSC">Calzado de seguridad con/sin casquillos</option>
                    <option value="MP">Mascarillas para polvo</option>
                    <option value="TA">Tapones auditivos</option>
                    <option value="C">Cascos</option>
                    <option value="Res">Respirador</option>
                    <option value="LS">Lentes de seguridad con o sin aumento</option>
                    <option value="LimEPP">Limitante para utilizar EPP(Equipo de protección personal)</option>

        </select>
      </fieldset>
    </div>
    <div class="col-md-3">
      <fieldset class="form-group">
        <label class="d-block">¿Usa lentes graduados?</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" name="examen_vista[]" value="si" id="lentes_si">
          <label class="custom-control-label" for="lentes_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" checked="" name="examen_vista[]" value="no" id="lentes_no">
          <label class="custom-control-label" for="lentes_no">no</label>
        </div>

      </fieldset>
      <fieldset class="form-group lentes">
        <label for="examen_vista">Fecha de último examen</label>
                <input type="date" class="form-control lentes" id="examen_vista" name="examen_vista[]" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-3">
      <fieldset class="form-group">
        <label class="d-block">¿Usa lentes de contacto?</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" name="uso_lentes_graduados[]" value="si" id="lentes_graduados_si">
          <label class="custom-control-label" for="lentes_graduados_si" onclick="showControll('graduados','show')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" checked="" name="uso_lentes_graduados[]" value="no" id="lentes_graduados_no">
          <label class="custom-control-label" for="lentes_graduados_no" onclick="hideControll('graduados','show')">no</label>
        </div>

              </fieldset>
      <fieldset class="form-group graduados" style="display:none">
        <label for="examen_vista">¿De que tipo?</label>
                <input type="text" class="form-control graduados" id="examen_vista" name="uso_lentes_graduados[]" placeholder="">
              </fieldset>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <fieldset class="form-group">
        <label for="">Otras exposiciones:</label>
                <input type="text" class="form-control" id="" name="otrasexpPuesto" placeholder="">
              </fieldset>
    </div>
    <div class="col-md-4">
      <fieldset class="form-group">
        <label class="d-block">¿Alguien de su familia trabaja con materiales peligrosos? (Asbesto, Plomo, Etc.)</label>

                    <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-success" name="ma_peligroso_fam" value="si" id="ma_fam_peligroso_si">
              <label class="custom-control-label" for="ma_fam_peligroso_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-success" checked="" name="ma_peligroso_fam" value="no" id="ma_fam_peligroso_no">
              <label class="custom-control-label" for="ma_fam_peligroso_no">no</label>
            </div>
              </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="d-block">Alguna vez ha vivido cerca de:</label>
        <select class="selectize-multiple" name="lugar_vivido[]" placeholder="Lugares donde has vivido" multiple="">

                              <option value="Fábricas">Fábricas</option>
                    <option value="Basurero">Basurero</option>
                    <option value="Mina">Mina</option>
                    <option value="Otro">Otro lugar que genere residuos peligrosos</option>

        </select>
      </fieldset>
    </div>
    <div class="col-md-12">
      <fieldset class="form-group">
        <label for="exp_mat_peligrosos">Alguna otra exposición a materiales peligrosos:</label>
                    <input type="text" class="form-control" id="exp_mat_peligrosos" name="exp_mat_peligrosos" placeholder="">
              </fieldset>
    </div>
  </div>
  <hr>
  <p class="text-center">Interrogatorio por aparatos y sistemas</p>
  <hr>
    <p class="text-center">¿Durante el año pasado tuvo usted alguno de los siguientes sínotmas?</p>
  <div class="row">
    <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Neurológico/Psicológico</label>
        <select class="selectize-multiple" name="neurologico_psicologico[]" placeholder="Neurológico/Psicológico" multiple="">

                              <option value="DCFI">Dolores de la cabeza frecuentes o intensos</option>
                    <option value="DFD">Dificultad para dormir</option>
                    <option value="VM">Vertigo o mareos</option>
                    <option value="PM(C)">Problemas con la memoria(Concentración)</option>
                    <option value="Tem">Temblor</option>
                    <option value="Ner">Nerviosismo</option>
                    <option value="Dep">Depresión</option>
                    <option value="AC">Ataques o Convulsiones</option>
                    <option value="PAPC">Paralisis de alguna parte del cuerpo</option>
                    <option value="PET">Problemas de estres en el trabajo</option>
                    <option value="PVF">Problemas en tu vida familiar</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Gastro Intestinal</label>
        <select class="selectize-multiple" name="gastro_intestinal[]" placeholder="Gastro Intestinal" multiple="">

                              <option value="PT">Problemas al tragar</option>
                    <option value="DAI">Dolor abdominal/Indigestion cronica</option>
                    <option value="CE">Cambio en sus evacuaciones</option>
                    <option value="DP">Diarrea persistente</option>
                    <option value="ENS">Evacuaciones negras o sangre</option>
                    <option value="VR">Vomitos repetitivos</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Genitourinario</label>
        <select class="selectize-multiple" name="genitourinario[]" placeholder="Genitourinario" multiple="">

                              <option value="PNOMU">Por la noche orina más de una vez</option>
                    <option value="OSPR">Orina con sangre/Piedras en el riñon</option>
                    <option value="PO">Problemas al orinar</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Cardiovascular</label>
        <select class="selectize-multiple" name="cardiovascular[]" placeholder="Cardiovascular" multiple="">

                              <option value="DP">Dolor de pecho</option>
                    <option value="Pal">Palpitaciones</option>
                    <option value="HT">Hinchazón de tobillos</option>
                    <option value="CPC">Calambre en las piernas al caminar</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Pulmunar</label>
        <select class="selectize-multiple" name="pulmunar[]" placeholder="Pulmunar" multiple="">

                              <option value="FA">Le falta el aire</option>
                    <option value="SP">Silbidos en el pecho</option>
                    <option value="TP">Tos persistente</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Endocrino</label>
        <select class="selectize-multiple" name="endocrino[]" placeholder="Endocrino" multiple="">

                              <option value="AP">Aumento o perdida de peso de mas de 5kgs</option>

        </select>
      </fieldset>
    </div>

        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Musculoesqueletico</label>
        <select class="selectize-multiple" name="musculoesqueletico[]" placeholder="Musculoesqueletico" multiple="">

                              <option value="DA">Dolor Articulaciones</option>
                    <option value="PEC">Problemas espalda/cuello</option>
                    <option value="CE">Cansancio excesivo</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Inmunológico</label>
        <select class="selectize-multiple" name="inmunologico[]" placeholder="Inmunológico" multiple="">

                              <option value="GC">Ganglo en cuello</option>
                    <option value="AI">Axila o ingle</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Dermatológico</label>
        <select class="selectize-multiple" name="dermatologico[]" placeholder="Dermatológico" multiple="">

                              <option value="PP">Problemas en la piel</option>

        </select>
      </fieldset>
    </div>
        <div class="col-md-4">
      <fieldset class="form-group">
        <label class="">Hematológico</label>
        <select class="selectize-multiple" name="hematologico[]" placeholder="Hematológico" multiple="">

                              <option value="SC">Sangrado poco comunes</option>

        </select>
      </fieldset>
    </div>
  </div>
  <hr>
  <p class="text-center">Alergias</p>
  <hr>
  <div class="row">
    <div class="col-md-6">
      <fieldset class="form-group">
        <label class="d-block">Reacción alergica a medicamentos</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-success" name="reaccion_alergicas_medicamentos" value="si" id="reac_alergica_si">
              <label class="custom-control-label" for="reac_alergica_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-success" checked="" name="reaccion_alergicas_medicamentos" value="no" id="reac_alergica_no">
              <label class="custom-control-label" for="reac_alergica_no">no</label>
            </div>
              </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="problema_salud">¿Algún problema de salud en este momento?</label>
                    <input type="text" class="form-control" id="problema_salud" name="problema_salud" placeholder="">
              </fieldset>
    </div>
  </div>
  <hr>
  <p class="text-center">Reproductivo</p>
  <hr>
    <div class="row">
                <div class="col-md-6">
      <fieldset class="form-group">
        <label class="">Mencione si ha presentado lo siguiente</label>
        <select class="selectize-multiple" name="padecimientos_hombre[]" placeholder="Padecimientos" multiple="">

                                                            <option value="HIH">¿Han tratado de tener hijos por más de un año sin exito?</option>

        </select>
      </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="otros_padecimientos_hombre">Otros padecimientos</label>
                  <input type="text" class="form-control" id="otros_padecimientos_hombre" name="otros_padecimientos_hombre">
              </fieldset>
    </div>
      </div>`;
var HLMVacio = `
<div class="row">
      <div class="col-md-6">
        <label>
          Ha realizado unos de los siguientes trabajos:
        </label>
        <select class="selectize-multiple" name="trabajos_realizados[]" placeholder="Trabajos" multiple="">

                    <option value="Granjero">Granjero</option>

                    <option value="Colocando aisalantes">Colocando aisalantes</option>

                    <option value="Textiles">Textiles</option>

                    <option value="Canteras">Canteras</option>|

                    <option value="Militares">Militares</option>

                    <option value="Minas">Minas</option>

                    <option value="Fundación">Fundación</option>

                    <option value="Petroquimicos">Petroquimicos</option>

                    <option value="Construcción">Construcción</option>

                    <option value="Mecanicos">Mecanicos</option>

                    <option value="Leñador">Leñador</option>

                    <option value="Embarques">Embarques</option>

                    <option value="Pulidor de arena">Pulidor de arena</option>

                    <option value="Pintor">Pintor</option>

                    <option value="Manuf. de papel">Manuf. de papel</option>

                    <option value="Desengrasador">Desengrasador</option>

        </select>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="otros_trabajos_">Otros Trabajos</label>
                    <input type="text" class="form-control" id="otros_trabajos_" name="otrosTrabajos" placeholder="">
                  </fieldset>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label>
          ¿Se ha expuesto o ha trabajado con alguno de estos materiales?
        </label>
        <select class="selectize-multiple" name="materiales_expuesto[]" placeholder="Materiales" multiple="">
                    <option value="Berilio">Berilio</option>

                    <option value="Humo de soldadura">Humo de soldadura</option>

                    <option value="Plastico/Hule">Plastico/Hule</option>

                    <option value="Ruido">Ruido</option>

                    <option value="Asbesto">Asbesto</option>

                    <option value="Dioxido de azufre">Dioxido de azufre</option>

                    <option value="Radiación">Radiación</option>

                    <option value="Plomo">Plomo</option>

                    <option value="Caustico(Acido,Base)">Caustico(Acido,Base)</option>

                    <option value="Silice">Silice</option>

                    <option value="Gases de cloro">Gases de cloro</option>

                    <option value="Traunma fis. Repetido">Traunma fis. Repetido</option>

                    <option value="Arsenico">Arsenico</option>

                    <option value="Talco">Talco</option>

                    <option value="Gases/Humo irritante">Gases/Humo irritante</option>

                    <option value="Fluoruro">Fluoruro</option>

                    <option value="Insecticidas/Otros">Insecticidas/Otros</option>

                    <option value="Otros materiales">Otros materiales</option>

                    <option value="Otros aceites">Otros aceites</option>

                    <option value="Otros polvos">Otros polvos</option>

                    <option value="Otros solventes">Otros solventes</option>

                    <option value="Otras fibras sinteticas">Otras fibras sinteticas</option>

                    <option value="Otro argon">Otro argon</option>

        </select>
      </div>

    </div>
    <div class="row">
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="otras_exposiciones">Otras exposiciones</label>
                    <input type="text" class="form-control" id="otras_exposiciones" name="otras_exposiciones">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Empleo Actual</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="nomEmpresa_actual">Nombre de la empresa:</label>
                    <input type="text" class="form-control" id="nomEmpresa_actual" name="nombreEmpresa_actual" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="actul_puesto">Puesto:</label>
                    <input type="text" class="form-control" id="actul_puesto" name="actual_puesto" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="actual_empleo_actividad">Descripción de su actividad:</label>
                    <input type="text" class="form-control" id="actual_empleo_actividad" name="actual_empleo_actividad" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="actual_empleo_duracion">Duración del empleo:</label>
                    <input type="text" class="form-control" id="actual_empleo_duracion" name="actual_empleo_duracion" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="actual_equipoS">Equipo de seguridad usado:</label>
                    <input type="text" class="form-control" id="actual_equipoS" name="actual_equipoS" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="actual_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                    <input type="text" class="form-control" id="actual_empleo_danos" name="actual_empleo_danos" placeholder="">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Empleo Anterior</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="nomEmpresa_anterior">Nombre de la empresa:</label>
                    <input type="text" class="form-control" id="nomEmpresa_anterior" name="nomEmpresa_anterior" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="anterior_puesto">Puesto:</label>
                    <input type="text" class="form-control" id="anterior_puesto" name="anterior_puesto" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="anterior_empleo_actividad">Descripción de su actividad:</label>
                    <input type="text" class="form-control" id="anterior_empleo_actividad" name="anterior_empleo_actividad" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="anterior_empleo_duracion">Duración del empleo:</label>
                    <input type="text" class="form-control" id="anterior_empleo_duracion" name="anterior_empleo_duracion" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="anteriro_equipoS">Equipo de seguridad usado:</label>
                    <input type="text" class="form-control" id="anteriro_equipoS" name="anteriro_equipoS" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="anterior_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                    <input type="text" class="form-control" id="anterior_empleo_danos" name="anterior_empleo_danos" placeholder="">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Empleo Anterior</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="nomEmpresa_anterior_two">Nombre de la empresa:</label>
                    <input type="text" class="form-control" id="nomEmpresa_anterior_two" name="nomEmpresa_anterior_two" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="two_anterior_puesto">Puesto:</label>
                    <input type="text" class="form-control" id="two_anterior_puesto" name="two_anterior_puesto" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="two_anterior_empleo_actividad">Descripción de su actividad:</label>
                    <input type="text" class="form-control" id="two_anterior_empleo_actividad" name="two_anterior_empleo_actividad" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="two_anterior_empleo_duracion">Duración del empleo:</label>
                    <input type="text" class="form-control" id="two_anterior_empleo_duracion" name="two_anterior_empleo_duracion" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="two_anteriro_equipoS">Equipo de seguridad usado:</label>
                    <input type="text" class="form-control" id="two_anteriro_equipoS" name="two_anteriro_equipoS" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="two_anterior_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                    <input type="text" class="form-control" id="two_anterior_empleo_danos" name="two_anterior_empleo_danos" placeholder="">
                  </fieldset>
      </div>
    </div>
    <hr>
        <p class="text-center">Equipo de seguridad usando en el puesto actual</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <select class="selectize-multiple" name="equipo_seguridad[]" placeholder="Equipo de seguridad" multiple="">

                                    <option value="R">Respiradores</option>
                        <option value="CSC">Calzado de seguridad con/sin casquillos</option>
                        <option value="MP">Mascarillas para polvo</option>
                        <option value="TA">Tapones auditivos</option>
                        <option value="C">Cascos</option>
                        <option value="Res">Respirador</option>
                        <option value="LS">Lentes de seguridad con o sin aumento</option>
                        <option value="LimEPP">Limitante para utilizar EPP(Equipo de protección personal)</option>

          </select>
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label class="d-block">¿Usa lentes graduados?</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="examen_vista[]" value="si" id="lentes_si">
            <label class="custom-control-label" for="lentes_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="examen_vista[]" value="no" id="lentes_no">
            <label class="custom-control-label" for="lentes_no">no</label>
          </div>

        </fieldset>
        <fieldset class="form-group lentes">
          <label for="examen_vista">Fecha de último examen</label>
                    <input type="date" class="form-control lentes" id="examen_vista" name="examen_vista[]" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label class="d-block">¿Usa lentes de contacto?</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" name="uso_lentes_graduados[]" value="si" id="lentes_graduados_si">
            <label class="custom-control-label" for="lentes_graduados_si" onclick="showControll('graduados','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked="" name="uso_lentes_graduados[]" value="no" id="lentes_graduados_no">
            <label class="custom-control-label" for="lentes_graduados_no" onclick="hideControll('graduados','show')">no</label>
          </div>

                  </fieldset>
        <fieldset class="form-group graduados" style="display:none">
          <label for="examen_vista">¿De que tipo?</label>
                    <input type="text" class="form-control graduados" id="examen_vista" name="uso_lentes_graduados[]" placeholder="">
                  </fieldset>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="">Otras exposiciones:</label>
                    <input type="text" class="form-control" id="" name="otrasexpPuesto" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label class="d-block">¿Alguien de su familia trabaja con materiales peligrosos? (Asbesto, Plomo, Etc.)</label>

                        <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" name="ma_peligroso_fam" value="si" id="ma_fam_peligroso_si">
                <label class="custom-control-label" for="ma_fam_peligroso_si">si</label>
              </div>
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" checked="" name="ma_peligroso_fam" value="no" id="ma_fam_peligroso_no">
                <label class="custom-control-label" for="ma_fam_peligroso_no">no</label>
              </div>
                  </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="d-block">Alguna vez ha vivido cerca de:</label>
          <select class="selectize-multiple" name="lugar_vivido[]" placeholder="Lugares donde has vivido" multiple="">

                                    <option value="Fábricas">Fábricas</option>
                        <option value="Basurero">Basurero</option>
                        <option value="Mina">Mina</option>
                        <option value="Otro">Otro lugar que genere residuos peligrosos</option>

          </select>
        </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="exp_mat_peligrosos">Alguna otra exposición a materiales peligrosos:</label>
                        <input type="text" class="form-control" id="exp_mat_peligrosos" name="exp_mat_peligrosos" placeholder="">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Interrogatorio por aparatos y sistemas</p>
    <hr>
        <p class="text-center">¿Durante el año pasado tuvo usted alguno de los siguientes sínotmas?</p>
    <div class="row">
      <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Neurológico/Psicológico</label>
          <select class="selectize-multiple" name="neurologico_psicologico[]" placeholder="Neurológico/Psicológico" multiple="">

                                    <option value="DCFI">Dolores de la cabeza frecuentes o intensos</option>
                        <option value="DFD">Dificultad para dormir</option>
                        <option value="VM">Vertigo o mareos</option>
                        <option value="PM(C)">Problemas con la memoria(Concentración)</option>
                        <option value="Tem">Temblor</option>
                        <option value="Ner">Nerviosismo</option>
                        <option value="Dep">Depresión</option>
                        <option value="AC">Ataques o Convulsiones</option>
                        <option value="PAPC">Paralisis de alguna parte del cuerpo</option>
                        <option value="PET">Problemas de estres en el trabajo</option>
                        <option value="PVF">Problemas en tu vida familiar</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Gastro Intestinal</label>
          <select class="selectize-multiple" name="gastro_intestinal[]" placeholder="Gastro Intestinal" multiple="">

                                    <option value="PT">Problemas al tragar</option>
                        <option value="DAI">Dolor abdominal/Indigestion cronica</option>
                        <option value="CE">Cambio en sus evacuaciones</option>
                        <option value="DP">Diarrea persistente</option>
                        <option value="ENS">Evacuaciones negras o sangre</option>
                        <option value="VR">Vomitos repetitivos</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Genitourinario</label>
          <select class="selectize-multiple" name="genitourinario[]" placeholder="Genitourinario" multiple="">

                                    <option value="PNOMU">Por la noche orina más de una vez</option>
                        <option value="OSPR">Orina con sangre/Piedras en el riñon</option>
                        <option value="PO">Problemas al orinar</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Cardiovascular</label>
          <select class="selectize-multiple" name="cardiovascular[]" placeholder="Cardiovascular" multiple="">

                                    <option value="DP">Dolor de pecho</option>
                        <option value="Pal">Palpitaciones</option>
                        <option value="HT">Hinchazón de tobillos</option>
                        <option value="CPC">Calambre en las piernas al caminar</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Pulmunar</label>
          <select class="selectize-multiple" name="pulmunar[]" placeholder="Pulmunar" multiple="">

                                    <option value="FA">Le falta el aire</option>
                        <option value="SP">Silbidos en el pecho</option>
                        <option value="TP">Tos persistente</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Endocrino</label>
          <select class="selectize-multiple" name="endocrino[]" placeholder="Endocrino" multiple="">

                                    <option value="AP">Aumento o perdida de peso de mas de 5kgs</option>

          </select>
        </fieldset>
      </div>

            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Musculoesqueletico</label>
          <select class="selectize-multiple" name="musculoesqueletico[]" placeholder="Musculoesqueletico" multiple="">

                                    <option value="DA">Dolor Articulaciones</option>
                        <option value="PEC">Problemas espalda/cuello</option>
                        <option value="CE">Cansancio excesivo</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Inmunológico</label>
          <select class="selectize-multiple" name="inmunologico[]" placeholder="Inmunológico" multiple="">

                                    <option value="GC">Ganglo en cuello</option>
                        <option value="AI">Axila o ingle</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Dermatológico</label>
          <select class="selectize-multiple" name="dermatologico[]" placeholder="Dermatológico" multiple="">

                                    <option value="PP">Problemas en la piel</option>

          </select>
        </fieldset>
      </div>
            <div class="col-md-4">
        <fieldset class="form-group">
          <label class="">Hematológico</label>
          <select class="selectize-multiple" name="hematologico[]" placeholder="Hematológico" multiple="">

                                    <option value="SC">Sangrado poco comunes</option>

          </select>
        </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Alergias</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label class="d-block">Reacción alergica a medicamentos</label>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" name="reaccion_alergicas_medicamentos" value="si" id="reac_alergica_si">
                <label class="custom-control-label" for="reac_alergica_si">si</label>
              </div>
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" checked="" name="reaccion_alergicas_medicamentos" value="no" id="reac_alergica_no">
                <label class="custom-control-label" for="reac_alergica_no">no</label>
              </div>
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="problema_salud">¿Algún problema de salud en este momento?</label>
                        <input type="text" class="form-control" id="problema_salud" name="problema_salud" placeholder="">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Reproductivo</p>
    <hr>
        <div class="row">
                        <div class="col-md-6">
        <fieldset class="form-group">
          <label class="">Mencione si ha presentado lo siguiente</label>
          <select class="selectize-multiple" name="padecimientos_hombre[]" placeholder="Padecimientos" multiple="">

                                                                  <option value="HIH">¿Han tratado de tener hijos por más de un año sin exito?</option>

          </select>
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="otros_padecimientos_hombre">Otros padecimientos</label>
                      <input type="text" class="form-control" id="otros_padecimientos_hombre" name="otros_padecimientos_hombre">
                  </fieldset>
      </div>
          </div>`;
var examenFVacio = `
 <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="apariencia">Apariencia general:</label>
                    <input type="text" class="form-control" id="apariencia" name="apariencia" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="temperatura">Temperatura</label>
                    <input type="text" class="form-control" id="temperatura" name="temperatura" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="fc">Fc:</label>
                    <input type="text" class="form-control" id="fc" name="fc" onkeypress="return validaNumericos(event)" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="fr">Fr:</label>
                    <input type="text" class="form-control" id="fr" name="fr" onkeypress="return validaNumericos(event)" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-2">
        <fieldset class="form-group">
          <label for="altura">Altura:</label>
                    <input type="text" class="form-control imc" pattern="^[0-9]+(.[0-9]+)?$" id="altura" placeholder="Metros" name="altura" step="0.01">
                  </fieldset>
      </div>
      <div class="col-md-2">
        <fieldset class="form-group">
          <label for="peso">Peso:</label>
                    <input type="text" class="form-control imc" id="peso" placeholder="Kilogramos" name="peso" pattern="^[0-9]+(.[0-9]+)?$">
                  </fieldset>
      </div>
      <div class="col-md-2">
        <fieldset class="form-group">
          <label for="imc">IMC</label>
                    <input type="text" class="form-control" id="imc" pattern="^[0-9]+(.[0-9]+)?$" name="imc" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="presion_derecho">Presión sanguínea: Brazo derecho</label>
                    <input type="text" class="form-control" id="presion_derecho" name="presion_derecho" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="presion_izquierdo">Brazo izquierdo</label>
                    <input type="text" class="form-control" id="presion_izquierdo" name="presion_izquierdo" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_cabeza">Cabeza y cuero/ojos</label>
                    <input type="text" class="form-control" id="observaciones_cabeza" name="observaciones_cabeza" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_oidos">Oidos/Nariz/Boca</label>
                    <input type="text" class="form-control" id="observaciones_oidos" name="observaciones_oidos" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_dientes">Dientes/Faringe</label>
                    <input type="text" class="form-control" id="observaciones_dientes" name="observaciones_dientes" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_cuello">Cuello</label>
                    <input type="text" class="form-control" id="observaciones_cuello" name="observaciones_cuello" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_tiroides">Tiroides</label>
                    <input type="text" class="form-control" id="observaciones_tiroides" name="observaciones_tiroides" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_nodo">Nodo Linfático</label>
                    <input type="text" class="form-control" id="observaciones_nodo" name="observaciones_nodo" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_torax">Torax y Pulmones</label>
                    <input type="text" class="form-control" id="observaciones_torax" name="observaciones_torax" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_pecho">Pecho</label>
                    <input type="text" class="form-control" id="observaciones_pecho" name="observaciones_pecho" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_corazon">Corazón</label>
                    <input type="text" class="form-control" id="observaciones_corazon" name="observaciones_corazon" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_abdomen">Abdomen</label>
                    <input type="text" class="form-control" id="observaciones_abdomen" name="observaciones_abdomen" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_rectal">Rectal Digital</label>
                    <input type="text" class="form-control" id="observaciones_rectal" name="observaciones_rectal" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_genitales">Genitales</label>
                    <input type="text" class="form-control" id="observaciones_genitales" name="observaciones_genitales" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_columna">Columna vertebral</label>
                    <input type="text" class="form-control" id="observaciones_columna" name="observaciones_columna" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_piel">Piel</label>
                    <input type="text" class="form-control" id="observaciones_piel" name="observaciones_piel" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_pulso">Pulso Arteral</label>
                    <input type="text" class="form-control" id="observaciones_pulso" name="observaciones_pulso" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_extremidades">Extremidades</label>
                    <input type="text" class="form-control" id="observaciones_extremidades" name="observaciones_extremidades" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_musculoesque">Musculoesqueleto</label>
                    <input type="text" class="form-control" id="observaciones_musculoesque" name="observaciones_musculoesque" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_relejos">Reflejos y Neurológicos</label>
                    <input type="text" class="form-control" id="observaciones_relejos" name="observaciones_relejos" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_mental">Estado Mental</label>
                    <input type="text" class="form-control" id="observaciones_mental" name="observaciones_mental" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_comentarios">Otros Comentarios</label>
                    <input type="text" class="form-control" id="observaciones_comentarios" name="observaciones_comentarios" placeholder="Observaciones Detectadas">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Agudeza Visual</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="agudeza_izquierda">Ojo Izquierdo</label>
                    <input type="text" class="form-control" id="agudeza_izquierda" name="agudeza_izquierdo" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="agudeza_derecho">Ojo Derecho</label>
                    <input type="text" class="form-control" id="agudeza_derecho" name="agudeza_derecho" placeholder="">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Valoración y plan de recomendaciones del personal médico</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="uso_epp">Uso de EPP:</label>
                    <input type="text" class="form-control" id="uso_epp" name="uso_epp" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="audiometria">Audiometria</label>
                    <input type="text" class="form-control" id="audiometria" name="audiometria" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="respiradores_r">Respiradoes</label>
                    <input type="text" class="form-control" id="respiradores_r" name="respiradores_r" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="reentrenar">Reentrenar en:</label>
                    <input type="text" class="form-control" id="reentrenar" name="reentrenar" placeholder="">
                  </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="otra_recomendacion">Otros</label>
                    <input type="text" class="form-control" id="otra_recomendacion" name="otra_recomendacion" placeholder="">
                  </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Recomendaciones de estilo de vida</p>
    <hr>
        <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="reentrenar">Recomendaciones</label>
          <select class="selectize-multiple" name="recomendaciones_vida[]" placeholder="Recomendaciones" multiple="">
                                            <option value="Df">Dejar de fumar</option>
                                <option value="RA">Reducir consume de alcohol</option>
                                <option value="BP">Bajar de peso</option>
                                <option value="TRC">Tratamineto para reducir de colesterol</option>
                                <option value="ME">Manejo de estres</option>
                                <option value="EPP">Examen del pecho personal</option>
                                <option value="ETP">Examen Testicular Personal</option>
                                <option value="RE">Rutina de ejercicio</option>
                                <option value="PRE">Programa regular de ejercicios</option>
                                <option value="O">Otros</option>

          </select>
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="diagnostico_audiologico">Diagnostico Audiologico</label>
                      <textarea name="diagnostico_audiologico" id="diagnostico_audiologico" class="form-control" rows="8" cols="80"></textarea>
                  </fieldset>
      </div>
    </div>`;
var resultadoVacio = `<div id="AlertaResultado">
  </div>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="estado_salud">Estado de salud</label>
                    <input type="text" class="form-control" id="estado_salud" name="estado_salud" placeholder="" required="">
                  </fieldset>
      </div>
            <div class="col-md-6">
        <fieldset class="form-group">
          <label for="estado_salud">Resultados de Aptitud</label>
          <select class="form-control custom-select" name="resultado_aptitud">
                                    <option value="Cumple" requerimientos="" médicos="" (satisfactorio)="">Cumple Requerimientos Médicos (Satisfactorio)</option>
                        <option value="Cumple" requerimientos="" médicos="" condicionados="" (satisfactorio="" condicionado)="">Cumple Requerimientos Médicos Condicionados (Satisfactorio Condicionado)</option>
                        <option value="No" cumple="" requerimientos="" médicos="" (no="" satisfactorios)="">No Cumple Requerimientos Médicos (No Satisfactorios)</option>
                                  </select>
        </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="comentarios_finales">Comentarios</label>
                    <textarea class="form-control" name="comentarios_finales" id="comentarios_finales" rows="8" cols="80"></textarea>
                  </fieldset>
      </div>
    </div>`;
function RecargarForm(curp) {
  if ($('input:radio[name=FormVacio]:checked').val() == "Vacio") {
    window.location.href = "../Historial-Clinico/" + curp + "/" + 0;
  }
  else {
    window.location.href = "../Historial-Clinico/" + curp + "/" + 1;
  }
}


function formatoOnChange(sel) {
  if (sel.value == "admision_contratista") {
    divex = document.getElementById("admision_contratista");
    divex.style.display = "";

  }

  else if (sel.value == "cadena_custodia"){

    divex = document.getElementById("cadena_custodia");
    divex.style.display = "";

    divcc = document.getElementById("examen_medico");
    divcc.style.display = "none";

  }


}
