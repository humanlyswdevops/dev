
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(".search").keyup( function() {
    var contador = 0;
    var rex = new RegExp($(this).val(), 'i');
    $(".cards_item").hide();
    $(".cards_item").filter( function() {
        if (rex.test($(this).text()))
        contador ++;
        return rex.test($(this).text());
    }).show();
    if (contador > 0) {
        $(".aviso-vacio").hide();
    } else {
        $(".aviso-vacio").show();
    }
});
