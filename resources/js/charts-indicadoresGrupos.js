var idGrupoSelect = $('#nomGrupo').val();

Enfermedades(idGrupoSelect);
Imc(idGrupoSelect);
TotalEmpleados(idGrupoSelect);

const selectElement = document.querySelector('.nameGrupo');

selectElement.addEventListener('change', (event) => {
    Enfermedades( $('#nomGrupo').val());
    Imc( $('#nomGrupo').val());
    TotalEmpleados( $('#nomGrupo').val());
});

// Se obtienen los datos de las enfermedades por edad y género.
function Enfermedades(idGrupoSelect)
{
  $.ajax({
      type:'get',
      dataType:'json',
      url:"getDatosEnfermedadesGrupo/"+idGrupoSelect,
      success: function(datosEnfermedades) {
        console.log(datosEnfermedades);
          var diabetes = datosEnfermedades.diabetes;
          var hipertension = datosEnfermedades.hipertension;

          var datosDiabetes = {
              'todos': [
                  ['< 18', diabetes.hombres[0] + diabetes.mujeres[0]],
                  ['18 - 28', diabetes.hombres[1] + diabetes.mujeres[1]],
                  ['29 - 39', diabetes.hombres[2] + diabetes.mujeres[2]],
                  ['40 - 59', diabetes.hombres[3] + diabetes.mujeres[3]],
                  ['60+', diabetes.hombres[4] + diabetes.mujeres[4]]
              ],
              'hombres': [
                  ['< 18', diabetes.hombres[0]],
                  ['18 - 28', diabetes.hombres[1]],
                  ['29 - 39', diabetes.hombres[2]],
                  ['40 - 59', diabetes.hombres[3]],
                  ['60+', diabetes.hombres[4]]
              ],
              'mujeres': [
                  ['< 18', diabetes.mujeres[0]],
                  ['18 - 28', diabetes.mujeres[1]],
                  ['29 - 39', diabetes.mujeres[2]],
                  ['40 - 59', diabetes.mujeres[3]],
                  ['60+', diabetes.mujeres[4]]
              ]
          };

          var datosHipertension = {
              'todos': [
                  ['< 18', hipertension.hombres[0] + hipertension.mujeres[0]],
                  ['18 - 28', hipertension.hombres[1] + hipertension.mujeres[1]],
                  ['29 - 39', hipertension.hombres[2] + hipertension.mujeres[2]],
                  ['40 - 59', hipertension.hombres[3] + hipertension.mujeres[3]],
                  ['60+', hipertension.hombres[4] + hipertension.mujeres[4]]
              ],
              'hombres': [
                  ['< 18', hipertension.hombres[0]],
                  ['18 - 28', hipertension.hombres[1]],
                  ['29 - 39', hipertension.hombres[2]],
                  ['40 - 59', hipertension.hombres[3]],
                  ['60+', hipertension.hombres[4]]
              ],
              'mujeres': [
                  ['< 18', hipertension.mujeres[0]],
                  ['18 - 28', hipertension.mujeres[1]],
                  ['29 - 39', hipertension.mujeres[2]],
                  ['40 - 59', hipertension.mujeres[3]],
                  ['60+', hipertension.mujeres[4]]
              ]
          };

          var chart = Highcharts.chart('container-enfermedades', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Enfermedades por edad'
              },
              credits: {
                  enabled: false
              },
              subtitle: {
                  text: 'Todos'
              },
              xAxis: {
                  categories: [
                      '< 18',
                      '18 - 28',
                      '29 - 39',
                      '40 - 59',
                      '60+'
                  ],
                  crosshair: true
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Empleados con cada enfermedad'
                  }
              },
              tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                      '<td style="padding:0"><b>{point.y}</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
              },
              plotOptions: {
                  column: {
                      pointPadding: 0.2,
                      borderWidth: 0
                  }
              },
              series: [{
                  name: 'Diabetes',
                  data: datosDiabetes['todos'].slice(),
                  color: '#e74c3c'

              }, {
                  name: 'Hipertensión',
                  data: datosHipertension['todos'].slice(),
                  color: '#419c99'
              }]
          });

          var tabs = ['todos', 'hombres', 'mujeres'];

          tabs.forEach(function (tab) {
              var btn = document.getElementById(tab + '-enfermedades');

              btn.addEventListener('click', function () {

                  document.querySelectorAll('.buttons.buttons-enfermedades button.active').forEach(function (active) {
                      active.className = '';
                  });

                  btn.className = 'active';

                  chart.update({
                      title: {
                          text: 'Enfermedades por edad'
                      },
                      subtitle: {
                          text: tab.charAt(0).toUpperCase() +  tab.slice(1)
                      },
                      series: [{
                          name: 'Diabetes',
                          data: datosDiabetes[tab].slice()
                      }, {
                          name: 'Hipertensión',
                          data: datosHipertension[tab].slice()
                      }]
                  }, true, false, {
                      duration: 800
                  });
              });
          });
      }
  });

}

function Imc(idGrupoSelect)
{
  // Se obtienen los datos de IMC por género.
  $.ajax({
      type:'get',
      dataType:'json',
      url:"getDatosIMCGrupo/"+idGrupoSelect,
      success: function(imc) {

          var datosIMC = {
              'todos': [
                  ['Debajo del peso', imc.hombres[0] + imc.mujeres[0]],
                  ['Saludable', imc.hombres[1] + imc.mujeres[1]],
                  ['Sobrepeso', imc.hombres[2] + imc.mujeres[2]],
                  ['Obesidad', imc.hombres[3] + imc.mujeres[3]],
                  ['Obesidad extrema', imc.hombres[4] + imc.mujeres[4]]
              ],
              'hombres': [
                  ['Debajo del peso', imc.hombres[0]],
                  ['Saludable', imc.hombres[1]],
                  ['Sobrepeso', imc.hombres[2]],
                  ['Obesidad', imc.hombres[3]],
                  ['Obesidad extrema', imc.hombres[4]]
              ],
              'mujeres': [
                  ['Debajo del peso', imc.mujeres[0]],
                  ['Saludable', imc.mujeres[1]],
                  ['Sobrepeso', imc.mujeres[2]],
                  ['Obesidad', imc.mujeres[3]],
                  ['Obesidad extrema', imc.mujeres[4]]
              ]
          };

          var chart = Highcharts.chart('container-imc', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Rangos de IMC'
              },
              credits: {
                  enabled: false
              },
              subtitle: {
                  text: 'Todos'
              },
              xAxis: {
                  categories: [
                      'Debajo del peso',
                      'Saludable',
                      'Sobrepeso',
                      'Obesidad',
                      'Obesidad extrema'
                  ],
                  crosshair: true
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Empleados en cada rango'
                  }
              },
              tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                      '<td style="padding:0"><b>{point.y}</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
              },
              plotOptions: {
                  column: {
                      pointPadding: 0.2,
                      borderWidth: 0
                  }
              },
              series: [{
                  name: 'IMC',
                  data: datosIMC['todos'].slice(),
                  color: '#e74c3c'

              }]
          });

          var tabs = ['todos', 'hombres', 'mujeres'];

          tabs.forEach(function (tab) {
              var btn = document.getElementById(tab + '-imc');

              btn.addEventListener('click', function () {

                  document.querySelectorAll('.buttons.buttons-imc button.active').forEach(function (active) {
                      active.className = '';
                  });

                  btn.className = 'active';

                  chart.update({
                      title: {
                          text: 'Rangos de IMC'
                      },
                      subtitle: {
                          text: tab.charAt(0).toUpperCase() +  tab.slice(1)
                      },
                      series: [{
                          name: 'IMC',
                          data: datosIMC[tab].slice()
                      }]
                  }, true, false, {
                      duration: 800
                  });
              });
          });
      }
  });
}

function TotalEmpleados(idGrupoSelect)
{
  // Se obtienen los datos de cantidades de empleados por género.
  $.ajax({
      type:'get',
      dataType:'json',
      url:"getDatosGenerosGrupo/"+idGrupoSelect,
      success: function(generos) {
          var hombres = generos.hombres;
          var mujeres = generos.mujeres;

          Highcharts.chart('container-generos', {
              chart: {
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
              },
              title: {
                  text: 'Personal por género'
              },
              credits: {
                  enabled: false
              },
              subtitle: {
                  text: 'Total: ' + (hombres + mujeres)
              },
              tooltip: {
                  pointFormat: 'Porcentaje: <b>{point.y:.1f}%</b><br>Cantidad: <b>{point.y2}</b>'
              },
              accessibility: {
                  point: {
                      valueSuffix: '%'
                  }
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: true,
                          format: '<b>{point.name}</b>: {point.y:.1f} %'
                      }
                  }
              },
              series: [{
                  name: 'Género',
                  colorByPoint: true,
                  data: [{
                      name: 'Hombres',
                      y: hombres / (hombres + mujeres) * 100,
                      y2: hombres,
                      color: '#0000ff'
                  }, {
                      name: 'Mujeres',
                      y: mujeres / (hombres + mujeres) * 100,
                      y2: mujeres,
                      color: '#ff0044'
                  }]
              }]
          });
      }
  });
}
