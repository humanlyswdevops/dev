$(document).ready(function(){
    $('.estudios_input').hide();
    $('.links').on('click',function(e){
        $('.estudios_input').hide();
        $('i').removeClass('actives');
        $(this).parent().find('i').addClass('actives');
        $('#selected').text($(this).text());
        var id = $(this).data('nombre');
        ajaxEstudio(id);
    });

    setInterval(function(){
        $('.alert-success').hide(700, "swing");
    },2000);
});

function ajaxEstudio(id){
    var cont = '.'+id;
    $(cont).show();
}

function loadEmpleados(encryptedID) {
    $('#tabla-empleados').DataTable({
        serverSide: true,
        ajax: "getEmpleadosDeGrupo/" + encryptedID,
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "",
            "infoEmpty": "",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        columns: [
            {data: 'clave'},
            {data: 'nombre'},
            {data: 'apellido_paterno'},
            {data: 'apellido_materno'},
            {data: 'CURP'},
            {data: 'grupo_id'},
            {data: 'btn'},
        ]
    });
}

function showSeleccionarModal() {
    $('#seleccionarModal').modal('show');
}

function showProgramarModal(nombre_grupo) {
    $.ajax({
        type:'get',
        dataType:'json',
        url:"getEstudiosProgramadosGrupo/" + nombre_grupo,
        success: function(datas){
            var estudios_programados = JSON.parse(JSON.stringify(datas));

            if (estudios_programados) {
                lista_estudios = JSON.parse(estudios_programados.estudios);

                document.getElementById("inputFechaInicioGrupo").value = estudios_programados.fecha_inicial;
                document.getElementById("inputFechaFinalGrupo").value = estudios_programados.fecha_final;

                for (estudio of lista_estudios) {
                    document.getElementById("id" + estudio).checked = true;
                }
            }
            $('#programarModal').modal('show');
        }
    });
}

function showEstudioModal(estudio_programado, estudio_nombre, id_array, index) {
    var estudio_id = id_array[index];

    document.getElementById("nombre-estudio").innerHTML = estudio_nombre;
    document.getElementById("fecha-inicio").innerHTML = getFechaFormateada(estudio_programado.fecha_inicial);
    document.getElementById("fecha-fin").innerHTML = getFechaFormateada(estudio_programado.fecha_final);
    document.getElementById("inputToken").value = estudio_programado.token;
    document.getElementById("inputNombreEstudio").value = estudio_nombre;
    document.getElementById("inputIDEstudio").value = estudio_id;
    document.getElementById("inputFechaInicialEliminar").value = estudio_programado.fecha_inicial;
    document.getElementById("inputFechaFinalEliminar").value = estudio_programado.fecha_final;

    $('#estudioModal').modal('show');
}

function showProgramarEmpleadoModal(curp, nombre) {
    $.ajax({
        type:'get',
        dataType:'json',
        url:"getEstudiosProgramadosEmpleado/" + curp,
        success: function(datas){
            var registros_estudios = JSON.parse(JSON.stringify(datas));

            for (estudios_programados of registros_estudios) {
                if (estudios_programados) {
                    lista_estudios = JSON.parse(estudios_programados.estudios);

                    if (estudios_programados.grupo_id) {
                        document.getElementById("inputFechaInicioEmpleado").value = estudios_programados.fecha_inicial;
                        document.getElementById("inputFechaFinalEmpleado").value = estudios_programados.fecha_final;
                    }

                    for (estudio of lista_estudios) {
                        document.getElementById("id" + estudio + "_empleado").checked = true;
                    }
                }
            }

            document.getElementById("programar_empleado_titulo").innerHTML = "Programar estudios para " + nombre;
            document.getElementById("inputCURP").value = curp;

            $('#programarEmpleadoModal').modal('show');
        }
    });
}

function showAgregarEmpleadosModal() {

    $.ajax({
        type:'get',
        dataType:'json',
        url:"../getEmpleadosSinGrupo",
        success: function(data) {
            var empleados = JSON.parse(JSON.stringify(data));
            var LISTA = document.getElementById("lista-empleados-agregar");
            LISTA.innerHTML = "";
            var index = 0;
            for (const empleado of empleados) {
                LISTA.innerHTML += `
                    <div class="empleado-card check">
                        <label for="id${empleado.CURP}" class="btn label-empleado col-6">
                            <input type="checkbox" name="empleados_agregar[${index}]" id="id${empleado.CURP}" value="${empleado.CURP}" class="hidden custom-control-input">
                            <div class="card-content">
                                <div class="card-icon">
                                    <img src="../../resources/sass/images/user_icon_white.png" alt="Ícono de usuario" width="60px" height="60px">
                                </div>
                                <div class="card-info">
                                    <p>${empleado.nombre} ${empleado.apellido_paterno} ${empleado.apellido_materno}</p>
                                    <p>ID: ${empleado.clave}</p>
                                    <p>CURP: ${empleado.CURP}</p>
                                </div>
                            </div>
                        </label>
                    </div>
                `;
                index++;
            }
            $('.label-empleado input').click(function() {
                $(this).parent().parent().toggleClass("checked-card");
            });+

            $('#agregarEmpleadosModal').modal('show');
        }
    });
    }

$(".campo-busqueda").keyup( function() {
    var contador = 0;
    var rex = new RegExp($(this).val(), 'i');

    $(".empleado-card").hide();

    $(".empleado-card").filter( function() {
        if (rex.test($(this).text()))
            contador ++;

        return rex.test($(this).text());
    }).show();

    if (contador > 0) {
        $(".aviso-vacio").hide();
    } else {
        $(".aviso-vacio").show();
    }
});

function getFechaFormateada(fecha) {
    var miFecha = new Date(fecha);
    var diaNumero = miFecha.getUTCDate();
    var diaNombre = getNombreDia(miFecha.getUTCDay());
    var mesNombre = getNombreMes(miFecha.getUTCMonth());
    var ano = miFecha.getUTCFullYear();

    var fechaCompleta = diaNombre + " " + diaNumero + " de " + mesNombre + " de " + ano;

    return fechaCompleta;
}

function getNombreDia(numero) {
    var nombre;
    switch (numero) {
        case 0:
            nombre = "Domingo";
            break;
        case 1:
            nombre = "Lunes";
            break;
        case 2:
            nombre = "Martes";
            break;
        case 3:
            nombre = "Miércoles";
            break;
        case 4:
            nombre = "Jueves";
            break;
        case 5:
            nombre = "Viernes";
            break;
        case 6:
            nombre = "Sábado";
            break;
        default:
            nombre = "";
            break;
    }

    return nombre;
}

function getNombreMes(numero) {
    var nombre;

    switch (numero) {
        case 0:
            nombre = "enero";
            break;
        case 1:
            nombre = "febrero";
            break;
        case 2:
            nombre = "marzo";
            break;
        case 3:
            nombre = "abril";
            break;
        case 4:
            nombre = "mayo";
            break;
        case 5:
            nombre = "junio";
            break;
        case 6:
            nombre = "julio";
            break;
        case 7:
            nombre = "agosto";
            break;
        case 8:
            nombre = "septiembre";
            break;
        case 9:
            nombre = "octubre";
            break;
        case 10:
            nombre = "noviembre";
            break;
        case 11:
            nombre = "diciembre";
            break;
        default:
            nombre = "";
            break;
    }

    return nombre;
}
/*seleccionar todos*/
$(document).ready(function() {
  selected = true;
  $('#BtnSeleccionar').click(function() {
    if (selected) {
      $('.estudios_input input[type=checkbox]').prop("checked", true);
      $('#BtnSeleccionar').val('Deseleccionar');

    } else {
      $('.estudios_input input[type=checkbox]').prop("checked", false);
      $('#BtnSeleccionar').val('Seleccionar todo');
    }
    selected = !selected;
  });
});

$(document).ready(function() {
  selected = true;
  $('#BtnSeleccionar1').click(function() {
    if (selected) {
      $('.estudios-por-programar input[type=checkbox]').prop("checked",true);
      $('#BtnSeleccionar1').val('Deseleccionar');

    } else {
      $('.estudios-por-programar input[type=checkbox]').prop("checked", false);
      $('#BtnSeleccionar1').val('Seleccionar todo');
    }
    selected = !selected;
  });
});
