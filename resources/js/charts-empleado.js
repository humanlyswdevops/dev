Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

var encryptedID_global;

function showModalIndicadores(encryptedID) {
    encryptedID_global = encryptedID;
    $('#alertIndicadores').hide();
    $('#modalIndicadores').modal('show');
}

// function showModalIndicadores() {
//     // encryptedID_global = encryptedID;
//     $('#alertIndicadores').hide();
//     $('#modalIndicadores').modal('show');
// }

// Gráficas de línea.
$('#modalIndicadores').on('shown.bs.modal', function (e) {
    var fechas = [];
    var datosEstatura = [];
    var datosPeso = [];
    var datosIMC = [];
    var datosFrecResp = [];
    var datosFrecCard = [];
    var datosTemperatura = [];
    var datosOxigeno = [];

    // Se consulta la información de las gráficas
    $.ajax({
        type:'get',
        dataType:'json',
        url:"../getHistorialesClinicos/" + encryptedID_global,
        beforeSend: function(){
            $('#area-graficas').hide();
            $('#loadIndicadores').show();
          }
    })
    .done(function(historialesClinicos) {
        $('#loadIndicadores').hide();
        $('#area-graficas').show();

        for (historialClinico of historialesClinicos) {
            var examenFisico = historialClinico;
          //  var identificacion = JSON.parse(historialClinico.identificacion);
            var fecha = new Date(historialClinico.created_at);
            fecha = fecha.addDays(1);
            var fechaTexto = "" + fecha.getDate() + "/" + getNombreMesCorto(fecha.getMonth()) + "/" + fecha.getFullYear();
            fechas.push(fechaTexto);

            var estatura = examenFisico.altura != null ? examenFisico.altura : 0;
            var peso = examenFisico.peso != null ? examenFisico.peso : 0;
            var IMC = examenFisico.imc != null ? examenFisico.imc : 0;
            var frecResp = examenFisico.fr != null ? examenFisico.fr : 0;
            var frecCard = examenFisico.fc != null ? examenFisico.fc : 0;
            var temperatura = examenFisico.temperatura != null ? examenFisico.temperatura : 0;
            var oxigeno = examenFisico.oxigeno != null ? examenFisico.oxigeno : 0;

            datosEstatura.push(estatura);
            datosPeso.push(peso);
            datosIMC.push(IMC);
            datosFrecResp.push(frecResp);
            datosFrecCard.push(frecCard);
            datosTemperatura.push(temperatura);
            datosOxigeno.push(oxigeno);
        }

        require.config({
            paths: {
                echarts: '../../../app-assets/vendors/js/charts/echarts'
            }
        });


        // Configuración de gráficas.

        require(
            [
                'echarts',
                'echarts/chart/bar',
                'echarts/chart/line'
            ],

            function (ec) {
                // Inicialización de gráficas.
                var graficaEstatura = ec.init(document.getElementById('grafica-estatura'));
                var graficaPeso = ec.init(document.getElementById('grafica-peso'));
                var graficaIMC = ec.init(document.getElementById('grafica-IMC'));
                var graficaFrecResp = ec.init(document.getElementById('grafica-frec-resp'));
                var graficaFrecCard = ec.init(document.getElementById('grafica-frec-card'));
                var graficaTemperatura = ec.init(document.getElementById('grafica-temperatura'));
                var graficaOxigeno = ec.init(document.getElementById('grafica-oxigeno'));

                // Opciones por gráfica.
                opcionesOxigeno = {
                        // Setup grid
                        grid: {
                        x: 40,
                        y: 45,
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    // Add Legend
                    legend: {
                        data:['Saturación de Oxígeno']
                    },

                    // Add custom colors
                    color: ['#FF4961'],

                    // Enable drag recalculate
                    calculable : true,

                    // Horizontal Axiz
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : fechas
                        }
                    ],

                    // Vertical Axis
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value} %'
                            }
                        }
                    ],

                    // Add Series
                    series : [
                        {
                            name:'Saturación de Oxígeno',
                            type:'line',
                            data: datosOxigeno,
                        }
                    ]
                };

                opcionesEstatura = {

                    // Setup grid
                    grid: {
                        x: 40,
                        y: 45,
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    // Add Legend
                    legend: {
                        data:['Estatura']
                    },

                    // Add custom colors
                    color: ['#FF4961'],

                    // Enable drag recalculate
                    calculable : true,

                    // Horizontal Axiz
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : fechas
                        }
                    ],

                    // Vertical Axis
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value} m'
                            }
                        }
                    ],

                    // Add Series
                    series : [
                        {
                            name:'Estatura',
                            type:'line',
                            data: datosEstatura,
                        }
                    ]
                };

                opcionesPeso = {

                    // Setup grid
                    grid: {
                        x: 40,
                        y: 45,
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    // Add Legend
                    legend: {
                        data:['Peso']
                    },

                    // Add custom colors
                    color: ['#FF4961'],

                    // Enable drag recalculate
                    calculable : true,

                    // Horizontal Axiz
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : fechas
                        }
                    ],

                    // Vertical Axis
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value} kg'
                            }
                        }
                    ],

                    // Add Series
                    series : [
                        {
                            name:'Peso',
                            type:'line',
                            data: datosPeso,
                        }
                    ]
                };

                opcionesIMC = {

                    // Setup grid
                    grid: {
                        x: 40,
                        y: 45,
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    // Add Legend
                    legend: {
                        data:['IMC']
                    },

                    // Add custom colors
                    color: ['#FF4961'],

                    // Enable drag recalculate
                    calculable : true,

                    // Horizontal Axiz
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : fechas
                        }
                    ],

                    // Vertical Axis
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value} kg/m2'
                            }
                        }
                    ],

                    // Add Series
                    series : [
                        {
                            name:'IMC',
                            type:'line',
                            data: datosIMC,
                        }
                    ]
                };

                opcionesFrecResp = {

                    // Setup grid
                    grid: {
                        x: 40,
                        y: 45,
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    // Add Legend
                    legend: {
                        data:['Frecuencia respiratoria']
                    },

                    // Add custom colors
                    color: ['#FF4961'],

                    // Enable drag recalculate
                    calculable : true,

                    // Horizontal Axiz
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : fechas
                        }
                    ],

                    // Vertical Axis
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value}'
                            }
                        }
                    ],

                    // Add Series
                    series : [
                        {
                            name:'Frecuencia respiratoria',
                            type:'line',
                            data: datosFrecResp,
                        }
                    ]
                };

                opcionesFrecCard = {

                    // Setup grid
                    grid: {
                        x: 40,
                        y: 45,
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    // Add Legend
                    legend: {
                        data:['Frecuencia cardiaca']
                    },

                    // Add custom colors
                    color: ['#FF4961'],

                    // Enable drag recalculate
                    calculable : true,

                    // Horizontal Axiz
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : fechas
                        }
                    ],

                    // Vertical Axis
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value}'
                            }
                        }
                    ],

                    // Add Series
                    series : [
                        {
                            name:'Frecuencia cardiaca',
                            type:'line',
                            data: datosFrecCard,
                        }
                    ]
                };

                opcionesTemperatura = {

                    // Setup grid
                    grid: {
                        x: 40,
                        y: 45,
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    // Add Legend
                    legend: {
                        data:['Temperatura']
                    },

                    // Add custom colors
                    color: ['#FF4961'],

                    // Enable drag recalculate
                    calculable : true,

                    // Horizontal Axiz
                    xAxis : [
                        {
                            type : 'category',
                            boundaryGap : false,
                            data : fechas
                        }
                    ],

                    // Vertical Axis
                    yAxis : [
                        {
                            type : 'value',
                            axisLabel : {
                                formatter: '{value} °C'
                            }
                        }
                    ],

                    // Add Series
                    series : [
                        {
                            name:'Temperatura',
                            type:'line',
                            data: datosTemperatura,
                        }
                    ]
                };


                // Se aplican las opciones.
                graficaEstatura.setOption(opcionesEstatura);
                graficaPeso.setOption(opcionesPeso);
                graficaIMC.setOption(opcionesIMC);
                graficaFrecResp.setOption(opcionesFrecResp);
                graficaFrecCard.setOption(opcionesFrecCard);
                graficaTemperatura.setOption(opcionesTemperatura);
                graficaOxigeno.setOption(opcionesOxigeno);


                // Función en caso de ajuste de tamaño.
                $(function () {

                    $(window).on('resize', resize);
                    $(".menu-toggle").on('click', resize);

                    // Función para reajustar tamaño.
                    function resize() {
                        setTimeout(function() {

                            // Reajuste de tamaño.
                            graficaEstatura.resize();
                            graficaPeso.resize();
                            graficaIMC.resize();
                            graficaFrecResp.resize();
                            graficaFrecCard.resize();
                            graficaTemperatura.resize();
                            graficaOxigeno.resize();
                        }, 200);
                    }
                });
            }
        );

    })
    .fail(function(error) {
        $('#loadIndicadores').hide();
        $('#alertIndicadores').show();
    })
});


function getNombreMesCorto(numero) {
    var nombre;

    switch (numero) {
        case 0:
            nombre = "ene";
            break;
        case 1:
            nombre = "feb";
            break;
        case 2:
            nombre = "mar";
            break;
        case 3:
            nombre = "abr";
            break;
        case 4:
            nombre = "may";
            break;
        case 5:
            nombre = "jun";
            break;
        case 6:
            nombre = "jul";
            break;
        case 7:
            nombre = "ago";
            break;
        case 8:
            nombre = "sep";
            break;
        case 9:
            nombre = "oct";
            break;
        case 10:
            nombre = "nov";
            break;
        case 11:
            nombre = "dic";
            break;
        default:
            nombre = "";
            break;
    }

    return nombre;
}
