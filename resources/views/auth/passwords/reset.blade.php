@extends('layouts.app')

@section('styles')
<link href="../../../resources/sass/fontawesome/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../../resources/sass/css/styles.css">
@endsection

@section('content')

<div class="bienvenidos_device"></div>

<div class="col-md-5">
    <div class="content">
        <div class="logo">
            <img src="../../../resources/sass/images/logotipo.png" alt="Diagnosticos Clinicos">
        </div>
        <div class="formulario">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <h2>Reestablecer contraseña</h2>

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="input-group">
                    <label for="email" class="col-md-4 col-form-label text-md-left">Correo electrónico</label>

                    <div class="col-md-8">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="input-group">
                    <label for="password" class="col-md-4 col-form-label text-md-left">Contraseña</label>

                    <div class="col-md-8">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="input-group">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-left">Repetir contraseña</label>

                    <div class="col-md-8">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <button type="submit" class="btn btn_custom">
                    Reestablecer contraseña
                </button>

            </form>

        </div>

    </div>
</div>

<div class="col-md-7 bienvenidos"></div>

@endsection
