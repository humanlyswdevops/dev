@extends('layouts.VuexyLogin')
@section('page_css')

@endsection
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
          <section class="row flexbox-container">
          <div class="col-xl-8 col-11 d-flex justify-content-center">
          <div class="card bg-authentication rounded mb-0">
          <div class="row m-0">
          <div class="col-lg-5 d-lg-block d-none text-center align-self-center px-1 py-0 p-3">
            {{-- <img src="{!! asset('public/propuestas-01.png') !!}" class="img-fluid" alt="branding logo"> --}}
            <img src="{!! asset('public/new_logo_has.svg') !!}" class="img-fluid w-100" alt="branding logo">
          </div>
          <div class="col-lg-7 col-12 p-0">
          <div class="card rounded mb-0 px-2">
              <div class="card-header pb-1">
                  <div class="card-title">
                      <h5 class="mb-0">Health Administration Solution</h5>
                  </div>
              </div>
              <p class="px-2">Bienvenido de nuevo, inicie sesión en su cuenta.</p>
              <div class="card-content">
                  <div class="card-body pt-1">
                      @if (count($errors) > 0)
                          @foreach ($errors as  $value)
                            <li>{{ $value }}</li>
                          @endforeach
                      @endif
                      <form method="POST" action="{{ route('login') }}">
                        @csrf
                          <fieldset class="form-label-group form-group position-relative has-icon-left">
                              <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Correo Electrónico" name="email" value="{{ old('email') }}" required>
                              <div class="form-control-position">
                                  <i class="feather icon-user"></i>
                              </div>
                              <label for="user-name">Correo Electrónico</label>
                          </fieldset>

                          <fieldset class="form-label-group position-relative has-icon-left">
                              <input type="password" autocomplete="current-password" name="password" class="form-control @error('password') is-invalid @enderror"  placeholder="Contraseña" required>
                              <div class="form-control-position">
                                  <i class="feather icon-lock"></i>
                              </div>
                              <label for="user-password">Contraseña</label>
                          </fieldset>
                          <div class="form-group d-flex justify-content-between align-items-center">
                              <div class="text-right">
                                {{-- <a href="{{ route('password.request') }}"  class="card-link">¿Se te olvidó tu contraseña?</a> --}}
                              </div>
                          </div>
                          <button type="submit" class="btn btn-primary btn-block float-right btn-inline">Iniciar Sesión</button>
                      </form>
                  </div>
              </div>
              <div class="login-footer">
                <div class="divider">
                  <div class="divider-text"></div>
                </div>
              </div>
          </div>
          </div>
          </div>
          </div>
          </div>
          </section>
        </div>
      </div>
    </div>

@endsection
