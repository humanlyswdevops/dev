@extends('layouts.VuexyLaboratorio')

@section('title', 'Pacientes')

@section('styles')
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/plugins/loaders/loaders.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/core/colors/palette-loader.css">

    <!-- END Page Level CSS-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CMuli:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!! asset('../app-assets/css/vendors.css') !!} ">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('../app-assets/css/app.css') !!} ">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css/plugins/extensions/toastr.css">

    <link rel="stylesheet" href="../../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../../resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="../../resources/sass/css/Laboratorio/ingresos_styles.css">

    {{-- Custom --}}
    <link rel="stylesheet" type="text/css" href=" {!! asset('../assets/css/style.css') !!}">
    <link rel="stylesheet" type="text/css" href="../../resources/sass/css/Laboratorio/Medico/estudios.css">
    <link rel="stylesheet" type="text/css" href="../../resources/sass/css/Laboratorio/Laboratorio/estudios.css">
@endsection

@section('content')

        <nav class="" aria-label="breadcrumb">
            <ol class="breadcrumb pl-1 " style="padding:0px">
                <li class="breadcrumb-item"><a href="{{route('pacienteLaboratorio')}}">Ingresos</a></li>
                <li class="breadcrumb-item active" aria-current="page"><span>Estudios de {{ucfirst(strtolower($empleado->nombre)) .' '. ucfirst(strtolower($empleado->apellido_paterno)).' '. ucfirst(strtolower($empleado->apellido_materno))}}</span></li>
            </ol>
        </nav>

        <div class="row">
            @if (count($errors)>0)
                <div class="alert alert-danger ml-5">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Error en la validación</strong>
                    <br>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {{session('success')}}
                </div>
            @endif


            <div class="col-sm-12">
              <div class="card">
                <div class="card-header bg-secondary">
                  <h4 class="card-title text-white">{{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <p style="font-size:15px">Edad: {{\Carbon::parse($empleado->fecha_nacimiento)->age . ' años'}}</p>
                    <p style="font-size:15px">Genero: {{$empleado->genero}}</p>
                    <p style="font-size:15px">CURP: {{$empleado->CURP}}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header bg-secondary">
                  <h4 class="card-title text-white">Estudios del paciente</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    @forelse ($ingresos as $ingreso)
                        <div class="col-md-4 cards_item bg-transparent" id="{{$ingreso->CURP}}" >
                            <a class="card  border-secondary" onclick="Modal('{{$ingreso->CURP}}','{{encrypt($ingreso->estudios_id)}}','{{encrypt($ingreso->ingreso)}}')">
                                <p class="text-center"> <strong>Estudio: {{$ingreso->estudio}}</strong> </p>
                            </a>
                        </div>
                    @empty
                        <p>No hay estudios agregados recientemente</p>
                    @endforelse
                    <div class="aviso-vacio ml-5 pl-4" style="display: none;">
                        <p>Ningun paciente coincide con la búsqueda.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>

    {{-- Modal --}}
    <div id="estudios" class="modal fade" role="dialog" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            {{-- Modal content --}}
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title float-left text-white" id="estudio_title">Estudio de {{$empleado->nombre}} </h4>
                </div>
                <div class="modal-body">
                    <div class="text-center mt-1 mb-1"
                    style="display:none" 
                    id="load">
                      <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    </div>

                    <div class="alert round bg-secondary alert-icon-left alert-arrow-left alert-dismissible mb-2" id="alert_success"
                    style="display:none"
                     role="alert">
                     <p class="text-center text-white">
                       <i class="feather icon-check-circle"></i>
                       Se ha indicado como estudio finalizado correctamente
                     </p>
                    </div>

                    <form  class="col-md-12 text-center" id="formuploadajax"  method="POST"  enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-12 seccion-registrar">
                            <input type="hidden" name="empleado_id" value="{{encrypt($empleado->id)}}">
                            <input type="hidden" name="estudio_id" id="estudio" >
                            <input type="hidden" name="registro_id" id="ingreso" >
                            <button type="submit" class="mx-auto btn btn-secondary">Registrar estudio como completado</button>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
  </div>
@endsection

@section('js_custom')

 <script src="../resources/js/Laboratorio/Laboratorio/Estudio.js"></script>
 <!-- BEGIN VENDOR JS-->
 <script src="../app-assets/vendors/js/vendors.min.js"></script>
 <!-- BEGIN VENDOR JS-->
 <!-- BEGIN PAGE VENDOR JS-->
 <script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
 <script src="../app-assets/vendors/js/extensions/toastr.min.js"></script>
 <!-- END PAGE VENDOR JS-->
 <!-- BEGIN ROBUST JS-->
 <script src="../app-assets/js/core/app-menu.js"></script>
 <script src="../app-assets/js/core/app.js"></script>
 <!-- END ROBUST JS-->
 <!-- BEGIN PAGE LEVEL JS-->
 <script src="../app-assets/js/scripts/extensions/toastr.js"></script>
 <!-- END PAGE LEVEL JS-->

@endsection
