@extends('layouts.VuexyLaboratorio')

@section('title', 'Pacientes')

@section('styles')
    {{-- Custom --}}
        <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="https://asesoresconsultoreslabs.com/expedienteclinico/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="../../resources/sass/css/Laboratorio/Medico/estudios.css">
@endsection

@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb ml-1">
    <li class="breadcrumb-item"><a href="{{route('medicina')}}">Ingresos</a></li>
    <li class="breadcrumb-item active" aria-current="page">Estudios de {{ucfirst($empleado->nombre) .' '. ucfirst($empleado->apellido_paterno).' '. ucfirst($empleado->apellido_materno)}}</li>
  </ol>
</nav>

<div class=" content_card">

    @if (count($errors)>0)
    <div class="alert alert-danger ml-5">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Error en la validación</strong> <br>
        <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
    @if(session('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{session('success')}}
    </div>
    @endif
    <div class="turnt content_card row">
        @foreach ($medicina as $ingreso)
        {{-- @dd($ingreso) --}}
        <div class="col-md-4 " id="{{$ingreso->CURP}}">
            @if ($ingreso->estudios_id == 1382)
            <a class="card" href="{!! route('espirometria',['id'=>encrypt($ingreso->ingreso)]) !!}">
            @elseif ($ingreso->estudios_id == 1383)
            <a class="card" href="{!! route('audiometria',['id'=>$ingreso->CURP,'registro_entrada_id' => $ingreso->ingreso]) !!}">
            @elseif ($ingreso->estudios_id == 1026)
            <a class="card" href="{!! url('formularios/'.encrypt($ingreso->ingreso)) !!}">
            @else
            <a class="card" onclick="Modal('{{$ingreso->CURP}}','{{encrypt($ingreso->estudios_id)}}','{{encrypt($ingreso->ingreso)}}')">
            @endif
                <div class="card-header">
                    <h6 class="card-title font-weight-normal">Estudio: {{$ingreso->estudio}}</h6>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <p class="small ">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                        <p class="small ">Genero: {{$ingreso->genero}}</p>
                        <p class="small ">Curp: {{$ingreso->CURP}}</p>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
        @foreach ($laboratorio as $ingreso)
        <div class="col-md-4 " id="{{$ingreso->CURP}}">
            <a class="card" onclick="Modal('{{$ingreso->CURP}}','{{encrypt($ingreso->estudios_id)}}','{{encrypt($ingreso->ingreso)}}')">
                <div class="card-header">
                    <h6 class="card-title font-weight-normal">Estudio: {{$ingreso->estudio}}</h6>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <p class="small ">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                        <p class="small ">Genero: {{$ingreso->genero}}</p>
                        <p class="small ">Curp: {{$ingreso->CURP}}</p>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>


<hr>

<div class="panel-body">
    <div class="row content_card" id="admin_list">
        <div id="search" class="col-md-12 mb-3">
            <div class="row">
                <div class="col-12 input-group input-focus">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-white" ><i class="fa fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control search border-left-0 col-12 input_buscador" name="search"  placeholder="Busqueda de estudios">
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="turnt content_card row list">
            @forelse ($resultados as $estudio)
                <div class="col-md-4 list_custom" id="{{$estudio->expediente->curp}}">
                    @if($estudio->estudio_id == 1382)
                        <a target="_blank" class="card" href="{!! url('espirometria-view/'.$estudio->espirometria->id) !!}">
                    @elseif ($estudio->estudio_id == 1383)
                        <a target="_blank" class="card" href="{!! url('resultadoAudiometria/'.$estudio->audiometria->id) !!}">
                    @elseif ($estudio->estudio_id == 1026)
                        <a target="_blank" class="card" href="#
                        ">
                    @else
                        <a class="card" onclick="Modal('{!! $estudio->archivo->archivo !!}','{!! $estudio->archivo->tipo !!}','{{$estudio->estudio->nombre}}')">
                    @endif
                    <div class="card-header">
                        <h6 class="card-title font-weight-normal search_estudio">Estudio: {{$estudio->estudio->nombre}}</h6>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <p class="small search_fecha">Fecha: {!! $estudio->created_at->format('d/M/Y h:i a') !!}</p>
                            @if ($estudio->medico == null)
                            <p class="small search_medico">Medico: S/A</p>
                            @else
                            <p class="small search_medico">Medico: {!! $estudio->medico->titulo_profesional.'. '.$estudio->medico->nombre_completo !!}</p>
                            @endif
                        </div>
                    </div>
                    </a>
                </div>
            @empty
                <h1>Sin estudios previos</h1>
            @endforelse
            </div>
        </div>
        <div class="col-12 mb-2 mt-1">
            <div class="row">
                <div class="col-12">
                    <h1 id="msg-result" class="d-none">0 resultados</h1>
                    <div class="d-flex align-items-center justify-content-center">
                        <div id="anterior"></div>
                        <ul class="pagination justify-content-center m-0"></ul>
                        <div id="siguiente"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="estudios" class="modal fade" role="dialog" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h6 class="modal-title float-left text-white" id="estudio_title">Estudio de {{$empleado->nombre}} </h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
           <div class="loader-wrapper mx-auto" style="display:none" id="load">
							<div class="loader-container">
								<div class="ball-clip-rotate-multiple loader-success">
									<div></div>
									<div></div>
								</div>
							</div>
					 </div>
           <div class="alert round bg-secondary alert-icon-left alert-arrow-left alert-dismissible mb-2" id="alert_success"
            style="display:none"
            role="alert">
            <p class="text-white">Los archivos se han enviado correctamente</p>

           </div>
          <form  class="col-md-12" id="formuploadajax"  method="POST"  enctype="multipart/form-data">
          @csrf
            <div class="row">
              <div class="col-md-10">
                <fieldset class="form-group addUrl">
                  <input type="url" class="form-control document" placeholder="Enlaces a documentos del estudio en SASS" required name="documentos[]">
                  <input type="hidden" name="empleado_id" value="{{encrypt($empleado->id)}}">
                  <input type="hidden" name="estudio_id" id="estudio" >
                  <input type="hidden" name="registro_id" id="ingreso" >
                </fieldset>
              </div>
              {{-- <div class="col-md-1">
                <div class="checkbox">
                <label class="pt-1">
                  <input data-toggle="tooltip" data-placement="top" title="Compartir Documentos" name="documentos[]"   type="checkbox" checked style="display: none">
                </label>
                </div>
              </div> --}}
              <div class="col-md-1">
                <button type="button" class="btn btn-primary" onclick="addUrl()" name="button">
                  <i class="feather icon-plus"></i>
                </button>
              </div>
            </div>
            <button type="submit" class="mx-auto btn  btn-secondary btn-sm">Enviar Enlaces</button>

        </form>
      </div>

    </div>

  </div>
</div>
@endsection

@section('js_custom')
  <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>
  <script src="{!! asset('public/js/Laboratorio/medicina/pacientes.js') !!}  "></script>
  <script src="../app-assets/vendors/js/extensions/toastr.min.js"></script>
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="../app-assets/js/scripts/extensions/toastr.js"></script>
  <!-- END PAGE LEVEL JS-->
@endsection
