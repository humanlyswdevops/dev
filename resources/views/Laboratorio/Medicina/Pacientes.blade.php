@extends('layouts.VuexyLaboratorio')

@section('title', 'Pacientes')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/medicinaPaci.css">
    <link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
@endsection

@section('content')

<div class="title-content">
   <h4 class="title">Pacientes Ingresados
   @foreach ($empresa as $nombreempresa)
 >>{{ $nombreempresa->nombre}}</h4>
  <input type="hidden" class="form-control" value="{{ $nombreempresa->id }}" id='id_empresa'/>
  @endforeach
</div>

<div class="panel panel-default">
<div class="panel-heading">
  <br>
  
</div>
<div class="panel-body">
  <div class="row content_card">
      <form id="search" method="post" class="col-md-12 mb-3">
        @csrf
        <div class="row">
          <div class="col-6 input-group input-focus">
            <div class="input-group-prepend">
              <span class="input-group-text bg-white" ><i class="fa fa-search"></i></span>
            </div>
            <input type="text" class="form-control search border-left-0 col-12" name="search"  placeholder="Nombre del paciente">
          </div>
          <div class="col-6 input-group input-focus">
            <button id="btnagregarempleado" type="button" class="btn btn-sm btn-primary waves-effect waves-float waves-light">Agregar Empleado</button>
          </div>
          
          </div>
        </div>
     </form>
     {{-- <div class="turnt content_card row">
       @forelse ($pacientes as $ingreso)
         @if ($ingreso->categoria_id == 3)



           <div class="col-md-4 cards_item" id="{{$ingreso->CURP}}">
            <a class="card" href="{{route('medicina_paciente',['id'=> encrypt($ingreso->id)])}}">
              <div class="card-header bg-secondary">
                <h6 class="card-title secondary font-weight-normal" style='color:#fff !important;'>{{ ucwords(strtolower($ingreso->nombre)).' '.ucwords(strtolower($ingreso->app)).' '.ucwords(strtolower($ingreso->apm))}}</h6>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <p class="small ">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                  <p class="small ">Genero: {{$ingreso->genero}}</p>
                  <p class="small ">Curp: {{$ingreso->CURP}}</p>
                </div>
              </div>
            </a>
          </div>
         @else
           @if ($ingreso->categoria_id == 1 && $ingreso->status == 1)



             <div class="col-md-4 cards_item" id="{{$ingreso->CURP}}">
              <a class="card" href="{{route('medicina_paciente',['id'=> encrypt($ingreso->id)])}}">
                <div class="card-header bg-secondary">
                  <h6 class="card-title font-weight-normal" style='color:#fff !important;'>{{ ucwords(strtolower($ingreso->nombre)).' '.ucwords(strtolower($ingreso->app)).' '.ucwords(strtolower($ingreso->apm))}}</h6>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <p class="small ">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                    <p class="small ">Genero: {{$ingreso->genero}}</p>
                    <p class="small ">Curp: {{$ingreso->CURP}}</p>
                  </div>
                </div>
              </a>
            </div>

           @endif



         @endif

       @empty
         <p>Aún no hay pacientes</p>
       @endforelse
       <div class="aviso-vacio ml-5 pl-4" style="display: none;">
           <p>Ningun paciente coincide con la búsqueda.</p>
       </div>
     </div> --}}
  
     <div class="turnt content_card row">
       @forelse ($pacientes as $ingreso)
           <div class="col-md-4 cards_item" id="{{$ingreso->CURP}}">
            <a class="card" href="../Historial-Clinico/{{encrypt($ingreso->CURP)}}">
              
              <div class="card-header bg-secondary">
                <h6 class="card-title secondary font-weight-normal text-white" style="color:#fff !important;" >{{ ucwords(strtolower($ingreso->nombre)).' '.ucwords(strtolower($ingreso->apellido_paterno)).' '.ucwords(strtolower($ingreso->apellido_materno))}}</h6>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <p class="small ">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                  <p class="small ">Genero: {{$ingreso->genero}}</p>
                  <p class="small ">Curp: {{$ingreso->CURP}}</p>
                </div>   
              </div>
            </a>
          </div>
      
       @empty
         <p>Aún no hay pacientes</p>
       @endforelse
       <div class="aviso-vacio ml-5 pl-4" style="display: none;">
           <p>Ningun paciente coincide con la búsqueda.</p>
       </div>
     </div>
  </div>

</div>

<div id="agregarmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="bg-primary modal-header">
              <h4 class="modal-title float-left">Buscar Empleado</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <section id="basic-datatable">
              <div class="row">
                  <div class="col-12">
                          <div class="card-content">
                              <div class="card-body card-dashboard">
                                  <div class='row'>
                                      <div class='col-md-8'>
                                          <input id='id_sass' name='id_sass' class="form-control input col-md-12" type="text"
                                              placeholder="Buscar nim..." tabindex="-1" data-search="search" value=''>
                                      </div>
                                      <div class='col-md-4'>
                                          <button type="button" class="btn btn-primary waves-effect waves-float waves-light"
                                              id="busca_empleado" onclick="obtener_estudios()">Buscar</button>
                                      </div>
                                  </div>
                                  <div class='row'>
                                  <div class='col-md-12 mt-2'>
                                      <div id='datos'>
                                      </div> 
                                      <div id="estudios">
                                           <div class='col-md-12' id='busqueda'>     
                                              </div>
                                          <div class="row list"></div>
                                          <div class="col-md-12 mt-1">
                                              <div class="d-flex align-items-center justify-content-center">
                                                  <div id="anterior"></div>
                                                  <ul class="pagination justify-content-center m-0"></ul>
                                                  <div id="siguiente"></div>
                                              </div>
                                          </div>
      
                                      </div>
                                      <div class='boton col-md-12'>
                                          
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                      
                  </div>
              </div>
          </section>
          </div>
      </div>
  </div>
</div>
@endsection

@section('js_custom')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js"></script>
<script src=" {!! asset('resources/js/Laboratorio/Medicina.js') !!} "></script>
<script src="{!! asset('public/js/empresa/empleado/resultados.js') !!}" charset="utf-8"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js"></script>
@endsection
