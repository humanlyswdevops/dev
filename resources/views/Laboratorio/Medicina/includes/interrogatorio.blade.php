
<fieldset>
    <div id="interrogatorio">
        
        <div id="AlertaInterrogatorio">
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset class="form-group">
                    <h5 class="primary text-center">TRABAJOS EN ALTURAS</h5>
                    <label for="interrogatorio" class="mb-2">Dirigido a identificar factores de Riesgo para realizar
                        Trabajos</label>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class='row'>
                                <div class="col-md-6">
                                    <label class="d-block">¿Alguna vez ha presentado mareos o vértigos?</label>
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" checked
                                            name="vertigos" value="si" id="vertigos_si">
                                        <label class="custom-control-label" for="vertigos_si"
                                            onclick="showControll('vertigos','show')">si</label>
                                    </div>
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" name="vertigos"
                                            value="no" id="vertigos_no">
                                        <label class="custom-control-label" for="vertigos_no"
                                            onclick="hideControll('vertigos','show')">no</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Cuándo está a nivel de piso?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="vertigo_piso" value="si" id="vertigo_piso_si">
                                            <label class="custom-control-label" for="vertigo_piso_si"
                                                onclick="showControll('vertigo_piso','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary"
                                                name="vertigo_piso" value="no" id="vertigo_piso_no">
                                            <label class="custom-control-label" for="vertigo_piso_no"
                                                onclick="hideControll('vertigo_piso','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Cuándo sube escaleras?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="escaleras" value="si" id="escaleras_si">
                                            <label class="custom-control-label" for="escaleras_si"
                                                onclick="showControll('escaleras','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary"
                                                name="escaleras" value="no" id="escaleras_no">
                                            <label class="custom-control-label" for="escaleras_no"
                                                onclick="hideControll('escaleras','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Cuándo se encuentra parado en una altura mayor a 2
                                            m?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="altura" value="si" id="altura_si">
                                            <label class="custom-control-label" for="altura_si"
                                                onclick="showControll('altura','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="altura"
                                                value="no" id="altura_no">
                                            <label class="custom-control-label" for="altura_no"
                                                onclick="hideControll('altura','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha presentado lesiones en oídos que le hayan roto la
                                            membrana
                                            timpánica?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="membrana" value="si" id="membrana_si">
                                            <label class="custom-control-label" for="membrana_si"
                                                onclick="showControll('membrana','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary"
                                                name="membrana" value="no" id="membrana_no">
                                            <label class="custom-control-label" for="membrana_no"
                                                onclick="hideControll('membrana','show')">no</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <label><b>** Exploración médica</b></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de marcha punta - talón</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="p_talon" value="si" id="p_talon_si">
                                            <label class="custom-control-label" for="p_talon_si"
                                                onclick="showControll('p_talon','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="p_talon"
                                                value="no" id="p_talon_no">
                                            <label class="custom-control-label" for="p_talon_no"
                                                onclick="hideControll('p_talon','show')">no</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Caminar por línea</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="linea" value="si" id="linea_si">
                                            <label class="custom-control-label" for="linea_si"
                                                onclick="showControll('linea','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="linea"
                                                value="no" id="linea_no">
                                            <label class="custom-control-label" for="linea_no"
                                                onclick="hideControll('linea','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para trabajos en
                                                alturas (
                                                SATISFACTORIO)</b></label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="t_altura" value="si" id="t_altura_si">
                                            <label class="custom-control-label" for="t_altura_si"
                                                onclick="showControll('t_altura','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary"
                                                name="t_altura" value="no" id="t_altura_no">
                                            <label class="custom-control-label" for="t_altura_no"
                                                onclick="hideControll('t_altura','show')">no</label>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-12">
                <fieldset class="form-group">
                    <h5 class="primary text-center">TRABAJOS EN ESPACIOS CONFINADOS</h5>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Cuando se encuentra en espacios cerrados, ¿se desespera?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="e_cerrados" value="si" id="e_cerrados_si">
                                            <label class="custom-control-label" for="e_cerrados_si"
                                                onclick="showControll('e_cerrados','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_cerrados"
                                                value="no" id="e_cerrados_no">
                                            <label class="custom-control-label" for="e_cerrados_no"
                                                onclick="hideControll('e_cerrados','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Tiene miedos (fobias) para trabajar en lugares cerrados, estrechos, con poca
                                            iluminación y poco aire?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="fobias" value="si" id="fobias_si">
                                            <label class="custom-control-label" for="fobias_si"
                                                onclick="showControll('fobias','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="fobias"
                                                value="no" id="fobias_no">
                                            <label class="custom-control-label" for="fobias_no"
                                                onclick="hideControll('fobias','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Cuando se encuentra en espacios estrechos, ¿siente que no puede respirar
                                            bien?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="e_estrechos" value="si" id="e_estrechos_si">
                                            <label class="custom-control-label" for="e_estrechos_si"
                                                onclick="showControll('e_estrechos','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_estrechos"
                                                value="no" id="e_estrechos_no">
                                            <label class="custom-control-label" for="e_estrechos_no"
                                                onclick="hideControll('e_estrechos','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para trabajos en espacios confinados
                                            (SATISFACTORIO)</b></label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="e_confinados" value="si" id="e_confinados_si">
                                            <label class="custom-control-label" for="e_confinados_si"
                                                onclick="showControll('e_confinados','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_confinados"
                                                value="no" id="e_confinados_no">
                                            <label class="custom-control-label" for="e_confinados_no"
                                                onclick="hideControll('e_confinados','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                
                               
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-12">
                <fieldset class="form-group">
                    <h5 class="primary text-center">TRABAJOS DE LEVANTAMIENTO Y MOVIMIENTO DE CARGAS</h5>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha tenido cirugías en su espalda (columna vertebral)?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="c_vertebral" value="si" id="c_vertebral_si">
                                            <label class="custom-control-label" for="c_vertebral_si"
                                                onclick="showControll('c_vertebral','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="c_vertebral"
                                                value="no" id="c_vertebral_no">
                                            <label class="custom-control-label" for="c_vertebral_no"
                                                onclick="hideControll('c_vertebral','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Se ha fracturado alguna vertebra?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="f_vertebral" value="si" id="f_vertebral_si">
                                            <label class="custom-control-label" for="f_vertebral_si"
                                                onclick="showControll('f_vertebral','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="f_vertebral"
                                                value="no" id="f_vertebral_no">
                                            <label class="custom-control-label" for="f_vertebral_no"
                                                onclick="hideControll('f_vertebral','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha padecido o padece de dolores de espalda?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="d_espalda" value="si" id="d_espalda_si">
                                            <label class="custom-control-label" for="d_espalda_si"
                                                onclick="showControll('d_espalda','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="d_espalda"
                                                value="no" id="d_espalda_no">
                                            <label class="custom-control-label" for="d_espalda_no"
                                                onclick="hideControll('d_espalda','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha tenido dolores de la ciática?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="d_ciatica" value="si" id="d_ciatica_si">
                                            <label class="custom-control-label" for="d_ciatica_si"
                                                onclick="showControll('d_ciatica','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="d_ciatica"
                                                value="no" id="d_ciatica_no">
                                            <label class="custom-control-label" for="d_ciatica_no"
                                                onclick="hideControll('d_ciatica','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Se le entumen sus brazos o piernas sin razón aparente?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="entumen_pb" value="si" id="entumen_pb_si">
                                            <label class="custom-control-label" for="entumen_pb_si"
                                                onclick="showControll('entumen_pb','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="entumen_pb"
                                                value="no" id="entumen_pb_no">
                                            <label class="custom-control-label" for="entumen_pb_no"
                                                onclick="hideControll('entumen_pb','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Tiene alguna limitación de movimiento de brazos, piernas o espalda?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="limitacion_bpe" value="si" id="limitacion_bpe_si">
                                            <label class="custom-control-label" for="limitacion_bpe_si"
                                                onclick="showControll('limitacion_bpe','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="limitacion_bpe"
                                                value="no" id="limitacion_bpe_no">
                                            <label class="custom-control-label" for="limitacion_bpe_no"
                                                onclick="hideControll('limitacion_bpe','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Tiene algún miembro inferior (pierna) más corto que el otro?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="m_inferior" value="si" id="m_inferior_si">
                                            <label class="custom-control-label" for="m_inferior_si"
                                                onclick="showControll('m_inferior','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="m_inferior"
                                                value="no" id="m_inferior_no">
                                            <label class="custom-control-label" for="m_inferior_no"
                                                onclick="hideControll('m_inferior','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <label><b class="secondary">** Exploración médica</b></label><br>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de curvaturas fisiológicas de columna?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="fisiologicas" value="si" id="fisiologicas_si">
                                            <label class="custom-control-label" for="fisiologicas_si"
                                                onclick="showControll('fisiologicas','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="fisiologicas"
                                                value="no" id="fisiologicas_no">
                                            <label class="custom-control-label" for="fisiologicas_no"
                                                onclick="hideControll('fisiologicas','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Marcha punta - talón</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="punta_t" value="si" id="punta_t_si">
                                            <label class="custom-control-label" for="punta_t_si"
                                                onclick="showControll('punta_t','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="punta_t"
                                                value="no" id="punta_t_no">
                                            <label class="custom-control-label" for="punta_t_no"
                                                onclick="hideControll('punta_t','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Palpación de apófisis espinosas (cervicales, dorsales y lumbares)</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="apofisis" value="si" id="apofisis_si">
                                            <label class="custom-control-label" for="apofisis_si"
                                                onclick="showControll('apofisis','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="apofisis"
                                                value="no" id="apofisis_no">
                                            <label class="custom-control-label" for="apofisis_no"
                                                onclick="hideControll('apofisis','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión (presión) de articulaciones sacroilíacas</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="sacroiliacas" value="si" id="sacroiliacas_si">
                                            <label class="custom-control-label" for="sacroiliacas_si"
                                                onclick="showControll('sacroiliacas','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="sacroiliacas"
                                                value="no" id="sacroiliacas_no">
                                            <label class="custom-control-label" for="sacroiliacas_no"
                                                onclick="hideControll('sacroiliacas','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión completa de arcos de movimiento de columna, miembros inferiores y
                                            superiores.</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="m_is" value="si" id="m_is_si">
                                            <label class="custom-control-label" for="m_is_si"
                                                onclick="showControll('m_is','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="m_is"
                                                value="no" id="m_is_no">
                                            <label class="custom-control-label" for="m_is_no"
                                                onclick="hideControll('m_is','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de signo Lasague y Lasague en dos tiempos.</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="Lasague" value="si" id="Lasague_si">
                                            <label class="custom-control-label" for="Lasague_si"
                                                onclick="showControll('Lasague','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="Lasague"
                                                value="no" id="Lasague_no">
                                            <label class="custom-control-label" for="Lasague_no"
                                                onclick="hideControll('Lasague','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para trabajos donde tenga que
                                            levantar o empujar más de 25 kgs.(SATISFACTORIO).</b></label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="25kg" value="si" id="25kg_si">
                                            <label class="custom-control-label" for="25kg_si"
                                                onclick="showControll('25kg','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="25kg"
                                                value="no" id="25kg_no">
                                            <label class="custom-control-label" for="25kg_no"
                                                onclick="hideControll('25kg','show')">no</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label><b class="secondary text-center">FUNCIÓN PULMONAR</b></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Le ha faltado respiración o ha sentido opresión en el pecho?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="opresion" value="si" id="opresion_si">
                                            <label class="custom-control-label" for="opresion_si"
                                                onclick="showControll('opresion','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="opresion"
                                                value="no" id="opresion_no">
                                            <label class="custom-control-label" for="opresion_no"
                                                onclick="hideControll('opresion','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha tosido por las mañanas en los últimos 12 meses?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="tosido" value="si" id="tosido_si">
                                            <label class="custom-control-label" for="tosido_si"
                                                onclick="showControll('tosido','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="tosido"
                                                value="no" id="tosido_no">
                                            <label class="custom-control-label" for="tosido_no"
                                                onclick="hideControll('tosido','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para trabajos donde esté expuesto a
                                            polvos o humos</b></label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="polvos_humos" value="si" id="polvos_humos_si">
                                            <label class="custom-control-label" for="polvos_humos_si"
                                                onclick="showControll('polvos_humos','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="polvos_humos"
                                                value="no" id="polvos_humos_no">
                                            <label class="custom-control-label" for="polvos_humos_no"
                                                onclick="hideControll('polvos_humos','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                <div class="col-md-12">
                                    <h5 class="primary text-center">AUDITIVA</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha notado recientemente cambios en su capacidad auditiva?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="c_auditiva" value="si" id="c_auditiva_si">
                                            <label class="custom-control-label" for="c_auditiva_si"
                                                onclick="showControll('c_auditiva','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="c_auditiva"
                                                value="no" id="c_auditiva_no">
                                            <label class="custom-control-label" for="c_auditiva_no"
                                                onclick="hideControll('c_auditiva','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha notado zumbidos o pitidos en sus oídos?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="o_zumbidos" value="si" id="o_zumbidos_si">
                                            <label class="custom-control-label" for="o_zumbidos_si"
                                                onclick="showControll('o_zumbidos','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="o_zumbidos"
                                                value="no" id="o_zumbidos_no">
                                            <label class="custom-control-label" for="o_zumbidos_no"
                                                onclick="hideControll('o_zumbidos','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Le resulta difícil participar en una conversación?</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="conversacion" value="si" id="conversacion_si">
                                            <label class="custom-control-label" for="conversacion_si"
                                                onclick="showControll('conversacion','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="conversacion"
                                                value="no" id="conversacion_no">
                                            <label class="custom-control-label" for="conversacion_no"
                                                onclick="hideControll('conversacion','show')">no</label>
                                        </div>
                                    </div>
                                </div>

                               
                               
                                <div class="col-md-12">
                                    <label><b class="secondary">** Exploración médica</b></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de conducto auditivo, buscando perforación timpánica</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="timpanica" value="si" id="timpanica_si">
                                            <label class="custom-control-label" for="timpanica_si"
                                                onclick="showControll('timpanica','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="timpanica"
                                                value="no" id="timpanica_no">
                                            <label class="custom-control-label" for="timpanica_no"
                                                onclick="hideControll('timpanica','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de conducto auditivo, buscando tapón de cerumen o infección.</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="cerumen" value="si" id="cerumen_si">
                                            <label class="custom-control-label" for="cerumen_si"
                                                onclick="showControll('cerumen','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="cerumen"
                                                value="no" id="cerumen_no">
                                            <label class="custom-control-label" for="cerumen_no"
                                                onclick="hideControll('cerumen','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block">Cumple con los requerimientos médicos para trabajos con exposición a ruido (SATISFACTORIO)</label>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked
                                                name="e_ruido" value="si" id="e_ruido_si">
                                            <label class="custom-control-label" for="e_ruido_si"
                                                onclick="showControll('e_ruido','show')">si</label>
                                        </div>
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_ruido"
                                                value="no" id="e_ruido_no">
                                            <label class="custom-control-label" for="e_ruido_no"
                                                onclick="hideControll('e_ruido','show')">no</label>
                                        </div>
                                    </div>
                                </div>
                                
                                
                               

                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</fieldset>





<script>
    TetanosSelecionado();

    function TetanosSelecionado() {
        if (document.getElementById('tetano_si').checked) {
            showControll('tetano', 'show');
        }
        if (document.getElementById('rubeola_si').checked) {
            showControll('rubeola', 'show');
        }
        if (document.getElementById('bcg_si').checked) {
            showControll('bcg', 'show');
        }
        if (document.getElementById('hepatitis_si').checked) {
            showControll('hepatitis', 'show');
        }
        if (document.getElementById('influenza_si').checked) {
            showControll('influenza', 'show');
        }
        if (document.getElementById('neumococica_si').checked) {
            showControll('neumococica', 'show');
        }
        if (document.getElementById('cigarro_si').checked) {
            showControll('cigarro', 'disabled');
        }
        if (document.getElementById('alcohol_si').checked) {
            showControll('alcohol', 'disabled')
        }
        if (document.getElementById('dogras_si').checked) {
            showControll('drogas', 'disabled')
        }
        if (document.getElementById('dieta_si').checked) {
            showControll('dieta', 'show')
        }
        if (document.getElementById('deporte_si').checked) {
            showControll('deporte_frecuencia', 'show')
        }

    }
</script>