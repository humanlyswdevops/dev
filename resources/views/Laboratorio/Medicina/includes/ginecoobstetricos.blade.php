<fieldset>
  <div id=gine>
    <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="inicio_regla">¿A qué edad ínicio su regla?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['inicioRegla']))
          <input type="text" class="form-control" id="inicio_regla" name="inicio_regla" placeholder="" value="{{$historial['genicoobstetrico']['inicioRegla']}}">
          @else
          <input type="text" class="form-control" id="inicio_regla" name="inicio_regla" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="frecuencia_regla">¿Frecuencia de regla?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['fRegla']))
          <input type="text" class="form-control" id="frecuencia_regla" name="frecuencia_regla" placeholder="" value="{{$historial['genicoobstetrico']['fRegla']}}">
          @else
          <input type="text" class="form-control" id="frecuencia_regla" name="frecuencia_regla" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="duracion_regla">¿Cuántos días dura su regla?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['duraRegla']))
          <input type="text" class="form-control" id="duracion_regla" name="duracion_regla" placeholder="" value="{{$historial['genicoobstetrico']['duraRegla']}}">
          @else
          <input type="text" class="form-control" id="duracion_regla" name="duracion_regla" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="inicio_re_sexual">¿Edad en que inicío sus relaciones sexuales?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['relacionSexInicio']))
          <input type="text" class="form-control" id="" name="inicio_re_sexual" placeholder="" value="{{$historial['genicoobstetrico']['relacionSexInicio']}}">
          @else
          <input type="text" class="form-control" id="" name="inicio_re_sexual" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="metodo_antic">¿Cuál metodo de planifcación ha usado?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['metodoUsado']))
          <input type="text" class="form-control" id="metodo_antic" name="metodo_antic" placeholder="" value="{{$historial['genicoobstetrico']['metodoUsado']}}">
          @else
          <input type="text" class="form-control" id="metodo_antic" name="metodo_antic" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="ult_mestruacion">¿Cuál fue la fecha de su ultima mestruación?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['ultimaMestruacion']))
          <input type="text" class="form-control" name="ult_mestruacion" id="ult_mestruacion" placeholder="" value="{{$historial['genicoobstetrico']['ultimaMestruacion']}}">
          @else
          <input type="text" class="form-control" name="ult_mestruacion" id="ult_mestruacion" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="embarazos">¿Cuántos embarazos ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numEmbarazos']))
          <input type="text" class="form-control" id="embarazos" name="embarazos" placeholder="" value="{{$historial['genicoobstetrico']['numEmbarazos']}}">
          @else
          <input type="text" class="form-control" id="embarazos" name="embarazos" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="partos">¿Cuántos partos ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numPartos']))
          <input type="text" class="form-control" id="partos" name="partos" placeholder="" value="{{$historial['genicoobstetrico']['numPartos']}}">
          @else
          <input type="text" class="form-control" id="partos" name="partos" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="cesareas">¿Cuántas cesáreas ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numCesareas']))
          <input type="text" class="form-control" id="cesareas" name="cesareas" placeholder="" value="{{$historial['genicoobstetrico']['numCesareas']}}">
          @else
          <input type="text" class="form-control" id="cesareas" name="cesareas" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="abortos">¿Cuántos abortos ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numAbortos']))
          <input type="text" class="form-control" id="abortos" name="abortos" placeholder="" value="{{$historial['genicoobstetrico']['numAbortos']}}">
          @else
          <input type="text" class="form-control" id="abortos" name="abortos" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="examen_matriz">¿Cuándo se realizó el último exámen del cáncer de matriz?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['fecha_examenCancer']))
          <input type="text" class="form-control" id="examen_matriz" name="examen_matriz" placeholder="" value="{{$historial['genicoobstetrico']['fecha_examenCancer']}}">
          @else
          <input type="text" class="form-control" id="examen_matriz" name="examen_matriz" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="examen_mamas">¿Cuándo se realizó el último exámen de mamas?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['fecha_examenMamas']))
          <input type="text" class="form-control" id="examen_mamas" name="examen_mamas" placeholder="" value="{{$historial['genicoobstetrico']['fecha_examenMamas']}}">
          @else
          <input type="text" class="form-control" id="examen_mamas" name="examen_mamas" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="menopausia">¿A qué edad tuvo su menopausia?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['edadMenopausia']))
          <input type="text" class="form-control" id="menopausia" name="menopausia" placeholder="" value="{{$historial['genicoobstetrico']['edadMenopausia']}}">
          @else
          <input type="text" class="form-control" id="menopausia" name="menopausia" placeholder="">
          @endif
        </fieldset>
      </div>
    </div>

  </div>
</fieldset>
