<fieldset>
    <div id="proveedor">
        <div id="Alertaproveedor">

        </div>
        <div class="row">
            <div class="col-md-8">
                <p><b>NOMBRE DEL PROVEDOR Ó RAZÓN SOCIAL:</b>Asesores Especializados en Laboratorios</p>
            </div>
            <div class="col-md-4">
                <p><b>REGIONAL:</b>Puebla</p>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="d-block">Lugar de extracción de la muestra:</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="laboratorio"
                            value="si" id="laboratorio_si">
                        <label class="custom-control-label" for="laboratorio_si"
                            onclick="showControll('laboratorio','show')">Laboratorio</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="consultorio_medico"
                            value="no" id="consultorio_medico_no">
                        <label class="custom-control-label" for="consultorio_medico_no"
                            onclick="hideControll('consultorio_medico','show')">Consultorio del médico</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="domicilio_solicitante"
                            value="no" id="domicilio_solicitante_no">
                        <label class="custom-control-label" for="domicilio_solicitante_no"
                            onclick="hideControll('domicilio_solicitante','show')">Domicilio del solicitante</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_cliente" value="no"
                            id="trabajo_cliente_no">
                        <label class="custom-control-label" for="trabajo_cliente_no"
                            onclick="hideControll('trabajo_cliente','show')">Trabajo del cliente</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="otro" value="no"
                            id="otro_no">
                        <label class="custom-control-label" for="otro_no"
                            onclick="hideControll('otro','show')">Otro</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="d-block">Fecha: </label>
                    <input type="date" class="form-control" name="fecha" id="fecha">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="d-block">Hora: </label>
                    <input type="time" class="form-control" name="hora" id="hora">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="d-block">Cliente en ayuno:</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="ayuno" value="si"
                            id="ayuno_si">
                        <label class="custom-control-label" for="ayuno_si"
                            onclick="showControll('ayuno','show')">Si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="ayuno" value="no"
                            id="ayuno_no">
                        <label class="custom-control-label" for="ayuno_no"
                            onclick="hideControll('ayuno','show')">No</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="d-block">Nombre y firma del Médico y/o laboratorio que extrajo,envaso y
                        etiqueto las muestras: </label>
                    <input type="text" class="form-control" name="medico" id="medico">
                </div>
            </div>

            <div class="col-md-3">
                <label class="d-block">Cedula Profesional:</label>
                <input type="text" class="form-control" name="cedula" id="cedula">
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <label class="d-block">Se tomo <b>GLUCOSA</b> en la plaza:</label>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="glucosa"
                                value="si" id="glucosa_si">
                            <label class="custom-control-label" for="glucosa_si"
                                onclick="showControll('glucosa','show')">Si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="glucosa" value="no"
                                id="glucosa_no">
                            <label class="custom-control-label" for="glucosa_no"
                                onclick="hideControll('glucosa','show')">No</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="d-block">Resultado:</label>
                        <input type="text" class="form-control" name="cedula" id="cedula">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label class="d-block">Muestra centrifugada</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="centrifugada"
                        value="si" id="centrifugada_si">
                    <label class="custom-control-label" for="centrifugada_si"
                        onclick="showControll('centrifugada','show')">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="centrifugada" value="no"
                        id="centrifugada_no">
                    <label class="custom-control-label" for="centrifugada_no"
                        onclick="hideControll('centrifugada','show')">No</label>
                </div>
            </div>
            
        </div>
    </div>
</fieldset>