<fieldset>
  <div id="patologico">

      <hr>
      <div class="row">
        <div class="col-md-8">
          <fieldset class="form-group">
            <label class="">Mencione si ha padecido alguna vez las siguientes enfermedades</label>
            <select class="selectize-multiple" class=" form-control" name="enfermedad_padecida[]" placeholder="Enfermedades" multiple>
              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Alta Presión', $historial['patologico']['enfermPadecidas']))
              <option selected value="Alta Presión">Alta Presión</option>
              @else
              <option value="Alta Presión">Alta Presión</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Soplo del corazón', $historial['patologico']['enfermPadecidas']))
              <option selected value="Soplo del corazón">Soplo del corazón</option>
              @else
              <option value="Soplo del corazón">Soplo del corazón</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Asma bronquial', $historial['patologico']['enfermPadecidas']))
              <option selected value="Asma bronquial">Asma bronquial</option>
              @else
              <option value="Asma bronquial">Asma bronquial</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Enfisema Pulmunar', $historial['patologico']['enfermPadecidas']))
              <option selected value="Enfisema Pulmunar">Enfisema Pulmunar</option>
              @else
              <option value="Enfisema Pulmunar">Enfisema Pulmunar</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Bronquitis', $historial['patologico']['enfermPadecidas']))
              <option selected value="Bronquitis">Bronquitis</option>
              @else
              <option value="Bronquitis">Bronquitis</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Diabetes', $historial['patologico']['enfermPadecidas']))
              <option selected value="Diabetes">Diabetes</option>
              @else
              <option value="Diabetes">Diabetes</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Migraña', $historial['patologico']['enfermPadecidas']))
              <option selected value="Migraña">Migraña</option>
              @else
              <option value="Migraña">Migraña</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Tuberculosis', $historial['patologico']['enfermPadecidas']))
              <option selected value="Tuberculosis">Tuberculosis</option>
              @else
              <option value="Tuberculosis">Tuberculosis</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Tuberculosis', $historial['patologico']['enfermPadecidas']))
              <option selected value="Tuberculosis">Tuberculosis</option>
              @else
              <option value="Tuberculosis">Tuberculosis</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Gastritis', $historial['patologico']['enfermPadecidas']))
              <option selected value="Gastritis">Gastritis</option>
              @else
              <option value="Gastritis">Gastritis</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Úlceras', $historial['patologico']['enfermPadecidas']))
              <option selected value="Úlceras">Úlceras</option>
              @else
              <option value="Úlceras">Úlceras</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Piedras en la vesícula', $historial['patologico']['enfermPadecidas']))
              <option selected value="Piedras en la vesícula">Piedras en la vesícula</option>
              @else
              <option value="Piedras en la vesícula">Piedras en la vesícula</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Hepatitis', $historial['patologico']['enfermPadecidas']))
              <option selected value="Hepatitis">Hepatitis</option>
              @else
              <option value="Hepatitis">Hepatitis</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Infección urinaria', $historial['patologico']['enfermPadecidas']))
              <option selected value="Infección urinaria">Infección urinaria</option>
              @else
              <option value="Infección urinaria">Infección urinaria</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Convulsiones', $historial['patologico']['enfermPadecidas']))
              <option selected value="Convulsiones">Convulsiones</option>
              @else
              <option value="Convulsiones">Convulsiones</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Piedras en el riñon', $historial['patologico']['enfermPadecidas']))
              <option selected value="Piedras en el riñon">Piedras en el riñon</option>
              @else
              <option value="Piedras en el riñon">Piedras en el riñon</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Enf. de transmisión sexual', $historial['patologico']['enfermPadecidas']))
              <option selected value="Enf. de transmisión sexual">Enf. de transmisión sexual</option>
              @else
              <option value="Enf. de transmisión sexual">Enf. de transmisión sexual</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Anemia', $historial['patologico']['enfermPadecidas']))
              <option selected value="Anemia">Anemia</option>
              @else
              <option value="Anemia">Anemia</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Enfermedades de la sangre', $historial['patologico']['enfermPadecidas']))
              <option selected value="Enfermedades de la sangre">Enfermedades de la sangre</option>
              @else
              <option value="Enfermedades de la sangre">Enfermedades de la sangre</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Fiebre tifoidea', $historial['patologico']['enfermPadecidas']))
              <option selected value="Fiebre tifoidea">Fiebre tifoidea</option>
              @else
              <option value="Fiebre tifoidea">Fiebre tifoidea</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Fiebre reumática', $historial['patologico']['enfermPadecidas']))
              <option selected value="Fiebre reumática">Fiebre reumática</option>
              @else
              <option value="Fiebre reumática">Fiebre reumática</option>
              @endif

              @if (isset($historial) && !empty($historial['patologico']['enfermPadecidas']) && in_array('Enfermedad de la tiroides', $historial['patologico']['enfermPadecidas']))
              <option selected value="Enfermedad de la tiroides">Enfermedad de la tiroides</option>
              @else
              <option value="Enfermedad de la tiroides">Enfermedad de la tiroides</option>
              @endif

            </select>
          </fieldset>
        </div>
        <div class="col-md-4">
          <fieldset class="form-group">
            <label for="transtornos">Trantornos emocionales o psiquiatricos</label>
            @if (isset($historial) && !empty($historial['patologico']['transtornos']))
            <input type="text" class="form-control" id="transtornos" name="transtornos_ep" value="{{$historial['patologico']['transtornos']}}">
            @else
            <input type="text" class="form-control" id="transtornos" name="transtornos_ep">
            @endif
          </fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="alergias">Alergias a medicamentos</label>
            @if (isset($historial) && !empty($historial['patologico']['alergiaMedica']))
            <input type="text" class="form-control" id="alergias" name="alergiasMedicas" placeholder="" value="{{$historial['patologico']['alergiaMedica']}}">
            @else
            <input type="text" class="form-control" id="alergias" name="alergiasMedicas" placeholder="">
            @endif
          </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="alergia_piel">Alergias en la piel o sensiblidad</label>
            @if (isset($historial) && !empty($historial['patologico']['alergiaPiel']))
            <input type="text" class="form-control" id="alergia_piel" name="alergia_piel" placeholder="" value="{{$historial['patologico']['alergiaPiel']}}">
            @else
            <input type="text" class="form-control" id="alergia_piel" name="alergia_piel" placeholder="">
            @endif
          </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="alergia_otro">Otro tipo de alergia</label>
            @if (isset($historial) && !empty($historial['patologico']['alergiaOtro']))
            <input type="text" class="form-control" id="alergia_otro" name="alergia_otro" placeholder="" value="{{$historial['patologico']['alergiaOtro']}}">
            @else
            <input type="text" class="form-control" id="alergia_otro" name="alergia_otro" placeholder="">
            @endif
          </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="tumo_cancer">Tumores o cancer</label>
            @if (isset($historial) && !empty($historial['patologico']['tumorCancer']))
            <input type="text" class="form-control" id="tumo_cancer" name="tumo_cancer" placeholder="" value="{{$historial['patologico']['tumorCancer']}}">
            @else
            <input type="text" class="form-control" id="tumo_cancer" name="tumo_cancer" placeholder="">
            @endif
          </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="problema_vista">Problemas de la vista</label>
            @if (isset($historial) && !empty($historial['patologico']['probVista']))
            <input type="text" class="form-control" id="problema_vista" name="problema_vista" placeholder="" value="{{$historial['patologico']['probVista']}}">
            @else
            <input type="text" class="form-control" id="problema_vista" name="problema_vista" placeholder="">
            @endif
          </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="enfer_oido">Enfermedades del oido</label>
            @if (isset($historial) && !empty($historial['patologico']['enfOido']))
            <input type="text" class="form-control" id="enfer_oido" name="enfer_oido" placeholder="" value="{{$historial['patologico']['enfOido']}}">
            @else
            <input type="text" class="form-control" id="enfer_oido" name="enfer_oido" placeholder="">
            @endif
          </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="columna_vertebral">Problema en la columna vertebral</label>
            @if (isset($historial) && !empty($historial['patologico']['probCulumVert']))
            <input type="text" class="form-control" id="columna_vertebral" name="columna_vertebral" placeholder="" value="{{$historial['patologico']['probCulumVert']}}">
            @else
            <input type="text" class="form-control" id="columna_vertebral" name="columna_vertebral" placeholder="">
            @endif
          </fieldset>
        </div>
        <div class="col-md-3">
          <fieldset class="form-group">
            <label for="hueso_articulacion">Husos y articulaciones</label>
            @if (isset($historial) && !empty($historial['patologico']['huesoArticulacion']))
            <input type="text" class="form-control" id="hueso_articulacion" name="hueso_articulacion" placeholder="" value="{{$historial['patologico']['huesoArticulacion']}}">
            @else
            <input type="text" class="form-control" id="hueso_articulacion" name="hueso_articulacion" placeholder="">
            @endif
          </fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="otro_problema">Otros problemas médicos no enlistados:</label>
            @if (isset($historial) && !empty($historial['patologico']['otroProbMedico']))
            <input type="text" class="form-control" id="otro_problema" name="otro_problema" value="{{$historial['patologico']['otroProbMedico']}}">
            @else
            <input type="text" class="form-control" id="otro_problema" name="otro_problema">
            @endif
          </fieldset>
        </div>
      </div>
      <hr>
      <p class="text-center">Indique si ha tenido alguna de las lesiones o cirugias siguientes:</p>
      <hr>
      <div class="row">
        <div class="col-md-3">
          <label class="d-block">Cirugía dental</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][0]['Cirugía dental']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_dental" value="si" id="c_dental_si">
            <label class="custom-control-label" for="c_dental_si" onclick="showControll('dental','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_dental" value="no" id="c_dental_no">
            <label class="custom-control-label" for="c_dental_no" onclick="hideControll('dental','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_dental" value="si" id="c_dental_si">
            <label class="custom-control-label" for="c_dental_si" onclick="showControll('dental','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_dental" value="no" id="c_dental_no">
            <label class="custom-control-label" for="c_dental_no" onclick="hideControll('dental','show')">no</label>
          </div>
          @endif
          <div class="dental" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][0]['Descripcion']))
            <input type="text" class="form-control dental" name="dental" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][0]['Descripcion']}}">
            @else
            <input type="text" class="form-control dental" name="dental" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugía de estomago/úlceras gastricas</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][1]['Cirugía de estomago/ úlceras gastricas']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_estomago" value="si" id="c_estomago_si">
            <label class="custom-control-label" for="c_estomago_si" onclick="showControll('estomago','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_estomago" value="no" id="c_estomago_no">
            <label class="custom-control-label" for="c_estomago_no" onclick="hideControll('estomago','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_estomago" value="si" id="c_estomago_si">
            <label class="custom-control-label" for="c_estomago_si" onclick="showControll('estomago','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_estomago" value="no" id="c_estomago_no">
            <label class="custom-control-label" for="c_estomago_no" onclick="hideControll('estomago','show')">no</label>
          </div>
          @endif
          <div class="estomago" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][1]['Descripcion']))
            <input type="text" class="form-control estomago" name="estomago" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][1]['Descripcion']}}">
            @else
            <input type="text" class="form-control estomago" name="estomago" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Lesion en cuello/ espalda / rodillas, manos, codos</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][2]['Lesion en cuello/ espalda / rodillas, manos, codos']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="lesiones" value="si" id="lesion_cer_si">
            <label class="custom-control-label" for="lesion_cer_si" onclick="showControll('cuello','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="lesiones" value="no" id="lesion_cer_no">
            <label class="custom-control-label" for="lesion_cer_no" onclick="hideControll('cuello','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="lesiones" value="si" id="lesion_cer_si">
            <label class="custom-control-label" for="lesion_cer_si" onclick="showControll('cuello','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="lesiones" value="no" id="lesion_cer_no">
            <label class="custom-control-label" for="lesion_cer_no" onclick="hideControll('cuello','show')">no</label>
          </div>
          @endif
          <div class="cuello" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][2]['Descripcion']))
            <input type="text" class="form-control cuello" name="cuello" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][2]['Descripcion']}}">
            @else
            <input type="text" class="form-control cuello" name="cuello" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugía de colon/recto</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][3]['Cirugía de colon/ recto']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_colon" value="si" id="c_colon_si">
            <label class="custom-control-label" for="c_colon_si" onclick="showControll('colon','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_colon" value="no" id="c_colon_no">
            <label class="custom-control-label" for="c_colon_no" onclick="hideControll('colon','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_colon" value="si" id="c_colon_si">
            <label class="custom-control-label" for="c_colon_si" onclick="showControll('colon','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_colon" value="no" id="c_colon_no">
            <label class="custom-control-label" for="c_colon_no" onclick="hideControll('colon','show')">no</label>
          </div>
          @endif
          <div class="colon" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][3]['Descripcion']))
            <input type="text" class="form-control colon" name="colon" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][3]['Descripcion']}}">
            @else
            <input type="text" class="form-control colon" name="colon" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugía de intestinos</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][4]['Cirugía de intestinos']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_intestino" value="si" id="c_intestino_si">
            <label class="custom-control-label" for="c_intestino_si" onclick="showControll('intestino','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_intestino" value="no" id="c_intestino_no">
            <label class="custom-control-label" for="c_intestino_no" onclick="hideControll('intestino','show')">no</label>
          </div>
          @else
            <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-secondary" name="c_intestino" value="si" id="c_intestino_si">
              <label class="custom-control-label" for="c_intestino_si" onclick="showControll('intestino','show')">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
              <input type="radio" class="custom-control-input bg-secondary" checked name="c_intestino" value="no" id="c_intestino_no">
              <label class="custom-control-label" for="c_intestino_no" onclick="hideControll('intestino','show')">no</label>
            </div>
          @endif
          <div class="intestino" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][4]['Descripcion']))
            <input type="text" class="form-control intestino" name="intestino" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][4]['Descripcion']}}">
            @else
            <input type="text" class="form-control intestino" name="intestino" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Oforectomia/Histerectomia</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][5]['Ooforectomia/ Histerectomia']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="ooforectomia" value="si" id="ooforectomia_si">
            <label class="custom-control-label" for="ooforectomia_si" onclick="showControll('Histerectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="ooforectomia" value="no" id="ooforectomia_no">
            <label class="custom-control-label" for="ooforectomia_no" onclick="hideControll('Histerectomia','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="ooforectomia" value="si" id="ooforectomia_si">
            <label class="custom-control-label" for="ooforectomia_si" onclick="showControll('Histerectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="ooforectomia" value="no" id="ooforectomia_no">
            <label class="custom-control-label" for="ooforectomia_no" onclick="hideControll('Histerectomia','show')">no</label>
          </div>
          @endif
          <div class="Histerectomia" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][5]['Descripcion']))
            <input type="text" class="form-control Histerectomia" name="Histerectomia" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][5]['Descripcion']}}">
            @else
            <input type="text" class="form-control Histerectomia" name="Histerectomia" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Salpingoclasia/Vasectomia</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][6]['Salpingoclasia/ Vasectomia']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="sal_vas" value="si" id="sal_vas_si">
            <label class="custom-control-label" for="sal_vas_si" onclick="showControll('Vasectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="sal_vas" value="no" id="sal_vas_no">
            <label class="custom-control-label" for="sal_vas_no" onclick="hideControll('Vasectomia','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="sal_vas" value="si" id="sal_vas_si">
            <label class="custom-control-label" for="sal_vas_si" onclick="showControll('Vasectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="sal_vas" value="no" id="sal_vas_no">
            <label class="custom-control-label" for="sal_vas_no" onclick="hideControll('Vasectomia','show')">no</label>
          </div>
          @endif
          <div class="Vasectomia" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][6]['Descripcion']))
            <input type="text" class="form-control Vasectomia" name="Vasectomia" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][6]['Descripcion']}}">
            @else
            <input type="text" class="form-control Vasectomia" name="Vasectomia" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugia de hernias</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][7]['Salpingoclasia/ Vasectomia']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_hernia" value="si" id="c_hernia_si">
            <label class="custom-control-label" for="c_hernia_si" onclick="showControll('hernias','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_hernia" value="no" id="c_hernia_no">
            <label class="custom-control-label" for="c_hernia_no" onclick="hideControll('hernias','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_hernia" value="si" id="c_hernia_si">
            <label class="custom-control-label" for="c_hernia_si" onclick="showControll('hernias','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_hernia" value="no" id="c_hernia_no">
            <label class="custom-control-label" for="c_hernia_no" onclick="hideControll('hernias','show')">no</label>
          </div>
          @endif
          <div class="hernias" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][7]['Descripcion']))
            <input type="text" class="form-control hernias" name="hernias" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][7]['Descripcion']}}">
            @else
            <input type="text" class="form-control hernias" name="hernias" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Cirugia de tiroides</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][9]['Cirugia de tiroides']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_tiroides" value="si" id="c_tiroides_si">
            <label class="custom-control-label" for="c_tiroides_si" onclick="showControll('tiroides','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_tiroides" value="no" id="c_tiroides_no">
            <label class="custom-control-label" for="c_tiroides_no" onclick="hideControll('tiroides','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="c_tiroides" value="si" id="c_tiroides_si">
            <label class="custom-control-label" for="c_tiroides_si" onclick="showControll('tiroides','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="c_tiroides" value="no" id="c_tiroides_no">
            <label class="custom-control-label" for="c_tiroides_no" onclick="hideControll('tiroides','show')">no</label>
          </div>
          @endif
          <div class="tiroides" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][9]['Cirugia de tiroides']))
            <input type="text" class="form-control tiroides" name="tiroides" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][9]['Cirugia de tiroides']}}">
            @else
            <input type="text" class="form-control tiroides" name="tiroides" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Colecistectomia</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][10]['Colecistectomia']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="colecistectomia" value="si" id="colecistectomia_si">
            <label class="custom-control-label" for="colecistectomia_si" onclick="showControll('Colecistectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="colecistectomia" value="no" id="colecistectomia_no">
            <label class="custom-control-label" for="colecistectomia_no" onclick="hideControll('Colecistectomia','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="colecistectomia" value="si" id="colecistectomia_si">
            <label class="custom-control-label" for="colecistectomia_si" onclick="showControll('Colecistectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="colecistectomia" value="no" id="colecistectomia_no">
            <label class="custom-control-label" for="colecistectomia_no" onclick="hideControll('Colecistectomia','show')">no</label>
          </div>
          @endif
          <div class="Colecistectomia" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][10]['Descripcion']))
            <input type="text" class="form-control Colecistectomia" name="Colecistectomia" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][10]['Descripcion']}}">
            @else
            <input type="text" class="form-control Colecistectomia" name="Colecistectomia" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Apéndicectomia</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][11]['Apéndicectomia']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="apendicectomia" value="si" id="apendicectomia_si">
            <label class="custom-control-label" for="apendicectomia_si" onclick="showControll('apendicectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="apendicectomia" value="no" id="apendicectomia_no">
            <label class="custom-control-label" for="apendicectomia_no" onclick="hideControll('apendicectomia','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="apendicectomia" value="si" id="apendicectomia_si">
            <label class="custom-control-label" for="apendicectomia_si" onclick="showControll('apendicectomia','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="apendicectomia" value="no" id="apendicectomia_no">
            <label class="custom-control-label" for="apendicectomia_no" onclick="hideControll('apendicectomia','show')">no</label>
          </div>
          @endif
          <div class="apendicectomia" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][11]['Descripcion']))
            <input type="text" class="form-control apendicectomia" name="apendicectomias" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][11]['Descripcion']}}">
            @else
            <input type="text" class="form-control apendicectomia" name="apendicectomias" placeholder="Descripción">
            @endif
          </div>
        </div>
        <div class="col-md-3">
          <label class="d-block">Fracturas o esguinces</label>
          @if (isset($historial) && $historial['patologico']['cirugiasLesion'][12]['Fracturas o esguinces']!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="fracturas" value="si" id="fracturas_si">
            <label class="custom-control-label" for="fracturas_si" onclick="showControll('esguinces','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="fracturas" value="no" id="fracturas_no">
            <label class="custom-control-label" for="fracturas_no" onclick="hideControll('esguinces','show')">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="fracturas" value="si" id="fracturas_si">
            <label class="custom-control-label" for="fracturas_si" onclick="showControll('esguinces','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="fracturas" value="no" id="fracturas_no">
            <label class="custom-control-label" for="fracturas_no" onclick="hideControll('esguinces','show')">no</label>
          </div>
          @endif
          <div class="esguinces" style="display:none">
            @if (isset($historial) && !empty($historial['patologico']['cirugiasLesion'][12]['Descripcion']))
            <input type="text" class="form-control esguinces" name="esguinces" placeholder="Descripción" value="{{$historial['patologico']['cirugiasLesion'][12]['Descripcion']}}">
            @else
            <input type="text" class="form-control esguinces" name="esguinces" placeholder="Descripción">

            @endif
          </div>
        </div>
      </div>
      <div class="row mt-1">
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="otra_cirugia">Otras cirugías incluyendo accidentes automovilísticos o del trabajo:</label>
            @if (isset($historial) && !empty($historial['patologico']['otra_cirugia']))
            <input type="text" class="form-control" name="otra_cirugia" id="otra_cirugia" placeholder="" value="{{$historial['patologico']['otra_cirugia']}}">
            @else
            <input type="text" class="form-control" name="otra_cirugia" id="otra_cirugia" placeholder="">

            @endif
          </fieldset>
        </div>
      </div>
  </div>
</fieldset>

<script>
  if (document.getElementById('c_dental_si').checked) {
    showControll('dental', 'show');
  }
  if (document.getElementById('lesion_cer_si').checked) {
    showControll('cuello', 'show');
  }
  if (document.getElementById('c_estomago_si').checked) {
    showControll('estomago', 'show');
  }
  if (document.getElementById('c_colon_si').checked) {
    showControll('colon', 'show');
  }
  if (document.getElementById('c_intestino_si').checked) {
    showControll('intestino', 'show');
  }
  if (document.getElementById('ooforectomia_si').checked) {
    showControll('Histerectomia', 'show');
  }
  if (document.getElementById('sal_vas_si').checked) {
    showControll('Vasectomia', 'show');
  }
  if (document.getElementById('c_hernia_si').checked) {
    showControll('hernias', 'show');
  }
  if (document.getElementById('c_tiroides_si').checked) {
    showControll('tiroides', 'show');
  }
  if (document.getElementById('colecistectomia_si').checked) {
    showControll('Colecistectomia', 'show');
  }
  if (document.getElementById('apendicectomia_si').checked) {
    showControll('apendicectomia', 'show');
  }
  if (document.getElementById('fracturas_si').checked) {
    showControll('esguinces', 'show');
  }
</script>
