<fieldset>
    <div id="resultado">
      <div id="AlertaResultado">
  
      </div>
      <h5 class="secondary text-center">RESULTADOS DE APTITUD PARA REALIZAR LA ACTIVIDAD LABORAL</h5>
      <p>Determinar si el trabajador es medicamente apto o no apto para realizar la actividad de trabajo, en función de los resultados en la exploración física- médica, antecedentes médicos así como el interrogatorio de los factores de riesgos.</p>
      <div class="row">
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="actividad_laboral">Preguntar al trabajador, e indicar en este apartado, la actividad laboral que realizará:</label>
            <textarea class="form-control" name="actividad_laboral" id="actividad_laboral" rows="8" cols="80"></textarea>
          </fieldset>
        </div> 
        <div class="col-md-12">
          <div class="form-group">
              <label class="d-block">CUMPLE CON LOS REQUERIMIENTOS MEDICOS</label>
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" checked class="custom-control-input bg-secondary" name="SATISFACTORIO" value="SATISFACTORIO" id="SATISFACTORIO">
                <label class="custom-control-label" for="SATISFACTORIO">SATISFACTORIO</label>
              </div>
              
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="estadoCivil" value="SATISFACTORIO_CONDICIONADO" id="SATISFACTORIO_CONDICIONADO">
                <label class="custom-control-label" for="SATISFACTORIO_CONDICIONADO">SATISFACTORIO CONDICIONADO</label>
              </div>
              
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio"  class="custom-control-input bg-secondary" name="NO_SATISFACTORIO" value="NO_SATISFACTORIO" id="NO_SATISFACTORIO">
                <label class="custom-control-label" for="NO_SATISFACTORIO">NO SATISFACTORIO</label>
              </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <h5 class="secondary text-center">IDENTIFICACIÓN DE FACTORES DE VULNERABILIDAD COVID-19</h5>
            <div class="col-md-12">
              <div class="form-group">
                  <label class="d-block">De acuerdo con los antecedentes patológicos personales y examen físico, el trabajador presenta factores de vulnerabilidad a COVID-19.</label>
                  <div class="d-inline-block custom-control custom-radio mr-1">
                      <input type="radio" class="custom-control-input bg-secondary" checked
                          name="COVID_19" value="si" id="COVID_19_si">
                      <label class="custom-control-label" for="COVID_19_si"
                          onclick="showControll('COVID_19','show')">si</label>
                  </div>
                  <div class="d-inline-block custom-control custom-radio mr-1">
                      <input type="radio" class="custom-control-input bg-secondary" name="COVID_19"
                          value="no" id="COVID_19_no">
                      <label class="custom-control-label" for="COVID_19_no"
                          onclick="hideControll('COVID_19','show')">no</label>
                  </div>
              </div>
          </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label class="secondary text-center">Indicar cuales:</label>
            <div class="col-md-12">
              <div class="form-group">
                <select class="selectize-multiple select2-selection select2-selection--multiple" class="form-control" name="vulnerabilidad_covid[]" id="select_vulnerabilidad_covid" placeholder="Factores de vulnerabilidad" multiple>
                 <option value="mayor">Edad mayor a 60 años</option>
                 <option value="Enfisema">Enfisema pulmonar</option>
                 <option value="Renal">Enfermedad renal</option>
                 <option value="Hipertension">Hipertensión (Sistólica mayor 150 mmHg o Diastólica mayor 100 mmHg) y no controlada</option>
                 <option value="Diabetes">Diabetes mayor o igual 140 mgs y no contralada /Hb glicolisada > 7</option>
                 <option value="Pulmonar">Enfermedad Pulmonar Obstructiva Cronica (EPOC)</option>
                 <option value="Cardiacas">Enfermedades cardiacas</option>
                 <option value="Cancer">Tumores o cáncer</option>
                 <option value="Embarazo">En estado de embarazo</option>
                 <option value="Brinquial">Asma bronquial</option>
                 <option value="IMC">IMC mayor o igual  a 40 kg/m2 </option>
                 <option value="sistema_inmune">Enfermedades que comprometan el sistema inmune </option>
                </select>
              </div>
          </div>
          </div>
        </div>
        
        
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="firma_medico">Nombre completo y firma del médico:</label>
            
            <input type="text" class="form-control" id="firma_medico" name="firma_medico" placeholder="Firma del medico">
          </fieldset>
        </div> 
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="cedula">Cédula profesional:</label>
            
            <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula Profesional">
          </fieldset>
        </div> 
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="firma_evaluado">Firma del evaluado:</label>
            
            <input type="text" class="form-control" id="firma_evaluado" name="firma_evaluado" placeholder="Firma del evaluado">
          </fieldset>
        </div> 
        <div class="col-md-12 text-center">
          <label for="actividad_laboral"><b>CONSENTIMIENTO SOBRE MANEJO DE INFORMACIÓN</b></label>
            
        </div>
        <div class="col-md-12">
          <fieldset class="form-group">
            <ul>
              <li>Autorizo que la información generada pueda ser capturada, almacenada y resumida en medios electrónicos con el propósito de que se utilice para fines estadísticos, así como la definición de campañas de salud y bienestar en beneficio mío y de otros empleados.</li>
              <li>Estoy de acuerdo que los datos recabados en una consulta médica, campañas de salud o los proporcionados por mí de forma voluntaria al personal médico gestionado por CEMEX, se manejarán de forma confidencial. </li>
              <li>En caso de solicitarse cualquier dato adicional por el área de Recursos Humanos de CEMEX para completar el expediente, doy mi consentimiento, estando consciente y de acuerdo con el resguardo de dicha documentación, ya sea en físico o digital. </li>
            </ul>
          </fieldset>
        </div> 
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="firma_evaluado1"> Nombre y firma del evaluado:</label>
            
            <input type="text" class="form-control" id="firma_evaluado1" name="firma_evaluado1" placeholder="Nombre y Firma del evaluado">
          </fieldset>
        </div>
        <div class="col-md-12">
          <fieldset class="form-group">
            <label for="lugar_fecha"> Lugar y fecha:</label>
            
            <input type="text" class="form-control" id="lugar_fecha" name="lugar_fecha" placeholder="Lugar y Fecha">
          </fieldset>
        </div>
       
        
      
      
      
    
      </div>
    </div>
</fieldset>