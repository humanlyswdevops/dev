<fieldset>
  <div id="heredofamiliar">
    <hr>
    <p class="text-center">Por favor indique si su familia (Padre, Madre, Hermanos(as)) tuvo alguna de las enfermedades que se mencionan</p>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Padre:</label>
        </div>
      </div>
      <div class="col-md-3">
        @if (isset($historial) && $historial['Heredofamiliar'][0]['estado'] == 'Vivo')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" checked name="padre_estado" id="vivo_padre">
          <label class="custom-control-label" for="vivo_padre">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" name="padre_estado" id="finado_padre">
          <label class="custom-control-label" for="finado_padre">Finado</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" name="padre_estado" id="vivo_padre">
          <label class="custom-control-label" for="vivo_padre">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" checked name="padre_estado" id="finado_padre">
          <label class="custom-control-label" for="finado_padre">Finado</label>
        </div>
        @endif
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple select2-selection select2-selection--multiple" class="form-control" name="select_padre[]" id="select_padre" placeholder="Enfermedades" multiple>

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Hepatitis', $historial['Heredofamiliar'][0]['enfermedad']) )
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Cancer', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Cancer">Cancer</option>
            @else
            <option value="Cancer">Cancer</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Asma', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Asma">Asma</option>
            @else
            <option value="Asma">Asma</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Colesterol Alto', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Colesterol Alto">Colesterol Alto</option>
            @else
            <option value="Colesterol Alto">Colesterol Alto</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Evc', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Evc">Evc</option>
            @else
            <option value="Evc">Evc</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Diabetes', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Diabetes">Diabetes</option>
            @else
            <option value="Diabetes">Diabetes</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Otros', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Otros">Otros</option>
            @else
            <option value="Otros">Otros</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Obesidad', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Obesidad">Obesidad</option>
            @else
            <option value="Obesidad">Obesidad</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Sida', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Sida">Sida</option>
            @else
            <option value="Sida">Sida</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Tuberculosis', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Tuberculosis">Tuberculosis</option>
            @else
            <option value="Tuberculosis">Tuberculosis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Sangrado', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Sangrado">Sangrado</option>
            @else
            <option value="Sangrado">Sangrado</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Anemia', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Anemia">Anemia</option>
            @else
            <option value="Anemia">Anemia</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Alcoholismo', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Alcoholismo">Alcoholismo</option>
            @else
            <option value="Alcoholismo">Alcoholismo</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Alta presión sanguinea', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Alta presión sanguinea">Alta presión sanguinea</option>
            @else
            <option value="Alta presión sanguinea">Alta presión sanguinea</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Ataque al corazón antes de los 55 años', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @else
            <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Defectos Congénitos', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Defectos Congénitos">Defectos Congénitos</option>
            @else
            <option value="Defectos Congénitos">Defectos Congénitos</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][0]['enfermedad']) && in_array('Problemas Emocionales/Psicológicos', $historial['Heredofamiliar'][0]['enfermedad']))
            <option selected value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @else
            <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @endif
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Madre:</label>
        </div>
      </div>
      <div class="col-md-3">
        @if (isset($historial) && $historial['Heredofamiliar'][1]['estado'] == 'Vivo')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" checked name="madre_estado" id="madre_viva">
          <label class="custom-control-label" for="madre_viva">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" name="madre_estado" id="madre_finada">
          <label class="custom-control-label" for="madre_finada">Finado</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" name="madre_estado" id="madre_viva">
          <label class="custom-control-label" for="madre_viva">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" checked name="madre_estado" id="madre_finada">
          <label class="custom-control-label" for="madre_finada">Finado</label>
        </div>
        @endif

      </div>
      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" class="form-control" name="select_madre[]" placeholder="Enfermedades" multiple>

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Hepatitis', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Cancer', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Asma', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Asma">Asma</option>
            @else
            <option value="Asma">Asma</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Colesterol Alto', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Colesterol Alto">Colesterol Alto</option>
            @else
            <option value="Colesterol Alto">Colesterol Alto</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Evc', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Evc">Evc</option>
            @else
            <option value="Evc">Evc</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Diabetes', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Diabetes">Diabetes</option>
            @else
            <option value="Diabetes">Diabetes</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Otros', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Otros">Otros</option>
            @else
            <option value="Otros">Otros</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Obesidad', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Obesidad">Obesidad</option>
            @else
            <option value="Obesidad">Obesidad</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Sida', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Sida">Sida</option>
            @else
            <option value="Sida">Sida</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Tuberculosis', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Tuberculosis">Tuberculosis</option>
            @else
            <option value="Tuberculosis">Tuberculosis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Sangrado', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Sangrado">Sangrado</option>
            @else
            <option value="Sangrado">Sangrado</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Anemia', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Anemia">Anemia</option>
            @else
            <option value="Anemia">Anemia</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Alcoholismo', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Alcoholismo">Alcoholismo</option>
            @else
            <option value="Alcoholismo">Alcoholismo</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Alta presión sanguinea', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Alta presión sanguinea">Alta presión sanguinea</option>
            @else
            <option value="Alta presión sanguinea">Alta presión sanguinea</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Ataque al corazón antes de los 55 años', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @else
            <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Defectos Congénitos', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Defectos Congénitos">Defectos Congénitos</option>
            @else
            <option value="Defectos Congénitos">Defectos Congénitos</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][1]['enfermedad']) && in_array('Problemas Emocionales/Psicológicos', $historial['Heredofamiliar'][1]['enfermedad']))
            <option selected value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @else
            <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @endif
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Conyuge:</label>
        </div>
      </div>
      <div class="col-md-3">
        @if (isset($historial) && $historial['Heredofamiliar'][2]['estado'] == 'Vivo')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" checked name="conyuge_estado" id="conyuge_vivo">
          <label class="custom-control-label" for="conyuge_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" name="conyuge_estado" id="conyuge_finado">
          <label class="custom-control-label" for="conyuge_finado">Finado</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" name="conyuge_estado" id="conyuge_vivo">
          <label class="custom-control-label" for="conyuge_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" checked name="conyuge_estado" id="conyuge_finado">
          <label class="custom-control-label" for="conyuge_finado">Finado</label>
        </div>
        @endif
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" class="form-control" name="select_conyuge[]" placeholder="Enfermedades" multiple>
            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Hepatitis', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Cancer', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Asma', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Asma">Asma</option>
            @else
            <option value="Asma">Asma</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Colesterol Alto', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Colesterol Alto">Colesterol Alto</option>
            @else
            <option value="Colesterol Alto">Colesterol Alto</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Evc', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Evc">Evc</option>
            @else
            <option value="Evc">Evc</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Diabetes', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Diabetes">Diabetes</option>
            @else
            <option value="Diabetes">Diabetes</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Otros', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Otros">Otros</option>
            @else
            <option value="Otros">Otros</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Obesidad', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Obesidad">Obesidad</option>
            @else
            <option value="Obesidad">Obesidad</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Sida', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Sida">Sida</option>
            @else
            <option value="Sida">Sida</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Tuberculosis', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Tuberculosis">Tuberculosis</option>
            @else
            <option value="Tuberculosis">Tuberculosis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Sangrado', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Sangrado">Sangrado</option>
            @else
            <option value="Sangrado">Sangrado</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Anemia', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Anemia">Anemia</option>
            @else
            <option value="Anemia">Anemia</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Alcoholismo', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Alcoholismo">Alcoholismo</option>
            @else
            <option value="Alcoholismo">Alcoholismo</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Alta presión sanguinea', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Alta presión sanguinea">Alta presión sanguinea</option>
            @else
            <option value="Alta presión sanguinea">Alta presión sanguinea</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Ataque al corazón antes de los 55 años', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @else
            <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Defectos Congénitos', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Defectos Congénitos">Defectos Congénitos</option>
            @else
            <option value="Defectos Congénitos">Defectos Congénitos</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][2]['enfermedad']) && in_array('Problemas Emocionales/Psicológicos', $historial['Heredofamiliar'][2]['enfermedad']))
            <option selected value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @else
            <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @endif
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">hermanos(as):</label>
        </div>
      </div>
      @if (isset($historial) && $historial['Heredofamiliar'][3]['estado'] == 'Vivo')
      <div class="col-md-3">
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" checked name="hermano_estado" id="hermano_vivo">
          <label class="custom-control-label" for="hermano_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" name="hermano_estado" id="hermano_finado">
          <label class="custom-control-label" for="hermano_finado">Finado</label>
        </div>
      </div>
      @else
      <div class="col-md-3">
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" name="hermano_estado" id="hermano_vivo">
          <label class="custom-control-label" for="hermano_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" checked name="hermano_estado" id="hermano_finado">
          <label class="custom-control-label" for="hermano_finado">Finado</label>
        </div>
      </div>
      @endif
      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" class="form-control" name="select_hermanos[]" placeholder="Enfermedades" multiple>
            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Hepatitis', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Cancer', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Asma', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Asma">Asma</option>
            @else
            <option value="Asma">Asma</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Colesterol Alto', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Colesterol Alto">Colesterol Alto</option>
            @else
            <option value="Colesterol Alto">Colesterol Alto</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Evc', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Evc">Evc</option>
            @else
            <option value="Evc">Evc</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Diabetes', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Diabetes">Diabetes</option>
            @else
            <option value="Diabetes">Diabetes</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Otros', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Otros">Otros</option>
            @else
            <option value="Otros">Otros</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Obesidad', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Obesidad">Obesidad</option>
            @else
            <option value="Obesidad">Obesidad</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Sida', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Sida">Sida</option>
            @else
            <option value="Sida">Sida</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Tuberculosis', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Tuberculosis">Tuberculosis</option>
            @else
            <option value="Tuberculosis">Tuberculosis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Sangrado', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Sangrado">Sangrado</option>
            @else
            <option value="Sangrado">Sangrado</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Anemia', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Anemia">Anemia</option>
            @else
            <option value="Anemia">Anemia</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Alcoholismo', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Alcoholismo">Alcoholismo</option>
            @else
            <option value="Alcoholismo">Alcoholismo</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Alta presión sanguinea', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Alta presión sanguinea">Alta presión sanguinea</option>
            @else
            <option value="Alta presión sanguinea">Alta presión sanguinea</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Ataque al corazón antes de los 55 años', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @else
            <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Defectos Congénitos', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Defectos Congénitos">Defectos Congénitos</option>
            @else
            <option value="Defectos Congénitos">Defectos Congénitos</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][3]['enfermedad']) && in_array('Problemas Emocionales/Psicológicos', $historial['Heredofamiliar'][3]['enfermedad']))
            <option selected value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @else
            <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @endif
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Hijos:</label>
        </div>
      </div>
      <div class="col-md-3">
        @if (isset($historial) && $historial['Heredofamiliar'][4]['estado'] == 'Vivo')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" checked name="hijos_estato" id="hijo_vivo">
          <label class="custom-control-label" for="hijo_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" name="hijos_estato" id="hijo_finado">
          <label class="custom-control-label" for="hijo_finado">Finado</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Vivo" name="hijos_estato" id="hijo_vivo">
          <label class="custom-control-label" for="hijo_vivo">Vivo</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" value="Finado" checked name="hijos_estato" id="hijo_finado">
          <label class="custom-control-label" for="hijo_finado">Finado</label>
        </div>
        @endif
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <select class="selectize-multiple" class="form-control" name="select_hijos[]" placeholder="Enfermedades" multiple>
            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Hepatitis', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Cancer', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Hepatitis">Hepatitis</option>
            @else
            <option value="Hepatitis">Hepatitis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Asma', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Asma">Asma</option>
            @else
            <option value="Asma">Asma</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Colesterol Alto', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Colesterol Alto">Colesterol Alto</option>
            @else
            <option value="Colesterol Alto">Colesterol Alto</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Evc', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Evc">Evc</option>
            @else
            <option value="Evc">Evc</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Diabetes', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Diabetes">Diabetes</option>
            @else
            <option value="Diabetes">Diabetes</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Otros', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Otros">Otros</option>
            @else
            <option value="Otros">Otros</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Obesidad', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Obesidad">Obesidad</option>
            @else
            <option value="Obesidad">Obesidad</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Sida', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Sida">Sida</option>
            @else
            <option value="Sida">Sida</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Tuberculosis', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Tuberculosis">Tuberculosis</option>
            @else
            <option value="Tuberculosis">Tuberculosis</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Sangrado', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Sangrado">Sangrado</option>
            @else
            <option value="Sangrado">Sangrado</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Anemia', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Anemia">Anemia</option>
            @else
            <option value="Anemia">Anemia</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Alcoholismo', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Alcoholismo">Alcoholismo</option>
            @else
            <option value="Alcoholismo">Alcoholismo</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Alta presión sanguinea', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Alta presión sanguinea">Alta presión sanguinea</option>
            @else
            <option value="Alta presión sanguinea">Alta presión sanguinea</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Ataque al corazón antes de los 55 años', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @else
            <option value="Ataque al corazón antes de los 55 años">Ataque al corazón antes de los 55 años</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Defectos Congénitos', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Defectos Congénitos">Defectos Congénitos</option>
            @else
            <option value="Defectos Congénitos">Defectos Congénitos</option>
            @endif

            @if (isset($historial) && !empty($historial['Heredofamiliar'][4]['enfermedad']) && in_array('Problemas Emocionales/Psicológicos', $historial['Heredofamiliar'][4]['enfermedad']))
            <option selected value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @else
            <option value="Problemas Emocionales/Psicológicos">Problemas Emocionales/Psicológicos</option>
            @endif
          </select>
        </div>
      </div>

    </div>
  </div>
  
  </fieldset>
