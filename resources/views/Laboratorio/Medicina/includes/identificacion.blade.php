<fieldset>
  <div id="identificacion">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="fechahoy">Fecha:</label>
          <input type="date" class="form-control" id="fechahoy" name="fecha_estudio" id="fecha" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}">
        </div>
      </div>
      <div class="col-md-2">
        @if (isset($historial))
        <div class="form-group">
          <label for="noEmpleado">No de paciente:</label>
          <input type="text" class="form-control" name="noEmpleado" id="noEmpleado" value="{{$historial['identificacion']['numEmpleado']}}">
        </div>
        @else
        <div class="form-group">
          <label for="noEmpleado">No de paciente:</label>
          <input type="text" class="form-control" name="noEmpleado" id="noEmpleado">
        </div>
        @endif

      </div>
      <div class="col-md-3">
        @if (isset($historial))
        <div class="form-group">
          <label for="departamento">Departamento:</label>
          <input type="text" class="form-control" name="departamento" id="departamento" value="{{$historial['identificacion']['departamento']}}">
        </div>
        @else
        <div class="form-group">
          <label for="departamento">Departamento:</label>
          <input type="text" class="form-control" name="departamento" id="departamento">
        </div>
        @endif
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label for="edad">Edad:</label>
          <input type="text" class="form-control" name="edad" id="edad" value="{{ \Carbon\Carbon::parse($paciente->fecha_nacimiento)->age}}">
        </div>
      </div>

      <div class="col-md-2">
        <div class="form-group">
          <label for="sexo">Sexo :</label>
          <input type="text" class="form-control" name="sexo" value="{{$paciente->genero}}" id="sexo">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="app">Apellido Paterno :</label>
          <input type="text" class="form-control" name="app" id="app" value="{{$paciente->apellido_paterno}}">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="apm">Apellido Materno :</label>
          <input type="text" class="form-control" id="apm" name="apm" value="{{$paciente->apellido_materno}}">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label for="nombre">Nombre :</label>
          <input type="text" class="form-control" id="nombre" name="nombre" value="{{$paciente->nombre}}">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8">
        <div class="form-group">
          @if (isset($historial) && $historial['identificacion']['estadoCivil']=='Casado')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" checked class="custom-control-input bg-secondary" name="estadoCivil" value="Casado" id="casado">
            <label class="custom-control-label" for="casado">Casado</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="estadoCivil" value="Casado" id="casado">
            <label class="custom-control-label" for="casado">Casado</label>
          </div>
          @endif

          @if (isset($historial) && $historial['identificacion']['estadoCivil']=='Divorciado')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" checked class="custom-control-input bg-secondary" name="estadoCivil" value="Divorciado" id="Divorciado">
            <label class="custom-control-label" for="Divorciado">Divorciado</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="estadoCivil" value="Divorciado" id="Divorciado">
            <label class="custom-control-label" for="Divorciado">Divorciado</label>
          </div>
          @endif

          @if (isset($historial) && $historial['identificacion']['estadoCivil']=='Soltero')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" checked class="custom-control-input bg-secondary" name="estadoCivil" value="Soltero" id="Soltero">
            <label class="custom-control-label" for="Soltero">Soltero</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="estadoCivil" checked value="Soltero" id="Soltero">
            <label class="custom-control-label" for="Soltero">Soltero</label>
          </div>
          @endif

          @if (isset($historial) && $historial['identificacion']['estadoCivil']=='Viudo')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" checked class="custom-control-input bg-secondary" name="estadoCivil" value="Viudo" id="Viudo">
            <label class="custom-control-label" for="Viudo">Viudo</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="estadoCivil" value="Viudo" id="Viudo">
            <label class="custom-control-label" for="Viudo">Viudo</label>
          </div>
          @endif

          @if (isset($historial) && $historial['identificacion']['estadoCivil']=='Unión libre')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" checked class="custom-control-input bg-secondary" name="estadoCivil" value="Unión libre" id="libre">
            <label class="custom-control-label" for="libre">Unión Libre</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="estadoCivil" value="Unión libre" id="libre">
            <label class="custom-control-label" for="libre">Unión Libre</label>
          </div>
          @endif


          @if (isset($historial) && $historial['identificacion']['estadoCivil']=='Separado')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" checked class="custom-control-input bg-secondary" name="estadoCivil" value="Separado" id="Separado">
            <label class="custom-control-label" for="Separado">Separado</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="estadoCivil" value="Separado" id="Separado">
            <label class="custom-control-label" for="Separado">Separado</label>
          </div>
          @endif

        </div>
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="lastName1">Escolaridad :</label>
          <input type="text" class="form-control" name="escolaridad" id="escolaridad" value="{{$historial['identificacion']['escolaridad']}}">
        </div>
        @else
        <div class="form-group">
          <label for="lastName1">Escolaridad :</label>
          <input type="text" class="form-control" name="escolaridad" id="escolaridad">
        </div>
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="domicilio">Domicilio :</label>
          <input type="text" class="form-control" id="domicilio" name="domicilio" placeholder="Calle/No/Colonia/Municipio/Estado" value="{{$historial['identificacion']['domicilio']}}">
        </div>
        @else
        <div class="form-group">
          <label for="domicilio">Domicilio :</label>
          <input type="text" class="form-control" id="domicilio" name="domicilio" placeholder="Calle/No/Colonia/Municipio/Estado">
        </div>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="lugarnacimiento">Lugar de Nacimiento :</label>
          <input type="text" class="form-control" id="lugarnacimiento" name="lugarnacimiento" value="{{$historial['identificacion']['lugarNacimiento']}}">
        </div>
        @else
        <div class="form-group">
          <label for="lugarnacimiento">Lugar de Nacimiento :</label>
          <input type="text" class="form-control" id="lugarnacimiento" name="lugarnacimiento">
        </div>
        @endif

      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="nacimiento">Fecha de Nacimiento :</label>
          <input type="date" class="form-control" id="nacimiento" name="nacimiento" value="{{$paciente->fecha_nacimiento}}">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        @if (isset($historial))
        <fieldset class="form-group">
          <label for="ciudad">Ciudad o Ejido</label>
          <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="" value="{{$historial['identificacion']['ciudad']}}">
        </fieldset>
        @else
        <fieldset class="form-group">
          <label for="ciudad">Ciudad o Ejido</label>
          <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="">
        </fieldset>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <fieldset class="form-group">
          <label for="municipio">Municipio</label>
          <input type="text" class="form-control" name="municipio" id="municipio" placeholder="" value="{{$historial['identificacion']['municipio']}}">
        </fieldset>
        @else
        <fieldset class="form-group">
          <label for="municipio">Municipio</label>
          <input type="text" class="form-control" name="municipio" id="municipio" placeholder="">
        </fieldset>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <fieldset class="form-group">
          <label for="estado">Estado</label>
          <input type="text" class="form-control" name="estado" id="estado" placeholder="" value="{{$historial['identificacion']['estado']}}">
        </fieldset>
        @else
        <fieldset class="form-group">
          <label for="estado">Estado</label>
          <input type="text" class="form-control" name="estado" id="estado" placeholder="">
        </fieldset>
        @endif
      </div>

    </div>
    <div class="row">
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="emergencia">En caso de emergencia llamar a:</label>
          <input type="text" class="form-control" id="emergencia" name="emergencia" value="{{$historial['identificacion']['nom_per_em']}}">
        </div>
        @else
        <div class="form-group">
          <label for="emergencia">En caso de emergencia llamar a:</label>
          <input type="text" class="form-control" id="emergencia" name="emergencia">
        </div>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="parentesco">Parentesco :</label>
          <input type="text" class="form-control" name="parentesco" id="parentesco" value="{{$historial['identificacion']['parentesco']}}">
        </div>
        @else
        <div class="form-group">
          <label for="parentesco">Parentesco :</label>
          <input type="text" class="form-control" name="parentesco" id="parentesco">
        </div>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="telefonoParenteso">Telefono :</label>
          <input type="text" maxlength="10" class="form-control" name="telefonoParenteso" onkeypress='return validaNumericos(event)' id="telefonoParenteso" value="{{$historial['identificacion']['telefono_1']}}">
        </div>
        @else
        <div class="form-group">
          <label for="telefonoParenteso">Telefono :</label>
          <input type="text" maxlength="10" class="form-control" name="telefonoParenteso" onkeypress='return validaNumericos(event)' id="telefonoParenteso">
        </div>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="domicilioEm">Domicilio :</label>
          <input type="text" class="form-control" id="domicilioEm" name="domicilioEm" value="{{$historial['identificacion']['domicilio_em']}}">
        </div>
        @else
        <div class="form-group">
          <label for="domicilioEm">Domicilio :</label>
          <input type="text" class="form-control" id="domicilioEm" name="domicilioEm">
        </div>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="lugtrabajo">Lugar de Trabajo :</label>
          <input type="text" class="form-control" name="lugtrabajo" id="lugtrabajo" value="{{$historial['identificacion']['lug_trabajo']}}">
        </div>
        @else
        <div class="form-group">
          <label for="lugtrabajo">Lugar de Trabajo :</label>
          <input type="text" class="form-control" name="lugtrabajo" id="lugtrabajo">
        </div>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial))
        <div class="form-group">
          <label for="telefonoParenteso2">Telefono :</label>
          <input type="text" maxlength="10" class="form-control" name="telefonoParenteso2" onkeypress='return validaNumericos(event)' id="telefonoParenteso2" value="{{$historial['identificacion']['telefono_2']}}">
        </div>
        @else
        <div class="form-group">
          <label for="telefonoParenteso2">Telefono :</label>
          <input type="text" maxlength="10" class="form-control" name="telefonoParenteso2" onkeypress='return validaNumericos(event)' id="telefonoParenteso2">
        </div>
        @endif
      </div>
      {{-- @if (isset($historial))
      <a class="btn btn-success" onclick="GuardarAntesModificado()" id="GuardarEditado">Guardar</a>
      @else
      <a type="button" class="btn btn-success" onclick="GuardarAntes()" id="Guardar">Guardar</a>
      @endif --}}
    </div>
  </div>
</fieldset>
