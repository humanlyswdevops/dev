@extends('layouts.VuexyLaboratorio')

@section('title')
  Espirometria
@endsection

@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('css_custom')
<style media="screen">
.table-sm td, .table-sm th {
padding: .3rem;
}
.fileContainer {
  overflow: hidden;
  position: relative;
}

.fileContainer [type=file] {
  cursor: inherit;
  display: block;
  font-size: 999px;
  filter: alpha(opacity=0);
  min-height: 100%;
  min-width: 100%;
  opacity: 0;
  position: absolute;
  right: 0;
  text-align: right;
  top: 0;
}

/* Example stylistic flourishes */

.fileContainer {
  background: red;
  border-radius: .5em;
  float: left;
  padding: .5em;
}

.fileContainer [type=file] {
  cursor: pointer;
}
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')

<section class="espirometria">
    @if ($espiro)
        <input type="hidden" id="nim" value="{{ $espiro->nim }}">
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title col-md-12 text-center">Reporte de Espirometria</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                      <div class="row">
                          <div class="col-md-4">
                              <div class="d-flex">
                                  <h5 class="mr-2">Referencia:</h5>
                                  <p>{{ $espiro->nim }}</p>
                              </div>
                              <div class="d-flex">
                                  <h5 class="mr-2">Nombre:</h5>
                                  <p>{{ $espiro->nombre }} {{ $espiro->apellidos }}</p>
                              </div>
                              <div class="d-flex">
                                  <h5 class="mr-2">Sexo:</h5>
                                  @if ($espiro->sexo == 'M')
                                      Masculino
                                      @else
                                      Femenino
                                  @endif

                              </div>
                              <div class="d-flex">
                                  <h5 class="mr-2">Edad:</h5>
                                  <p>{{ $espiro->edad }} </p>
                              </div>
                          </div>
                          <div class="col-md-4">

                              <div class="d-flex">
                                  <h5 class="mr-2">Fecha:</h5>
                                  <p>
                                      @php
                                          $split = explode(" ",$espiro->tomaFecha);
                                      @endphp
                                      {{$split[0]}}
                                  </p>
                              </div>

                              <div class="d-flex">
                                  <h5 class="mr-2">Press(mmHg):</h5>
                                  <p>{{ $espiro->presion }}</p>
                              </div>

                              <div class="d-flex">
                                  <h5 class="mr-2">Temperatura:</h5>
                                  <p>{{ $espiro->temperatura }}</p>
                              </div>


                          </div>

                          <div class="col-md-4">

                              <div class="d-flex">
                                  <h5 class="mr-2">Hora:</h5>
                                  <p>
                                      @php
                                          $split = explode(" ",$espiro->tomaFecha);
                                      @endphp
                                      {{$split[1]}}
                                  </p>
                              </div>

                              <div class="d-flex">
                                  <h5 class="mr-2"> Humedad:</h5>
                                  <p>{{ $espiro->humedad }}</p>
                              </div>

                              <div class="d-flex">
                                  <h5 class="mr-2"> Imc:</h5>
                                  <p>
                                      @php
                                          $altura = ($espiro->talla/100)*2;
                                          $peso = $espiro->peso;
                                      @endphp
                                      {{ round($peso/$altura,1) }}
                                  </p>
                              </div>

                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title col-md-12 text-center">INFORME DE FVC  </h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-sm">
                            <thead class="bg-secondary text-white">
                                <tr>
                                    <th scope="col">Parametro</th>
                                    <th scope="col"></th>
                                    <th scope="col">M1</th>
                                    <th scope="col">%REF</th>
                                    <th scope="col">M2</th>
                                    <th scope="col">%REF</th>
                                    <th scope="col">M3</th>
                                    <th scope="col">%REF</th>
                                    <th scope="col">REF</th>
                                    <th scope="col">LNN</th>
                                </tr>
                              </thead>
                              <tbody>
                               @foreach ($espiro->resultados as  $value)

                                  <tr>
                                   <td scope="row">{{ $value->title }}</td>
                                   <td>{{ $value->units }}</td>
                                   <td>{{ $value->data }}</td>

                                   @if ($value->title == "Mejor Fvc")
                                      <td>{{round(($value->data*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                      @elseif ($value->title == "Mejor Fev1")
                                        <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                    @elseif ($value->title == "MFEV1/MFVC")
                                        <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                   @endif

                                   <td>{{ $value->data }}</td>

                                  @if ($value->title == "Mejor Fvc")
                                     <td>{{round(($value->data*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                     @elseif ($value->title == "Mejor Fev1")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                   @elseif ($value->title == "MFEV1/MFVC")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                  @endif
                                   <td>{{ $value->data }}</td>

                                  @if ($value->title == "Mejor Fvc")
                                     <td>{{round(($value->data*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                     @elseif ($value->title == "Mejor Fev1")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                   @elseif ($value->title == "MFEV1/MFVC")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                  @endif

                                   @if ($value->title == "Mejor Fvc")
                                      <td>{{ $espiro->ref->where('title','RFVC')->first()->data }}</td>
                                      @elseif ($value->title == "Mejor Fev1")
                                        <td>{{ $espiro->ref->where('title','RFEV1')->first()->data }}</td>
                                    @elseif ($value->title == "MFEV1/MFVC")
                                        <td>{{ $espiro->ref->where('title','RFEV1-FVC')->first()->data }}</td>
                                   @endif

                                   @if ($value->title == "Mejor Fvc")
                                      <td>{{ $espiro->lln->where('title','RFVC LLN')->first()->data }}</td>
                                      @elseif ($value->title == "Mejor Fev1")
                                        <td>{{ $espiro->lln->where('title','RFEV1 LLN')->first()->data }}</td>
                                    @elseif ($value->title == "MFEV1/MFVC")
                                        <td>{{ $espiro->lln->where('title','RFEV1-FVC LLN')->first()->data }}</td>
                                   @endif
                                  </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>

                      <div class="row">

                         @for ($i=1; $i <= $espiro->maniobras; $i++)

                             <div class="col-md-3">
                                 <div class="table-responsive">
                                     <table class="table table-sm">
                                         <thead class="bg-secondary text-white">
                                             <tr>
                                                 <th scope="col">Parametro</th>
                                                 <th scope="col">Unidad</th>
                                                 <th scope="col">M{{ $i }}</th>
                                                 <th scope="col">%REF</th>
                                             </tr>

                                           </thead>
                                           <tbody>
                                             @foreach ($espiro->maneobras->where('maneobra',$i)->all() as $value)
                                                 <tr>
                                                     <td>{{ $value->title }}</td>
                                                     <td>{{ $value->units }}</td>
                                                     <td>
                                                         {{ $value->data }}
                                                         @if ($value->title == 'FVC')
                                                         <input type="hidden" id="fvc_{{ $i }}" value="{{ $value->data }}">
                                                         @endif

                                                        </td>
                                                     <td>

                                                         @if ($value->title == "FVC")
                                                            {{round(($value->data*100)/$espiro->ref->where('title','RFVC')->first()->data) }}
                                                          @elseif ($value->title == "FEV1")
                                                             {{round(($value->data*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}
                                                          @elseif ($value->title == "FEV1/FVC")
                                                            {{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}
                                                          @elseif ($value->title == "PEF")
                                                            {{round(($value->data*100)/$espiro->ref->where('title','RPEF')->first()->data) }}
                                                          @elseif ($value->title == "FEF50%")
                                                            {{round(($value->data*100)/$espiro->ref->where('title','RFEF50')->first()->data) }}
                                                          @elseif ($value->title == "FEF25%-FEF75%")
                                                            {{round(($value->data*100)/$espiro->ref->where('title','RFEF25-75')->first()->data) }}
                                                          @elseif ($value->title == "FEV1/FEV0.5")
                                                            {{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FEV0.5')->first()->data) }}
                                                          @elseif ($value->title == "FEV1/PEF")
                                                            {{round(($value->data*100)/$espiro->ref->where('title','RFEV1-PEF')->first()->data) }}
                                                         @endif

                                                     </td>
                                                 </tr>
                                             @endforeach
                                             <input type="hidden" id="maneobra_{{ $i }}"  value="{{ $espiro->maneobras->where('maneobra',$i)->first()->graph->sflow }}">
                                         </tbody>
                                     </table>
                                 </div>
                             </div>

                         @endfor

                          <div class="col-md-3">
                              <div class="table-responsive">
                                  <table class="table table-sm">
                                      <thead class="bg-secondary text-white">
                                          <tr>
                                              <th>Param</th>
                                              <th scope="col">REF</th>
                                              <th scope="col">LNN</th>
                                          </tr>
                                        </thead>
                                      <tbody>

                                          @foreach ($espiro->maneobras->where('maneobra', 3)->all() as $value)
                                              <tr>
                                                  <td>{{ $value->title }}</td>
                                                  @if ($value->title == "FVC")
                                                     <td>{{$espiro->ref->where('title','RFVC')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1")
                                                      <td>{{$espiro->ref->where('title','RFEV1')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1/FVC")
                                                     <td>{{$espiro->ref->where('title','RFEV1-FVC')->first()->data }}</td>
                                                   @elseif ($value->title == "PEF")
                                                     <td>{{$espiro->ref->where('title','RPEF')->first()->data }}</td>
                                                   @elseif ($value->title == "FEF50%")
                                                     <td>{{$espiro->ref->where('title','RFEF50')->first()->data }}</td>
                                                   @elseif ($value->title == "FEF25%-FEF75%")
                                                     <td>{{$espiro->ref->where('title','RFEF25-75')->first()->data }}</td>
                                                   @elseif ($value->title == "FEF50%/FIF50%")
                                                     <td>{{$espiro->ref->where('title','RFEF50-FIF50')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1/FEV0.5")
                                                     <td>{{$espiro->ref->where('title','RFEV1-FEV0.5')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1/PEF")
                                                     <td>{{$espiro->ref->where('title','RFEV1-PEF')->first()->data }}</td>
                                                  @endif

                                                  @if ($value->title == "FVC")
                                                     <td>{{$espiro->lln->where('title','RFVC LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1")
                                                      <td>{{$espiro->lln->where('title','RFEV1 LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1/FVC")
                                                     <td>{{$espiro->lln->where('title','RFEV1-FVC LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "PEF")
                                                     <td>{{$espiro->lln->where('title','RPEF LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "FEF50%")
                                                     <td>{{$espiro->lln->where('title','RFEF50 LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "FEF25%-FEF75%")
                                                     <td>{{$espiro->lln->where('title','RFEF25-75 LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "FEF50%/FIF50%")
                                                     <td>{{$espiro->lln->where('title','RFEF50-FIF50 LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1/FEV0.5")
                                                     <td>{{$espiro->lln->where('title','RFEV1-FEV0.5 LLN')->first()->data }}</td>
                                                   @elseif ($value->title == "FEV1/PEF")
                                                     <td>{{$espiro->lln->where('title','RFEV1-PEF LLN')->first()->data }}</td>
                                                  @endif
                                              </tr>
                                          @endforeach


                                      </tbody>
                                  </table>
                              </div>
                          </div>

                      </div>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    <h5 class="text-center col-md-12">Graficos</h5>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body" >
                      <div class="row">
                        <div class="col-md-6">
                            <h6 class="text-center">
                                Flujo y Volumen
                            </h6>
                            <div class="formControl">
                                <form id="fv" enctype="multipart/form-data">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" accept="image/*" id="fv_input" name="fv">
                                        <input type="hidden" name="id" value="{{ $espiro->id }}">
                                        <label class="custom-file-label" for="inputGroupFile01">Selecciona la imagen</label>
                                    </div>
                                </form>
                            </div>
                            <div class="text-center mt-1 fv-load" style="display: none">
                                <div class="spinner-border secondary" style="width: 3rem; height: 3rem;" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>

                            <div class="fv">
                                <img class="fv_img" width="100%" src="{!! asset('storage/app/espirometria/'.$espiro->fv) !!}" alt="">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <h6 class="text-center">Volumen y Tiempo</h6>
                            <div class="formControl">
                                <form id="vt" enctype="multipart/form-data">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="vt" id="vt_input" accept="image/*">
                                        <input type="hidden" name="id" value="{{ $espiro->id }}">
                                        <label class="custom-file-label" >Selecciona la imagen</label>
                                    </div>
                                </form>

                            </div>
                            <div class="text-center mt-1 vt-load" style="display: none">
                                <div class="spinner-border secondary" style="width: 3rem; height: 3rem;" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                            <div class="vt">
                                <img class="vt_img" width="100%" src="{!! asset('storage/app/espirometria/'.$espiro->vt) !!}" alt="">
                            </div>

                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-content collapse show">
                  <div class="card-body">
                      <div class="row">
                          <div class="col-md-6">
                            <h5>Interpretación</h5>
                            <textarea class="form-control" id="interpretacion" rows="5" placeholder="Interpretación del Estudio">
                                {{ $espiro->interpretacion }}
                            </textarea>
                          </div>
                          <div class="col-md-6">
                            <h5>
                                Mis Datos Profesionales
                                <button class="btn updateMedico float-right btn-sm btn-icon btn-secondary">
                                    <i class="feather icon-edit"></i>
                                </button>

                                <small class="d-block">Estos datos son necesarios para generar reportes.</small>
                            </h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <h6>Titulo Profesionales:</h6>
                                    <p class="titulo">{{ $medico->titulo_profesional }}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>Nombre Completo:</h6>
                                    <p class="nombre">{{ $medico->nombre_completo }}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>Cedula Profesional:</h6>
                                    <p class="cedula">{{ $medico->cedula }}</p>
                                </div>
                                <div class="col-md-6">
                                    <h6>Firma:</h6>
                                    <img width="100" class="firmaMedico" src="{{ asset('storage/app/datosHtds/'.$medico->firma) }}" alt="">
                                </div>
                            </div>
                          </div>
                      </div>

                    <div class="col-md-12 text-center">
                        <form id="saveForm" method="post">
                            <input type="hidden" id ="medicoToken" value="{{ $medico->firma }}" >
                            <input type="hidden" id="espirometria" value="{{ $espiro->id }}">
                            <input type="hidden" name="ingreso" id="ingreso" value="{{ $ingreso->id }}">
                            <button type="submit" class="btn mt-1 btn-primary btn-sm" name="button">Finalizar Estudios</button>
                            <a target="_blank" href="{!! route('pdfEspirometria',['id'=>$espiro->id]) !!}" class="btn btn-secondary btn-sm mt-1">
                                Visualizar Pdf
                            </a>
                        </form>
                    </div>

                  </div>
                </div>
              </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Espirometria</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                     <input type="hidden" class="empresaId" value="{{ $empresa->indetificador }}">
                     <input type="hidden" class="empleadoId" value="{{ $paciente->id }}">
                     <button type="button" onclick="initToma()" class="btn btn-sm btn-primary" name="button">
                         Iniciar Toma
                     </button>
                  </div>
                </div>
              </div>
            </div>
        </div>
    @endif
</section>

{{-- Modificacion de todos los datos del medico --}}

<div class="modal fade text-left" id="datosMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header bg-primary white">
            <h5 class="modal-title" id="myModalLabel160">Actualizar mis datos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <form method="post" id="updateFormData" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <label>Titulo Profesional:</label>
                        <input type="text" name="titulo_profesional" value="{{ $medico->titulo_profesional }}" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Nombre Completo:</label>
                        <input type="text" name="nombre_completo" value="{{ $medico->nombre_completo }}" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Cedula Profesional:</label>
                        <input type="text" name="cedula" value="{{ $medico->cedula }}" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Firma</label>
                        <div class="custom-file">
                            <input type="file" accept="image/*" class="custom-file-input" name="firma" id="firma">
                            <label class="custom-file-label" for="firma">Seleccionar Firma</label>
                        </div>
                    </div>
                    <div class="col-md-12 mt-1 text-center">
                        <button type="submit" class="btn btn-secondary btn-sm">
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
            <div class="col-md-12 text-center loadFormUpdate" style="display: none">
                <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
            </div>
        </div>
        </div>
    </div>
</div>

@endsection


@section('page_vendor_js')
 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection

@section('page_js')
  <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
  <script src=" {!! asset('public/vuexy/app-assets/js/scripts/modal/components-modal.min.js') !!} "></script>
  <script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection


@section('js_custom')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js" charset="utf-8"></script>
    <script src="{!! asset('public/js/Laboratorio/Espirometria/espirometria.js') !!}" charset="utf-8"></script>
@endsection
