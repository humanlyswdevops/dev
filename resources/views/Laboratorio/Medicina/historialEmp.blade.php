@extends('layouts.VuexyLaboratorio')

@section('title','Empresa')


@section('style_vendor')
  <!-- BEGIN VENDOR CSS-->

  <!-- END VENDOR CSS-->
@endsection
@section('page_level_css')
 <link rel="stylesheet" type="text/css" href="../../../app-assets/css/pages/project.css">
@endsection
@section('styles')

<!-- BEGIN Page Level CSS Custom-->
<link rel="stylesheet" type="text/css" href="../../resources/sass/css/Laboratorio/menu_styles.css">
<link rel="stylesheet" type="text/css" href="../../resources/sass/css/styleScroll.css">
<link rel="stylesheet" type="text/css" href="../../resources/sass/css/Laboratorio/medicinaDiagnostico.css">
<link rel="stylesheet" href="../../resources/sass/fontawesome/css/all.css">
<!-- END Page Level CSS Custom-->

@endsection
{{-- BEGIN body html --}}
@section('content')
<div class="content_all">
  <div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Historiales clinicos por realizar</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    </ul>
                </div>
                <hr>
            </div>
            <div class="card-content">
              <div class="ml-2 mt-1 mb-1">
                <span class="badge badge-primary">Finalizado</span>
                <span class="badge badge-secondary ml-1">Pendiente</span>
              </div>
                <div id="recent-buyers" class="media-list">
                  <fieldset class="form-group position-relative pl-2 pr-2">
                      <input type="text" class="form-control search" placeholder="Buscar Paciente">
                      <div class="form-control-position pr-3">
                          <i class="ft-search"></i>
                      </div>
                  </fieldset>
                  <div>

                    @if (\Session::get('empresa_select')==6)
                      @forelse ($pacientes as $paciente)
                          <a href="{{route('historialFormulario',['curp'=> encrypt($paciente->id)])}}" class="media border-0 pacientes" style="border-bottom:solid 1.3px #434fa5 !important">
                            <div class="media-left pr-1 col-md-9 row">
                              <div class="col-md-2">
                                <span class="avatar avatar-md avatar-online">
                                  <img class="media-object rounded-circle" src="{!! asset('resources/sass/images/login.png') !!}" alt="Paciente" width="40">
                                </span>
                              </div>
                              <div class="col-md-10">
                                <h6 class="list-group-item-heading">{{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}</h6>
                                <h6 class="list-group-item-heading">{{$paciente->genero}}</h6>
                                <h6 class="list-group-item-heading">{{\Carbon::parse($paciente->fecha_nacimiento)->age.' años'}}</h6>
                                <h6 class="list-group-item-heading">{{$paciente->CURP}}</h6>

                              </div>
                            </div>
                          </a>
                        @empty
                          No hay ningun Paciente
                      @endforelse
                    @else
                     @forelse ($pacientes as $paciente)
                      @if ($paciente->tiene_pendientes)
                        <div class="media border-0 pacientes" style="border-bottom:solid 1.3px #434fa5 !important">
                      @else
                        <a href="{{route('historialFormulario',['curp'=> encrypt($paciente->empleado_id)])}}" class="row border-0 pacientes" style="border-bottom:solid 1.3px #434fa5 !important">
                      @endif
                          <div class="col-md-7 row">
                            <div class="col-md-2">
                              <span class="avatar avatar-md avatar-online">
                                <img class="media-object rounded-circle" src=" {!! asset('resources/sass/images/login.png') !!} " alt="Paciente" width="40">
                              </span>
                            </div>
                            <div class="col-md-10">
                              <h6 class="list-group-item-heading">{{$paciente->nombre.' '.$paciente->app.' '.$paciente->apm}}</h6>
                              <h6 class="list-group-item-heading">{{$paciente->genero}}</h6>
                              <h6 class="list-group-item-heading">{{\Carbon::parse($paciente->fecha_nacimiento)->age.' años'}}</h6>
                              <h6 class="list-group-item-heading">{{$paciente->CURP}}</h6>
                              @if ($paciente->tiene_pendientes)
                                <p style="color: red">El paciente aún tiene estudios en proceso.</p>
                              @endif
                            </div>
                          </div>
                          <div class="col-md-5">
                              <p class="list-group-item-text mb-0">
                                @foreach ($paciente->estudios as $estudio)
                                  @if ($estudio->status == 2)
                                    <span class="badge badge-primary">{{$estudio->nombre}} {{$estudio->status}}</span>
                                  @else
                                    <span class="badge badge-secondary">{{$estudio->nombre}} {{$estudio->status}}</span>
                                  @endif
                                @endforeach
                              </p>
                          </div>
                       @if ($paciente->tiene_pendientes)
                        </div>
                       @else
                        </a>
                       @endif
                    @empty
                      No hay ningun paciente registrado por el momento
                    @endforelse
                    @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
          <div class="card">
             <div class="card-head">
                <div class="card-header">
                   <h4 class="card-title">Pacientes</h4>
                   <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                   <div class="heading-elements">
                      <ul class="list-inline mb-0">
                         <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      </ul>
                   </div>
                </div>
             </div>
             <div class="card-content">
                <div class="card-body">
                   <div id="task-pie-chart" class="height-400 echart-container" _echarts_instance_="1583767306676" style="-webkit-tap-highlight-color: transparent; user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;">
                     <div style="position: relative; overflow: hidden; width: 307px; height: 400px;">
                       <div data-zr-dom-id="bg" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 307px; height: 400px; user-select: none;">

                       </div>
                     <canvas width="307" height="400" data-zr-dom-id="0" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 307px; height: 400px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                     </canvas>
                     <canvas width="307" height="400" data-zr-dom-id="1" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 307px; height: 400px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                     </canvas>
                     <canvas width="307" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element" style="position: absolute; left: 0px; top: 0px; width: 307px; height: 400px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                     </canvas>
                     <div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 118px; top: 268px;">Browsers <br>Closed: 82 (82.00%)
                     </div>
                   </div>
                 </div>
                 <hr>
                 <div class="row">
                   <div class="badge  badge-border mx-auto">
                     <i class="fa fa-female"></i>
        							<span id="mujeres"></span>
        						</div>
                    <div class="badge  badge-border mx-auto">
                      <i class="fa fa-male"></i>
        							<span id="hombres"></span>
        						</div>
                    <div class="badge  badge-border mx-auto">
                      <i class="fa fa-venus-mars"></i>
        							<span id="total"></span>
        						</div>
                 </div>
                </div>
             </div>
          </div>
       </div>
     </div>
  </div>
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
    <script src=" {!! asset('../app-assets/vendors/js/charts/echarts/echarts.js') !!} "></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<!-- BEGIN PAGE LEVEL JS-->
<script src="https://asesoresconsultoreslabs.com/expedienteclinico/app-assets/vendors/js/charts/echarts/chart/pie.js"></script>
<script src="https://asesoresconsultoreslabs.com/expedienteclinico/app-assets/vendors/js/charts/echarts/chart/funnel.js"></script>
  <script src="{!! asset('resources/js/Laboratorio/Medico/charts.js') !!}   "></script>

  <!-- END PAGE LEVEL JS-->
@endsection
