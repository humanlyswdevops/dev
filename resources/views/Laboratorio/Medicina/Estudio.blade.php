@extends('layouts.VuexyLaboratorio')

@section('title')
Paciente: {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}
{{-- Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert.css">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')
<style>
  .app-content .wizard.wizard-circle>.steps>ul>li:before,
  .app-content .wizard.wizard-circle>.steps>ul>li:after {
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.done .step {
    border-color: #f26b3e;
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.current .step {
    color: #f26b3e;
    border-color: #f26b3e;
  }

  .app-content .wizard>.actions>ul>li>a {
    background-color: #f26b3e;
    border-radius: .4285rem;
  }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb ml-1">
    <li class="breadcrumb-item"><a href="{{ url('Medicina-Pacientes') }}">Pacientes</a></li>
    <li class="breadcrumb-item active" aria-current="page">Historial Clinico de
      {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}</li>
  </ol>
</nav>


<div class="">
  <div class=" content-body">
    <section id="number-tabs">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header bg-secondary">
              <h4 class="card-title pb-2" style="color:#fff !important;">Estudios Programados</h4>
            </div>
            <div class="card-content collapse show" style="">
              <div class="col-md-4">
                @foreach ($estudios as $estudio)
                <div class="text-center shadow bg-white p-1  rounded">
                  <h5>{{ $estudio->estudios }}</h5>
                  <hr>
                  <small class="d-block">{{ $estudio->nombre }}</small>
                  <small class="d-block">{{ $estudio->nombre }}</small>
                  <hr>
                  <a type="button" target="_blank" href="../Resultados/256" class="btn-sm btn-secondary btn"
                    name="button">
                    <i class="feather icon-external-link"></i>
                  </a>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- inicio de formulario --}}
      @if ($paciente->empresa_id == 21 || $paciente->empresa_id == 8)
      <div class="card">
        <div class="card-content collapse show">
          <div class="card-header">
            <h5 class="card-title">Formatos</h5>
          </div>
          <div class="card-body">

            <select class="form-control" id="exampleFormControlSelect1" name="formatos" onChange="formatoOnChange(this)">
              <option value="">Selecciona un formato</option>
              <option value="admision_contratista">Admisión Contratistas</option>
            </select>
          </div>
        </div>
        <div class="col-12">
          <div class="card">
            @include('Laboratorio.Medicina.formatos.examenpreadmision')
          </div>
          <div id="admision_contratista" style="display:none;">
            @include('Laboratorio/Medicina/formatos/admisioncontratista')
          </div>
        </div>
      </div>

      @elseif($paciente->empresa_id==22)
      <div class="card">
        <div class="card-content collapse show">
          <div class="card-header">
            <h5 class="card-title">Formatos</h5>
          </div>
          <div class="card-body">

            <select class="form-control" id="exampleFormControlSelect1" name="formatos_fletera"
              onChange="formatoOnChange(this)">
              <option value="">Selecciona un formato</option>
              <option value="admision">Historial Clínica</option>
            </select>
          </div>
        </div>
      </div>
      @elseif($paciente->empresa_id==25)
      <div class="card">
        <div class="card-content collapse show">
          <div class="card-header">
            <h5 class="card-title">Formatos</h5>
          </div>
          <div class="card-body">

            <div id="cadena_custodia">
              @include('Laboratorio.Medicina.formatos.cadenacustodia')
            </div>
          </div>
        </div>
      </div>
      @endif

  </div>
  </section>
</div>
</div>
{{-- fin de formulario --}}

<!-- Modal -->
<div class="modal animated bounceInDown text-left" id="bounceInDown" tabindex="-1" role="dialog"
  aria-labelledby="myModalLabel47" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel47">Resultados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-2">
            <div class="col-md-12" id="iframes">
              <a href="#" id="toolIframe">

              </a>
            </div>
          </div>
          <div class="col-md-10" id="contentIframe">
            <iframe width="" height="" class="col-12" style="display:none; height: 67vh;" id="iframeBg"></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="../app-assets/vendors/js/extensions/sweetalert.min.js"></script>
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<!-- BEGIN PAGE LEVEL JS-->
{{-- <script src="app-assets/js/scripts/forms/wizard-steps.js"></script> --}}
{{-- checkbox --}}
<script src="../app-assets/js/scripts/forms/checkbox-radio.js"></script>
<script src="../resources/js/Laboratorio/MedicinaPaciente.js"></script>
<script src="../app-assets/js/scripts/forms/select/form-selectize.js"></script>
<!-- END PAGE LEVEL JS-->
@endsection