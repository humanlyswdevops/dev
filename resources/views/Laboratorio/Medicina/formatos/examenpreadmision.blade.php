<div class="card">
    <div class="card-header bg-secondary">
      
        <h4 class="card-title text-white mb-1" id="titulo">Examen Médico</h4>
        <a class="float-right text-white mb-1" href="../Historial-Clinico-Pdf/{{$paciente->CURP}}" target="_blank">
          <i class="feather icon-printer "></i>
        </a>
        @if(count($errors) > 0)
         <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>Error en la validación</strong> <br>
          <ul>
           @foreach($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
          </ul>
         </div>
        @endif
    </div>
  
    <div class="card-content collapse show">
        <div class="card-body">
            <form id="form" method="post" class="number-tab-steps wizard-circle">
              @csrf
              <input type="hidden" name="formulario" value="examenpreadmision">
              <input type="hidden" name="pacienteId" value="{{encrypt($paciente->id)}}">
              <input type="hidden" id="generoPaciente" name="generoPaciente" value="{{$paciente->genero}}">
              <input type="hidden" id="curp" name="curp" value="{{$paciente->CURP}}">
              
                 <!-- Step 1 -->
                <h6>Identificacón</h6>
                @include('Laboratorio.Medicina.includes.identificacion')
                <!-- Step 2 -->
                <h6>Heredofamiliares</h6>
                @include('Laboratorio.Medicina.includes.heredofamiliares')
                 <!-- Step 3 -->
                  <h6>Antecedentes personales no patológicos</h6>
                @include('Laboratorio.Medicina.includes.no_patologicos')
                <!-- Step 4 -->
                <h6>Antecedentes personales patológicos</h6>
                @include('Laboratorio.Medicina.includes.patologicos')
                <!-- Step 5 (damas) -->
                @if ($paciente->genero=='Femenino'||$paciente->genero=='FEMENINO')
                  <h6>Antecedentes Ginecoobstetricos</h6>
                @include('Laboratorio.Medicina.includes.ginecoobstetricos')
                @endif
                <!-- Step 6 -->
                <h6>Historial Laboral</h6>
                @include('Laboratorio.Medicina.includes.historia_laboral')
                
                 <!-- Step 7 -->
                  <h6>Examen Físico</h6>
                @include('Laboratorio.Medicina.includes.examen_fisico')
           
               <h6>Resultados de Aptitud</h6>
                @include('Laboratorio.Medicina.includes.resultados')
            </form>
        </div>
    </div>
</div>