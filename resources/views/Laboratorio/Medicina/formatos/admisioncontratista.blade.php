<div class="card-header bg-secondary">
  <h4 class="card-title text-white">Admisión Contratistas</h4>
  @if(count($errors) > 0)
   <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Error en la validación</strong> <br>
    <ul>
     @foreach($errors->all() as $error)
     <li>{{ $error }}</li>
     @endforeach
    </ul>
   </div>
  @endif

</div>

<div class="card-content collapse show">
  <div class="card-body">
      <form id="form" method="post" class="admision_contratista wizard-circle">
        @csrf
        <input type="hidden" name="pacienteId" value="{{encrypt($paciente->id)}}">
        <input type="hidden" id="generoPaciente" name="generoPaciente" value="{{$paciente->genero}}">
         
          <h6>Interrogatorio y exploración médica</h6>
          @include('Laboratorio.Medicina.includes.interrogatorio')
         <h6>Resultados de Aptitud</h6>
          @include('Laboratorio.Medicina.includes.resultadoscontratistas')
      </form>
  </div>
</div>