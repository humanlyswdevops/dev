<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Humanly Software">
    <title>Empresas</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon" href="https://has.humanly-sw.com/public/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/vendors.min.css') !!} ">

    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css"
        href="{!! asset('public/vuexy/app-assets/css/bootstrap-extended.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/colors.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/components.min.css') !!}">
    <link rel="stylesheet" type="text/css"
        href="{!! asset('public/vuexy/app-assets/css/themes/dark-layout.min.css') !!}">
    <link rel="stylesheet" type="text/css"
        href="{!! asset('public/vuexy/app-assets/css/themes/semi-dark-layout.min.css') !!}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
        href="{!! asset('public/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.min.css') !!} ">
    <link rel="stylesheet" type="text/css"
        href="{!! asset('public/vuexy/app-assets/css/core/colors/palette-gradient.min.css') !!} ">

    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/assets/css/style.css') !!}">

    <!-- END: Custom CSS-->

</head>

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static menu-content menu-collapsed"
    data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99"
            style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <nav
        class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow bg-secondary">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                                    class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                        class="ficon feather icon-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            <li class="nav-item d-none d-lg-block text-white">
                                Asesores Diagnóstico Clínico
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('lab-menu') }}">
                        <img src="{!! asset('public/logo.png') !!}" width="45" alt="">
                        <h2 class="brand-text secondary mb-0">HAS</h2>
                    </a>
                </li>
                <li class="nav-item nav-toggle toolbar_toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                        <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon secondary"
                            data-ticon="icon-disc"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
    </div>
    <div class="header-navbar-shadow"></div>
    <div id="appContentMainLayaout" class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <div class="content_all">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="card" id="empresas">
                                <div class="card-content collapse show">
                                    <div class="card-header">
                                        <h4 class="card-title">Empresas</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="fa fa-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            </ul>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-8 mb-1">
                                                <input type="text" class="form-control search input-sm"
                                                    placeholder="Buscar Empresa">
                                            </div>
                                            <div class="col-md-4 mb-1">
                                                <button class='btn btn-primary waves-effect waves-float waves-light'
                                                    id='btnagregar'>Agregar Empresa</button>
                                            </div>

                                            <div class='col-md-12'>
                                                <div class="row list">
                                                    @forelse ($empresas as $empresa)

                                                    <div class="col-lg-3 ml-md-auto mr-md-auto" style='border-radius: 10px;
                                                        box-shadow: 0 1px 24px 0 rgb(34 41 47 / 17%);'>
                                                        <div class="card">
                                                            <div class="card-content">
                                                                <div class="card-body">
                                                                    <div class='row'>
                                                                    <button 
                                                                    class="float-right btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light"
                                                                    onclick="eliminar({{ $empresa->id }})">
                                                                    <i class="feather icon-trash"></i>
                                                                    </button>
                                                                    <button 
                                                                    class="float-right btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light mr-1"
                                                                    onclick="cargadatos({{ $empresa->id }})">
                                                                    <i class="feather icon-edit"></i>
                                                                    </button>
                                                                   </div>
                                                                    <div class="user-avatar-section">
                                                                        
                                                                        
                                                                        <div class="d-flex align-items-center flex-column">



                                                                            @if ($empresa->logo != null)
                                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                                src="{!! asset('storage/app/empresas/' . $empresa->logo) !!} "
                                                                                height="110" width="110"
                                                                                alt="User avatar">
                                                                            @else
                                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                                src="{!! asset('storage/app/empresas/' . $empresa->logo) !!} "
                                                                                height="110" width="110"
                                                                                alt="User avatar">
                                                                            @endif
                                                                            <div
                                                                                class="user-info d-block text-center nombre">
                                                                                <h4 class='primary'>
                                                                                    {{ $empresa->nombre }}</h4>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <p
                                                                        class="card-text height-75  d-block text-center direccion">
                                                                        {{ $empresa->direccion }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer text-muted mt-0">

                                                                <a href="Medicina-Pacientes/{{ $empresa->id }}"  class="float-right btn btn-block btn-sm btn-secondary"></i>Pacientes</a>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    @empty
                                                    No hay ningun paciente registrado por el momento
                                                    @endforelse
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-1">
                                            <div class="d-flex align-items-center justify-content-center">
                                                <div id="anterior"></div>
                                                <ul class="pagination justify-content-center m-0"></ul>
                                                <div id="siguiente"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
    </div>
    <div id="agregarmodal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="bg-primary modal-header">
                    <h4 class="modal-title float-left">Nueva Empresa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" id="form_empresas">
                        <div class="form-row">
                            
                            <div class="form-group col-md-12">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" required>
                            </div>
                            <div class='form-group col-md-12'>
                                <input type="file" accept=".png,.svg,.jpg"
                                    class="custom-file-input" name="logo" id='logo' required>
                                <label for="archivo" class='custom-file-label'>Subir Logo</label>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="direccion">Dirección</label>
                                <input type="text" name="direccion" class="form-control" id="direccion">
                            </div>
                        </div>
                        <div class="button-guardar float-right">
                            <button type="submit" class="btn btn-sm btn-secondary" id="btn_empresa_modal">
                                <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true"
                                    style="display: none;"></span>
                                Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="editar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="bg-primary modal-header">
                    <h4 class="modal-title float-left">Editar Empresa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" id="form_edit_empresa">
                        <div class="form-row">
                            <input type="hidden" name="id_e" class="form-control" id="id_e" value='' required>
                            
                            <div class="form-group c ol-md-12">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre_e" class="form-control" id="nombre_e" value='' required>
                            </div>
                            <div class='form-group col-md-12'>
                                <input type="file" accept=".png,.svg,.jpg"
                                    class="custom-file-input" name="logo_e" id='logo_e' value=''>
                                <label for="archivo" class='custom-file-label'>Actualizar Logo</label>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="clave"> Dirección</label>
                                <input type="text" name="direccion_e" class="form-control" id="direccion_e" value=''>
                            </div>
                           
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-sm btn-primary" id="btnedit">
                                <span class="spinner-border  spinner-border-sm" role="status" aria-hidden="true"
                                    style="display: none;"></span>
                                Actualizar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

        <footer class="footer footer-static footer-light ">
            <p class="clearfix blue-grey lighten-2 mb-0"><span
                    class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT © 2020<a
                        class="text-bold-800 grey darken-2" href="" target="_blank">Health Administration
                        Solution.</a>Todos los derechos reservados.</span><span
                    class="float-md-right d-none d-md-block">Humanly Software</span>
                <button class="btn btn-primary btn-icon scroll-top waves-effect waves-light" type="button"
                    style="display: none;"><i class="feather icon-arrow-up"></i></button>
            </p>
        </footer>




        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="{!! asset('public/vuexy/app-assets/vendors/js/vendors.min.js') !!}"></script>

        <script src="{!! asset('public/vuexy/app-assets/js/core/app-menu.min.js') !!}"></script>
        <script src="{!! asset('public/vuexy/app-assets/js/core/app.min.js') !!}"></script>
        <script src="{!! asset('public/vuexy/app-assets/js/scripts/components.min.js') !!}"></script>
        <script src="{!! asset('public/vuexy/app-assets/js/scripts/customizer.min.js') !!}"></script>
        <script src="{!! asset('public/vuexy/app-assets/js/scripts/footer.min.js') !!}"></script>
        <script src="{!! asset('public/vuexy/app-assets/js/scripts/footer.min.js') !!}"></script>
        <script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js"></script>



        <script type="text/javascript">
            < script src =
                "https://asesoresconsultoreslabs.com/expedienteclinico/app-assets/vendors/js/charts/echarts/chart/pie.js" >
        </script>
        <script
            src="https://asesoresconsultoreslabs.com/expedienteclinico/app-assets/vendors/js/charts/echarts/chart/funnel.js">
        </script>

        </script>
        <script src="{!! asset('public/js/Laboratorio/audiometria/historialempresas.js') !!}"></script>
        <script type="text/javascript">
            
        var recepcion = "772a2af944f0cb295424bff440274ebf4d93f1d6c9e8a07eda8bee137612460c1511a05afe4d99c014e746be6619bb7188fc34e1dff81ad6b8c256ce77df30eb";
        var tecnicoRadiologo = "2628f3a9495fe3f5f753cb2d4462d555acd55968c906dcd2e7599e94ede4c7159bf422734d34b304eac66f8bc23345e45c5445c7d99dba616044792e6c788451";
        var medicoRadiologo = "b4a1977fb651e08c933f56e8b00908d6099d4177f037f6ec219fecd43ce7e29630d06105fae887675c8ce1879658d4b459911f5376e2b8e38ed40d0d8d6b14fe";
        var Medicina = "5c6cd82bec3b8f61b962e86ff3f47f26197570069857e63fb7009bd0629ff7ad9368fe20eb8bf2d2cc96982f582f0e02d7338cd3237a5f711fa1917128940a15";
        var LaboratorioS = "15";
        function sesionMedicina() {
             window.location.replace("auth/lab-login/" + Medicina);
        }
   
            </script>
</body>

</html>