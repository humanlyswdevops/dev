@extends('layouts.VuexyLaboratorio')

@section('title', 'Pacientes')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/medicinaPaci.css">
@endsection

@section('content')
<div class="title-content">
   <h1 class="title">Formularios disponibles</h1>
</div>
<div class="panel panel-default>
    <div class="panel-heading">
        <br>
    </div>
    <div class="panel-body">
        <div class="row content_card" id="admin_list">
            @foreach ($formularios as $item)
            <div class="col-md-4">
                <a class="card" href="{!! url('formularios/'.encrypt($registroEntrada->id).'/'.$item->id) !!}">
                    <div class="card-header">
                        <h6 class="card-title font-weight-normal">{!! $item->nombre !!}</h6>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <p class="small ">{!! $item->descripcion !!}</p>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('js_custom')
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>
@endsection
