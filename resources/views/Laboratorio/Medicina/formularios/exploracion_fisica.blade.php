@extends('layouts.VuexyLaboratorio')

@section('title')
Historial Clinico de {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}
@endsection

@section('begin_vendor_css')
    <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.default.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/select2.min.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/extensions/sweetalert.css") !!}">
    <!-- END VENDOR CSS-->
@endsection
@section('page_css')
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/animate/animate.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/selectize/selectize.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/checkboxes-radios.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/wizard.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/pickers/daterange/daterange.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset("css/Laboratorio/odontograma.css") !!}">
@endsection
@section('css_custom')
    <style>
        .app-content .wizard.wizard-circle > .steps > ul > li:before, .app-content .wizard.wizard-circle > .steps > ul > li:after{
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.done .step{
            border-color: #f26b3e;
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.current .step{
            color: #f26b3e;
            border-color: #f26b3e;
        }
        .app-content .wizard > .actions > ul > li > a{
            background-color: #f26b3e;
            border-radius: .4285rem;
        }
    </style>
@endsection
{{-- BEGIN body html --}}
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item"><a href="{{ url('Medicina-Pacientes') }}">Pacientes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Historial Clinico de {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}</li>
        </ol>
    </nav>
    {{-- inicio de formulario --}}
    <div class="">
        <div class="content-body"><!-- Form wizard with number tabs section start -->
            <section id="number-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <h4 class="card-title text-white">Examen Médico</h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="number-tab-steps wizard-circle">
                                        <h6>Generales</h6>
                                        <fieldset>
                                            <div id="generales">
                                                <form method="POST" id="form_0" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="nombre">Nombre:</label>
                                                            <input type="text" class="form-control" name="nombre" id="nombre" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->nombre !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="apellido_paterno">Apellido paterno:</label>
                                                            <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->apellido_paterno !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="apellido_materno">Apellido materno:</label>
                                                            <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->apellido_materno !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-5">
                                                        <div class="form-group">
                                                            <label for="puesto_solicitado">Puesto solicitado:</label>
                                                            <input type="text" class="form-control" name="puesto_solicitado" id="puesto_solicitado" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->puesto_solicitado !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-5">
                                                        <div class="form-group">
                                                            <label for="empresa">Empresa:</label>
                                                            <input type="text" class="form-control" name="empresa" id="empresa" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->empresa !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="edad">Edad:</label>
                                                            <input type="text" class="form-control" name="edad" id="edad" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->edad !!}">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Somatometría</h6>
                                        <fieldset>
                                            <div id="somatometria">
                                                <form method="POST" id="form_1" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="temperatura">Temperatura:</label>
                                                            <input type="text" class="form-control" name="temperatura" id="temperatura" value="{!! is_null($historial_clinico->somatometria_n) ?'': $historial_clinico->somatometria_n->temperatura !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="talla">Talla:</label>
                                                            <input type="text" class="form-control" name="talla" id="talla" value="{!! is_null($historial_clinico->somatometria_n) ?'': $historial_clinico->somatometria_n->talla !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="peso">Peso:</label>
                                                            <input type="text" class="form-control" name="peso" id="peso" value="{!! is_null($historial_clinico->somatometria_n) ?'': $historial_clinico->somatometria_n->peso !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="imc">IMC:</label>
                                                            <input type="text" class="form-control" name="imc" id="imc" value="{!! is_null($historial_clinico->somatometria_n) ?'': $historial_clinico->somatometria_n->imc !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="perimetro_abdominal">Perímetro abdominal:</label>
                                                            <input type="text" class="form-control" name="perimetro_abdominal" id="perimetro_abdominal" value="{!! is_null($historial_clinico->somatometria_n) ?'': $historial_clinico->somatometria_n->perimetro_abdominal !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="perimetro_toracico">Perímetro torácico:</label>
                                                            <input type="text" class="form-control" name="perimetro_toracico" id="perimetro_toracico" value="{!! is_null($historial_clinico->somatometria_n) ?'': $historial_clinico->somatometria_n->perimetro_toracico !!}">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Signos vitales</h6>
                                        <fieldset>
                                            <div id="signos_vitales">
                                                <form method="POST" id="form_2" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="ta">T.A.:</label>
                                                            <input type="text" class="form-control" name="ta" id="ta" value="{!! is_null($historial_clinico->signos_vitales_n) ?'': $historial_clinico->signos_vitales_n->ta !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="fc">F.C.:</label>
                                                            <input type="text" class="form-control" name="fc" id="fc" value="{!! is_null($historial_clinico->signos_vitales_n) ?'': $historial_clinico->signos_vitales_n->fc !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="fr">F.R.:</label>
                                                            <input type="text" class="form-control" name="fr" id="fr" value="{!! is_null($historial_clinico->signos_vitales_n) ?'': $historial_clinico->signos_vitales_n->fr !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="sat_o2">SAT O2:</label>
                                                            <input type="text" class="form-control" name="sat_o2" id="sat_o2" value="{!! is_null($historial_clinico->signos_vitales_n) ?'': $historial_clinico->signos_vitales_n->sat_o2 !!}">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Inspección general</h6>
                                        <fieldset>
                                            <div id="inspeccion_general">
                                                <form method="POST" id="form_3" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-6">
                                                        <label for="agudeza_visual">Agudeza visual:</label>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label for="o_d">O.D:</label>
                                                                    <input type="text" class="form-control" name="o_d" id="o_d" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->o_d !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label for="sl_d">S.L.:</label>
                                                                    <input type="text" class="form-control" name="sl_d" id="sl_d" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->sl_d !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label for="cl_d">C.L.:</label>
                                                                    <input type="text" class="form-control" name="cl_d" id="cl_d" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->cl_d !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label for="o_i">O.I:</label>
                                                                    <input type="text" class="form-control" name="o_i" id="o_i" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->o_i !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label for="sl_i">S.L.:</label>
                                                                    <input type="text" class="form-control" name="sl_i" id="sl_i" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->sl_i !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label for="cl_i">C.L.:</label>
                                                                    <input type="text" class="form-control" name="cl_i" id="cl_i" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->cl_i !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label for="vision_colores">Visión a los colores:</label>
                                                                    <input type="text" class="form-control" name="vision_colores" id="vision_colores" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->vision_colores !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="custom-control custom-switch mr-2 mb-1">
                                                            <label for="usa_lentes">¿Usa lentes?:</label>
                                                            <input type="checkbox" class="custom-control-input" id="usa_lentes" name="usa_lentes" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->usa_lentes == "on" ? "checked":"") !!}>
                                                            <label class="custom-control-label" for="usa_lentes">
                                                                <span class="switch-text-left">Si</span>
                                                                <span class="switch-text-right">No</span>
                                                            </label>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group">
                                                            <label for="causa">Causa:</label>
                                                            <input type="text" class="form-control" name="causa" id="causa" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->causa !!}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="reflejos_oculares">Reflejos oculares:</label>
                                                            <input type="text" class="form-control" name="reflejos_oculares" id="reflejos_oculares" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->reflejos_oculares !!}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="otros">Otros:</label>
                                                            <input type="text" class="form-control" name="otros" id="otros" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->otros !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label for="nariz">Nariz</label>
                                                                    <ul class="list-unstyled mb-0">
                                                                        <li class="d-inline-block mr-2">
                                                                            <fieldset>
                                                                                <div class="vs-radio-con vs-radio-primary">
                                                                                    <input type="radio" name="nariz" checked value="normal">
                                                                                    <span class="vs-radio">
                                                                                        <span class="vs-radio--border"></span>
                                                                                        <span class="vs-radio--circle"></span>
                                                                                    </span>
                                                                                    <span class="">Normal</span>
                                                                                </div>
                                                                            </fieldset>
                                                                        </li>
                                                                        <li class="d-inline-block mr-2">
                                                                            <fieldset>
                                                                                <div class="vs-radio-con vs-radio-primary">
                                                                                    <input type="radio" name="nariz" value="anormal">
                                                                                    <span class="vs-radio">
                                                                                        <span class="vs-radio--border"></span>
                                                                                        <span class="vs-radio--circle"></span>
                                                                                    </span>
                                                                                    <span class="">Anormal</span>
                                                                                </div>
                                                                            </fieldset>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Oídos:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Derecho:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Izquierdo:</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Agudeza auditiva:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="agudeza_auditiva_derecho" id="agudeza_auditiva_derecho" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->agudeza_auditiva_derecho !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="agudeza_auditiva_izquierdo" id="agudeza_auditiva_izquierdo" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->agudeza_auditiva_izquierdo !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Conductos auditivos:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="conductos_auditivos_derecho" id="conductos_auditivos_derecho" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->conductos_auditivos_derecho !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="conductos_auditivos_izquierdo" id="conductos_auditivos_izquierdo" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->conductos_auditivos_izquierdo !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Membrana timpánica:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="membrana_timpanica_derecho" id="membrana_timpanica_derecho" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->membrana_timpanica_derecho !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="membrana_timpanica_izquierdo" id="membrana_timpanica_izquierdo" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->membrana_timpanica_izquierdo !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group"></div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Normal:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Anormal:</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Cavidad oral:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="cavidad_oral_normal" id="cavidad_oral_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->cavidad_oral_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="cavidad_oral_anormal" id="cavidad_oral_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->cavidad_oral_anormal !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Orofaringe:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="orofaringe_normal" id="orofaringe_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->orofaringe_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="orofaringe_anormal" id="orofaringe_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->orofaringe_anormal !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Amigdalas:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="amigdalas_normal" id="amigdalas_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->amigdalas_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="amigdalas_anormal" id="amigdalas_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->amigdalas_anormal !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <label>Lengua:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="lengua_normal" id="lengua_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->lengua_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="lengua_anormal" id="lengua_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->lengua_anormal !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div class="col-3"></div>
                                                            <div class="col-2"><label for="">Normal</label></div>
                                                            <div class="col-2"><label for="">Anormal</label></div>
                                                            <div class="col-5"><label for="">Observaciones</label></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Cuello</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="cuello_normal" id="cuello_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->cuello_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="cuello_anormal" id="cuello_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->cuello_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="cuello_observaciones" id="cuello_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->cuello_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Toráx</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="torax_normal" id="torax_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->torax_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="torax_anormal" id="torax_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->torax_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="torax_observaciones" id="torax_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->torax_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Campos pulmonares</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="campos_pulmonares_normal" id="campos_pulmonares_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->campos_pulmonares_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="campos_pulmonares_anormal" id="campos_pulmonares_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->campos_pulmonares_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="campos_pulmonares_observaciones" id="campos_pulmonares_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->campos_pulmonares_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Movimientos respiratorios</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_respiratorios_normal" id="movimientos_respiratorios_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_respiratorios_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_respiratorios_anormal" id="movimientos_respiratorios_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_respiratorios_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_respiratorios_observaciones" id="movimientos_respiratorios_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_respiratorios_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Ruidos cardiacos</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="ruidos_cardiacos_normal" id="ruidos_cardiacos_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->ruidos_cardiacos_normal!!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="ruidos_cardiacos_anormal" id="ruidos_cardiacos_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->ruidos_cardiacos_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="ruidos_cardiacos_observaciones" id="ruidos_cardiacos_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->ruidos_cardiacos_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Abdomen</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="abdomen_normal" id="abdomen_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->abdomen_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="abdomen_anormal" id="abdomen_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->abdomen_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="abdomen_observaciones" id="abdomen_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->abdomen_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Inspección</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="inspeccion_normal" id="inspeccion_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->inspeccion_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="inspeccion_anormal" id="inspeccion_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->inspeccion_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="inspeccion_observaciones" id="inspeccion_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->inspeccion_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Ruidos peristálticos</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="ruidos_peristalticos_normal" id="ruidos_peristalticos_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->ruidos_peristalticos_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="ruidos_peristalticos_anormal" id="ruidos_peristalticos_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->ruidos_peristalticos_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="ruidos_peristalticos_observaciones" id="ruidos_peristalticos_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->ruidos_peristalticos_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="dolor_palpacion">Dolor a la palpación:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="dolor_palpacion" name="dolor_palpacion" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->dolor_palpacion == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="dolor_palpacion">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="dolor_palpacion_observaciones" id="dolor_palpacion_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->dolor_palpacion_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="palpacion">Palpación:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="palpacion" name="palpacion" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->palpacion == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="palpacion">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="colon">Colon:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="colon" name="colon" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->colon == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="colon">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="diastasis_rectos">Diástasis de rectos:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="diastasis_rectos" name="diastasis_rectos" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->diastasis_rectos == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="diastasis_rectos">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="higado">Hígado:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="higado" name="higado" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->higado == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="higado">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="vibices">Vibices:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="vibices" name="vibices" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->vibices == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="vibices">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="hernias">Hernias:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="hernias" name="hernias" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->hernias == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="hernias">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="bazo">Bazo:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="bazo" name="bazo" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->bazo == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="bazo">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="tumoracion">Tumoración:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="tumoracion" name="tumoracion" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->tumoracion == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="tumoracion">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="cicatriz">Cicatriz:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="cicatriz" name="cicatriz" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->cicatriz == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="cicatriz">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <label for="ganglios">Ganglios:</label>
                                                                    <input type="checkbox" class="custom-control-input" id="ganglios" name="ganglios" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->ganglios == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="ganglios">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="row mb-2">
                                                            <div class="col-3"><label for="">Miembros pélvicos</label></div>
                                                            <div class="col-2"><label for="">Normal</label></div>
                                                            <div class="col-2"><label for="">Anormal</label></div>
                                                            <div class="col-5"><label for="">Observaciones</label></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Movimientos</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_normal" id="movimientos_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_anormal" id="movimientos_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_observaciones" id="movimientos_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Marcha</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="marcha_normal" id="marcha_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->marcha_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="marcha_anormal" id="marcha_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->marcha_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="marcha_observaciones" id="marcha_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->marcha_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="acortamiento">¿Hay acortamiento?:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="acortamiento" name="acortamiento" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->acortamiento == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="acortamiento">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="acortamiento_observaciones" id="acortamiento_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->acortamiento_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="flogosis">Flogosis:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="flogosis" name="flogosis" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->flogosis == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="flogosis">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="flogosis_observaciones" id="flogosis_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->flogosis_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="ulceras">ulceras:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="ulceras" name="ulceras" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->ulceras == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="ulceras">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="ulceras_observaciones" id="ulceras_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->ulceras_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="reflejo_patelar">Reflejo patelar:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="reflejo_patelar" name="reflejo_patelar" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->reflejo_patelar == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="reflejo_patelar">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="reflejo_patelar_observaciones" id="reflejo_patelar_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->reflejo_patelar_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="varices">Varices:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="varices" name="varices" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->varices == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="varices">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="varices_observaciones" id="varices_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->varices_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="micosis">Micosis:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="micosis" name="micosis" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->micosis == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="micosis">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="micosis_observaciones" id="micosis_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->micosis_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="llenado_capilar">Llenado capilar:</label>
                                                            </div>
                                                            <div class="col-2 text-center">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                    <input type="checkbox" class="custom-control-input" id="llenado_capilar" name="llenado_capilar" {!! is_null($historial_clinico->inspeccion_general_n) ?'': ($historial_clinico->inspeccion_general_n->llenado_capilar == "on" ? "checked":"") !!}>
                                                                    <label class="custom-control-label" for="llenado_capilar">
                                                                        <span class="switch-text-left">Si</span>
                                                                        <span class="switch-text-right">No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-7">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="llenado_capilar_observaciones" id="llenado_capilar_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->llenado_capilar_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="row mb-2">
                                                            <div class="col-3"><label for="">Miembros torácicos</label></div>
                                                            <div class="col-2"><label for="">Normal</label></div>
                                                            <div class="col-2"><label for="">Anormal</label></div>
                                                            <div class="col-5"><label for="">Observaciones</label></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Movimientos</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_toracicos_normal" id="movimientos_toracicos_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_toracicos_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_toracicos_anormal" id="movimientos_toracicos_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_toracicos_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="movimientos_toracicos_observaciones" id="movimientos_toracicos_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->movimientos_toracicos_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Llenado capilar</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="llenado_capilar_toracico_normal" id="llenado_capilar_toracico_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->llenado_capilar_toracico_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="llenado_capilar_toracico_anormal" id="llenado_capilar_toracico_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->llenado_capilar_toracico_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="llenado_capilar_toracico_observaciones" id="llenado_capilar_toracico_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->llenado_capilar_toracico_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <label for="">Flogosis</label>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="flogosis_toracico_normal" id="flogosis_toracico_normal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->flogosis_toracico_normal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="flogosis_toracico_anormal" id="flogosis_toracico_anormal" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->flogosis_toracico_anormal !!}">
                                                                </div>
                                                            </div>
                                                            <div class="col-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="flogosis_toracico_observaciones" id="flogosis_toracico_observaciones" value="{!! is_null($historial_clinico->inspeccion_general_n) ?'': $historial_clinico->inspeccion_general_n->flogosis_toracico_observaciones !!}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Odontograma</h6>
                                        <fieldset>
                                            <div id="odontograma">
                                                <form method="POST" id="form_4" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-1 text-center" style="writing-mode: vertical-lr;transform: rotate(180deg);">
                                                        Derecha
                                                    </div>
                                                    <div class="col-10">
                                                        <div class="row">
                                                            <div class="col-12 text-center">
                                                                Superior
                                                            </div>
                                                            <div class="col-6 border border-1 pb-1 pt-1 border-black text-center" style="border-top: none !important; border-left: none !important">
                                                                <div class="justify-content-center list-group list-group-horizontal-sm">
                                                                    @for ($i = 1; $i < 9; $i++)
                                                                    @php
                                                                        $nombre = 'input_diente_'.$i;
                                                                        $estado_1 = $estado_2 = $estado_3 = $estado_4 = $estado_5 = '';
                                                                        $diente_1 = $diente_2 = $diente_3 = $diente_4 = $diente_5 = 'diente';
                                                                        $ausente = 'ausente';
                                                                        $corona = 'corona';
                                                                        $endodoncia = 'endodoncia';
                                                                        $implante = 'implante';
                                                                    @endphp
                                                                    @if(!is_null($historial_clinico->odontograma_n->$nombre))
                                                                        @foreach (json_decode($historial_clinico->odontograma_n->$nombre) as $key => $value)
                                                                            @if($key == 1)
                                                                                @php $diente_1 = $clases[$value]; $estado_1 = $value @endphp
                                                                            @elseif($key == 2)
                                                                                @php $diente_2 = $clases[$value]; $estado_2 = $value @endphp
                                                                            @elseif($key == 3)
                                                                                @php $diente_3 = $clases[$value]; $estado_3 = $value @endphp
                                                                            @elseif($key == 4)
                                                                                @php $diente_4 = $clases[$value]; $estado_4 = $value @endphp
                                                                            @elseif($key == 5)
                                                                                @php $diente_5 = $clases[$value]; $estado_5 = $value @endphp
                                                                            @elseif($key == 6 ||$key == 7)
                                                                                @php $ausente = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 8)
                                                                                @php $corona = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 9)
                                                                                @php $endodoncia = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 10)
                                                                                @php $implante = $clases[$value]; $estado = $key @endphp
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                    <div class="">
                                                                        <h6>{{ $i }}</h6>
                                                                        <svg height="60" class="" width="60"  data-ng-repeat="i in adultoArriva" id="diente_{!! $i !!}" data-id="{!! $i !!}">
                                                                            <polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="{!! $ausente !!}" />
                                                                            <polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="{!! $ausente !!}" />
                                                                            <circle cx="30" cy="30" r="16" estado="8" value="8" class="{!! $corona !!}"/>
                                                                            <circle cx="30" cy="30" r="20" estado="3" value="9" class="{!! $endodoncia !!}"/>
                                                                            <polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="{!! $implante !!}"/>
                                                                            <polygon points="10,10 50,10 40,20 20,20" estado="{!! $estado_1 !!}" value="1" class="{!! $diente_1 !!}" />
                                                                            <polygon points="50,10 50,50 40,40 40,20" estado="{!! $estado_2 !!}" value="2" class="{!! $diente_2 !!}" />
                                                                            <polygon points="50,50 10,50 20,40 40,40" estado="{!! $estado_3 !!}" value="3" class="{!! $diente_3 !!}" />
                                                                            <polygon points="10,50 20,40 20,20 10,10" estado="{!! $estado_4 !!}" value="4" class="{!! $diente_4 !!}" />
                                                                            <polygon points="20,20 40,20 40,40 20,40" estado="{!! $estado_5 !!}" value="5" class="{!! $diente_5 !!}" />
                                                                        </svg>
                                                                    </div>
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                            <div class="col-6 border border-1 pb-1 pt-1 border-black text-center" style="border-top: none !important; border-right: none !important">
                                                                <div class="justify-content-center list-group list-group-horizontal-sm">
                                                                    @for ($i = 9; $i < 17; $i++)
                                                                    @php
                                                                        $nombre = 'input_diente_'.$i;
                                                                        $estado_1 = $estado_2 = $estado_3 = $estado_4 = $estado_5 = '';
                                                                        $diente_1 = $diente_2 = $diente_3 = $diente_4 = $diente_5 = 'diente';
                                                                        $ausente = 'ausente';
                                                                        $corona = 'corona';
                                                                        $endodoncia = 'endodoncia';
                                                                        $implante = 'implante';
                                                                    @endphp
                                                                    @if(!is_null($historial_clinico->odontograma_n->$nombre))
                                                                        @foreach (json_decode($historial_clinico->odontograma_n->$nombre) as $key => $value)
                                                                            @if($key == 1)
                                                                                @php $diente_1 = $clases[$value]; $estado_1 = $value @endphp
                                                                            @elseif($key == 2)
                                                                                @php $diente_2 = $clases[$value]; $estado_2 = $value @endphp
                                                                            @elseif($key == 3)
                                                                                @php $diente_3 = $clases[$value]; $estado_3 = $value @endphp
                                                                            @elseif($key == 4)
                                                                                @php $diente_4 = $clases[$value]; $estado_4 = $value @endphp
                                                                            @elseif($key == 5)
                                                                                @php $diente_5 = $clases[$value]; $estado_5 = $value @endphp
                                                                            @elseif($key == 6 ||$key == 7)
                                                                                @php $ausente = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 8)
                                                                                @php $corona = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 9)
                                                                                @php $endodoncia = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 10)
                                                                                @php $implante = $clases[$value]; $estado = $key @endphp
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                    <div class="">
                                                                        <h6>{{ $i }}</h6>
                                                                        <svg height="60" class="" width="60"  data-ng-repeat="i in adultoArriva" id="diente_{!! $i !!}" data-id="{!! $i !!}">
                                                                            <polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="{!! $ausente !!}" />
                                                                            <polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="{!! $ausente !!}" />
                                                                            <circle cx="30" cy="30" r="16" estado="8" value="8" class="{!! $corona !!}"/>
                                                                            <circle cx="30" cy="30" r="20" estado="3" value="9" class="{!! $endodoncia !!}"/>
                                                                            <polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="{!! $implante !!}"/>
                                                                            <polygon points="10,10 50,10 40,20 20,20" estado="{!! $estado_1 !!}" value="1" class="{!! $diente_1 !!}" />
                                                                            <polygon points="50,10 50,50 40,40 40,20" estado="{!! $estado_2 !!}" value="2" class="{!! $diente_2 !!}" />
                                                                            <polygon points="50,50 10,50 20,40 40,40" estado="{!! $estado_3 !!}" value="3" class="{!! $diente_3 !!}" />
                                                                            <polygon points="10,50 20,40 20,20 10,10" estado="{!! $estado_4 !!}" value="4" class="{!! $diente_4 !!}" />
                                                                            <polygon points="20,20 40,20 40,40 20,40" estado="{!! $estado_5 !!}" value="5" class="{!! $diente_5 !!}" />
                                                                        </svg>
                                                                    </div>
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                            <div class="col-6 border border-1 pb-1 pt-1 border-black text-center" style="border-bottom: none !important; border-left: none !important">
                                                                <div class="justify-content-center list-group list-group-horizontal-sm">
                                                                    @for ($i = 17; $i < 25; $i++)
                                                                    @php
                                                                        $nombre = 'input_diente_'.$i;
                                                                        $estado_1 = $estado_2 = $estado_3 = $estado_4 = $estado_5 = '';
                                                                        $diente_1 = $diente_2 = $diente_3 = $diente_4 = $diente_5 = 'diente';
                                                                        $ausente = 'ausente';
                                                                        $corona = 'corona';
                                                                        $endodoncia = 'endodoncia';
                                                                        $implante = 'implante';
                                                                    @endphp
                                                                    @if(!is_null($historial_clinico->odontograma_n->$nombre))
                                                                        @foreach (json_decode($historial_clinico->odontograma_n->$nombre) as $key => $value)
                                                                            @if($key == 1)
                                                                                @php $diente_1 = $clases[$value]; $estado_1 = $value @endphp
                                                                            @elseif($key == 2)
                                                                                @php $diente_2 = $clases[$value]; $estado_2 = $value @endphp
                                                                            @elseif($key == 3)
                                                                                @php $diente_3 = $clases[$value]; $estado_3 = $value @endphp
                                                                            @elseif($key == 4)
                                                                                @php $diente_4 = $clases[$value]; $estado_4 = $value @endphp
                                                                            @elseif($key == 5)
                                                                                @php $diente_5 = $clases[$value]; $estado_5 = $value @endphp
                                                                            @elseif($key == 6 ||$key == 7)
                                                                                @php $ausente = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 8)
                                                                                @php $corona = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 9)
                                                                                @php $endodoncia = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 10)
                                                                                @php $implante = $clases[$value]; $estado = $key @endphp
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                    <div class="">
                                                                        <h6>{{ $i }}</h6>
                                                                        <svg height="60" class="" width="60"  data-ng-repeat="i in adultoArriva" id="diente_{!! $i !!}" data-id="{!! $i !!}">
                                                                            <polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="{!! $ausente !!}" />
                                                                            <polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="{!! $ausente !!}" />
                                                                            <circle cx="30" cy="30" r="16" estado="8" value="8" class="{!! $corona !!}"/>
                                                                            <circle cx="30" cy="30" r="20" estado="3" value="9" class="{!! $endodoncia !!}"/>
                                                                            <polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="{!! $implante !!}"/>
                                                                            <polygon points="10,10 50,10 40,20 20,20" estado="{!! $estado_1 !!}" value="1" class="{!! $diente_1 !!}" />
                                                                            <polygon points="50,10 50,50 40,40 40,20" estado="{!! $estado_2 !!}" value="2" class="{!! $diente_2 !!}" />
                                                                            <polygon points="50,50 10,50 20,40 40,40" estado="{!! $estado_3 !!}" value="3" class="{!! $diente_3 !!}" />
                                                                            <polygon points="10,50 20,40 20,20 10,10" estado="{!! $estado_4 !!}" value="4" class="{!! $diente_4 !!}" />
                                                                            <polygon points="20,20 40,20 40,40 20,40" estado="{!! $estado_5 !!}" value="5" class="{!! $diente_5 !!}" />
                                                                        </svg>
                                                                    </div>
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                            <div class="col-6 border border-1 pb-1 pt-1 border-black text-center" style="border-bottom: none !important; border-right: none !important">
                                                                <div class="justify-content-center list-group list-group-horizontal-sm">
                                                                    @for ($i = 25; $i < 33; $i++)
                                                                    @php
                                                                        $nombre = 'input_diente_'.$i;
                                                                        $estado_1 = $estado_2 = $estado_3 = $estado_4 = $estado_5 = '';
                                                                        $diente_1 = $diente_2 = $diente_3 = $diente_4 = $diente_5 = 'diente';
                                                                        $ausente = 'ausente';
                                                                        $corona = 'corona';
                                                                        $endodoncia = 'endodoncia';
                                                                        $implante = 'implante';
                                                                    @endphp
                                                                    @if(!is_null($historial_clinico->odontograma_n->$nombre))
                                                                        @foreach (json_decode($historial_clinico->odontograma_n->$nombre) as $key => $value)
                                                                            @if($key == 1)
                                                                                @php $diente_1 = $clases[$value]; $estado_1 = $value @endphp
                                                                            @elseif($key == 2)
                                                                                @php $diente_2 = $clases[$value]; $estado_2 = $value @endphp
                                                                            @elseif($key == 3)
                                                                                @php $diente_3 = $clases[$value]; $estado_3 = $value @endphp
                                                                            @elseif($key == 4)
                                                                                @php $diente_4 = $clases[$value]; $estado_4 = $value @endphp
                                                                            @elseif($key == 5)
                                                                                @php $diente_5 = $clases[$value]; $estado_5 = $value @endphp
                                                                            @elseif($key == 6 ||$key == 7)
                                                                                @php $ausente = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 8)
                                                                                @php $corona = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 9)
                                                                                @php $endodoncia = $clases[$value]; $estado = $key @endphp
                                                                            @elseif($key == 10)
                                                                                @php $implante = $clases[$value]; $estado = $key @endphp
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                    <div class="">
                                                                        <h6>{{ $i }}</h6>
                                                                        <svg height="60" class="" width="60"  data-ng-repeat="i in adultoArriva" id="diente_{!! $i !!}" data-id="{!! $i !!}">
                                                                            <polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="{!! $ausente !!}" />
                                                                            <polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="{!! $ausente !!}" />
                                                                            <circle cx="30" cy="30" r="16" estado="8" value="8" class="{!! $corona !!}"/>
                                                                            <circle cx="30" cy="30" r="20" estado="3" value="9" class="{!! $endodoncia !!}"/>
                                                                            <polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="{!! $implante !!}"/>
                                                                            <polygon points="10,10 50,10 40,20 20,20" estado="{!! $estado_1 !!}" value="1" class="{!! $diente_1 !!}" />
                                                                            <polygon points="50,10 50,50 40,40 40,20" estado="{!! $estado_2 !!}" value="2" class="{!! $diente_2 !!}" />
                                                                            <polygon points="50,50 10,50 20,40 40,40" estado="{!! $estado_3 !!}" value="3" class="{!! $diente_3 !!}" />
                                                                            <polygon points="10,50 20,40 20,20 10,10" estado="{!! $estado_4 !!}" value="4" class="{!! $diente_4 !!}" />
                                                                            <polygon points="20,20 40,20 40,40 20,40" estado="{!! $estado_5 !!}" value="5" class="{!! $diente_5 !!}" />
                                                                        </svg>
                                                                    </div>
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                            <div class="col-12 text-center">
                                                                Inferior
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1 text-center" style="writing-mode: vertical-lr;">
                                                        Izquierda
                                                    </div>
                                                    <div class="col-12 mt-3 mb-3">
                                                        <table align="center" class="table table-bordered w-100">
                                                            <tr class="text-center">
                                                                <th>Amalgama</th>
                                                                <th>Caries</th>
                                                                <th>Endodoncia</th>
                                                                <th>Ausente</th>
                                                                <th>Resina</th>
                                                                <th>Implante</th>
                                                                <th>Sellante</th>
                                                                <th>Corona</th>
                                                                <th>Normal</th>
                                                            </tr>
                                                                <td class="tdCustom"><center><button type="button" class="color colorActive text-white btn btn-icon waves-effect waves-light border-none" value="1" style="background-color:red;">Activo</button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="2" style="background-color:#5f0400;"></button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="3" style="background-color:orange;"></button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="4" style="background-color:tomato;"></button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="5" style="background-color:#CC6600;"></button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="6" style="background-color:#CC66CC;"></button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="7" style="background-color:green;"></button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="8" style="background-color:blue;"></button></center></td>
                                                                <td class="tdCustom"><center><button type="button" class="color text-white btn btn-icon waves-effect waves-light border-none" value="9" style="background-color:black;"></button></center></td>
                                                            <tr>
                                                        </table>
                                                        @for ($i = 1; $i < 33; $i++)
                                                        @php $nombre = 'input_diente_'.$i; @endphp
                                                        <input name="input_diente_{!! $i !!}" id="input_diente_{!! $i !!}" type="hidden" value="{{ $historial_clinico->odontograma_n->$nombre }}">
                                                        @endfor
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Conclusiones</h6>
                                        <fieldset>
                                            <div id="conclusiones">
                                                <form method="POST" id="form_5" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="transtorno_conducta">¿Observó algún trastorno de la conducta?</label>
                                                            <input type="text" class="form-control" name="transtorno_conducta" id="transtorno_conducta">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="incoherencia">¿Observó alguna incoherencia?</label>
                                                            <input type="text" class="form-control" name="incoherencia" id="incoherencia">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="transtorno_atencion">¿Observó alguna transtorno de la atención?</label>
                                                            <input type="text" class="form-control" name="transtorno_atencion" id="transtorno_atencion">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="laboratorios">Laboratorios</label>
                                                            <textarea class="form-control"  cols="30" rows="3" name="laboratorios" id="laboratorios"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="diagnosticos">Diagnostico(s)</label>
                                                            <textarea class="form-control"  cols="30" rows="3" name="diagnosticos" id="diagnosticos"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="recomendaciones">Recomendacione(s)</label>
                                                            <textarea class="form-control"  cols="30" rows="3" name="recomendaciones" id="recomendaciones"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="conclusiones">Concluisiones</label>
                                                            <ul class="list-unstyled mb-0">
                                                                <li class="d-inline-block mr-2">
                                                                    <fieldset>
                                                                        <div class="vs-radio-con vs-radio-primary">
                                                                            <input type="radio" name="coclusion" checked value="satifactorio">
                                                                            <span class="vs-radio">
                                                                                <span class="vs-radio--border"></span>
                                                                                <span class="vs-radio--circle"></span>
                                                                            </span>
                                                                            <span class="">Satisfactorio</span>
                                                                        </div>
                                                                    </fieldset>
                                                                </li>
                                                                <li class="d-inline-block mr-2">
                                                                    <fieldset>
                                                                        <div class="vs-radio-con vs-radio-primary">
                                                                            <input type="radio" name="coclusion" value="satisfactorio_condicionado">
                                                                            <span class="vs-radio">
                                                                                <span class="vs-radio--border"></span>
                                                                                <span class="vs-radio--circle"></span>
                                                                            </span>
                                                                            <span class="">Satisfactorio condicionado</span>
                                                                        </div>
                                                                    </fieldset>
                                                                </li>
                                                                <li class="d-inline-block mr-2">
                                                                    <fieldset>
                                                                        <div class="vs-radio-con vs-radio-primary">
                                                                            <input type="radio" name="coclusion" value="no_satisfactorio">
                                                                            <span class="vs-radio">
                                                                                <span class="vs-radio--border"></span>
                                                                                <span class="vs-radio--circle"></span>
                                                                            </span>
                                                                            <span class="">No satisfactorio</span>
                                                                        </div>
                                                                    </fieldset>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    {{-- fin de formulario --}}
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="{!! url("app-assets/vendors/js/extensions/sweetalert.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/extensions/jquery.steps.min.js") !!}"></script>
{{-- <script src="{!! url("app-assets/vendors/js/forms/select/selectize.min.js") !!}"></script> --}}
<script src="{!! url("app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/pickers/daterange/daterangepicker.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/validation/jquery.validate.min.js") !!}"></script>
<script src="{!! url("app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js") !!}"></script>

{{-- checkbox --}}
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/icheck/icheck.min.js") !!}"></script>
<!-- END PAGE VENDOR JS-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
@endsection


@section('js_custom')
<script src="{!! url("app-assets/js/scripts/forms/checkbox-radio.js") !!}"></script>
<script>
    $(".number-tab-steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        enableAllSteps: true,
        titleTemplate: '<span class="step">#index#</span> #title#',
        cssClass: 'wizard',
        labels: {
            finish: 'Guardar',
            previous: 'Anterior',
            next: 'Siguiente'
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: $("#form_"+currentIndex).serialize(),
                url: '../../save_formulario_3/'+currentIndex
            }).done((res) => {
                console.log(res);
            }).fail((err) => {
                console.log(err);
            });
            return true;
        },
        onFinished: function(event, currentIndex) {
            swal({
                title: "¿Seguro que desea terminar el historial clinico?",
                text: "Esta acción es irreversible",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Si, terminar",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
                }).then(isConfirm => {
                if (isConfirm) {
                    // let data = {
                    //     "historial_clinico_id":$("input[name='historial_clinico_id']").val()
                    // }
                    // $.ajax({
                    //     type: 'POST',
                    //     dataType: 'json',
                    //     data: data,
                    //     url: '../../save_formulario_3/finalizado'
                    // }).done((res) => {
                    //     console.log(res);
                    //     location.reload();
                    // }).fail((err) => {
                    //     console.log(err);
                    // });
                    swal("Exito", "Guardado con exito", "success");
                } else {
                    swal("Operacion Cancelada", "Puede continuar...", "error");
                }
            });
        }
    });
</script>

<script>
	var color = 1;

	$(document).ready(function(){

		$('polygon').on('click',function(){
			var desabilitado = $(this).attr('class');
			if (desabilitado != 'desabilitado' ) {
				pintar(color,this)
			}

		});

		$('.color').click(function(){
            $(".color").text('');
            $(".color").removeClass('colorActive')
            $(this).addClass('colorActive')
            setTimeout(() => {
                $(this).text('Activo');
            }, 200);
			color = $(this).attr('value');
		});

    });


    function quitarEspecial(objeto)
    {
        $(objeto).parent().find('polygon').each(function(){
            if ($(this).attr('value') >= 6 && $(this).attr('value') <= 7  ) {
                $(this).attr('class','ausente');
            }
            else if ($(this).attr('value') == 10) {
                $(this).attr('class','implante');
            }
        });
        $(objeto).parent().find('circle').each(function(){
            if ($(this).attr('value') == 8 ) {
                $(this).attr('class','corona');
            }
            else if ($(this).attr('value') == 9) {
                $(this).attr('class','endodoncia');
            }
        });
    }

    function limpiarLados(objeto)
    {
        $(objeto).parent().find('polygon').each(function(){
            if ($(this).attr('value') < 6 ) {
                $(this).attr({class:'diente',
                            estado:0});
            }
        });
    }

    function pintar(color,objeto){
        let valores = "";
        if (color == 1) {
            quitarEspecial(objeto);
            $(objeto).attr({class:'marcadoRojo marcado',
                            estado:color});		}
        else if(color == 2){
            quitarEspecial(objeto);
            $(objeto).attr({class:'marcadoCafe marcado',
                            estado:color});
        }
        else if(color == 3){
            limpiarLados(objeto);
            quitarEspecial(objeto);
            $(objeto).parent().find('.endodoncia').each(function(){
                $(this).attr({class:'marcadoNaranja marcado',
                            estado:color});
            });
        }
        else if(color == 4){
            limpiarLados(objeto);
            quitarEspecial(objeto);
            $(objeto).parent().find('.ausente').each(function(){
                $(this).attr({class:'marcadoTomate marcado',
                            estado:color});
            });

        }
        else if(color == 5){
            quitarEspecial(objeto);
            $(objeto).attr({class:'marcadoMarron marcado',
                            estado:color});

        }
        else if(color == 6){
            limpiarLados(objeto);
            quitarEspecial(objeto);
            $(objeto).parent().find('.implante').each(function(){
                $(this).attr({class:'marcadoMorado marcado',
                            estado:color});
            });
        }
        else if(color == 7){
            quitarEspecial(objeto);
            $(objeto).attr({class:'marcadoVerde marcado',
                            estado:color});
        }
        else if(color == 8){
            limpiarLados(objeto);
            quitarEspecial(objeto);
            $(objeto).parent().find('.corona').each(function(){
                $(this).attr({class:'marcadoAzul marcado',
                            estado:color});
            });
        }
        else if(color == 9){
            quitarEspecial(objeto);
            $(objeto).attr({class:'diente',
                            estado:color});
                            console.log(color);
        }

        let ele = $(objeto).parent();
        let array_aux = ele.find("*");
        let ele_id = ele.data('id');

        // estado
        // 1 = amalgama
        // 2 = caries
        // 3 = endodoncia
        // 4 = ausente
        // 5 = resina
        // 6 = implante
        // 7 = sellante
        // 8 = corona
// ---------------------------------
        // value
        // 1 = superior
        // 2 = derecho
        // 3 = inferior
        // 4 = izquierdo
        // 5 = centro

        valores = '{';
        $.map( array_aux, function( val, i ) {
            let elemento = $(val);
            let elemento_marcado = elemento.hasClass('marcado');
            let elemento_estado = elemento.attr('estado');
            let elemento_value = elemento.attr('value');

            if(elemento_marcado == true)
            {
                valores+= '"'+elemento_value+'":'+elemento_estado+",";
            }
        });
        valores = valores.substring(0, valores.length -1);
        if(valores.length != 0)
        {
            valores+="}";
        }
        $('#input_diente_'+ele_id).val(valores);
    }
</script>
@endsection
