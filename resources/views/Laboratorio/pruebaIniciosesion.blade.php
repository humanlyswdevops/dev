<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Prueba de inicio de sesión</title>
</head>
<body>
    <button onclick="sesionRecepcion()">Recepción</button>
    <button onclick="sesionTecnicoRadiologo()">Técnico radiólogo</button>
    <button onclick="sesionMedicoRadiologo()">Médico radiólogo</button>
    <button onclick="sesionMedicina()">Medicina</button>
    <button onclick="sesionLaboratorio()">Laboratorio</button>

    <!--Scripts-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('js/Laboratorio/audiometria/enlaceaudiometria.js') !!}"></script>
    
</body>

</html>
