@extends('layouts.Robust')
@section('title')
Paciente: {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}
Estudio: {{$estudio->nombre}}
@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/Laboratorio/menu_styles.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/Laboratorio/estudio.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/styleScroll.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/vendors/css/forms/icheck/custom.css">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/css/vendors.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/vendors/css/editors/summernote.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/vendors/css/editors/codemirror.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/vendors/css/editors/theme/monokai.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/css/plugins/loaders/loaders.min.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/css/core/colors/palette-loader.css">
<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/../app-assets/vendors/css/extensions/toastr.css">
<link rel="stylesheet" href="{!! asset('/') !!}/resources/sass/fontawesome/css/all.css">

<link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/public/css/Laboratorio/Style.css">
<!-- END VENDOR CSS-->
@endsection
{{-- BEGIN body html --}}
@section('content')
<div class="content_all">


  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('pacientes')}}">Pacientes</a>
      </li>
      <li class="breadcrumb-item"><a href="{{route('estudios_paciente',['id'=> encrypt($paciente->id)])}}">Estudios de {{ ucfirst(strtolower($paciente->nombre))}}</a>
      </li>
      <li class="breadcrumb-item active"><span>Estudio</span>
      </li>
    </ol>
  </nav>

  <!-- Summernote Click to edit start -->
  <section id="summernote-edit-save">
    <div class="row">
      <div class="col-12">
        <div class="card" id="infoEstudios">
          <div class="card-header" style="background:#6a76be; color:white">
            <h4 class="card-title text-white">Información de estudio</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="card-content collapse show" id="infoestudioscard">
            <div class="card-body">
              <span class="d-block mt-2 mb-1">
                <strong>Paciente: </strong>{{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}
              </span>
              <span class="d-block mb-1">
                <strong>Estudio: </strong>{{$estudio->nombre}}
              </span>
              <span class="d-block">
                <strong>Fecha: </strong>{{\Carbon\Carbon::parse($registro->updated_at)->diffForHumans()}}
              </span>
              <hr>
              <h4 class="card-title">Interpretación del estudio</h4>
              <div class="loader-wrapper" id="loaders">
                <div class="loader-container">
                  <div class="ball-clip-rotate-multiple loader-success">
                    <div></div>
                    <div></div>
                  </div>
                </div>
                <div class="loader-wrapper">
                  <div class="loader-container">
                    <div class="ball-clip-rotate-multiple loader-success">
                      <div></div>
                      <div></div>
                    </div>
                  </div>
                </div>
              </div>
              <form id="obs" method="POST">
                {{-- <form class="form-horizontal" action="{{route('oldds')}}" method="POST"> --}}
                @csrf
                <div class="form-group">
                  <button id="edit" class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" data-original-title="Editar Documento"><i class="fas fa-edit"></i> </button>
                  <button id="save" class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" data-original-title="Visualizar"><i class="icon-eye"></i> </button>
                  <button id="guardar" class="btn btn-success" type="submit" data-toggle="tooltip" data-placement="top" data-original-title="Guardar"><i class="fa fa-save"></i></button>
                  <button type="button" onclick="fixed()" style="display:none" id="fexed" class="btn btn-dark"><i class="fas fa-scroll"></i> Fijar documento</button>
                  <button type="button" onclick="version1()" id="v1" data-toggle="tooltip" data-placement="top" data-original-title="Version 1" class="btn btn-icon btn-outline-success float-right "><i id="vi" class="far fa-file-word text-success"></i></button>
                  <button type="button" onclick="version2()" id="v2" data-toggle="tooltip" data-placement="top" data-original-title="Version 2" class="btn btn-icon btn-outline-success float-right mr-1"><i id="va" class="far fa-file-word text-success"></i></button>
                  <button type="button" onclick="version3()" id="v3" data-toggle="tooltip" data-placement="top" data-original-title="Version 3" class="btn btn-icon btn-outline-success float-right mr-1"><i id="" class="far fa-file-word text-success"></i></button>
                </div>
                <div class="form-group">
                  <textarea name="interpretacion" id="html_text" hidden rows="8" cols="80"></textarea>
                  <input type="hidden" name="empleado_id" value="{{encrypt($paciente->id)}}">
                  <input type="hidden" name="registro_id" value="{{encrypt($registro->id)}}">
                  <input type="hidden" name="estudio_id" value="{{encrypt($estudio->id)}}">
                  <div class="summernote-edit" id="summernote">

                    {{-- <h1 style="text-align: center;"><span style="color: #3366ff;"><strong>Diagn&oacute;stico M&eacute;dico</strong></span></h1>
                      <p style="text-align: justify;">Este documento deber&aacute; ser llenado por el m&eacute;dico familiar o el m&eacute;dico de su elecci&oacute;n; ante cualquier informaci&oacute;n falsa que se proporcione, el AEL no tendr&aacute; ninguna responsabilidad legal. La informaci&oacute;n proporcionada es confidencial y s&oacute;lo el personal m&eacute;dico y de enfermer&iacute;a tienen acceso a &eacute;l. Cualquier padecimiento o enfermedad que presente el solicitante, no ser&aacute; motivo de rechazo en su ingreso al AEL. Para que este certificado sea v&aacute;lido, deber&aacute; contener todos los datos solicitados.</p>
                      <p style="text-align: justify;">&nbsp;</p>
                      <h4 style="padding-left: 120px;"><strong>Fecha:</strong></h4>
                      <h4 style="padding-left: 120px;"><strong>Nombre:</strong></h4>
                      <h4 style="padding-left: 120px;"><strong>Expediente AEL:</strong></h4>
                      <h4 style="padding-left: 120px;"><strong>Fecha de nacimiento:</strong></h4>
                      <h4 style="padding-left: 120px;"><strong>Sexo:</strong></h4>
                      <p style="padding-left: 120px;">&nbsp;</p>
                      <table style="height: 678px;" border="1px" width="632; ">
                      <tbody>
                      <tr>
                      <td style="width: 505px;">
                      <h4 style="text-align: center;"><strong>Historia Personal</strong></h4>
                      </td>
                      <td style="width: 52px;">
                      <p style="text-align: center;"><strong>S&iacute;</strong></p>
                      </td>
                      <td style="width: 53px;">
                      <p style="text-align: center;"><strong>No</strong></p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>1. &iquest;Ha presentado crisis epil&eacute;pticas?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>2. &iquest;Ha presentado crisis asm&aacute;ticas?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>3. &iquest;Usa lentes?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>4. &iquest;Fuma habitualmente?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>5. &iquest;Ingiere bebidas alcoh&oacute;licas habitualmente?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>6. &iquest;Ha estado hospitalizado durante el &uacute;ltimo a&ntilde;o?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>7. &iquest;Ha tenido una cirug&iacute;a, enfermedades o lesi&oacute;n m&eacute;dica seria?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>8. &iquest;Alguna vez se ha desmayado o ha perdido el conocimiento?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>9. &iquest;Es al&eacute;rgico a alg&uacute;n medicamento o alimento?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>10. &iquest;Est&aacute; bajo tratamiento m&eacute;dico?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>11. &iquest;Tiene alguna limitaci&oacute;n para su participaci&oacute;n deportiva?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      <tr>
                      <td style="width: 505px;">
                      <p>12. &iquest;Ha tenido limitaci&oacute;n m&eacute;dica para practicar alg&uacute;n deporte?</p>
                      </td>
                      <td style="width: 52px;">
                      <p>&nbsp;</p>
                      </td>
                      <td style="width: 53px;">
                      <p>&nbsp;</p>
                      </td>
                      </tr>
                      </tbody>
                      </table>
                      <p>&nbsp;</p>
                      <h4 style="padding-left: 90px;"><strong>Peso:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>Estatura:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>T.A:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>F.C:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>F.R:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>Extremidades:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>Coraz&oacute;n:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>Abdomen:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>Otros:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>Recomendaciones:</strong></h4>
                      <h4 style="padding-left: 90px;"><strong>Observaciones:</strong></h4>
                      <p>&nbsp;</p> --}}
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        {{-- IMAGENES DICOM --}}

        <div class="card text-white box-shadow-0 bg-info">
          <div class="card-header">
            <h4 class="card-title text-white">Imagenes Dicom</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>

              </ul>
            </div>
          </div>
          <div class="card-content collapse show">
            <div class="card-body d-flex">
              @forelse ($dicoms as $dicom)
              <a class="mx-auto" onclick="downloadAndView('{{$dicom->ruta}}')" download>
                <div class="card" data-toggle="tooltip" data-placement="top" data-original-title="Archivo Dicom">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="align-self-center text-center">
                          <i class="icon-picture info  d-block font-large-2 float-left"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
              @empty

              @endforelse
            </div>
          </div>
        </div>

        <div class="card" id="dicom">
          <div class="card-header">
            <h4 class="card-title ">Visualizar Imágen</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus black"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw black"></i></a></li>
                <li class="expand_"><a data-action="expand"><i class="ft-maximize black"></i></a></li>
                <li><a onclick="double()"><i class="far fa-eye black double"></i></a></li>
              </ul>
            </div>
            <div class="col s12" id="herramientas">
              <p class="escalas">
                <label>
                  <input type="checkbox" id="toggleVOILUT" />
                  <span class="check-escalas">Escala de grises</span>
                </label>


                <!-- no quitar !!!! -->
                <input type="checkbox" id="toggleModalityLUT" class="d-none" />

                <button onclick="gira_dicom(90)" id="noventaGrados" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                  <img src="https://img.icons8.com/ultraviolet/30/000000/90-degrees.png" />
                </button>
                <button onclick="gira_dicom(180)" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                  <img src="https://img.icons8.com/ultraviolet/30/000000/180-degrees.png" />
                </button>
                <button onclick="gira_dicom(270)" id="docientos_setenta" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                  <img src="https://img.icons8.com/ultraviolet/30/000000/270-degrees.png" />
                </button>
                <button onclick="gira_dicom(360)" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                  <img src="https://img.icons8.com/color/30/000000/360-view.png" />
                </button>



              </p>
            </div>
          </div>
          <div class="card-content collapse show">
            <div class="card-body">
              <div class="loader-wrapper" id="loadersDicom">
                <div class="loader-container">
                  <div class="ball-clip-rotate-multiple loader-success">
                    <div></div>
                    <div></div>
                  </div>
                </div>
                <div class="loader-wrapper">
                  <div class="loader-container">
                    <div class="ball-clip-rotate-multiple loader-success">
                      <div></div>
                      <div></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row" id="dicomview">
                <div class="col-md-12">
                  <div style="width:100%;height:702px;position:relative;color: white;display:inline-block;border-style: dashed; border-width: 1px; border-color:gray;" oncontextmenu="return false" class='disable-selection noIbar' unselectable='on' onselectstart='return false;' onmousedown='return false;'>
                    <div id="dicomImage" style="width:100%;height:100vh;top:0px;left:0px; position:absolute;border: 1px dashed #000;">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>





        <div class="card text-white box-shadow-0 bg-info">
          <div class="card-header">
            <h4 class="card-title text-white">Informacion de la imagen</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="card-content collapse show">
            <div class="card-body">
              <div class="row">
                <div class="col-md-6 col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Información del paciente</h4>
                      <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                        <ul class="list-inline mb-0">
                          <li><a data-action="collapse"><i class="ft-minus black"></i></a></li>
                          <li><a data-action="reload"><i class="ft-rotate-cw black"></i></a></li>
                        </ul>
                      </div>
                      <hr>
                    </div>
                    <div class="card-content collapse show">
                      <div class="card-body">
                        <p class="text-muted text-sm-left text-bold-600">
                          Nombre del paciente: <span class="text-bold-400 text-info" id="nombrepaciente"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          ID de paciente: <span class="text-bold-400 text-info" id="idpaciente"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Fecha de nacimiento del paciente: <span class="text-bold-400 text-info" id="nacimiento"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Sexo del paciente: <span class="text-bold-40 text-info" id="sexo"></span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Información de estudio</h4>
                      <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                        <ul class="list-inline mb-0">
                          <li><a data-action="collapse"><i class="ft-minus black"></i></a></li>
                          <li><a data-action="reload"><i class="ft-rotate-cw black"></i></a></li>
                        </ul>
                      </div>
                      <hr>
                    </div>
                    <div class="card-content collapse show">
                      <div class="card-body">
                        <p class="text-muted text-sm-left text-bold-600">
                          Descripción del estudio: <span class="text-bold-400 text-info" id="desestudio"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Nombre del protocolo: <span class="text-bold-400 text-info" id="nomprotocol"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Número de acceso: <span class="text-bold-400 text-info" id="numacceso"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Id del estudio: <span class="text-bold-400 text-info" id="idestudio"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Fecha de estudio: <span class="text-bold-400 text-info" id="fechaestudio"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Tiempo de estudio: <span class="text-bold-400 text-info" id="timestudio"></span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Información de la serie</h4>
                      <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                        <ul class="list-inline mb-0">
                          <li><a data-action="collapse"><i class="ft-minus black"></i></a></li>
                          <li><a data-action="reload"><i class="ft-rotate-cw black"></i></a></li>
                        </ul>
                      </div>
                      <hr>
                    </div>
                    <div class="card-content collapse show">
                      <div class="card-body">
                        <p class="text-muted text-sm-left text-bold-600">
                          Descripción de la serie: <span class="text-bold-400 text-info" id="descserie"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Serie #: <span class="text-bold-400 text-info" id="numserie"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Modalidad: <span class="text-bold-400text-info" id="modalidad"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Parte del cuerpo: <span class="text-bold-400 text-info" id="partecuerpo"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Fecha de estudio: <span class="text-bold-400 text-info" id="fechaestudio"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Tiempo de serie: <span class="text-bold-400 text-info" id="timserie"></span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Informacion del equipo</h4>
                      <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                        <ul class="list-inline mb-0">
                          <li><a data-action="collapse"><i class="ft-minus black"></i></a></li>
                          <li><a data-action="reload"><i class="ft-rotate-cw black"></i></a></li>
                        </ul>
                      </div>
                      <hr>
                    </div>
                    <div class="card-content collapse show">
                      <div class="card-body">
                        <p class="text-muted text-sm-left text-bold-600">
                          Fabricante: <span class="text-bold-400 text-info" id="fabricante"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Modelo: <span class="text-bold-400 text-info" id="modelo"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Nombre de la estación: <span class="text-bold-400 text-info" id="estacion"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Título AE: <span class="text-bold-400 text-info" id="tituloae"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Nombre de la Institución: <span class="text-bold-400 text-info" id="istitucion"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Versión de software: <span class="text-bold-400 text-info" id="software"></span>
                        </p>
                        <p class="text-muted text-sm-left text-bold-600">
                          Nombre de la versión de implementación: <span class="text-bold-400 text-info" id="implementacion"></span>
                        </p>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>


@endsection

@section('script')
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{!! asset('/') !!}/../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="{!! asset('/') !!}/../app-assets/vendors/js/editors/codemirror/lib/codemirror.js"></script>
<script src="{!! asset('/') !!}/../app-assets/vendors/js/editors/codemirror/mode/xml/xml.js"></script>
<script src="{!! asset('/') !!}/../app-assets/vendors/js/editors/summernote/summernote.js"></script>
<!-- include summernote css/js -->

<!-- END PAGE VENDOR JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{!! asset('/') !!}/../app-assets/js/scripts/editors/editor-summernote.js"></script>
<!-- END PAGE LEVEL JS-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
{{-- inicia --}}
<script src="https://asesoresconsultoreslabs.com/imagenologia/app-assets/js/scripts/gallery/photo-swipe/photoswipe-script.js" type="text/javascript"></script>
<script src="https://asesoresconsultoreslabs.com/imagenologia/app-assets/vendors/js/forms/tags/form-field.js" type="text/javascript"></script>
<script>
  window.cornerstoneWADOImageLoader || document.write('<script src="https://unpkg.com/cornerstone-wado-image-loader">\x3C/script>')
</script>
<script src="{{ asset('public/Laboratorio/librerias_dicom/librerias-dicom/cornerstone.min.js') }} "></script>
<SCRIPT src=" {{ asset(' public/Laboratorio/librerias_dicom/librerias-dicom/cornerstoneMath.min.js') }} "></SCRIPT>
<SCRIPT src=" {{ asset('public/Laboratorio/librerias_dicom/librerias-dicom/cornerstoneTools.min.js') }} "></SCRIPT>
<!-- include the dicomParser library as the WADO image loader depends on it -->
<script src="{{ asset('public/Laboratorio/librerias_dicom/librerias-dicom/dicomParser.min.js') }} "></script>
<!-- include the cornerstoneWADOImageLoader library -->

<script src="https://rawgit.com/cornerstonejs/cornerstoneWADOImageLoader/master/examples/utils/initializeWebWorkers.js"></script>
<script src="https://rawgit.com/cornerstonejs/cornerstoneWADOImageLoader/master/examples/dicomfile/uids.js"></script>
<!-- BEGIN Optional Codecs -->
<!-- OpenJPEG based jpeg 2000 codec -->
{{-- <script src="public/Laboratorio/librerias_dicom/librerias-dicom/openJPEG-FixedMemory.js"></script> --}}
<!-- PDF.js based jpeg 2000 codec -->
<!-- NOTE: do not load the OpenJPEG codec if you use this one -->
<!--<script src="js/jpx.min.js"></script>-->
<!-- JPEG-LS codec -->
{{-- <script src="public/Laboratorio/librerias_dicom/librerias-dicom/charLS-FixedMemory-browser.js"></script> --}}
<!-- JPEG Lossless codec -->
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/jpegLossless.js"></script>
<!-- JPEG Baseline codec -->
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/jpeg.js"></script>
<!-- Deflate transfer syntax codec -->
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/pako.min.js"></script>

<!-- END PAGE LEVEL JS-->
<script src="{!! asset('/') !!}/../app-assets/js/scripts/extensions/toastr.js"></script>
<script src="{!! asset('/') !!}/../app-assets/vendors/js/extensions/toastr.min.js"></script>
<script src="{!! asset('/') !!}/resources/js/Laboratorio/estudio.js"></script>


{{-- termina --}}



@endsection
{{-- END JS --}}
