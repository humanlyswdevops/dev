@extends('layouts.laboratorioApp')

@section('title', $empresa->nombre)

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../../../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/styleScroll.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/Laboratorio/empresaEmpleados.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/js/ajax/datatables/datas.css">
    <link rel="stylesheet" href="  https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

  <!--link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"-->



@endsection

@section('content')

    <main class="content">
        <section class="messages-alerts">
            @if(session('message'))
                <div class="alert alert-success" >
                {{session('message')}}
                </div>
            @elseif ($errors->any())
                <div class="alert alert-danger" >
                    <p>Ha ocurrido un error al programar los estudios</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </section>

        <div class="title-content">
      <h1 class="title">Información de la empresa </h1>
  </div>

  <div class="panel panel-default">
   <div class="panel-heading">
   </div>
   <div class="panel-body">
     <section class="principal-completo">
         {{--
             div class="titulo-empresa">
           <!--div class="titlEmp ">
               <h1>{{ $empresa->nombre }}</h1>
           </div-->
         </div>
         <div class="imaemp">
           <img src="{{route('getLogoEmpresa', ['filename'=>$empresa->logo])}}" alt="Logo de {{ $empresa->nombre }}" class="rounded-circle img-thumbnail shadow-sm img-sized"></a>
         </div
         --}}
              <div class="row align-items-center">
               <div class="col-5 imaemp ">
                   <img src="{{route('getLogoEmpresa', ['filename'=>$empresa->logo])}}" alt="Logo de {{ $empresa->nombre }}" class="rounded-circle img-thumbnail shadow-sm img-sized"></a>
               </div>
               <div class="titulo-empresa col-7">
                   <h1>{{ $empresa->nombre }}</h1>
               </div>
           </div>
           <br>

         <div class="container">
                 <div class="table-responsive">
                     <table id="tabla-empleados" class="table table-striped table-bordered "style="width:100%" >
                         <thead>
                             <tr>
                                 <th>Nombre</th>
                                 <th>Primer apellido</th>
                                 <th>Segundo apellido</th>
                                 <th>CURP</th>
                                 <th>&nbsp;</th>
                             </tr>
                         </thead>
                     </table>
             </div>
         </div>
     </section>
   </div>
  </div>

    </main>

@endsection

@section('specificScripts')
    <script src="../../../resources/js/Laboratorio/info_empresa.js"></script>
    <script> loadEmpleados('{{ $encryptedID }}');</script>
    <script src="../../../resources/js/ajax/datatables/datas.js"> </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

@endsection
