@extends('layouts.laboratorioApp')

@section('title', 'Pacientes')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CMuli:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="{!! asset('/') !!}/resources/sass/css/normalize.css">
    <link rel="stylesheet" href="{!! asset('/') !!}/resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/Laboratorio/ingresos_styles.css">
@endsection

@section('content')
    <main class="content">
      <nav class="" aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('Ingresos')}}" class="font-weight-light">Ingresos</a></li>
          <li class="breadcrumb-item active" aria-current="page" class="font-weight-light"><span class="font-weight-light">Estudios de {{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}</span></li>
        </ol>
      </nav>
      <div class="title-content">
        <h1 class="title">Estudios</h1>
    </div>
    <div class="panel panel-default">
     <div class="panel-heading">
     </div>
     <div class="panel-body">
       <div class="row content_card">
         @if (count($errors)>0)
           <div class="alert alert-danger ml-5">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Error en la validación</strong> <br>
            <ul>
             @foreach($errors->all() as $error)
             <li>{{ $error }}</li>
             @endforeach
            </ul>
           </div>
         @endif
         @if(session('success'))
           <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
             {{session('success')}}
           </div>
         @endif
          <div class="turnt content_card row">
            @forelse ($ingresos as $ingreso)
              <div class="col-md-4 cards_item" id="{{$ingreso->CURP}}" >
                <a class="card" href="#" onclick="estudioPrev('{{$ingreso->nombre}}','{{encrypt($ingreso->id)}}','{{encrypt($ingreso->ingreso)}}','{{$ingreso->CURP}}','{{encrypt($ingreso->estudios_id)}}','{{ $ingreso->estudio }}')" >
                  <i class="fas fa-x-ray"></i>
                  <h3 class="ml-3">{{ ucwords(strtolower($ingreso->nombre)).' '.ucwords(strtolower($ingreso->apellido_paterno)).' '.ucwords(strtolower($ingreso->apellido_materno))}}</h3>
                  <p class="small ml-3"> <strong>Estudio: {{$ingreso->estudio}}</strong> </p>
                  {{-- <p class="small ml-3">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                  <p class="small ml-3">Genero: {{$ingreso->genero}}</p>
                  <p class="small ml-3">Curp: {{$ingreso->CURP}}</p> --}}
                  <div class="go-corner" href="#">
                    <div class="go-arrow">
                      →
                    </div>
                  </div>
                </a>
              </div>
            @empty
              <p>Aún no hay pacientes ingresados</p>
            @endforelse
            <div class="aviso-vacio ml-5 pl-4" style="display: none;">
                <p>Ningun paciente coincide con la búsqueda.</p>
            </div>
          </div>
       </div>

     </div>
    </div>
</main>

<!-- Modal -->
<div id="estudios" class="modal fade" role="dialog" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title float-left" id="estudio_title"> </h6>
        <button type="button" class="close" data-dismiss="modal">×</button>

      </div>
      <div class="modal-body">
        <div id="load" class="mx-auto text-center">
            <img src="{!! asset('/') !!}/resources/sass/images/66.gif" class="d-block mx-auto" width="100%" alt="">
            <span class="d-block">Cargando...</span>
        </div>
        <div class="alert alert-success col-12" id="alert_success" role="alert">
            La imagen dicom se a subido correctamente
        </div>
        <div class="alert alert-danger col-12" id="alert" role="alert">
            Algo a ocurrido, verifica tu conexión o intentalo mas tarde...
        </div>
        {{-- <form action="{{route('dicom')}}" class="ocult col-12" i method="POST" enctype="multipart/form-data"> --}}
          <form  class="ocult col-12" id="formuploadajax" method="POST">
          @csrf
              <div class=" mb-2">
                <input type="url" class="form-control frmtext" placeholder="Url Archivo" name="dicom" id="img_dicom"  lang="es">
              </div>
              <fieldset class="form-group">
                  <input type="hidden" class="form-control" name="id" id="id_encrypt_2">
                  <input type="hidden"  name="estudio_id" id="estudio_id">
                <input type="text" class="form-control frmtext" placeholder="Nim del Empleado" name="nim" id="nim" required>
              </fieldset>
              <button type="submit" class="mx-auto btn btn-primary btn-personalizado">Subir archivo dicom</button>
        </form>
        <form class="col-12" id="realizado" style="display:none"  method="POST">
          <div class="alert alert-warning" role="alert">
          <h4 class="alert-heading  text-center">¡Aviso importante!</h4>
                <p>¿Seguro que deseas terminar con el estudio?</p>
          </div>
            @csrf
            <input type="hidden" class="form-control" name="id" id="id_encrypt">
            <input type="hidden" class="form-control" name="ingreso" id="ingreso_encrypt">
            <input type="hidden" class="form-control" name="curp" id="curp">
            <span class="label label-default"></span>
            <button type="submit" class="btn btn-outline-success btn-block ">Finalizar</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-success finalizar" onclick="finalizar()">Finalizar estudio</button>
        <button type="button" class="btn btn-info regresar btn-personalizado" style="display:none" onclick="regresar()">Regresar</button>
        <button type="button" class=" btn btn-outline-danger" id="cerrarModal" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
@endsection

@section('specificScripts')
 <script src="{!! asset('/') !!}/resources/js/Laboratorio/ingresos.js"></script>
@endsection
