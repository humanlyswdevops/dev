<link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
<title>CADENA DE CUSTODIA</title>
<style media="screen">
    p {
        font-size: 14px;
    }

    hr {
        border-top: 2px solid #002b46 !important;
    }

    .dash {
        border-top: 1px dashed #002b46 !important;
    }

    .alert-primary {
        background-color: #002b46 !important;
        color: #fff;
    }

    .copy {
        display: block;
        page-break-inside: avoid;
    }

    @media print {
        .copy {
            display: block;
            page-break-inside: avoid;
        }
    }

</style>

<div>
    <table width="100%">
        <tr>
            <td valign="top" width="100%">
                {{-- <img src="{{asset('images/meteor-logo.png')}}" alt="" width="150"/> --}}
                <img width="230" width="50" class="float-right"
                    src="https://has-humanly.com/empresa/seguromonterrey.png"
                    style="margin-top:-30px;margin-right:-40px;">
                {{-- <img width="100" class="mr-4" src="../storage/app/empresas/{{ \Session::get('empresa')->logo }}"> --}}

            </td><br><br><br>

        </tr>
    </table>
    

    <div class=" p-0 text-center" style="background-color:#000 !important;color:#fff !important;">
        <b>CADENA DE CUSTODIA</b>
    </div>

    <div class="mt-2 p-0 text-center" style="background-color:#000 !important;color:#fff !important;">
        <b>INFORMACIÓN DEL CLIENTE</b>
    </div>

    <label style="font-size:11px;"><b> Identificación de las muestras</b></label>

    <div style="border:1px solid #000;" class="pl-2 pr-2 pb-2 pt-3 mt-2" width="100%">
        <table width="100%">
            <tr>
                <td width="12%" style="font-size:11px;">
                    <small>
                        No.Folio Web Flow:
                    </small>
                </td>
                <td width="38%" style="border-bottom: 1px solid #000;font-size:11px;">
                    <small>
                        RESPUESTAS
                    </small>
                </td>
                <td width="5%" style="font-size:11px;">
                    <small>

                    </small>
                </td>

                <td width="5%" style="font-size:11px;">
                    <small>
                        <b>EDAD:</b>
                    </small>
                </td>
                <td width="20%" style="border-bottom: 1px solid #000;font-size:11px;">
                    <small>
                        23 años
                    </small>
                </td>
                <td width="20%" style="font-size:11px;">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="27%">
                    <small>
                        Carta compromiso: Anexar copia de la carta.
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        SI
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;text-aling:center;" class="text-center"
                    width="5%">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        NO
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;" class="text-center" width="5%">
                    <small>
                        X
                    </small>
                </td>
                <td width="14%">
                    <small>

                    </small>
                </td>

                <td style="font-size:10px;">
                    <small>
                        PLAN:VIDA
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;">
                    <small>
                        Respuesta
                    </small>
                </td>
                <td style="font-size:11px;">
                    <small>
                        GM
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;">
                    <small>
                        Respuesta
                    </small>
                </td>
                <td style="font-size:11px;">
                    <small>
                        GMRP
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;">
                    <small>
                        Respuesta
                    </small>
                </td>
                <td style="font-size:11px;">
                    <small>
                        REC.HUM.
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;">
                    <small>
                        Respuesta
                    </small>
                </td>
                <td width="20%">
                    <small>

                    </small>
                </td>

            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="35%">
                    <small>
                        Nombre completo de la persona a la que se le tomaron las muestras:
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;" width="49%">
                    <small>
                        yazmin carcao carcao
                    </small>
                </td>
                <td width="3%">

                    </small>
                </td>


            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="35%">
                    <small>
                        Firma de persona a la que se le tomaron las muestras:
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;" width="30%">
                    <small>

                    </small>
                </td>
                <td style="" width="10%">
                    <small>

                    </small>
                </td>

                <td style="font-size:11px;">
                    <small>
                        FUMADOR:SI
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;">
                    <small>
                        NO
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:11px;" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="" width="2%">
                    <small>

                    </small>
                </td>


            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="50%">
                    <small>
                        El solicitante se identifico con la <b>credencial oficial</b>(con fotografía)
                    </small>
                </td>
                <td style="font-size:11px;" width="12%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="50%">
                    <small>
                        <b>Tipo de muestras:</b>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="8%">
                    <small>
                        Expedida por:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="22%">
                    <small>
                        Dr.Alejandro
                    </small>
                </td>
                <td style="" width="14%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="11%">
                    <small>
                        Muestra de orina
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        SI
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        NO
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="" width="10%">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="4%">
                    <small>
                        Numero
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="22%">
                    <small>
                        123456
                    </small>
                </td>
                <td style="font-size:11px;" width="6%">
                    <small>
                        De Fecha
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="10%">
                    <small>
                        123456
                    </small>
                </td>
                <td style="" width="2%">
                    <small>

                    </small>
                </td>

                <td style="font-size:11px;" width="11%">
                    <small>
                        Muestra de sangre
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        SI
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        NO
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="" width="10%">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>

        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="50%">
                    <small>
                        <b>Información Adicional</b>
                    </small>
                </td>
                <td style="font-size:12px;" width="12%">
                    <small>

                    </small>
                </td>
                <td style="font-size:12px;" width="50%">
                    <small>
                        <b>DOCUMENTOS</b>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="4%">
                    <small>
                        Peso(kg)
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="7%">
                    <small>
                        23KG
                    </small>
                </td>
                <td style="" width="10%">

                </td>
                <td style="font-size:11px;" width="8%">
                    <small>
                        Horas de Ayuno
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="7%">
                    <small>
                        8 hrs
                    </small>
                </td>
                <td style="" width="7%">
                    <small>

                    </small>
                </td>

                <td style="font-size:11px;" width="11%">
                    <small>
                        Examen Medico
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        SI
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        NO
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="" width="10%">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="4%">
                    <small>
                        Talla(mt)
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="7%">
                    <small>
                        ch
                    </small>
                </td>
                <td style="" width="10%">

                </td>
                <td style="font-size:11px;" width="8%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="7%">
                    <small>

                    </small>
                </td>
                <td style="" width="7%">
                    <small>

                    </small>
                </td>

                <td style="font-size:11px;" width="11%">
                    <small>
                        Electrocardiograma
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        SI
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        NO
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="" width="10%">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="4%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="7%">
                    <small>

                    </small>
                </td>
                <td style="" width="10%">

                </td>
                <td style="font-size:11px;" width="8%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="7%">
                    <small>

                    </small>
                </td>
                <td style="" width="7%">
                    <small>

                    </small>
                </td>

                <td style="font-size:11px;" width="11%">
                    <small>
                        Papanicolau
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        SI
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        NO
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="" width="10%">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-1" width="100%">
        </div>
    </div>
    <div class="mt-2 p-0 text-center" style="background-color:#000 !important;color:#fff !important;">
        <b>INFORMACIÓN DEL PROVEEDOR MEDICO Ó LABORATORIO</b>
    </div>
    <div style="border:1px solid #000;" class="pl-2 pr-2 pb-3 pt-3 mt-2" width="100%">
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:14px;" width="40%">
                    <small>
                        <b>NOMBRE DEL PROVEEDOR Ó RAZÓN SOCIAL.</b>
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:15px;" width="35%">
                    <small>
                        Asesores Especializados en Laboratorios
                    </small>
                </td>
                <td style="" width="2%">
                    <small>

                    </small>
                </td>
                <td style="" width="5%">
                    <small>
                        <b> REGIONAL</b>
                    </small>
                </td>
                <td style="border-bottom: 1px solid #000;font-size:15px;" width="15%">
                    <small>
                        Puebla
                    </small>
                </td>
                <td style="font-size:15px;" width="8%">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="50%">
            <tr>
                <td style="font-size:11px;" width="50%">
                    <small>
                        Lugar de extracción de la muestra:
                    </small>
                </td>

            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="10%">
                    <small>
                        Laboratorio ( X )
                    </small>
                </td>
                <td style="font-size:11px;" width="18%">
                    <small>
                        Consultorio del médico ( X )
                    </small>
                </td>
                <td style="font-size:11px;" width="18%">
                    <small>
                        Domicilio del solicitante ( X )
                    </small>
                </td>
                <td style="font-size:11px;" width="16%">
                    <small>
                        Trabajo del cliente ( X )
                    </small>
                </td>
                <td style="font-size:11px;" width="2%">
                    <small>
                        Otro:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="5%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="28%">
                    <small>

                    </small>
                </td>

            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="70%">
            <tr>
                <td style="font-size:11px;" width="3%">
                    <small>
                        a la hora:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="2%">
                    <small>
                        5:00
                    </small>
                </td>
                <td style="font-size:11px;" width="3%">
                    <small>
                        del dia
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;" width="3%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="5%">
                    <small>
                        Cliente en ayuno
                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>
                        SI
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="2%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>
                        NO
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="2%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>

        <table width="100%">
            <tr>
                <td style="font-size:11px;" width="50%">
                    <small>
                        Nombre y firma del Médico y/o laboratorista que extrajo,envaso,etiqueto las muestras:
                    </small>
                </td>
                <td style="font-size:12px;" width="50%">
                    <small>
                        Yazmin carcamo carcamo
                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="30%">
            <tr>
                <td style="font-size:11px;" width="40%">
                    <small>
                        cedula profesional:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="50%">
                    <small>
                        12344567
                    </small>
                </td>
            </tr>
        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="80%">
            <tr>
                <td style="font-size:11px;" width="5%">
                    <small>
                        Se tomo <b>GLUCOSA</b> en la plaza
                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>
                        si
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>
                        Resultado
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>
                        no
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%" class="text-center">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="4%">
                    <small>
                        Muestra centrifugada
                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>
                        si
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" class="text-center" width="1%">
                    <small>
                        X
                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>
                        no
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" class="text-center" width="1%">
                    <small>
                        X
                    </small>
                </td>

            </tr>
        </table>

    </div>
    <div style="border:1px solid #000;" class="pl-2 pr-2 pb-3 pt-3 mt-2" width="100%">
        <table width="100%">
            <tr>
                <td style="font-size:14px;" width="40%">
                    <small>
                        <b>Envio de las muestras a la division.</b>
                    </small>
                </td>
                <td style="font-size:15px;" width="8%">
                    <small>

                    </small>
                </td>
            </tr>
        </table>

        <table width="80%">
            <tr>
                <td style="font-size:11px;" width="60%">
                    <small>
                        Firma y nombre del responsable Médico ó laboratorio que envia las muestras:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="40%">
                    <small>

                    </small>
                </td>
            </tr>

        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="70%">
            <tr>
                <td style="font-size:11px;" width="25%">
                    <small>
                        Realizado el envió a la hora:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="10%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="10%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="10%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;" width="10%">
                    <small>
                        ,del dia:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="10%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="10%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="10%">
                    <small>
                        /
                    </small>
                </td>
            </tr>
        </table>
    </div>
    <div class="mt-2 p-0 mb-2" style="font-size:14px;">
        <b>IMPORTANTE:La cadena de custodia debera de llenarse completamente para su envio</b>
    </div>

    <div style="border:1px solid #000;" class="" width=" 100%">
        <div class="p-0 text-center" style="background-color:#000 !important;color:#fff !important;">
            <b>LLENADO EXCLUSIVO POR EL LABORATORIO DE SMNLY.</b>
        </div>
        <table width="80%" class="pl-2 pr-2">
            <tr>
                <td style="font-size:11px;" width="60%">
                    <small>
                        <b>Nombre y Firma del responsable del laboratorio que recibe las muestras:</b>
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="40%">
                    <small>

                    </small>
                </td>
            </tr>

        </table>
        <div class="mt-2" width="100%">
        </div>
        <table width="70%" class="pl-2 pr-2">
            <tr>
                <td style="font-size:11px;" width="3%">
                    <small>
                        a la hora:
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="2%">
                    <small>
                        5:00
                    </small>
                </td>
                <td style="font-size:11px;" width="3%">
                    <small>
                        del dia
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;border-bottom: 1px solid #000;" width="1%">
                    <small>
                        /
                    </small>
                </td>
                <td style="font-size:11px;" width="3%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="5%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="2%" class="text-center">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="1%">
                    <small>

                    </small>
                </td>
                <td style="font-size:11px;" width="2%" class="text-center">
                    <small>

                    </small>
                </td>
            </tr>
        </table>
        <table width="100%" class="pl-2 pr-2">
            <tr>
                <td style="font-size:14px;" width="3%">
                    <small>
                        <b>DMPMF-10</b>
                    </small>
                </td>


            </tr>
        </table>

    </div>
</div>



<script src="{!! asset('js/bootstrap.min.js') !!}"></script>
