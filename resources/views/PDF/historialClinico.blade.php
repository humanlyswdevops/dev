<!DOCTYPE html>
<html>

<head>

    <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        .letra {
            font-size: 10px !important;
        }

        .fondo_titulos {
            background-color: #636363 !important;
            color: #fff !important;
        }
        .list_none{
          list-style:none;
        }

    </style>
</head>

<body>
    <div class="content letra">
        <div class="text-center mb-1">
            <h4 class="font-weight-normal" style="font-size:20px">EXAMEN MÉDICO DE PREADMISIÓN</h4>
        </div>
        <div id="identificacion">
            <div class="p-0 text-center fondo_titulos">
                <b> So.<br> IDENTIFICACION </b>
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>FECHA (Día/Mes/Año): {{ \Carbon::parse($identificacion->fecha)->format('d/m/Y') }}</td>
                        <td>N° de Empleado: {{ $identificacion->numEmpleado }}{{ $empleado->curp }}</td>
                        <td>Departamento: {{ $identificacion->departamento }}</td>
                        <td>Edad: {{ \Carbon::parse($empleado->fecha_nacimiento)->age }}</td>
                        <td>SEXO {{ $empleado->genero }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">APELLIDO PATERNO: {{ $empleado->apellido_paterno }}</td>
                        <td colspan="1">APELLIDO MATERNO: {{ $empleado->apellido_materno }}</td>
                        <td colspan="2">NOMBRE (S): {{ $empleado->nombre }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">ESTADO CIVIL: {{ $identificacion->estadoCivil }}</td>
                        <td colspan="2">ESCOLARIDAD: {{ $identificacion->escolaridad }}</td>
                    </tr>
                    <tr>
                        <td colspan="6">DOMICILIO: {{ $identificacion->domicilio }}</td>
                    </tr>
                    <tr>
                        <td colspan="2"> <span
                                class="">LUGAR DE NACIMIENTO: {{ $identificacion->lugarNacimiento }}</span> </td>
                        <td colspan="3">FECHA DE NACIMIENTO: <br> {{ $empleado->fecha_nacimiento }}</td>
                    </tr>
                    <tr>
                        <td>CIUDAD Ó EJIDO: {{ $identificacion->ciudad }}</td>
                        <td>MUNICIPIO: {{ $identificacion->municipio }}</td>
                        <td colspan="3">ESTADO: {{ $identificacion->estado }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">EN CASO DE EMERGENCIA LLAMAR A: {{ $identificacion->nom_per_em }}</td>
                        <td colspan="2">PARENTESCO: {{ $identificacion->parentesco }}</td>
                        <td colspan="2">TELEFONO: {{ $identificacion->telefono_1 }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">DOMICILIO: {{ $identificacion->domicilio_em }}</td>
                        <td colspan="2">LUGAR DE TRABAJO:</td>
                        <td>TELEFONO: {{ $identificacion->telefono_2 }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="heredofamiliares">
            <div class="text-center fondo_titulos" role="alert">
                ANTECEDENTES HEREDOFAMILIARES
            </div>
            <table class="table table-sm table-bordered">
              <tbody>
                  <tr>
                      <td>
                        <ul class="list_none">
                          <li>1.	HEPATITIS</li>
                          <li>2.	CANCER</li>
                          <li>3.	ASMA</li>
                          <li>4.	COLESTEROL ALTO</li>
                          <li>5.	EVC</li>
                          <li>6.	DIABETES</li>
                          <li>7.	OTROS</li>
                        </ul>
                      </td>
                      <td>
                        <ul class="list_none">
                          <li>8.	OBESIDAD</li>
                          <li>9.	SIDA</li>
                          <li>10.	TUBERCULOSIS</li>
                          <li>11.	SANGRADO</li>
                          <li>12.	ANEMIA</li>
                          <li>13.	ALCOHOLISMO</li>
                          
                        </ul>
                      </td>
                      <td>
                        <ul class="list_none">
                          <li>14.	ALTA PRESIÓN SANGUINEA</li>
                          <li>15.	ATAQUE AL CORAZÓN ANTES DE LOS 55 AÑOS</li>
                          <li>16.	DEFECTOS CONGÉNITOS</li>
                          <li>17.	PROBLEMAS EMOCIONALES/PSICOLÓGICOS</li>
                        </ul>
                      </td>
                      
                  </tr>
                  <tr>
                    <td colspan="5" style="font-size:7.5px !important;">POR FAVOR INDIQUE SI SU FAMILIA (PADRE, MADRE, HERMANOS (AS) ) TUVO ALGUNA DE LAS ENFERMEDADES ARRIBA MENCIONADAS, Coloca el numero en el renglón de enfermedad
                    </td>
                  </tr>
              </tbody>
          </table>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <th class="text-center fondo_titulos">Familiar</th>
                        <th class="text-center fondo_titulos">Estado</th>
                        <th class="text-center fondo_titulos">Enfermedades</th>
                    </tr>
                    @php
                        $heredofamilia = json_decode($heredofamiliar->data);
                    @endphp
                    @foreach ($heredofamilia as $familiar)
                        <tr>
                            <td>{{ $familiar->familiar }}</td>
                            <td>{{ $familiar->estado }}</td>
                            <td>
                                @if ($familiar->enfermedad != null)
                                    @foreach ($familiar->enfermedad as $value)
                                        {{ $value . ', ' }}
                                    @endforeach
                                @else
                                    Ninguno
                                @endif

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="nopatologico">
            @php
                $vacunas = $nopatologico->vacunas;
            @endphp
            <div class="text-center fondo_titulos">
              ANTECEDENTES PERSONALES NO PATOLÓGICOS
            </div>
            <p class="text-center">Vacunas Recibidas</p>
            <hr>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <th class="text-center">Vacuna</th>
                        <th class="text-center">Realizado</th>
                        <th class="text-center">Comentario</th>
                        <th class="text-center">Vacuna</th>
                        <th class="text-center">Realizado</th>
                        <th class="text-center">Comentario</th>
                    </tr>
                    <tr>
                        @foreach ($vacunas->tetano as $value)
                            @if ($value == 'No' || $value == 'no')
                                <td>Tetano</td>
                                <td>No</td>
                            @else
                                @if ($value == 'Tetano')
                                    <td>{{ $value }}</td>
                                    <td>Si</td>
                                @endif
                                @if ($value != 'no' && $value != 'Tetano' && $value != null)
                                    <td>{{ $value }}</td>
                                @endif
                                @if ($value == null)
                                    <td>Ninguno</td>
                                @endif
                            @endif
                           
                        @endforeach

                        @foreach ($vacunas->rubeola as $value)
                            @if ($value == 'no')
                                <td>Rubeola</td>
                                <td>No</td>
                            @else
                                @if ($value == 'Rubeola')
                                    <td>{{ $value }}</td>
                                    <td>Si</td>
                                @endif
                                @if ($value != 'no' && $value != 'Rubeola' && $value != null)
                                    <td>{{ $value }}</td>
                                @endif
                                @if ($value == null)
                                    <td>Ninguno</td>
                                @endif
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        @foreach ($vacunas->hepatitis as $value)
                            @if ($value == 'no')
                                <td>Hepatitis</td>
                                <td>No</td>
                            @else
                                @if ($value == 'Hepatitis')
                                    <td>{{ $value }}</td>
                                    <td>Si</td>
                                @endif
                                @if ($value != 'no' && $value != 'Hepatitis' && $value != null)
                                    <td>{{ $value }}</td>
                                @endif
                                @if ($value == null)
                                    <td>Ninguno</td>
                                @endif
                            @endif
                        @endforeach
                        @foreach ($vacunas->bcg as $value)
                            @if ($value == 'no')
                                <td>(BCG)</td>
                                <td>No</td>
                            @else
                                @if ($value == '(BCG)')
                                    <td>{{ $value }}</td>
                                    <td>Si</td>
                                @endif
                                @if ($value != 'no' && $value != '(BCG)' && $value != null)
                                    <td>{{ $value }}</td>
                                @endif
                                @if ($value == null)
                                    <td>Ninguno</td>
                                @endif
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        @foreach ($vacunas->influenza as $value)
                            @if ($value == 'no')
                                <td>Influenza</td>
                                <td>No</td>
                            @else
                                @if ($value == 'Influenza')
                                    <td>{{ $value }}</td>
                                    <td>Si</td>
                                @endif
                                @if ($value != 'no' && $value != 'Influenza' && $value != null)
                                    <td>{{ $value }}</td>
                                @endif
                                @if ($value == null)
                                    <td>Ninguno</td>
                                @endif
                            @endif
                        @endforeach
                        @foreach ($vacunas->neumococica as $value)
                            @if ($value == 'no')
                                <td>Neumococica</td>
                                <td>No</td>
                            @else
                                @if ($value == 'Neumococica')
                                    <td>{{ $value }}</td>
                                    <td>Si</td>
                                @endif
                                @if ($value != 'no' && $value != 'Neumococica' && $value != null)
                                    <td>{{ $value }}</td>
                                @endif
                                @if ($value == null)
                                    <td>Ninguno</td>
                                @endif
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td colspan="2">Otras Vacunas:</td>
                        <td colspan="3">{{ $vacunas->otros }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">Grupo Sanguinio: <br> {{ $nopatologico->g_sanguineo }}</td>
                        <td colspan="3">¿Alguna vez ha recibido usted alguna transfusión sanguinea?
                            {{ $nopatologico->transfusion }}</td>
                    </tr>
                    <tr>
                       <td colspan="6">Mencione los medicamentos que haya tomado en las últimas dos semanas:
                            <br>{{ $nopatologico->medica_ingeridos }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="page-break-after:always;"></div>
            <div class="text-center fondo_titulos">
                USO DEL CIGARRO
              </div>
            @php
                $cigarro = $nopatologico->cigarro;
                $alcohol = $nopatologico->alcohol;
                $drogas = $nopatologico->drogas;
                $du = $nopatologico->drogas_usadas;
                $deporte = $nopatologico->deporte;
                $dieta = $nopatologico->dieta;
                
            @endphp
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A)	¿FUMA USTED? {{ $cigarro->Cigarro }}</td>
                        <td>B)	¿DESDE QUE EDAD?<br> {{ $cigarro->edad }}</td>
                        <td>C)	¿NUMERO PROMEDIO DE CIGARROS QUE FUMA?<br> {{ $cigarro->frecuencia }}</td>
                        <td>D)	¿A QUE EDAD DEJÓ DE FUMAR?<br> {{ $cigarro->noConsumir }}</td>
                    </tr>
                </tbody>
            </table>
           
            <div class="text-center fondo_titulos">
                USO DEL ALCOHOL
              </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A)	¿TOMA BEBIDAS ALCOHÓLICAS?{{ $alcohol->Alcohol }}</td>
                        <td>B)	¿DESDE QUE EDAD? <br> {{ $alcohol->edad }}</td>
                        <td colspan="2">C)	¿CON QUE FRECUENCIA Y CANTIDAD? <br> {{ $alcohol->frecuencia }}</td>
                    </tr>
                </tbody>
            </table>
            <div class="text-center fondo_titulos">
                USO DE DROGAS
              </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A)	¿ALGUNA VEZ USÓ DROGAS? {{ $drogas->Drogas }}</td>
                        <td>B)	¿DESDE QUE EDAD? {{ $drogas->edad }}</td>
                        <td colspan="2">C)	¿CON QUE FRECUENCIA Y CANTIDAD? {{ $drogas->frecuencia }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">MENCIONE QUE DROGAS HA USADO:
                            @if ($du != null)
                                @foreach ($du as $value)
                                    {{ $value . ', ' }}
                                @endforeach
                            @else
                                Ninguna
                            @endif
                        </td>

                        <td>
                            ¿SE LE HA INDICADO ALGUNA DIETA? {{ $dieta->Dieta . ' ' . $dieta->descripcion }}
                        </td>
                        <td>
                            ¿PRACTICA USTED ALGUN DEPORTE? {{ $deporte->Deporte }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">ESPECIFIQUE CUAL Y FRECUENCIA: {{ $deporte->frecuencia }}</td>
                        <td colspan="2">INDIQUE CUÁL ES SU PASATIEMPO FAVORITO: {{ $nopatologico->pasatiempo }}</td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div id="patologico">
            @php
                $enfermedades = $patologico->enfermPadecidas;
                $cl = $patologico->cirugiasLesion;
            @endphp
            
            <div class="text-center fondo_titulos">
                ANTECEDENTES PATOLÓGICOS PERSONALES
              </div>
              <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>ENFERMEDADES PADECIDAS:</td>
                        <td colspan="2">
                            @if ($enfermedades == null)
                                Ninguna
                            @else
                                @foreach ($enfermedades as $value)
                                    {{ $value . ', ' }}
                                @endforeach
                            @endif
                        </td>
                    </tr>
                </tbody>
              </table>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th scope="col" class="fondo_titulos text-center letra">DESCRIPCIÓN</th>
                        <th scope="col" class="fondo_titulos text-center letra">CUÁL</th>
                        <th scope="col" class="fondo_titulos text-center letra">SI</th>
                        <th scope="col" class="fondo_titulos text-center letra">NO</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <td>TRANSTORNOS EMOCIONALES  O PSIQUIATRICOS, EJEMPLOS ANSIDEDAD, DEPRESIÓN</td>
                        <td>{{ $patologico->transtornos }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>ALERGIAS A MEDICAMENTOS: </td>
                        <td>{{ $patologico->alergiaMedica }}</td>
                        <td></td>
                        <td></td>
                        
                    </tr>
                    <tr>
                        <td>ALERGIAS EN LA PIEL O SENSIBILIDAD:</td>
                        <td> {{ $patologico->alergiaPiel }}</td>
                        <td></td>
                        <td></td>
                    
                    </tr>
                    <tr>
                        <td>OTRO TIPO DE ALERGIAS: </td>
                        <td> {{ $patologico->alergiaOtro }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>TUMORES O CANCER: </td>
                        <td> {{ $patologico->tumorCancer }}</td>
                        <td></td>
                        <td></td>
                        
                    </tr>
                    <tr>
                        <td>PROBLEMAS DE LA VISTA: </td>
                        <td> {{ $patologico->probVista }}</td>
                        <td></td>
                        <td></td>
                        
                    </tr>
                    <tr>
                       
                        <td>ENFERMEDAD DEL OIDO:</td>
                        <td>  {{ $patologico->enfOido }}</td>
                        <td></td>
                        <td></td>
                        
                    </tr>
                    <tr>
                        
                        <td>PROBLEMA EN COLUMNA VERTEBRAL: </td>
                        <td> {{ $patologico->probCulumVert }}</td>
                        <td></td>
                        <td></td>
                        
                    </tr>
                    <tr>
                      
                        <td>HUESOS Y ARTICULACIONES: </td>
                        <td> {{ $patologico->huesoArticulacion }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3">Otros problemas médicos no enlistados:</td>
                        <td colspan="3"> {{ $patologico->otroProbMedico }}</td>
                       
                       
                    </tr>
                </tbody>
            </table>
            <p class="text-center"><b>INDIQUE SI HA TENIDO ALGUNA DE LAS LESIONES O CIRUGIAS SIGUIENTES:</b></p>
            <hr>
            <table class="table table-sm table-bordered">
                <tbody>
                    @foreach ($cl as $json)
                        <tr>
                            @foreach ($json as $key => $value)
                                <td>{{ strtoupper($key) }}</td>
                                <td>{{ strtoupper($value) }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="1">OTRAS CIRUGÍAS INCLUYENDO ACCIDENTES AUTOMOVILÍSTICOS O DEL TRABAJO:</td>
                        <td colspan="3">{{ $patologico->otra_cirugia }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        @if ($empleado->genero == 'FEMENINO' || $empleado->genero == 'Femenino')
            <div id="genicoobstetrico">
                <div class="text-center fondo_titulos">
                    ANTECEDENTES GINECOOBSTETRICOS
                </div>
                    
                
                <table class="table table-sm table-bordered">
                    <tbody>
                        <tr>
                            <td>¿A QUÉ EDAD INICIÓ SU REGLA?</td>
                            <td>{{ $genicoobstetrico->inicioRegla }}</td>
                            <td>¿FRECUENCIA DE REGLA?</td>
                            <td>{{ $genicoobstetrico->fRegla }}</td>
                        </tr>
                        <tr>
                            <td>¿CUÁNTOS DÍAS DURA SU REGLA?</td>
                            <td>{{ $genicoobstetrico->duraRegla }}</td>
                            <td>¿EDAD QUE INICIÓ SUS RELACIONES SEXUALES?</td>
                            <td>{{ $genicoobstetrico->relacionSexInicio }}</td>
                        </tr>
                        <tr>
                            <td>¿CUÁL MÉTODO DE PLANIFICACIÓN HA USADO?</td>
                            <td>{{ $genicoobstetrico->metodoUsado }}</td>
                            <td>¿CUÁL FUE LA FECHA DE SU ÚLTIMA MENSTRUACION?</td>
                            <td>{{ $genicoobstetrico->ultimaMestruacion }}</td>
                        </tr>
                        <tr>
                            <td>¿CUÁNTOS EMBARAZOS HA TENIDO?</td>
                            <td>{{ $genicoobstetrico->numEmbarazos }}</td>
                            <td>¿CUANTOS PARTOS HA TENIDO?</td>
                            <td>{{ $genicoobstetrico->numPartos }}</td>
                        </tr>
                        <tr>
                            <td>¿CUÁNTAS CESÁREAS HA TENIDO?</td>
                            <td>{{ $genicoobstetrico->numCesareas }}</td>
                            <td>¿CUÁNTOS ABORTOS HA TENIDO?</td>
                            <td>{{ $genicoobstetrico->numAbortos }}</td>
                        </tr>
                        <tr>
                            <td>¿CUÁNDO SE REALIZÓ EL ÚLTIMO EXÁMEN DEL CÁNCER DE MATRIZ?</td>
                            <td>{{ $genicoobstetrico->fecha_examenCancer }}</td>
                            <td>  ¿CUÁNDO SE REALIZÓ EL ÚLTIMO EXÁMEN DE MAMAS?</td>
                            <td>{{ $genicoobstetrico->fecha_examenMamas }}</td>
                        </tr>
                        <tr>
                            <td>¿A QUÉ EDAD TUVO SU MENOPAUSIA?</td>
                            <td class="border-right">{{ $genicoobstetrico->edadMenopausia }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endif
        <div id="historialaboral">
            @php
                $hl = $historiaLaboral->trabajoRealizados;
                $tm = $historiaLaboral->trabajoMateriales;
                $empleos = $historiaLaboral->empleos;
                $equipo = $historiaLaboral->equipoSegActual;
                $vista = $historiaLaboral->examenVista;
                $lentes = $historiaLaboral->lentesContacto;
                $lugares = $historiaLaboral->lugaresVividos;
            @endphp
            <div class="text-center fondo_titulos">
                HISTORIAL LABORAL
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>
                            1.- HA REALIZADO UNO DE LOS SIGUIENTES TRABAJOS:
                        </td>
                        <td>
                            @if ($hl == null)
                                Ninguno
                            @else
                                @foreach ($hl as $value)
                                    {{ $value . ', ' }}
                                @endforeach
                            @endif
                        </td>
                        <td>Otros Trabajos Realizados:</td>
                        <td>{{ $historiaLaboral->otrosTrabajos }}</td>
                    </tr>
                    <tr>
                        <td>2.- ¿SE HA EXPUESTO O HA TRABAJADO CON ALGUNO DE ESTOS MATERIALES?</td>
                        <td>
                            @if ($tm == null)
                                Ninguno
                            @else
                                @foreach ($tm as $value)
                                    {{ $value . ', ' }}
                                @endforeach
                            @endif
                        </td>
                        <td>Otras exposiciones:</td>
                        <td>{{ $historiaLaboral->otrasExposiciones }}</td </tr>
                </tbody>
            </table>
            @foreach ($empleos as $empleo)
                <div class="text-center fondo_titulos">
                    @if ($empleo->tipo == 1)
                    EMPLEO ACTUACL
                    @elseif ($empleo->tipo==2)
                    EMPLEO ANTERIOR
                    @elseif ($empleo->tipo==3)
                    EMPLEO ANTERIOR 
                    @endif
                </div>
                <hr>
                <table class="table table-sm table-bordered">
                    <tbody>
                        <tr>
                            <td>NOMBRE DE LA EMPRESA: {{ $empleo->nombre }}</td>
                            <td>PUESTO: {{ $empleo->puesto }}</td>
                        </tr>
                        <tr>
                            <td>DESCRIPCIÓN DE SU ACTIVIDAD:{{ $empleo->actividad }}</td>
                            <td>DURACIÓN DEL EMPLEO: {{ $empleo->duracion }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">EQUIPO DE SEGURIDAD USADO: {{ $empleo->seguridad }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">HA SUFRIDO ENFERMEDADES RELACIONADAS AL TRABAJO:
                                {{ $empleo->danos }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">HA SUFRIDO ACCIDENTE DE TRABAJO QUE GENERARA INCAPACIDAD O SECUELAS
                                {{ $empleo->danos }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            @endforeach
            <div class="text-center fondo_titulos">
                EQUIPO DE SEGURIDAD USADO EN EL PUESTO ACTUAL
            </div>
            
            <hr>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td class="border-0"></td>
                        <td class="border-0"></td>
                        <td class="border-0"></td>
                        <td class="border-0"></td>
                    </tr>
                    <tr>
                        <td>Equipo:</td>
                        <td colspan="3">
                            @if ($equipo == null)
                                Ninguno
                            @else
                                @foreach ($equipo as $value)
                                    {{ $value . ', ' }}
                                @endforeach
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>¿USA LENTES GRADUADOS? {{ $vista[0] }}</td>
                        <td>FECHA DEL ÚLTIMO EXÁMEN VISUAL: {{ $vista[1] }}</td>
                        @if (count($lentes) == 1)
                            <td>¿USA LENTES DE CONTACTO? {{ $lentes[0] }}</td>
                            <td>¿DE QUE TIPO?</td>
                        @else
                            <td>¿USA LENTES DE CONTACTO? {{ $lentes[0] }}</td>
                            <td>¿DE QUE TIPO? {{ $lentes[1] }}</td>
                        @endif
                    </tr>
                    <tr>
                        <td colspan="2">OTRAS EXPOSICIONES: {{ $historiaLaboral->otras_exposiciones }}</td>
                        <td colspan="2">¿ALGUIEN DE SU FAMILIA TRABAJA CON MATERIALES PELIGROSOS? (ASBESTOS, PLOMO, ETC.)
                            {{ $historiaLaboral->famMaterialPeligroso }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">ALGUNA VEZ HA VIVIDO CERCA DE:FABRICAS, BASURERO, MINA, OTRO LUGAR QUE GENERE RESIDUOS PELIGROSOS
                            @if ($historiaLaboral->lugaresVividos == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->lugaresVividos as $value)
                                    {{ $value . ', ' }}
                                @endforeach

                            @endif
                        </td>
                        <td colspan="2">ALGUNA OTRA EXPOSICIÓN A MATERIALES PELIGROSOS:
                            {{ $historiaLaboral->expMaterialPeligroso }}
                        </td>
                    </tr>
                </tbody>
            </table>
           
            <div class="text-center fondo_titulos">
                INTERROGATORIO POR APARATOS Y SISTEMAS
            </div>
            <hr>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr class="text-center">
                        <td colspan="4">¿DURANTE EL AÑO PASADO TUVO USTED ALGUNO DE LOS SIGUIENTES SÍNTOMAS?</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td>Síntomas</td>
                        <td></td>
                        <td>Síntomas</td>
                    </tr>
                    <tr>
                        <td>NEUROLÓGICO / PSICOLÓGICO</td>
                        <td>
                            @if ($historiaLaboral->neurologico == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->neurologico as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                        <td>CARDIOVASCULAR</td>
                        <td>
                            @if ($historiaLaboral->cardiovasucular == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->cardiovasucular as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>GASTRO INTESTINAL</td>
                        <td>
                            @if ($historiaLaboral->gastroIntestinal == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->gastroIntestinal as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                        <td>PULMONAR</td>
                        <td>
                            @if ($historiaLaboral->pulmunar == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->pulmunar as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>GENITOURINARIO</td>
                        <td>
                            @if ($historiaLaboral->genitourinario == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->genitourinario as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                        <td>ENDOCRINO</td>
                        <td>
                            @if ($historiaLaboral->endocrino == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->endocrino as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>MUSCOLOESQUELETICO</td>
                        <td>
                            @if ($historiaLaboral->musculoesqueletico == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->musculoesqueletico as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                        <td>INMUNOLÓGICO</td>
                        <td>
                            @if ($historiaLaboral->inmunologico == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->inmunologico as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>DERMATOLÓGICO</td>
                        <td>
                            @if ($historiaLaboral->dermatologico == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->dermatologico as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                        <td>HEMATOLÓGICO</td>
                        <td>
                            @if ($historiaLaboral->hematologico == null)
                                Ninguno
                            @else
                                @foreach ($historiaLaboral->hematologico as $value)
                                    {{ $value . ' ' }}
                                @endforeach
                            @endif
                        </td>
                    </tr>
                    {{-- <tr class="text-center">
                        <td colspan="4">Alergias</td>
                    </tr>
                    <tr>
                        <td colspan="2">Reacción alergica a medicamentos:
                            @if ($historiaLaboral->alergiaMedicamentos == 'no')
                                No
                            @else
                                Si
                            @endif
                        </td>
                        <td colspan="2">¿Algún problema de salud en este momento?
                            {{ $historiaLaboral->problemaSalud }}</td>
                    </tr> --}}
                    <tr class="text-center">
                        <td colspan="4"><b>REPRODUCTIVO</b></td>
                    </tr>
                    
                    @if ($empleado->genero == 'FEMENINO' || $empleado->genero == 'Femenino')
                        <tr>
                            <td>SE HA PRESENTADO LO SIGUIENTE:</td>
                            <td colspan="3">
                                @if ($historiaLaboral->problemaMujer == null)
                                    NINGUN PROBLEMA PRESENTADO
                                @else
                                    @foreach ($historiaLaboral->problemaMujer as $value)
                                        {{ $value . ' ' }}
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">¿Ha tenido abortos?¿En que año? {{ $historiaLaboral->abortos }}</td>
                            <td colspan="2">Si esta embarazada cual es la fecha probable de parto:
                                {{ $historiaLaboral->fechaParto }}</td>
                        </tr>
                    @else
                        <tr>
                            <td>SE HA PRESENTADO LO SIGUIENTE:</td>
                            <td>
                                @if ($historiaLaboral->problemaHombre == null)
                                    NINGUN PROBLEMA PRESENTADO
                                @else
                                    @foreach ($historiaLaboral->problemaHombre as $value)
                                        {{ $value . ' ' }}
                                    @endforeach
                                @endif
                            </td>
                            <td colspan="2">OTROS PADECIMIENTOS: {{ $historiaLaboral->padecimientoHombre }}</td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
        <div id="examenFisico">
            <div class="text-center fondo_titulos">
                EXAMEN FÍSICO
            </div>
            <table class="table table-sm table-bordered">
                <tr>
                    <td colspan="2">APARIENCIA GENERAL: {{ $examen->apariencia }}</td>
                    <td>TEMPERATURA: {{ $examen->temperatura }}</td>
                    <td>FC: {{ $examen->fc }}</td>
                    <td>FR: {{ $examen->fr }}</td>
                </tr>
                <tr>
                    <td>ALTURA: {{ $examen->altura }}</td>
                    <td>PESO: {{ $examen->peso }}</td>
                    <td>IMC: {{ $examen->imc }}</td>
                    <td>PRESIÓN SANGUÍNEA EN REPOSO(BRAZO DERECHO):{{ $examen->psBrazoDerecho }}</td>
                    <td>BRAZO IZQUIERDO: {{ $examen->psBrazoDerecho }}</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <th>SIN DATOS PATOLOGICOS</th>
                    <th>OBSERVACIONES DETECTADAS </th>
                    <th colspan="2">INTERPRETACION DE ESTUDIOS DE GABINETE REALIZADOS
                        POR EL PERSONAL MÉDICO</th>
                </tr>
                <tr>
                    <td colspan="2">CABEZA, CUERO CABELLUDO / OJOS</td>
                    <td> </td>
                    <td>{{ $examen->cabezaCueroOjos }}</td>
                    <td colspan="2">AUDIOMETRIA</td>
                </tr>
                <tr>
                    <td colspan="2"> OIDOS / NARIZ / BOCA</td>
                    <td> </td>
                    <td>{{ $examen->oidoNarizBoca }}</td>
                    <td colspan="2">ESPIROMETRIA</td>
                </tr>
                <tr>
                    <td colspan="2"> DIENTES / FARINGE</td>
                    <td> </td>
                    <td>{{ $examen->dientesFaringe }}</td>
                    <td colspan="2">RADIOGRAFIA TORAX</td>
                </tr>
                <tr>
                    <td colspan="2">CUELLO</td>
                    <td></td>
                    <td>{{ $examen->cuello }}</td>
                    <td colspan="2">RADIOGRAFIA COLUMNA</td>
                </tr>
                <tr>
                    <td colspan="2">TIROIDES</td>
                    <td></td>
                    <td>{{ $examen->tiroides }}</td>
                    <td colspan="2">METABOLICO</td>
                </tr>
                <tr>
                    <td colspan="2">NODO LINFÁTICO</td>
                    <td></td>
                    <td>{{ $examen->tiroides }}</td>
                    <td colspan="2">METABOLICO</td>
                </tr>
                
                {{-- <tr>
                    <td colspan="2">Torak y Pulmones</td>
                    <td>{{ $examen->toraxPulmones }}</td>
                    <td>Pecho</td>
                    <td>{{ $examen->pecho }}</td>
                </tr> --}}
                <tr>
                    <td colspan="2">CORAZÓN</td>
                    <td></td>
                    <td>{{ $examen->corazon }}</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">ABDOMEN</td>
                    <td></td>
                    <td>{{ $examen->abdomen }}</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">GENITALES</td>
                    <td></td>
                    <td>{{ $examen->genitales }}</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">EXTREMIDADES</td>
                    <td></td>
                    <td>{{ $examen->extremidades }}</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">MUSCULOESQUELETO</td>
                    <td></td>
                    <td>{{ $examen->musculoesqueleto }}</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">REFLEJOS Y NEUROLÓGICOS</td>
                    <td></td>
                    <td>{{ $examen->reflejosNeurologicos }}</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">ESTADO MENTAL</td>
                    <td></td>
                    <td>{{ $examen->estadoMental }}</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">OTROS COMENTARIOS</td>
                    <td></td>
                    <td>{{ $examen->otrosComentarios }}</td>
                    <td colspan="2"></td>
                </tr>
                
                <tr class="text-center">
                    <td colspan="6">AGUDEZA VISUAL</td>
                </tr>
                <tr>
                    <td colspan="1">OJO IZQUIERDO</td>
                    <td colspan="2">{{ $examen->ojoIzquierdo }}</td>
                    <td colspan="1">OJO DERECHO</td>
                    <td colspan="2"> {{ $examen->ojoDerecho }}</td>
                </tr>
                <tr class="text-center">
                    <td colspan="6">RECOMENDACIONES DE ACUERDO A RESULTADOS</td>
                </tr>
                <tr>
                    <td colspan="3">USO DE EPP:</td>
                    <td>{{ $examen->uso_epp }}</td>
                    <td>AUDIOMETRIA</td>
                    <td>{{ $examen->audiometria }}</td>
                </tr>
                <tr>
                    <td colspan="3">RESPIRADORES</td>
                    <td>{{ $examen->respiradores }}</td>
                    <td>REENTRENAR EN:</td>
                    <td>{{ $examen->reentrenar }}</td>
                </tr>
                <tr>
                    <td colspan="3">OTROS:</td>
                    <td colspan="3">{{ $examen->otrasRecomend }}</td>
                </tr>
                <tr class="text-center">
                    <td colspan="6">RECOMENDACIONES DE ESTILO DE VIDA</td>
                </tr>
                <tr>

                    <td colspan="6">
                        @if ($examen->recomEstiloVida == null)
                            SIN RECOMENDACIONES
                        @else
                            @foreach ($examen->recomEstiloVida as $value)
                                {{ $value . ', ' }}
                            @endforeach
                        @endif
                    </td>
                </tr>
                {{-- <tr class="text-center">
                    <td colspan="5">Diagnostico Audiológico</td>
                </tr>
                <tr>
                    <td colspan="5">{{ $examen->diagnosticoAudio }}</td>
                </tr> --}}
            </table>
        </div>
        <div id="Resultados">
            {{-- <div class="alert alert-secondary text-center" role="alert">
                RESULTADOS
            </div> --}}
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>ESTADO DE SALUD:</td>
                        <td>{{ $resultados->estadoSalud }}</td>
                    </tr>
                    <tr>
                        <td>RESULTADOS DE APTITUD:</td>
                        <td>{{ $resultados->aptitud }}</td>
                    </tr>
                    <tr>
                        <td>COMENTARIOS: </td>
                        <td>{{ $resultados->comentarios }}</td>
                    </tr>
                </tbody>
            </table>
            
            
        </div>
        <div class="mt-5" width="100%">
        </div>
        <table class="pl-5 pr-5">
           
            <tbody>
                <tr> 
                    <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px #000;"></td>
                    <td class="text-center pl-5 pr-5"></td>
                    <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px #000;"></td>
                </tr>
                <tr> 
                    <td class="text-center">NOMBRE Y FIRMA DEL CANDIDATO</td>
                    <td class="text-center"></td>
                    <td class="text-center">NOMBRE Y FIRMA DEL CANDIDATO</td>
                </tr>
                <tr>
                    <td class="text-center" style="font-size:9px !important;">Doy consentimiento para realización de exámenes y <br>
                        laboratorios que la empresa considere necesarios para mi contratación.<br>
                        La información proporcionada es de manera voluntaria y en apego a la verdad</td>
                        <td class="text-center"></td>
                    <td class="text-center">CEDULA PROFESIONAL</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
<script src="{!! asset('js/bootstrap.min.js') !!}"></script>

</html>
