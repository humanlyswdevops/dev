
<link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
<title>{!! $paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno !!}</title>
<style media="screen">
  p{
    font-size:  14px;
  }
  hr{
     border-top: 2px solid #002b46!important;
  }
  .dash{
    border-top: 1px dashed #002b46!important;
  }
  .alert-primary{
    background-color: #002b46!important;
    color: #fff;
  }
  .copy {
      display: block;
      page-break-inside: avoid;
  }
  @media print {
    .copy {
        display: block;
        page-break-inside: avoid;
    }
}
</style>

<div>
    <table width="100%">
        <tr>
            <td valign="top" width="30%">
                {{-- <img src="{{asset('images/meteor-logo.png')}}" alt="" width="150"/> --}}
                <img width="100" class="mr-4" style="border-radius: 50px" src="{!! asset('storage/app/empresas/'.$empresa->logo) !!}">
                {{-- <img width="100" class="mr-4" src="../storage/app/empresas/{{ \Session::get('empresa')->logo }}"> --}}

            </td>
            <td>
                <div class="text-center ml-2">
                    <small></small>
                    <p class="display-6 mb-0 p-0">
                      {{  $user->nombre }}
                    </p>
                    <p class="display-6 mb-0 mt-0 p-0">{{ $user->espec }}</p>
                    <small> {{ $user->institucion }}</small><br>
                    <small>Cédula Profesional {{ $user->cedula }}</small>
                </div>

            </td>
            <td align="right">
                <small>{{ $empresa->nombre }}</small>
                <p class="mb-0 p-0">Procedimientos</p>
                <small class="mb-0 mt-0 p-0">Impreso por: {{  $user->nombre }}</small>
                <br>
                <small>Fecha: {{ \Carbon::now()->format('d-m-Y  g:i A') }}</small>
            </td>
        </tr>
    </table>
    <hr class="p-0 mt-1 mb-0">
    <table width="100%">
        <tr>
            <td valign="top" width="33%">
                <div class="">
                    <p class="p-0 m-0">Paciente:</p>
                    <small>
                      {{ $paciente->nombre }} {{ $paciente->apellido_paterno }} {{ $paciente->apellido_materno }}
                    </small>
                </div>
            </td>
            <td>
                <div class="text-center">
                    <p class="m-0 p-0">
                        Sexo:
                        <small>{{ $paciente->genero }}</small>
                    </p>

                </div>

            </td>
            <td align="right">
                <div class="">
                    <p class="m-0 p-0">
                        Fecha de Nacimiento:
                        <small>{{ $paciente->fecha_nacimiento }}</small>
                    </p>
                    <p class="p-0 m-0">
                        Fecha de Consulta
                        <small>{{ \Carbon::parse($json->created_at)->format('d-m-Y  g:i A') }}</small>
                    </p>
                </div>
            </td>
        </tr>

    </table>
    <hr class="p-0 mt-0 mb-2">
    <div class="alert m-0 p-0 alert-primary text-center">
        Procedimientos
    </div>
    @if ($procedimientos)
        @foreach ($procedimientos as $key => $value)
            <div class="col-md-12 mt-2 mb-1 pl-0 pr-0">
                <small class="p-0 m-0">{{ ($key+1).'.-'.$value->nombre }}</small>
                <br>
                {!! $value->comentario !!}
            </div>
        @endforeach
    @endif

    <div class="col-md-4 mt-5 pt-3 pl-0 pr-0 " width="30%">
        <div class="text-left">
            <hr class="m-0 p-0 " width="30%">
            {{  $user->nombre }}
        </div>
    </div>

    <footer>
        <hr>
        <table width="100%">
            <tr>
                <td width="33%">
                    <small>
                        {{ $empresa->direccion }}
                    </small>
                </td>
                <td width="33%" class="text-center">
                    <small class="text-center">
                        Telefono: {{ $user->telefono }}
                    </small>
                </td>
                <td width="33%">
                    <small>
                        Receta electrónica generada con Has
                    </small>
                </td>
            </tr>
        </table>
    </footer>
</div>
<hr class="dash" >
<div class="copy">
    <table width="100%">
        <tr>
            <td valign="top" width="30%">
                {{-- <img src="{{asset('images/meteor-logo.png')}}" alt="" width="150"/> --}}
                <img width="100" class="mr-4" style="border-radius: 50px" src="{!! asset('storage/app/empresas/'.$empresa->logo) !!}">
            </td>
            <td>
                <div class="text-center ml-2">
                    <small></small>
                    <p class="display-6 mb-0 p-0">
                      {{  $user->nombre }}
                    </p>
                    <p class="display-6 mb-0 mt-0 p-0">{{ $user->especialidad }}</p>
                    <small> {{ $user->institucion }}</small><br>
                    <small>Cédula Profesional {{ $user->cedula }}</small>
                </div>

            </td>
            <td align="right">
                <br />
                <small>{{ $empresa->nombre }}</small>
                <p class="mb-0 p-0">Procedimientos</p>
                <small class="mb-0 mt-0 p-0">Impreso por: {{  $user->nombre }}</small>
                <br>
                <small>Fecha: {{ \Carbon::now()->format('d-m-Y  g:i A') }}</small>
            </td>
        </tr>
    </table>
    <hr class="p-0 mt-1 mb-0">
    <table width="100%">
        <tr>
            <td valign="top" width="33%">
                <div class="">
                    <p class="p-0 m-0">Paciente:</p>
                    <small>
                      {{ $paciente->nombre }} {{ $paciente->apellido_paterno }} {{ $paciente->apellido_materno }}
                    </small>
                </div>
            </td>
            <td>
                <div class="text-center">
                    <p class="m-0 p-0">
                        Sexo:
                        <small>{{ $paciente->genero }}</small>
                    </p>

                </div>

            </td>
            <td align="right">
                <div class="">
                    <p class="m-0 p-0">
                        Fecha de Nacimiento:
                        <small>{{ $paciente->fecha_nacimiento }}</small>
                    </p>
                    <p class="p-0 m-0">
                        Fecha de Consulta
                        <small>{{ \Carbon::parse($json->created_at)->format('d-m-Y  g:i A') }}</small>
                    </p>
                </div>
            </td>
        </tr>

    </table>
    <hr class="p-0 mt-0 mb-2">
    <div class="alert m-0 p-0 alert-primary text-center">
        Procedimientos
    </div>
    @if ($procedimientos)
        @foreach ($procedimientos as  $key => $value)
            <div class="col-md-12 mt-2 mb-1 pl-0 pr-0">
                <small class="p-0 m-0">{{ ($key+1).'.-'.$value->nombre }}</small>
                <br>
                {!! $value->comentario !!}
            </div>
        @endforeach
    @endif

    <div class="col-md-4 mt-5 pt-3 pl-0 pr-0 " width="30%">
        <div class="text-left">
            <hr class="m-0 p-0 " width="30%">
            {{  $user->nombre }}
        </div>
    </div>

    <footer>
        <hr>
        <table width="100%">
            <tr>
                <td width="33%">
                    <small>
                        {{ $empresa->direccion }}
                    </small>
                </td>
                <td width="33%" class="text-center">
                    <small class="text-center">
                        Telefono: {{ $user->telefono }}
                    </small>
                </td>
                <td width="33%">
                    <small>
                        Receta electrónica generada con Has
                    </small>
                </td>
            </tr>
        </table>
    </footer>
</div>


<script src="{!! asset('js/bootstrap.min.js') !!}"></script>
