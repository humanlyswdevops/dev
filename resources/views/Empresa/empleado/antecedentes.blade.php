<section class="antecedentes col-md-4">

  <div class="card">
      <div class="card-header bg-secondary" style="padding: .6rem !important;">
        <h6 class="card-title font-weight-light  text-white">
          Historia Clinica
          
        </h6>
        @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Historial Clínico')['insert']))
        <a type="button" href="{!! url('Configuracion#historialClinico') !!}" data-toggle="popover" data-placement="top" data-content="Personaliza tu Historia Clinica" data-trigger="hover" data-original-title="Configuración" class="btn btn-icon btn-icon btn-sm rounded-circle btn-primary waves-effect waves-light text-white">
          <i class="feather icon-settings"></i>
        </a>
        
       
        @endif
      </div>
      <div class="card-content m-0 p-0 collapse show">
      @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Historial Clínico')['insert']))
        @foreach ($antecedentes as  $value)

        <div class="col-md-12 form_{{ $value->id }}" style="padding-left:8px !important;padding-right:8px !important">
          <div class="card-body p-0">
            <div class="accordion">
              <div class="collapse-margin">
                <div class="card-header  secondary">
                  <span class="lead collapse-title d-block" style="padding:5px">
                  @if ($value->nombre=='Antecedentes Ginecoobstetricos')
                    @if ( $empleado->genero=='Femenino')
                      {{ $value->nombre}} {{ $value->POLO }}
                    
                  </span>
                  <i data-id="{{ $value->id }}" data-nombre="{{$value->nombre}}" class="feather icon-edit float-right patologico"></i>
                  @if ( $value->user)
                  <small class="block" style="font-size: 10px;">Creado por: {{ $value->user->nombre }} </small>
                  @endif
                </div>
                <hr class="m-0">
                  <div >
                    <div class="card-body content_fields">

                      @php
                        $fields = \DB::table('antecedentes')
                        ->where('antecedenteF_id',$value->id)
                        ->where('id_expediente',$expediente_id)
                        ->get();
                      @endphp

                      @forelse ($fields as $field)

                        @php
                        $answer = \DB::table('antecedentesAnswer')
                        ->where('id',$field->answerA_id)
                        ->first();
                        @endphp

                        <div class="col-md-12 field_{{ $field->answerA_id }}">
                          <h6 class="primary">
                            <i class="feather icon-check"></i>
                            {{ $answer->pregunta }}
                        @if($field->status)
                            <span class="primary status">(Positivo)</span>
                          </h6>
                          
                          <small class="secondary field_text">{{  strip_tags($field->respuesta)  }}</small>
                        @else
                        
                          <span class="primary status">(Negativo)</span><br>
                          <small class="secondary field_text"></small>
                         
                         
                         
                          
                        @endif
                        </div>
                      @empty
                        <div class="alert alert-primary alert_{{ $value->id }}">
                            <p class="text-center">
                              No hay datos
                            </p>
                        </div>
                      @endforelse

                    </div>
                    @endif
                  
                  @else
                   {{ $value->nombre}} {{ $value->POLO }}
                   </span>
                  <i data-id="{{ $value->id }}" data-nombre="{{$value->nombre}}" class="feather icon-edit float-right patologico"></i>
                  @if ( $value->user)
                  <small class="block" style="font-size: 10px;">Creado por: {{ $value->user->nombre }} </small>
                  @endif
                </div>
                <hr class="m-0">
                  <div >
                    <div class="card-body content_fields">

                      @php
                        $fields = \DB::table('antecedentes')
                        ->where('antecedenteF_id',$value->id)
                        ->where('id_expediente',$expediente_id)
                        ->get();
                      @endphp

                      @forelse ($fields as $field)

                        @php
                        $answer = \DB::table('antecedentesAnswer')
                        ->where('id',$field->answerA_id)
                        ->first();
                        @endphp

                        <div class="col-md-12 field_{{ $field->answerA_id }}">
                          <h6 class="primary">
                            <i class="feather icon-check"></i>
                            {{ $answer->pregunta }}
                        @if($field->status)
                            <span class="primary status">(Positivo)</span>
                          </h6>
                          
                          <small class="secondary field_text">{{  strip_tags($field->respuesta)  }}</small>
                        @else
                        
                          <span class="primary status">(Negativo)</span><br>
                          <small class="secondary field_text"></small>
                         
                         
                         
                          
                        @endif
                        </div>
                      @empty
                        <div class="alert alert-primary alert_{{ $value->id }}">
                            <p class="text-center">
                              No hay datos
                            </p>
                        </div>
                      @endforelse

                    </div>
                   
                 @endif
                  </div>
                   
              </div>

            </div>
          </div>
        </div>
        @endforeach
        @else
        <div style="height: 38px"></div>
       @endif

       </div>
    </div>
    <div id="validacion_medicamentos" data-validacion="@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Medicamentos')['delete'])) true @else false @endif"></div>
    @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Medicamentos')['insert']))
    
    <div class="card">
    <div class="card-header bg-secondary" style="padding: .6rem !important;">
        <h6 class="card-title font-weight-light  text-white">
        Medicamentos Activos
        </h6>
        @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Historial Clínico')['insert']))
        <a type="button" href="../historialmedicamentos/{{ $empleado->CURP }}" data-toggle="popover" data-placement="top" data-content="Historial de medicamentos" data-trigger="hover" data-original-title="Historial Medicamentos" class="btn btn-icon btn-icon btn-sm rounded-circle btn-primary waves-effect waves-light text-white">
        <img src="https://img.icons8.com/ios-filled/20/ffffff/pills.png"/>
        </a>
        
       
        @endif
      </div>
        
        <div class="card-content collapse show">
          <div class="card-body">
            <select class="cat_med" style="width:100%">

            </select>
            <div class="row content_medicamento">
              @foreach ($medicamentosActivos as $medicamento)
                <div id="medicamento_{{ $medicamento->id }}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
                    <div class="mr-50">
                        <img src="https://img.icons8.com/cotton/48/000000/medical-history.png"/>
                    </div>
                      <div class="user-page-info">
                        <h6 class="mb-0">
                        {{ $medicamento->nombre }}
                        </h6>
                        <small class="block">{{ $medicamento->clave }}</small>
                        <span class="font-small-2">
                          {{
                            Carbon::parse($medicamento->created_at)->diffForHumans()
                          }}
                        </span>
                      </div>
                    @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Medicamentos')['delete']))
                      <a type="button" data-key="{{ $medicamento->id }}" class="btn deleteM btn-danger btn-icon ml-auto waves-effect waves-light text-white">
                        <i class="feather icon-trash"></i>
                      </a>
                    @endif
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
    @endif

</section>

