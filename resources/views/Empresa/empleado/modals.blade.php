<div id="modalIndicadores" class="modal fade" role="dialog">
    <div id="modal-dialog-nota" class="modal-dialog modal-xl">

    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary ">
                <h4 class="modal-title float-left">Indicadores</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body d-block">
                <div id="loadIndicadores">
                    <div class="alert alert-secondary">
                      <span>Cargando...</span>
                    </div>
                </div>
                <div class="alert alert-danger" id="alertIndicadores" role="alert">
                    Algo ha ocurrido: No se han podido actualizar las gráficas.
                </div>

                <div id="area-graficas">
                    {{-- Gráfica de línea de estatura --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Estatura</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div id="grafica-estatura" class="height-400 echart-container" _echarts_instance_="1589034945494" style="user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"><div style="position: relative; overflow: hidden; width: 1561px; height: 400px;"><div style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 197px; top: 127px;">Tue<br>Highest temperature : 11<br>Minimum temperature : -2</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{-- Gráfica de línea de peso --}}
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Peso</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div id="grafica-peso" class="height-400 echart-container" _echarts_instance_="1589034945495" style="user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"><div style="position: relative; overflow: hidden; width: 1561px; height: 400px;"><div style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 197px; top: 127px;">Tue<br>Highest temperature : 11<br>Minimum temperature : -2</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{-- Gráfica de línea de IMC --}}

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">IMC</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div id="grafica-IMC" class="height-400 echart-container" _echarts_instance_="1589034945496" style="user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"><div style="position: relative; overflow: hidden; width: 1561px; height: 400px;"><div style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 197px; top: 127px;">Tue<br>Highest temperature : 11<br>Minimum temperature : -2</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{-- Gráfica de línea de frecuencia respiratoria --}}
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Frecuencia respiratoria</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div id="grafica-frec-resp" class="height-400 echart-container" _echarts_instance_="1589034945497" style="user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"><div style="position: relative; overflow: hidden; width: 1561px; height: 400px;"><div style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 197px; top: 127px;">Tue<br>Highest temperature : 11<br>Minimum temperature : -2</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{-- Gráfica de línea de frecuencia cardiaca --}}
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Frecuencia cardiaca</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div id="grafica-frec-card" class="height-400 echart-container" _echarts_instance_="1589034945498" style="user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"><div style="position: relative; overflow: hidden; width: 1561px; height: 400px;"><div style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 197px; top: 127px;">Tue<br>Highest temperature : 11<br>Minimum temperature : -2</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{-- Gráfica de línea de temperatura --}}
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Temperatura</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div id="grafica-temperatura" class="height-400 echart-container" _echarts_instance_="1589034945499" style="user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"><div style="position: relative; overflow: hidden; width: 1561px; height: 400px;"><div style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 197px; top: 127px;">Tue<br>Highest temperature : 11<br>Minimum temperature : -2</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Saturación de Oxígeno</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div id="grafica-oxigeno" class="height-400 echart-container" _echarts_instance_="1589034945491" style="user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"><div style="position: relative; overflow: hidden; width: 1561px; height: 400px;"><div style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 1561px; height: 400px; user-select: none;" width="1561" height="400" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 197px; top: 127px;">Tue<br>Highest temperature : 11<br>Minimum temperature : -2</div></div></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Antectedentes patologicos --}}

<div id="patologico" class="modal fade modalPatologico"  role="dialog">
  <div class="modal-dialog modal-lg  modal-dialog-scrollable">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary white">
      <h4 class="modal-title float-left antecedente_name">Antecedentes Patológicos</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body first" style="display:none">

        <form id="patologico_form" class="patologicoForm" method="post">
          @csrf
          <input type="hidden" id="keyForm">
          <select class="select2 input-sm antecedentes_select" style="width:100%" placeholder="Antecedentes" multiple>

          </select>
          <hr>
        </form>
        <div class="row content_detail">

        </div>
    </div>
    <div class="modal-body second text-center" >
      <div class="spinner-border secondary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
  </div>
</div>
</div>

