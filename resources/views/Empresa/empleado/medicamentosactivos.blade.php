@extends('layouts.Vuexy')
@section('title','Historial Medicamentos')
@section('begin_vendor_css')
<link rel="stylesheet" type="text/css"
    href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css"
    href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href=" {!! asset('public/css/daicoms/daicoms.css') !!}">
<link rel="stylesheet" type="text/css"
    href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css"
    href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
<link rel="stylesheet" type="text/css"
    href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('content')
<nav aria-label="breadcrumb">

    <ol class="breadcrumb breadcrumb-chevron">
        <li class="breadcrumb-item"><a href="{{ route('empleados') }}">Empleados</a></li>

        <li class="breadcrumb-item"><a href="{{ route('empleados.show', $empleado->CURP)}}">{{$empleado->nombre}} {{$empleado->apellido_paterno}} {{$empleado->apellido_materno}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">Historial de Medicamentos</li>
    </ol>


   
</nav>
<div class='row'>
 
  <div class='col-md-6'>
    <div class="card" id="medicamentos">
        <div class="card-header bg-secondary" style="padding: .6rem !important;">
            <h6 class="card-title font-weight-light text-white">Medicamentos</h6>
        </div>
        <div class="card-content collapse show" style="">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12 mb-1">
                        <input type="text" class="form-control search input-sm" placeholder="Buscar Medicamentos">
                    </div>
                    <div class="col-md-12 list">
                      @forelse($empleado->expediente->consultas as $consulta)
                        @foreach($consulta->medicamentos as $medicamento)
                        <div class="twitter-feed">
                            <div class="d-flex justify-content-start align-items-center">
                                    <div class="col-md-12">
                                        <small class="badge bg-secondary mb-1 fecha">{{ \Carbon::parse($medicamento->created_at)->format('M / d / Y') }}</small>
                                        <h6 class="text-bold-600 mb-0 text-center primary nombre">{{ $medicamento->nombre }}</h6>
                                      
                                        <h6 class="d-block line-ellipsis text-center frecuencia"><b class='secondary'>Tomar:</b> {{ $medicamento->frecuencia }}</h6>
                                        <h6 class="d-block line-ellipsis text-center duracion"><b class='secondary'>Durante:</b> {{ $medicamento->duracion }}</h6>
                                        <h6 class="d-block  line-ellipsis text-center comentario"><b class='secondary'>Comentarios:</b> {{ $medicamento->comentario }}</h6>
                                    </div>
                                
                            </div>
                            <hr>
                        </div>
                        @endforeach
                        @empty
                    <div class="alert">
                        <div class="alert alert-danger">
                            No hay Medicamentos
                        </div>
                     </div>
                    @endforelse
                    </div>

                    <div class="col-md-12 mt-1">
                        <div class="d-flex align-items-center justify-content-center">
                            <div id="anterior"></div>
                            <ul class="pagination justify-content-center m-0"></ul>
                            <div id="siguiente"></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
  </div>
  <div class='col-md-6'>
    <div class="card">
      <div class="card-header bg-secondary">
                <h4 class="text-white text-center">Indicadores</h4>
      </div>
      <div class="card-content collapse show" style="">
        <div class="card-body  text-center">
            
              <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                <div class="avatar-content">
                <img src="https://img.icons8.com/cotton/38/000000/hand-with-a-pill.png"/>
                </div>
              </div>
              <h2 class="text-bold-700">{{ $empleado->expediente->consultas->count() }}</h2>
              <p class="mb-0 line-ellipsis">Medicamentos Activos</p>
            
        </div>
        <div id="chart" style="min-height: 365px;">
        </div>
       
      </div>
     </div>
  </div>
</div>





{{-- @dd($empleado->expediente->consultas->count()) --}}

{{-- @foreach ($empleado->expediente->consultas as $consulta)
    @foreach($consulta->medicamentos as $medicamento)
            {{$medicamento->nombre}}<br>
    @endforeach
@endforeach --}}



@endsection


@section('page_vendor_js')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://use.fontawesome.com/6571a6d6ab.js"></script>


@endsection
@section('page_js')
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/js/scripts/modal/components-modal.min.js') !!} "></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection
@section('js_custom')

<script src="{!! asset('public/js/empresa/info_paciente.js') !!}"></script>
<script src="https://ac-labs.com.mx/asesores/app-assets2/vendors/js/charts/echarts/echarts.js"></script>
<script src="https://ac-labs.com.mx/asesores/app-assets2/vendors/js/charts/echarts/chart/bar.js"></script>
<script src="{!! asset('resources/js/charts-empleado.js') !!} "></script>
<script src="{!! asset('public/js/empresa/antecedente/antecedentes.js') !!}"></script>
<script src="{{ asset('public/js/empresa/empleado/medicamentos.js') }}"></script>

<script src="{!! asset('public/vuexy/app-assets/js/scripts/popover/popover.min.js') !!}" charset="utf-8"></script>
@endsection