@extends('layouts.Vuexy')
@section('title','Pacientes')
@section('begin_vendor_css')
 <link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('css_custom')

@endsection
@section('content')

<section id="basic-datatable">
  <div class="row">
      <div class="col-12">
      
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Resultados de Laboratorio</h4>
              </div>
             <div class="card-content">
                <div class="card-body card-dashboard">
                        <div class='row'>
                 
                            <div class='col-4'>
                                <label for="start">Fecha Inicial:</label>
                                <input type="date" id="fecha_inicial" name="fecha_inicial" value="2021-07-22" min="2020-01-01" max="2021-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
                            </div>
                            <div class='col-4'>
                                <label for="start">Fecha Final:</label>
                                <input type="date" id="fecha_final" name="fecha_final" value="2021-07-22" min="2020-01-01" max="2021-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
                            </div>
                            <div class='col-4'>
                                
                                <a data-id="" class="btn btn-secondary  buscar_fecha text-white mt-2">
                                            Buscar</a>
                            </div>
                       
                          </div>                       
                        <div>
                        <div class="col md 12 text-center loadResultados" style="">
                            <div class="cargar md 12">
                            </div>                     
                        </div>
                        <div id='tabla_dinamica'>
                        <table class="table_estudios table" style="width:100%">
                             <thead>
                             <tr>
                               <th>Empleado</th>
                               <th>Toma</th>
                                                         
                               <th>Fecha Inicial</th> 
                               <th>Fecha Final</th>                                
                               <th>Acciones</th>                                
                             </tr>
                             </thead>
                             <tbody>
                             
                                 @if ($resultados)
                                 
                                     @foreach($resultados as $row)
                                     
                                     <tr>
                                         <td>{{ $row->nombre.' '.$row->apellido_paterno.' '.$row->apellido_materno}}</td>
                                         <td>{{$row->folio}}</td> 
                                      
                                         <td>{{\Carbon::parse($row->fecha_inicial)->format('M / d / Y') }}</td>
                                         <td>{{\Carbon::parse($row->fecha_final)->format('M / d / Y') }}</td>
                                         
                                         <td><a data-id="{{$row->id}}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>    
                                         </td>                            
                                     </a>
                                     </td>


                                     </tr>
                                     @endforeach
                                 @endif
                           </tbody>
                        </table>
                        </div>
                        <div id="tabla_estudios">
                        </div>

                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

</section>
<div class="modal fade text-left" id="estudiosShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    
        <div class="modal-content">
        <div class="modal-header bg-primary ">
                <h4 class="modal-title float-left">Estudios Programados</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body mt-2">
                
                <div class="col-md-12 text-center loadEstudios" style="display:none">
                    <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>


                <div class="row contEstudios">

                </div>


            </div>
        </div>
    
    </div>
</div>

@endsection


@section('page_vendor_js')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}" ></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}"></script>

@endsection

@section('page_js')

@endsection
@section('js_custom')

<script src="{!! asset('public/js/empresa/resultados.js') !!}" charset="utf-8"></script>

@endsection
