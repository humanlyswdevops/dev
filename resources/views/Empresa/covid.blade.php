@extends('layouts.Robust')

@section('title')
{{-- Paciente: {{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}} --}}
{{-- Estudio: {{$estudio->nombre}} --}}
@endsection

@section('style_vendor')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/extensions/sweetalert.css">
<!-- END VENDOR CSS-->
@endsection
@section('page_level_css')
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type=x"text/css" href="../../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('styles')
<!-- BEGIN Page Level CSS Custom-->
<link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
<link rel="stylesheet" type="text/css" href="../resources/sass/css/styleScroll.css">
<link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/medicinaDiagnostico.css">
<link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
<!-- END Page Level CSS Custom-->
@endsection
{{-- BEGIN body html --}}
@section('content')
<div class="app-content content">
  <div class="content-header-left col-12 mt-1 ml-1">
    <div class="row breadcrumbs-top">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{route('admin')}}">Inicio</a>
          </li>
          <li class="breadcrumb-item ">
            <a href="{{route('empleados')}}">Empleados</a>

          </li>
          <li class="breadcrumb-item">
            <a href="{{route('empleados.show',$empleado->CURP)}}">{{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}</a>
          </li>
          <li class="breadcrumb-item active"> Covid
          </li>
        </ol>
      </div>
    </div>
  </div>
  {{-- inicio de formulario --}}
  <div class="content-wrapper pt-1">
    <div class="content-body">
      <!-- Form wizard with number tabs section start -->
      <section id="number-tabs">
        <div class="row">
          <div class="col-md-12">
            <div class="card">

              <div class="card-content collapse show" style="">
                <div class="row pr-3 pl-3">
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
      			<div class="card">
      				<div class="card-content collapse show">
      					<div class="card-body">
                  <div class="row">
                    <div class="col-md-3">
                      Paciente:
                      <p>
                        {{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}
                      </p>
                    </div>
                    <div class="col-md-3">
                      Curp del Paciente:
                      <p>{{ $empleado->CURP }}</p>
                    </div>
                    <div class="col-md-3">
                      Fecha de la Captura:
                      <p>
                        {{ \Carbon::parse($estudioCaso->fechaInsert)->format('d-m-Y')}}
                      </p>
                    </div>
                    <div class="col-md-2">
                        <form action="{{ route('covid_2',['curp'=>$empleado->CURP]) }}" method="get">
                        @csrf
                        Versión de la Encuesta:
                        <select class="form-control input-sm" id="covid_encuesta" name="version">
                          @foreach ($version as $value)
                            @if ( $estudioCaso->fechaInsert ==$value->fechaInsert)
                              <option selected value="{{ $value->fechaInsert }}">{{ \Carbon::parse($value->fechaInsert)->format('d-m-Y') }}</option>
                            @else
                             <option value="{{ $value->fechaInsert }}">{{ \Carbon::parse($value->fechaInsert)->format('d-m-Y') }}</option>
                            @endif



                          @endforeach
                        </select>
                          <button class="btn btn-block mt-1 btn-sm btn-primary">
                            Ver
                          </button>
                      </form>

                    </div>
                    <div class="col-md-3">
                      Nombre de la unidad:
                      <p>
                        {{ $estudioCaso->nomUnidad }}
                      </p>
                    </div>
                    <div class="col-md-3">
                      Resultado:
                      <p>
                        {{ $estudioCaso->resultados }}
                      </p>
                    </div>
                  </div>

      					</div>
      				</div>
      			</div>
            <div class="card">
              {{-- <div class="card-header">
                <h4 class="card-title">Covid</h4>
                @if(count($errors) > 0)
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Error en la validación</strong> <br>
                  <ul>
                    @foreach($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
                @endif
                <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  </ul>
                </div>
              </div> --}}
              <div class="card-content collapse show">
                <div class="card-body">
                  <form id="form" method="post" class="number-tab-stepssVer wizard-circle" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="Desarrollo" id="Desarrollo" value={{$empleado->CURP}}>

                    <!-- Step 1 -->
                    <h6>Datos Generales</h6>
                    @include('Empresa.includesCovid.DatosGenerales')
                    <!-- Step 2 -->
                    <h6>Datos Clinicos</h6>
                    @include('Empresa.includesCovid.DatosClinicos')
                    <!-- Step 3 -->
                    <h6>Tratamiento</h6>
                    @include('Empresa.includesCovid.Tratamiento')
                    <!-- Step 4 -->
                    <h6>Antecedentes patológicos</h6>
                    @include('Empresa.includesCovid.AntecedentesEpidemiologicos')
                    <!-- Step 5 (damas) -->
                    <h6>Laboratorio</h6>
                    @include('Empresa.includesCovid.Laboratorio')
                    <!-- Step 6 -->
                    <h6>Evolución</h6>
                    @include('Empresa.includesCovid.Evolucion')
                    <!-- Step 7 -->
                    <h6>Contactos</h6>
                    @include('Empresa.includesCovid.Contactos')
                    <h6>Medico Elaboro</h6>
                    @include('Empresa.includesCovid.DatosMedico')
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  {{-- fin de formulario --}}
</div>
<!-- Modal -->
<div class="modal animated bounceInDown text-left" id="bounceInDown" tabindex="-1" role="dialog" aria-labelledby="myModalLabel47" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel47">Resultados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-2">
            <div class="col-md-12" id="iframes">
              <a href="#" id="toolIframe">

              </a>
            </div>
          </div>
          <div class="col-md-10" id="contentIframe">
            <iframe width="" height="" class="col-12" style="display:none; height: 67vh;" id="iframeBg"></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="../../app-assets/vendors/js/extensions/sweetalert.min.js"></script>
<script src="../../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('script')
<!-- BEGIN PAGE LEVEL JS-->
{{-- <script src="../../app-assets/js/scripts/forms/wizard-steps.js"></script> --}}
{{-- checkbox --}}
<script src="../../app-assets/js/scripts/forms/checkbox-radio.js"></script>
<script src="./resources/js/ajax/Empresa/encuestaCovid.js"></script>
<script src="../../app-assets/js/scripts/forms/select/form-selectize.js"></script>
<!-- END PAGE LEVEL JS-->
@endsection
