<a href="{{ route('empleados.show', $CURP)}}" class="btn btn-sm btn-secondary">
  <i class="feather icon-eye"></i>
</a>
@isset(\Session::get('Programar Estudios')['insert'])
  <button  type="button" class="btn btn-sm btn-primary" onclick="showProgramarEmpleadoModal('{{$CURP}}', '{{$nombre}}')">
    <i class="feather icon-book"></i>
  </button>
@endisset
