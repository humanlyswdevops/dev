@extends('layouts.Vuexy')
@section('title')
  Resultados
@endsection
@section('begin_vendor_css')
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('content')

  <input type="hidden" class="form-control" value="{{$toma['toma']->folio}}" id="idSass" name="toma">
<input type="hidden" class="form-control" value="{{$toma['estudios']->id}}" id="idExamen" name="estudio">

  <!-- End vertical navbar -->

 {{-- <div class="title-content">
     <div class="seccion-navegacion">
         <p class="navegacion"> <a href="{{route('admin')}}">Inicio</a> / <a href="{{ route('empleados.show', $paciente->CURP)}}">Expediente</a> / <span>Documentos</span> </p>
     </div>
 </div> --}}

 <section>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb breadcrumb-chevron">
        <li class="breadcrumb-item"><a href="../empleados/{{$toma['empleado']->CURP}}">Expediente</a></li>
        <li class="breadcrumb-item active" aria-current="page">Resultados de {{$toma['estudios']->nombre}}</li>
      </ol>
    </nav>
  </section>

<div class="panel panel-default">


 <div class="row">
   <div class="col-md-12">
    <div class="card">
      <div class="card-header bg-secondary text-center">
          <h4 class="card-title text-center text-white">Resultados</h4>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <div class="col md 12 text-center loadResultados" style="display:none">
            <div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
          </div>
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Sección</th>
                    <th>Examen</th>
                    <th>Abreviatura</th>
                    <th>Resultado</th>
                    <th>Metodo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>

</div>
</div>


@endsection


@section('js_custom')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
 <script src="{!! asset('public/js/empresa/empleado/resultados.js') !!}"></script>
@endsection
