
@extends('layouts.Vuexy')
@section('title','Grupos')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
@endsection


@section('content')
  <section >
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb breadcrumb-chevron">
        <li class="breadcrumb-item"><a href="{!! route('empleados') !!}">Empleados</a></li>
        <li class="breadcrumb-item active" aria-current="page">Carga Masiva</li>
      </ol>
    </nav>
  </section>

    <h3 class="primary text-center">Alta de Empleados</h3>
    @if(count($errors) > 0)
     <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Error en la validación</strong> <br>
      <ul>
       @foreach($errors->all() as $error)
       <li>{{ $error }}</li>
       @endforeach
      </ul>
     </div>
    @endif

   @if(session('error'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong> Error en la validación</strong>
     <br>
      <ul>
        @foreach (session('error') as $item)
         <li>{{$item[0]}}</li>
        @endforeach
      </ul>
    </div>
   @endif

    @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
     <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
    </div>
    @endif


    <div class="panel panel-default">
     <div class="panel-heading">
       <div class="col-md-12 text-center">
        <a href="{!! asset('public/download/ListaEmpleados.xlsx') !!}" class="btn  btn-sm btn-primary">Descargar plantilla</a>
        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#myModal">Agregar archivo .xls</button>
      </div>
      <h3 class="panel-title">Empleados</h3>
     </div>

     <div class="panel-body">

      <div class="table-responsive">
       <table id="table_estudios"  class=" clinicaTable table data-list-view" style="width:100%">
            <thead>
            <tr>
            <th>Clave</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>CURP</th>
            <th>Género</th>
            <th>Fecha de Nacimiento</th>
            <th>Dirección</th>
            <th>Email</th>
            <th>Teléfono</th>
            <th>Modificar</th>
            </tr>
            </thead>
            <tbody>
                @if (session('data'))
                    @foreach(session('data') as $row)
                    <tr>
                    @foreach ($row as $item => $value)
                      @if (empty($value))
                        @else
                          <td>{{ $value}}</td>
                      @endif

                        @if ($item == 'telefono')
                        <td><button onclick="showModal('{{$row['curp']}}')" class="btn btn-info">Modificar</button></td>
                        @endif

                    @endforeach
                    </tr>
                    @endforeach
                @endif
          </tbody>
       </table>
      </div>
     </div>
    </div>
  </div>

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title float-left">Registro de Empleados</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <form method="post" id="formExcel" enctype="multipart/form-data" action="{{ url('public/import_excel/import') }}">
              @csrf
              <div class="form-group">
                  <label for="excel"></label>

                  <input type="file" class="form-control" name="excel" id="excel" required />
              </div>
              <div class="form-row">
                  <div class="form-group col-md-12">
                      <label for="grupo">Grupos</label>
                      <select id="grupo" name="grupo" class="form-control mb-1"required>
                          @foreach ($grupos as $grupo)
                            <option class="grupo_add" value="{{$grupo->id}}">{{$grupo->nombre}}</option>
                          @endforeach
                          <option value="Otro" class="grupo_add">Otro</option>
                      </select>

                      <div class="form-group" id="grupo_name">
                        <label for="">Nombre del Grupo</label>
                        <input type="text" class="form-control"   name="grupo_name">
                      </div>
                  </div>
              </div>
              <button type="submit" class="btn btn-sm btn-primary">
                Registrar
              </button>
          </form>
      </div>
    </div>
    </div>
  </div>

  <!-- Modal -->
  <div id="update" class="modal fade"  role="dialog">
   <div class="modal-dialog modal-dialog-scrollable">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title float-left">Alta Empleado</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       {{-- formulario --}}

        <form method="POST" id='alta'>
            @csrf
            <div id="alerts">
            </div>
            <input type="hidden" name="encryptedID" value="" id="inputEncryptedIDProgramar">
              <div class="form-group">
                <label for="clave">Clave</label>
                <input type="text" class="form-control @error('clave') is-invalid @enderror" name="clave" id="clave">
              </div>
             <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" id="nombre">
              </div>
            <div class="form-row">

              <div class="form-group col-md-6">
                <label for="app">Apellido Paterno</label>
                <input type="text" class="form-control @error('app') is-invalid @enderror" name="app" id="app">
              </div>
              <div class="form-group col-md-6">
                <label for="apm">Apellido Materno</label>
                <input type="text" class="form-control @error('apm') is-invalid @enderror" name="apm" id="apm">
              </div>

            </div>

            <div class="form-group">
              <label for="curp">Curp</label>
            <input type="text" class="form-control @error('curp') is-invalid @enderror" name="curp" value="{{old('curp')}}" id="curp" maxlength="18">
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                  <label for="nacimiento">Fecha de Nacimiento</label>
                  <input type="date" class="form-control @error('nacimiento') is-invalid @enderror" name="nacimiento" id="nacimiento">
                </div>

                <div class="form-group col-md-6">
                    <label for="genero">Genero</label>
                    <select id="genero" name="genero" class="form-control @error('genero') is-invalid @enderror">
                      <option selected>Selecciona...</option>
                      <option value="Masculino">Masculino</option>
                      <option value="Femenino">Femenino</option>
                    </select>
                </div>

              </div>

            <div class="form-group">
              <label for="direccion">Dirección</label>
              <input type="text" class="form-control @error('direccion') is-invalid @enderror" id="direccion" name="direccion">
            </div>

            <div class="form-row">

              <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email">
              </div>

              <div class="form-group col-md-6">
                <label for="telefono">Telefono</label>
                <input type="text" onkeypress='return validaNumericos(event)' class="form-control  @error('telefono') is-invalid @enderror" id="telefono" name="telefono">
              </div>

            </div>


            <button type="submit" id="btn_enviar" class="btn btn-primary">Modificar</button>
          </form>
          {{-- fin formulario --}}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
  </div>
  <!--fin modal-->
@endsection



@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
@endsection
@section('page_js')

@endsection
@section('js_custom')
  <script src="resources/js/ajax/datatables/datas.js"> </script>
  <script src="{!! asset('public/js/empresa/pacientes/altamasiva.js') !!}"></script>
@endsection
