@extends('layouts.Vuexy')
@section('title')
  Administrador
@endsection
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') !!}">

@endsection
@section('begin_vendor_css')
  <style media="screen">
    #container{
      overflow-x: auto !important;
    }
  </style>
@endsection
@section('content')
  <section id="dashboard-analytics">


    {{-- @dd(Session::all()) --}}
  <div class="row">
      {{-- <div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Line Chart</h4>
          </div>
          <div class="card-content">
            <div class="card-body">
              <div id="line-chart" class="responsive"></div>
            </div>
          </div>
        </div>
      </div> --}}
      <div class="col-xl-3 col-md-4 col-sm-6">
        <div class="card text-center">
          <div class="card-content">
            <div class="card-body">
              <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                <div class="avatar-content">
                  <i class="feather icon-check text-info font-medium-5"></i>
                </div>
              </div>
              <h2 class="text-bold-700">{{ $consultas }}</h2>
              <p class="mb-0 line-ellipsis">Consultas Terminadas</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-4 col-sm-6">
        <div class="card text-center">
          <div class="card-content">
            <div class="card-body">
              <div class="avatar bg-rgba-success p-50 m-0 mb-1">
                <div class="avatar-content">
                  <i class="feather icon-users text-success font-medium-5"></i>
                </div>
              </div>
              <h2 class="text-bold-700">{{ $personal }}</h2>
              <p class="mb-0 line-ellipsis">Pacientes</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-4 col-sm-6">
        <div class="card text-center">
          <div class="card-content">
            <div class="card-body">
              <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                <div class="avatar-content">
                  <i class="fa fa-venus text-danger font-medium-5"></i>
                </div>
              </div>
              <h2 class="text-bold-700">{{ $femenino }}</h2>
              <p class="mb-0 line-ellipsis">Femenino</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-4 col-sm-6">
        <div class="card text-center">
          <div class="card-content">
            <div class="card-body">
                <div class="avatar bg-rgba-primary p-50 m-0 mb-1">
                  <div class="avatar-content">
                    <i class="fa fa-mars secondary font-medium-5"></i>
                  </div>
                </div>
                <h2 class="text-bold-700">{{ $masculino }}</h2>
              <p class="mb-0 line-ellipsis">Masculino</p>
            </div>
          </div>
        </div>
      </div>


  </div>
  <div class="row">


    <div class="animate__animated animate__fadeInLeft  col-md-6">
      <div class="card">

        <div class="card-content">
          <div class="card-body">
            <figure class="highcharts-figure" style="overflow-x: auto;">
              <div id="container" ></div>
              <p class="highcharts-description"></p>
            </figure>
          </div>
        </div>
      </div>
    </div>

    <div class="animate__animated animate__fadeInRight  col-md-6">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Diagnosticos más detectados </h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <form id="diagnosticos" method="post">
              <div class="row">
                <div class="col-md-5">
                  <input type="date" value="{{ \Carbon::now()->subDays(30)->format('Y-m-d') }}" class="form-control input-sm" name="inicial">
                </div>
                <div class="col-md-5">
                  <input type="date" value="{{ \Carbon::now()->format('Y-m-d') }}" class="form-control input-sm" name="final">
                </div>
                <div class="col-md-2">
                  <button class="btn-primary btn btn-sm">
                    Aplicar
                  </button>
                </div>
              </div>
            </form>
            <div id="bar-chart"></div>
          </div>
        </div>
      </div>
    </div>

  </div>


  </section>
@endsection
@section('page_vendor_js')

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
  <script src=" {!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!} "></script>


@endsection

@section('js_custom')
  <script src="{!! asset('public/js/empresa/dash.js') !!}" charset="utf-8"></script>
  <script src=" {!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!} "></script>

@endsection
