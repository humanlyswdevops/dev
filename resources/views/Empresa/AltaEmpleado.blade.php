@extends('layouts.Vuexy')

@section('content')

  <div class="row">

    <div class="Altaem col-7 text-center">
      <!--div class="seccion-navegacion">
            <p class="navegacion"> <a href="{{route('admin')}}">Inicio</a> / <span>Alta de empleados</span> </p>
        </div-->

      <h2>
        Alta de empleados por grupo
      </h2>

      <div class="Altaem_content">
        <div class="attrib">
          <p style=" text-align: center;">Para poder cargar un archivo tipo hoja de <br> cálculo tiene que ser en los siguientes formatos:
            <br>
            <b>.xls, .xlsx, .ods y .csv.</b>
            <br>
            <br>
          </p>

          <br>
          <p style="text-align: center;">Si se introducen datos inválidos el <br> proceso se cancelará.
          </p>

          <button class="btn btn-primary btn_altaemp" type="submit" onclick="location.href='{{ route('altagrupo') }}'">Elegir esta opción</button>
          <button class="btn btn-primary btn_altaemp" type="submit" onclick="location.href='{{route('download')}}'">Descargar esta plantilla</button>

          <!--a href="{{route('download')}}" class="btn btn-success btn-primary">Descargar plantilla</a-->


        </div>

      </div>

    </div>

    <div class="lista col-5 text-center">
      <div class="imag">
      </div>
      <div class="content_Alta">

        <h2 class="txt">Alta de empleado individual</h2>
        <br>
        <p style="text-align: center;">
          Introducirá los datos de un empleado <br>
          manualmente.
        </p>
        <button class="btn btn-primary btn_altaemp " data-toggle="modal" data-target="#myModal" type="button">Elegir esta opción</button>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title float-left">Alta Empleado</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          {{-- formulario --}}

          <form method="POST" id='alta'>
            @csrf
            <div id="alerts">

            </div>

            <div class="form-group">
              <label for="grupo">*Grupo</label>
              <select id="grupo" name="grupo" class="form-control @error('grupo') is-invalid @enderror">
                <option selected>Selecciona...</option>
                <div id="opciones-grupos">
                  @foreach ($grupos as $grupo)
                  <option value="{{$grupo->id}}">{{$grupo->nombre}}</option>
                  @endforeach
                </div>
                <option value="Otro">Otro</option>
              </select>
            </div>

            <div class="form-group">
              <input placeholder="Escriba aquí el nombre del nuevo grupo" type="text" class="form-control @error('nuevoGrupo') is-invalid @enderror" name="nuevoGrupo" id="inputNuevoGrupo" disabled>
            </div>

            <div class="form-group">
              <label for="clave">*Clave</label>
              <input type="text" class="form-control @error('clave') is-invalid @enderror" name="clave" id="clave">
            </div>
            <div class="form-group">
              <label for="nombre">*Nombre</label>
              <input type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" id="nombre">
            </div>
            <div class="form-row">

              <div class="form-group col-md-6">
                <label for="app">*Apellido Paterno</label>
                <input type="text" class="form-control @error('app') is-invalid @enderror" name="app" id="app">
              </div>
              <div class="form-group col-md-6">
                <label for="apm">*Apellido Materno</label>
                <input type="text" class="form-control @error('apm') is-invalid @enderror" name="apm" id="apm">
              </div>

            </div>

            <div class="form-group">
              <label for="curp">*CURP <a href="https://www.gob.mx/curp/" target="blank"> (Consulta curp aquí)</a></label>
              <input type="text" class="form-control @error('nombre') is-invalid @enderror" name="curp" value="{{old('curp')}}" id="curp" maxlength="18">
            </div>

            <div class="form-row">

              <div class="form-group col-md-6">
                <label for="nacimiento">*Fecha de Nacimiento</label>
                <input type="date" class="form-control @error('nombre') is-invalid @enderror" name="nacimiento" id="nacimiento">
              </div>

              <div class="form-group col-md-6">
                <label for="genero">*Género</label>
                <select id="genero" name="genero" class="form-control @error('genero') is-invalid @enderror">
                  <option selected>Selecciona...</option>
                  <option value="Masculino">Masculino</option>
                  <option value="Femenino">Femenino</option>
                </select>
              </div>

            </div>

            <div class="form-row">

              <div class="form-group col-md-6">
                <label for="lugarNacimiento">Lugar de nacimiento</label>
                <input type="text" class="form-control @error('lugarNacimiento') is-invalid @enderror" id="lugarNacimiento" name="lugarNacimiento">
              </div>
              <div class="form-group col-md-6">
                <label for="nss">Número de seguridad social</label>
                <input type="text" class="form-control @error('nss') is-invalid @enderror" name="nss" id="nss">
              </div>

            </div>

            <div class="form-group">
              <label for="direccion">*Dirección</label>
              <input type="text" class="form-control @error('direccion') is-invalid @enderror" id="direccion" name="direccion">
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="colonia">Colonia</label>
                <input type="text" class="form-control @error('colonia') is-invalid @enderror" id="colonia" name="colonia">
              </div>
              <div class="form-group col-md-6">
                <label for="cp">Codigo postal</label>
                <input type="text" class="form-control @error('cp') is-invalid @enderror" name="cp" id="cp">
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="municipio">Municipio</label>
                <input type="text" class="form-control @error('municipio') is-invalid @enderror" id="municipio" name="municipio">
              </div>
              <div class="form-group col-md-6">
                <label for="estado">Estado</label>
                <input type="text" class="form-control @error('estado') is-invalid @enderror" id="estado" name="estado">
              </div>
            </div>

            <div class="form-row">

              <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email">
              </div>

              <div class="form-group col-md-6">
                <label for="telefono">Teléfono</label>
                <input type="text"  class="form-control @error('telefono') is-invalid @enderror" id="telefono" name="telefono">
              </div>

            </div>


            <button type="submit" id="btn_enviar" class="btn btn-primary">Agregar</button>
          </form>
          {{-- fin formulario --}}

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

@endsection



@section('js_custom')

  <script src="resources/js/ajax/Empresa/AltaEmpleado.js"></script>
@endsection
{{-- <!--fin modal-->
</body>

</html> --}}
