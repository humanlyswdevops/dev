<section class="padecimientos">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
            <div class="card-header">
                <div class="card-title" >Signos Vitales</div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                <div class="row mb-1">
                    <div class="col-2 text-center">
                    <i class="fa fa-long-arrow-up icon_vital"></i>
                    </div>
                    <div class="col-5">
                    Estatura
                    </div>
                    <div class="col-3">
                    @if ($datos["examenFisico"] != null)
                        {!! $datos["examenFisico"]->altura !!}
                    @endif
                    </div>
                    <div class="col-2">
                    <span class="text-muted">m</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-2 text-center">
                    <i class="fa fa-tachometer icon_vital"></i>
                    </div>
                    <div class="col-5">
                    Peso
                    </div>
                    <div class="col-3">
                    @if ($datos["examenFisico"] != null)
                        {!! $datos["examenFisico"]->peso !!}
                    @endif
                    </div>
                    <div class="col-2">
                    <span class="text-muted">Kg</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-2 text-center">
                    <i class="fa fa-male icon_vital"></i>
                    </div>
                    <div class="col-5">
                    Masa Corporal
                    </div>
                    <div class="col-3">
                    <label class="imc_label"></label>
                    @if ($datos["examenFisico"] != null)
                        @if($datos["examenFisico"]->peso != null && $datos["examenFisico"]->altura != null)
                        {!! round($datos["examenFisico"]->peso / ($datos["examenFisico"]->altura * $datos["examenFisico"]->altura ),1) !!}
                        @endif
                    @endif
                    </div>
                    <div class="col-2">
                    <span class="text-muted">kg/m2</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-2 text-center">
                    <i class="fa fa-thermometer-three-quarters icon_vital"></i>
                    </div>
                    <div class="col-5">
                    Temperatura
                    </div>
                    <div class="col-3">
                    @if ($datos["examenFisico"] != null)
                        {!! $datos["examenFisico"]->temperatura !!}
                    @endif
                    </div>
                    <div class="col-2">
                    <span class="text-muted">C</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-2 text-center">
                    <i class="feather icon-activity icon_vital"></i>
                    </div>
                    <div class="col-5">
                        Frecuencia Respiratoria
                    </div>
                    <div class="col-3">
                    @if ($datos["examenFisico"] != null)
                        {!! $datos["examenFisico"]->fr !!}
                    @endif
                    </div>
                    <div class="col-2">
                    <span class="text-muted">r/m</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-2 text-center">
                    <i class="fa fa-heartbeat icon_vital"></i>
                    </div>
                    <div class="col-5">
                        Frecuencia Cardiaca
                    </div>
                    <div class="col-3">
                    @if ($datos["examenFisico"] != null)
                        {!! $datos["examenFisico"]->fc !!}
                    @endif
                    </div>
                    <div class="col-2">
                    <span class="text-muted">bpm</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-2 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="28" height="28" viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#002b46"><path d="M86.25895,10.757c-0.43616,-0.02245 -0.87176,0.0075 -1.30176,0.09798c-0.36192,0.07167 -0.67831,0.18303 -1.00081,0.28695c-0.3225,0.14333 -0.64416,0.32474 -0.93083,0.50391c-0.28667,0.215 -0.57501,0.43162 -0.82585,0.67887c-0.99975,1.00692 -1.57471,2.39921 -1.57471,3.80029c0,1.42975 0.57496,2.79696 1.57471,3.80029c1.00692,1.00333 2.36696,1.57471 3.80029,1.57471c0.71667,0 1.3952,-0.14109 2.03662,-0.39193c0.68083,-0.28667 1.25859,-0.67753 1.75668,-1.18278c1.00333,-0.99975 1.57471,-2.36696 1.57471,-3.80029c0,-1.40108 -0.57137,-2.79338 -1.57471,-3.80029c-0.94062,-0.94063 -2.22587,-1.50035 -3.53434,-1.56771zM86.25895,28.67367c-0.43616,-0.02245 -0.87176,0.0075 -1.30176,0.09798c-0.36192,0.07167 -0.67831,0.18303 -1.00081,0.28695c-0.3225,0.14333 -0.64416,0.32474 -0.93083,0.50391c-0.28667,0.215 -0.57501,0.43162 -0.82585,0.67887c-0.24725,0.25442 -0.46387,0.50335 -0.67887,0.82585c-0.17917,0.28667 -0.36057,0.60833 -0.50391,0.93083c-0.10392,0.3225 -0.21886,0.64948 -0.28695,1.00781c-0.07167,0.3225 -0.10498,0.67389 -0.10498,1.03581c0,1.42975 0.57496,2.79696 1.57471,3.80029c1.00692,1.00333 2.36696,1.57471 3.80029,1.57471c0.71667,0 1.3952,-0.14109 2.03662,-0.39193c0.68083,-0.28667 1.25859,-0.67753 1.75668,-1.18278c1.00333,-0.99975 1.57471,-2.36696 1.57471,-3.80029c0,-1.40108 -0.57137,-2.79338 -1.57471,-3.80029c-0.94062,-0.94062 -2.22587,-1.50035 -3.53434,-1.56771zM58.81006,35.83333c-7.49275,0 -14.23697,4.15432 -17.60172,10.84798l-35.63737,70.83382c-0.07883,0.16125 -0.15587,0.32491 -0.22396,0.49691c-1.16817,3.06375 -1.76367,6.2813 -1.76367,9.56722c0,16.58725 13.50015,30.0874 30.0874,30.0874h0.37093c20.7475,0 37.625,-16.8775 37.625,-37.625v-8.25146c3.35758,-3.053 9.73233,-9.96385 14.33333,-21.40902c4.601,11.44517 10.97575,18.35602 14.33333,21.40902v8.25146c0,20.7475 16.8775,37.625 37.625,37.625h0.37093c16.58725,0 30.0874,-13.50015 30.0874,-30.0874c0,-3.28592 -0.59551,-6.50347 -1.76367,-9.56722c-0.06808,-0.172 -0.14154,-0.33566 -0.22396,-0.49691l-35.63737,-70.83382c-3.36475,-6.69367 -10.10897,-10.84798 -17.60172,-10.84798c-7.08783,0 -12.85661,5.76878 -12.85661,12.85661v46.35238c-4.44333,-7.04842 -8.95833,-18.08934 -8.95833,-34.01367v-1.90365v-7.16667c0,-2.967 -2.408,-5.375 -5.375,-5.375c-2.967,0 -5.375,2.408 -5.375,5.375v7.16667v1.90365c0,15.92433 -4.515,26.96526 -8.95833,34.01367v-46.35238c0,-7.08783 -5.76878,-12.85661 -12.85661,-12.85661z"></path></g></g></svg>
                    </div>
                    <div class="col-5">
                        Saturación de Oxígeno
                    </div>
                    <div class="col-3">
                        @if ($datos["examenFisico"] != null)
                            {!! $datos["examenFisico"]->oxigeno !!}
                        @endif
                    </div>
                    <div class="col-2">
                        <span class="text-muted">%</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-2 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="28" height="28" viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#002b46"><path d="M24.08,6.88c-9.64576,0 -17.2,11.33136 -17.2,25.8c0,12.6936 5.81704,22.97393 13.76,25.30281v83.05719h0.06719c0.83936,10.96328 9.61297,19.73345 20.57281,20.57281l41.28,0.06719c11.38296,0 20.64,-9.25704 20.64,-20.64v-3.44h37.84c1.90232,0 3.44,-1.54112 3.44,-3.44v-17.43515c13.5536,-1.7028 24.08,-13.27717 24.08,-27.28485c0,-14.00768 -10.5264,-25.58205 -24.08,-27.28485v-38.07515c0,-1.89888 -1.53768,-3.44 -3.44,-3.44h-82.56c-1.90232,0 -3.44,1.54112 -3.44,3.44v110.08c0,1.89888 1.53768,3.44 3.44,3.44h37.84v3.44c0,7.58864 -6.17136,13.76 -13.76,13.76h-39.56c-8.53464,0 -15.48,-6.94536 -15.48,-15.48v-81.33719c7.94296,-2.32888 13.76,-12.60577 13.76,-25.30281c0,-14.46864 -7.55424,-25.8 -17.2,-25.8zM141.04,68.8c11.38296,0 20.64,9.25704 20.64,20.64c0,11.38296 -9.25704,20.64 -20.64,20.64c-11.38296,0 -20.64,-9.25704 -20.64,-20.64c0,-11.38296 9.25704,-20.64 20.64,-20.64zM141.10047,85.94625c-0.90042,0.01806 -1.82013,0.38904 -2.49265,1.06156c-1.34504,1.34504 -1.48952,3.68445 0,4.86437c2.93776,2.32544 12.75219,7.88781 12.75219,7.88781c0,0 -5.56237,-9.81443 -7.88781,-12.75219c-0.58996,-0.74476 -1.4713,-1.07962 -2.37172,-1.06156z"></path></g></g></svg>
                    </div>
                    <div class="col-5">
                        Presión arterial
                    </div>
                    <div class="col-3">
                        @if ($datos["examenFisico"] != null)
                            {!! $datos["examenFisico"]->presion !!}
                        @endif
                    </div>
                    <div class="col-2">
                        <span class="text-muted">%</span>
                    </div>
                </div>

                </div>

            </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
            <div class="card-header">
                <div class="card-title" >Razón de la visita</div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                <div id="razon">
                    @if ($datos["padecimiento"] != null)
                        {!! $datos["padecimiento"]->razon !!}
                    @endif
                </div>
                </div>
            </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
            <div class="card-header">
                <div class="card-title" >Sintomas</div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                <div id="sintomas">
                    @if ($datos["padecimiento"] != null)
                        {!! $datos["padecimiento"]->sintomas !!}
                    @endif
                </div>
                </div>
            </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
            <div class="card-header">
                <div class="card-title" >Exploración Física</div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                <div id="exploracion">
                    @if ($datos["padecimiento"] != null)
                        {!! $datos["padecimiento"]->exploracion !!}
                    @endif
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
