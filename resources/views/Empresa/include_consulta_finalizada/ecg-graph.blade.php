@extends('layouts.Vuexy')
@section('begin_vendor_css')
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/toastr.css') !!}   ">
@endsection
@section('page_css')
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/plugins/extensions/toastr.min.css') !!}">

@endsection
@section('title')
  Electrocardiograma
@endsection
@section('css_custom')

@endsection

@section('content')
<input type="hidden" id="ecgId" value="{{ decrypt($id)}}">
<input type="hidden" id="nombre" value="{{ $ecg->patient }}">
<input type="hidden" id="surnames" value="{{ $ecg->surnames }}">
<input type="hidden" id="sexo" value="{{ $ecg->sexo }}">
<div class="col-md-12">
  <div class="card">
      <div class="card-header bg-secondary">
        <h4 class="card-title text-white">Medidas</h4>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <ul class="list-group">
          <div class="row">
            @foreach ($medidas->sortBy('created_at') as  $medida)
              <div class="col-md-4 mb-1">
                <li class="list-group-item">
                  {{ $medida->code }} - {{ $medida->value }} <small>{{ $medida->unit }}</small>
                </li>
              </div>
            @endforeach

          </div>
          </ul>
          <div class="col-md-12 mt-1">
            @foreach ($interpretacion as $item)
              <p class="m-0">{{ $item->value }}</p>
            @endforeach
          </div>



        </div>
      </div>
    </div>
</div>
{{-- <section >
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-chevron">
      <li class="breadcrumb-item"><a href="{!! route('empleados') !!}">Empleados</a></li>
      <li class="breadcrumb-item"><a href="{{ route('empleados.show', $empleado->CURP)}}">Expediente</a></li>
      <li class="breadcrumb-item active" aria-current="page">Consulta: {{$empleado->nombre}} {{$empleado->apellido_paterno}}</li>
    </ol>
  </nav>
</section> --}}
<div class="col-md-12 text-center spinner" style="display:none">
  <div class="spinner-border text-primary" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>

<div class="row" id="content">



  {{-- <div id="chartContainer" style="height: 400px; width: 100%;"></div> --}}
</div>

@endsection

@section('page_vendor_js')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="{!! asset('public/vuexy/app-assets/vendors/js/extensions/toastr.min.js') !!} "></script>

@endsection
@section('page_js')
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js" charset="utf-8"></script>

@endsection
@section('js_custom')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{!! asset('public/js/empresa/consulta/ecg-graph.js') !!}" charset="utf-8"></script>
@endsection
