<section class="diagnostico">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Diagnostico</div>
        </div>
        <div class="card-body">
          <div class="row  cie_card">
            @if ($datos["diagnostico"] != null)
                @foreach ($datos["diagnostico"] as $diagnostico)
                <div class="col-12 border p-2 mb-1 rounded">
                    <div class="h-100">
                        <div class="w-100 border-bottom mb-1 pb-1" style="display: inline-flex">
                            <div class="w-25">
                                <div class="badge badge-secondary">
                                    <i class="fa fa-flask"></i>
                                    <span>{!! $diagnostico->clave !!}</span>
                                </div>
                            </div>
                            <small class="w-75 text-right">{!! $diagnostico->nombre !!}</small>
                        </div>
                        <div>
                            {!! $diagnostico->comentario !!}
                        </div>
                    </div>
                </div>
                @endforeach
            @else
            <div class="col-12">
                <h6>Sin datos</h6>
            </div>
            @endif
          </div>
        </div>

      </div>
    </div>


    <div class="col-md-6">
        <div class="card">
          <div class="card-header">
              <div class="display-5 col-md-12">Discapacidades (CIF)
              </div>
          </div>
          <div class="card-body">
              <div class="row  proc_card">
                  @if ($datos["discapacidad"] != null)
                      @foreach ($datos["discapacidad"] as $discapacidad)
                      <div class="col-12 border p-2 mb-1 rounded">
                          <div class="h-100">
                              <div class="w-100 border-bottom mb-1 pb-1" style="display: inline-flex">
                                  <div class="w-25">
                                      <div class="badge badge-secondary">
                                          <i class="fa fa-flask"></i>
                                          <span>{!! $discapacidad->clave !!}</span>
                                      </div>
                                  </div>
                                  <small class="w-75 text-right">{!! $discapacidad->nombre !!}</small>
                              </div>
                              <div>
                                  {!! $discapacidad->comentario !!}
                              </div>
                          </div>
                      </div>
                      @endforeach
                  @else
                  <div class="col-12">
                      <h6>Sin datos</h6>
                  </div>
                  @endif
              </div>
          </div>
        </div>
      </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
            <div class="display-5 col-md-12">Procedimientos
                @if ($cedula=='si')
                    <a class="float-right" id="email_procedimientos">
                        <i class="feather icon-mail"></i>
                    </a>
                    <a class=" float-right mr-1" id="pdf_procedimientos">
                        <i class="feather icon-printer"></i>
                    </a>
                @else
                    <a class=" float-right alertCedula">
                        <i class="feather icon-mail"></i>
                    </a>
                    <a class=" float-right mr-1 alertCedula">
                        <i class="feather icon-printer"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class="card-body">
            <div class="row  proc_card">
                @if ($datos["procedimiento"] != null)
                    @foreach ($datos["procedimiento"] as $procedimiento)
                    <div class="col-12 border p-2 mb-1 rounded">
                        <div class="h-100">
                            <div class="w-100 border-bottom mb-1 pb-1" style="display: inline-flex">
                                <div class="w-25">
                                    <div class="badge badge-secondary">
                                        <i class="fa fa-flask"></i>
                                        <span>{!! $procedimiento->clave !!}</span>
                                    </div>
                                </div>
                                <small class="w-75 text-right">{!! $procedimiento->nombre !!}</small>
                            </div>
                            <div>
                                {!! $procedimiento->comentario !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                <div class="col-12">
                    <h6>Sin datos</h6>
                </div>
                @endif
            </div>
        </div>
      </div>
    </div>





      <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="display-5 col-md-12">
            Medicamentos a recetar
            @if ($cedula=='si')
                <a class=" float-right" id="email_receta">
                    <i class="feather icon-mail"></i>
                </a>
                <a class=" float-right mr-1" id="pdf_receta">
                    <i class="feather icon-printer"></i>
                </a>
            @else
                <a class=" float-right alertCedula">
                    <i class="feather icon-mail"></i>
                </a>
                <a class=" float-right mr-1 alertCedula">
                    <i class="feather icon-printer"></i>
                </a>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="row  medic_card">
            @if ($datos["medicamento"] != null)
                @foreach ($datos["medicamento"] as $medicamento)
                <div class="col-4 border p-2 rounded">
                    <div class="h-100">
                        <div class="w-100 border-bottom mb-1 pb-1" style="display: inline-flex">
                            <div class="w-25">
                                <div class="badge badge-primary">
                                    <i class="fa fa-flask"></i>
                                    <span>{!! $medicamento->clave !!}</span>
                                </div>
                            </div>
                            <small class="w-75 text-right">{!! $medicamento->nombre !!}</small>
                        </div>
                        <div>
                            <p>Tomar: {!! $medicamento->tomar !!}</p>
                            <p>Frecuencia: {!! $medicamento->frecuencia !!}</p>
                            <p>Duración: {!! $medicamento->duracion !!}</p>
                        </div>
                        <div>
                            {!! $medicamento->comentario !!}
                        </div>
                    </div>
                </div>
                @endforeach
            @else
            <div class="col-12">
                <h6>Sin datos</h6>
            </div>
            @endif
          </div>
        </div>

      </div>
    </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5 col-sm-12">Tratamiento
            @if ($cedula=='si')
                <a id="email_tratamiento" class=" float-right" >
                    <i class="feather icon-mail"></i>
                </a>
                <a class=" float-right mr-1" id="pdf_tratamiento">
                    <i class="feather icon-printer"></i>
                </a>
            @else
                <a class=" float-right alertCedula">
                    <i class="feather icon-mail"></i>
                </a>
                <a class=" float-right mr-1 alertCedula">
                    <i class="feather icon-printer"></i>
                </a>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="tratamiento">
            @if($datos["tratamiento"] != null)
            {!! $datos["tratamiento"]->tratamiento !!}
            @endif
          </div>
        </div>

      </div>
    </div>


    <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <div class="display-5 col-sm-12">Notas Paciente
              @if ($cedula=='si')
                  <a  class="notasEmail float-right" >
                      <i class="feather icon-mail"></i>
                  </a>

              @else
                  <a class=" float-right alertCedula">
                      <i class="feather icon-mail"></i>
                  </a>

              @endif
            </div>
          </div>
          <div class="card-body">
            <div class="tratamiento">
              @if($datos["tratamiento"] != null)
              {!! $datos["tratamiento"]->notas !!}
              @endif
            </div>
          </div>

        </div>
      </div>

  </div>
</section>


<div id="notas" class="modal fade"  role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body first" >
          <div class="col-md-12 text-center mb-1">
            <h6>Correo Electrónico del Paciente</h6>
          </div>
          <div class="row">
            <div class="col-md-9">
              <input type="text" class="form-control input-sm" id="notaEmailPatient" value="{{ $empleado->email }}" placeholder="Correo electronico.">
            </div>
            <div class="col-md-3">
              <button class="btn btn-secondary enviarEmailNota">
                Enviar
              </button>
            </div>
          </div>

      </div>
    </div>
  </div>
  </div>

