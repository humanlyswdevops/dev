<section class="archivos">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Documentos (xlsx, csv, doc, docx, txt, pdf).</div>
        </div>
        <div class="card-body">
          <div class="row content_doc">
            @if(count($archivos) > 0)
                @foreach ($archivos as $value)
                    <a href="{!! asset('storage/app/consultas/'.$value->storage) !!}" download class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
                        <div class="mr-50">
                            @if ($value->extencion == "pdf")
                                <img src="https://img.icons8.com/fluent/48/000000/pdf.png"/>
                            @endif
                            @if ($value->extencion == "xlsx")
                                <img src="https://img.icons8.com/color/48/000000/xls.png"/>
                            @endif
                            @if ($value->extencion == "csv")
                                <img src="https://img.icons8.com/color/48/000000/csv.png"/>
                            @endif
                            @if ($value->extencion == "docx" || $value->extencion == "doc")
                                <img src="https://img.icons8.com/color/48/000000/word.png"/>
                            @endif
                            @if ($value->extencion == "txt")
                                <img src="https://img.icons8.com/color/48/000000/txt.png"/>
                            @endif
                        </div>
                        <div class="user-page-info">
                            <h6 class="mb-0">{{ $value->nombre }}</h6>
                            <span class="font-small-2">{{ \Carbon\Carbon::parse($value->created_at)->diffForHumans() }}</span>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="col-12">
                    <h6>Sin archivos</h6>
                </div>
            @endif
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Imágenes (png, jpg, jpeg,).</div>
        </div>
        <div class="card-body">
          <div class="row content_image">
            @if(count($imagenes) > 0)
                @foreach ($imagenes as $value)
                    <a href="{!! asset('storage/app/consultas/'.$value->storage) !!}" download class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
                        <div class="mr-50">
                            @if ($value->extencion == "png" || $value->extencion == "PNG" )
                                <img src="https://img.icons8.com/officel/48/000000/png.png"/>
                            @endif
                            @if ($value->extencion == "jpg" || $value->extencion == "jpeg")
                                <img src="https://img.icons8.com/officel/48/000000/jpg.png"/>
                            @endif
                        </div>
                        <div class="user-page-info">
                            <h6 class="mb-0">{{ $value->nombre }}</h6>
                            <span class="font-small-2">{{ \Carbon\Carbon::parse($value->created_at)->diffForHumans() }}</span>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="col-12">
                    <h6>Sin imágenes</h6>
                </div>
            @endif
          </div>
        </div>

      </div>
    </div>

  </div>
</section>
