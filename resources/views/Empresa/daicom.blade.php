
@extends('layouts.Vuexy')
@section('title','IMAGENOLOGIA')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href=" {!! asset('public/css/daicoms/daicoms.css') !!}">
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
<link rel="stylesheet" href="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/css/quill.bubble.min.css">
    <link rel="stylesheet" href="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/css/quill.snow.min.css">
    <link rel="stylesheet" href="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/css/index.css">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
<link rel="stylesheet" type="text/css"  
    href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-chevron">
      <li class="breadcrumb-item"><a href="{{ route('empleados') }}">Empleados</a></li>
      
      <li class="breadcrumb-item"><a href="{{ route('empleados.show', $toma['empleado']->CURP)}}">{{$toma['empleado']->nombre}} {{$toma['empleado']->apellido_paterno}} {{$toma['empleado']->apellido_materno}}</a></li>
      <li class="breadcrumb-item active" aria-current="page"> {{$toma['estudios']->nombre}}</li>
    </ol>
  </nav>
<input type="hidden" class="form-control" value="{{$toma['toma']}}" id="toma" name="toma">
<input type="hidden" class="form-control" value="{{$toma['estudios']}}" id="estudio" name="estudio">

<div class='card'>
    <div class='card-header bg-secondary text-center'>
        <h6 class='card-title text-center text-white pb-2'>IMAGENOLOGÍA</h6>
    </div>
    <div class='card-body'>
        <div class='col-md-12'>
       
            <div id='estudios'>
            
            </div>
        </div>
       
        <div class="row contImagenologia">
        
       
        </div>
        
    </div>
</div>

@endsection
@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
window.cornerstoneWADOImageLoader || document.write(
    '<script src="https://unpkg.com/cornerstone-wado-image-loader">\x3C/script>')
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/js/quill.min.js"></script>
<script src="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/js/index.js"></script>
<script src=" {!! asset('public/js/librerias-dicom/cornerstone.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/cornerstoneMath.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/cornerstoneTools.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/dicomParser.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/initializeWebWorkers.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/uids.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/openJPEG-FixedMemory.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/logica.js') !!}   "></script>
<script src=" {!! asset('public/js/empresa/empleado/obtenerdaicom.js') !!}   "></script>

@endsection
  
