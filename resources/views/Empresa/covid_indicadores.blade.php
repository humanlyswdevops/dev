@extends('layouts.Vuexy')
@section('begin_vendor_css')
    <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('content')
  <div class="panel panel-default">
    {{-- <div class="row">
      <div class="col-md-6">
        <table id="resultado" class="table table-sm table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th colspan="2" class="text-center">Estudios Realizados por Genero</th>
              </tr>
                <tr>
                    <th>Sexo </th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($sexo as  $value)
                <tr>
                  <td>{{ $value->Sexo }}</td>
                  <td>{{ $value->total }}</td>
                </tr>
              @endforeach

            </tbody>
        </table>
      </div>
      <div class="col-md-6">
        <table id="resultado" class="table table-sm table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th colspan="2" class="text-center">Resultados</th>
              </tr>
                <tr>
                    <th>Resultado </th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($resultados_procentaje as  $value)
                <tr>
                  <td>{{ $value->ResultadoAlfaNumerico }}</td>
                  <td>{{ $value->total }}</td>
                </tr>
              @endforeach

            </tbody>
        </table>

      </div>

    </div> --}}
      <h4 class="text-center">Resultados de pruebas Covid</h4>
      <hr>
      <div class="panel-heading"></div>

      <div class="panel-body">
          <div class="row">
              <div class="col-4">
                  <div class="area-grafica">
                      <figure class="highcharts-figure">
                          <div id="container-generos"></div>
                          <p class="highcharts-description"></p>
                        </figure>
                  </div>
              </div>
              <div class="col-4">
                  <div class="area-grafica">
                      <figure class="highcharts-figure">
                          <div id="container-covid"></div>
                          <p class="highcharts-description"></p>
                        </figure>
                  </div>
              </div>
              <div class="col-4">
                  <div class="area-grafica">
                      <figure class="highcharts-figure">
                          <div id="container-covid-genero"></div>
                          <p class="highcharts-description"></p>
                        </figure>
                  </div>
              </div>
              <div class="col-6" style="width:1000px">
                  <div class="area-grafica">
                      <figure class="highcharts-figure">
                          <div id="container-covid-edad_hombre"></div>
                          <p class="highcharts-description"></p>
                        </figure>
                  </div>
              </div>
              <div class="col-6" style="width:1000px">
                  <div class="area-grafica">
                      <figure class="highcharts-figure">
                          <div id="container-covid-edad_mujer"></div>
                          <p class="highcharts-description"></p>
                        </figure>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="panel panel-default">

    <h4 class="text-center">Encuentas Realizadas</h4>
    <hr>
      <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
              <tr>
                  <th>Nombre </th>
                  <th>Apellido Paterno</th>
                  <th>Apellido Materno</th>
                  <th>Curp</th>
                  <th>Resultado</th>
                  <th>Acción</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($covid as  $value)
              <tr>
                <td>{{ $value->nombre }}</td>
                <td>{{ $value->aPaterno }}</td>
                <td>{{ $value->aMaterno }}</td>
                <td>{{ $value->curp }}</td>
                <td>{{ $value->resultados }}</td>
                <td>
                  <a type="button" onclick="covid('{{$value->curp}}')" class="btn text-white btn-sm btn-primary" name="button">Ver</a>
                </td>
              </tr>
            @endforeach
          </tbody>
      </table>
  </div>
@endsection
@section('page_vendor_js')
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
@endsection

@section('js_custom')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>

  <script src="resources/js/covid.js"></script>

@endsection
