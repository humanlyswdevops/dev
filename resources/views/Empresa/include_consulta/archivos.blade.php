<section class="archivos">
  <div class="row">

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Documentos (xlsx, csv, doc, docx, txt, pdf).</div>
          <span class="spinner-border spinner doc primary float-rigth spinner-border-sm" role="status" aria-hidden="true" ></span>
        </div>
        <div class="card-body">
          <div class="row">
            <form class="col-md-12" id="document" method="post" enctype="multipart/form-data">
              @csrf
                <input type="hidden" name="tipo" value="1">
                <input type="hidden" name="consulta" value="{{ $historial->id }}">
                <input type="file" accept=".xlsx,.pdf,.csv,.doc,.docx,.txt" class="form-control input-sm" name="archivo" required>
                <button type="submit" class="btn btn-sm mt-1 btn-block btn-secondary" name="button">Subir</button>

            </form>
          </div>
          <div class="row content_doc">
            @foreach ($archivos as $value)
              <div id="{{ $value->id }}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
                <div class="mr-50">
                  @if ($value->extencion == "pdf")
                    <img src="https://img.icons8.com/fluent/48/000000/pdf.png"/>
                  @endif
                  @if ($value->extencion == "xlsx")
                    <img src="https://img.icons8.com/color/48/000000/xls.png"/>
                  @endif
                  @if ($value->extencion == "csv")
                    <img src="https://img.icons8.com/color/48/000000/csv.png"/>
                  @endif
                  @if ($value->extencion == "docx" || $value->extencion == "doc")
                    <img src="https://img.icons8.com/color/48/000000/word.png"/>
                  @endif
                  @if ($value->extencion == "txt")
                    <img src="https://img.icons8.com/color/48/000000/txt.png"/>
                  @endif

                </div>
                <div class="user-page-info">
                  <h6 class="mb-0">{{ $value->nombre }}</h6>
                  <span class="font-small-2">{{ \Carbon\Carbon::parse($value->created_at)->diffForHumans() }}</span>
                </div>
                <a type="button" target="_blank" href="{!! asset('storage/app/consultas/'.$value->storage) !!}" class="btn btn-secondary btn-icon ml-auto waves-effect waves-light"><i class="feather icon-eye"></i></i></a>
                <button type="button" data-key="{{$value->id}}" class="delete btn btn-danger btn-icon ml-1 waves-effect waves-light"><i class="feather icon-trash"></i></i></button>
              </div>
            @endforeach

          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Imágenes (png, jpg, jpeg,).</div>
          <span class="spinner-border img spinner primary float-rigth spinner-border-sm" role="status" aria-hidden="true" ></span>
        </div>
        <div class="card-body">
          <div class="row">
            <form class="col-md-12" id="img" method="post" enctype="multipart/form-data">
              <input type="hidden" name="tipo" value="2">
              <input type="hidden" name="consulta" value="{{ $historial->id }}">
              <input type="file" accept="image/*" class="form-control input-sm" name="archivo">
              <button type="submit"  class=" btn btn-sm mt-1 btn-block btn-secondary" name="button">Subir</button>
            </form>
          </div>
          <div class="row content_image">
            @foreach ($imagenes as $value)
              <div id="{{ $value->id }}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
                <div class="mr-50">
                  @if ($value->extencion == "png" || $value->extencion == "PNG" )
                    <img src="https://img.icons8.com/officel/48/000000/png.png"/>
                  @endif
                  @if ($value->extencion == "jpg" || $value->extencion == "jpeg")
                    <img src="https://img.icons8.com/officel/48/000000/jpg.png"/>


                  @endif

                </div>
                <div class="user-page-info">
                  <h6 class="mb-0">{{ $value->nombre }}</h6>
                  <span class="font-small-2">{{ \Carbon\Carbon::parse($value->created_at)->diffForHumans() }}</span>
                </div>
                <a type="button" target="_blank" href="{!! asset('storage/app/consultas/'.$value->storage) !!}" class="btn btn-secondary btn-icon ml-auto waves-effect waves-light"><i class="feather icon-eye"></i></i></a>
                <button type="button" data-key="{{$value->id}}" class="delete btn btn-danger btn-icon ml-1 waves-effect waves-light">
                  <i class="feather icon-trash"></i></i>
                </button>
              </div>
            @endforeach
          </div>
        </div>

      </div>
    </div>

  </div>
</section>
