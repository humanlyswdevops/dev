<section class="diagnostico">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Diagnostico</div>
        </div>
        <div class="card-body">
          <select class="cie" style="width:100%" name="cie" >

          </select>
            <hr>
          <div class="row  cie_card">

          </div>
        </div>

      </div>
    </div>

    <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <div class="display-5">Discapacidad</div>
          </div>
          <div class="card-body">
            <select class="cat_disc" style="width:100%" name="disc" ></select>
            <hr>
            <div class="row  disc_card">
            </div>
          </div>

        </div>
      </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Procedimientos</div>
        </div>
        <div class="card-body">
          <select class="cat_proc" style="width:100%" name="pro" >

          </select>
          <hr>
          <div class="row  proc_card">

          </div>
        </div>

      </div>
    </div>



    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="display-5 col-md-12">
            Medicamentos a recetar
            <small class="primary save_alert medicamentoSave">Guardando...</small>
            @if ($cedula=='si')
              <a class=" float-right email" >
                <i class="feather icon-mail"></i>
              </a>
              <a class=" float-right mr-1 pdf">
                <i class="feather icon-printer"></i>
              </a>
              @else
                <a class=" float-right alertCedula">
                  <i class="feather icon-mail"></i>
                </a>
                <a class=" float-right mr-1 alertCedula">
                  <i class="feather icon-printer"></i>
                </a>
            @endif

          </div>

        </div>
        <div class="card-body">
          <select class="cat_med" style="width:100%" name="medi" >

          </select>
          <hr>
          <div class="row  medic_card">
          </div>

        </div>

      </div>
    </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Tratamiento</div>
        </div>
        <div class="card-body">
          <div class="tratamiento">

          </div>
        </div>

      </div>
    </div>

    <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <div class="display-5 col-md-12">
                Notas Paciente
                @if ($cedula=='si')
                    <a class=" float-right notasEmail" >
                       <i class="feather icon-mail"></i>
                    </a>
                @else
                    <a class=" float-right alertCedula">
                      <i class="feather icon-mail"></i>
                    </a>
                @endif
            </div>

          </div>
          <div class="card-body">
            <div class="notas">

            </div>
          </div>

        </div>
      </div>
  </div>
</section>

{{-- Modal de notas del paciente --}}


<div id="notas" class="modal fade"  role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body first" >
          <div class="col-md-12 text-center mb-1">
            <h6>Correo Electrónico del Paciente</h6>
          </div>
          <div class="row">
            <div class="col-md-9">
              <input type="text" class="form-control input-sm" id="notaEmailPatient" value="{{ $empleado->email }}" placeholder="Correo electronico.">
            </div>
            <div class="col-md-3">
              <button class="btn btn-secondary enviarEmailNota">
                Enviar
              </button>
            </div>
          </div>

      </div>
    </div>
  </div>
  </div>

