<section class="archivos">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
            <div class="card-header">
                <div class="display-5">Electrocardiograma (xml).</div>
                <span class="spinner-border spinner ecg primary float-rigth spinner-border-sm" role="status" aria-hidden="true" ></span>
            </div>
            <div class="card-body">
                <div class="row">
                <form class="col-md-12" id="form_file_ecg" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="consulta" value="{{ $historial->id }}">
                    <input type="file" accept=".xml" class="form-control input-sm" name="file_ecg" id="file_ecg" required>
                    <button type="submit" class="btn btn-sm mt-1 btn-block btn-secondary" name="button">Subir</button>
                </form>
                </div>
                <div class="row content_ecg">
                    @foreach ($historial->ecgs as $ecg)
                        <div id="ecg_{{ $ecg->id }}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
                            <div class="mr-50">
                                <img src="https://img.icons8.com/color/48/000000/xml-file.png"/>
                            </div>
                            <div class="user-page-info">
                                <h6 class="mb-0">ECG </h6>
                                <span class="font-small-2">{{ \Carbon\Carbon::parse($ecg->created_at)->diffForHumans() }}</span>
                            </div>
                            <a type="button" href="{!! route('view_ecg',['id'=>encrypt($ecg->id)]) !!}" class="btn btn-secondary btn-icon ml-auto waves-effect waves-light"><i class="feather icon-eye"></i></i></a>
                            <button type="button" data-key="{{$ecg->id}}" class="delete_ecg btn btn-danger btn-icon ml-1 waves-effect waves-light"><i class="feather icon-trash"></i></i></button>
                        </div>
                  @endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
