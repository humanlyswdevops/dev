@extends('layouts.Chat')
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('vuexy/app-assets/vendors/css/animate/animate.css') !!}">
@endsection
@section('page_css')
<link rel="stylesheet" href="{!! asset('vuexy/app-assets/css/pages/app-chat.min.css') !!}">
@endsection
@section('title','Chat')
@section('css_custom')
@endsection

@section('content')
  <div class="content-area-wrapper">
          <div class="sidebar-left">
            <div class="sidebar"><!-- User Chat profile area -->
  <div class="chat-profile-sidebar">
    <header class="chat-profile-header">
      <span class="close-icon">
        <i class="feather icon-x"></i>
      </span>
      <div class="header-profile-sidebar">
        <div class="avatar">
          <img src="vuexy/app-assets/images/portrait/small/avatar-s-11.jpg" alt="user_avatar" height="70" width="70">
          <span class="avatar-status-online avatar-status-lg"></span>
        </div>
        <h4 class="chat-user-name">John Doe</h4>
      </div>
    </header>
    <div class="profile-sidebar-area">
      <div class="scroll-area ps ps--active-y">
        <h6>About</h6>
        <div class="about-user">
          <fieldset class="mb-0">
              <textarea data-length="120" class="form-control char-textarea active" id="textarea-counter" rows="5" placeholder="About User" style="color: rgb(78, 81, 84);">Dessert chocolate cake lemon drops jujubes. Biscuit cupcake ice cream bear claw brownie brownie marshmallow.</textarea>
          </fieldset>
          <small class="counter-value float-right" style="background-color: rgb(115, 103, 240);"><span class="char-count">112</span> / 120 </small>
        </div>
        <h6 class="mt-3">Status</h6>
        <ul class="list-unstyled user-status mb-0">
          <li class="pb-50">
            <fieldset>
              <div class="vs-radio-con vs-radio-success">
                <input type="radio" name="userStatus" value="online" checked="checked">
                <span class="vs-radio">
                  <span class="vs-radio--border"></span>
                  <span class="vs-radio--circle"></span>
                </span>
                <span class="">Active</span>
              </div>
            </fieldset>
          </li>
          <li class="pb-50">
            <fieldset>
              <div class="vs-radio-con vs-radio-danger">
                <input type="radio" name="userStatus" value="busy">
                <span class="vs-radio">
                  <span class="vs-radio--border"></span>
                  <span class="vs-radio--circle"></span>
                </span>
                <span class="">Do Not Disturb</span>
              </div>
            </fieldset>
          </li>
          <li class="pb-50">
            <fieldset>
              <div class="vs-radio-con vs-radio-warning">
                <input type="radio" name="userStatus" value="away">
                <span class="vs-radio">
                  <span class="vs-radio--border"></span>
                  <span class="vs-radio--circle"></span>
                </span>
                <span class="">Away</span>
              </div>
            </fieldset>
          </li>
          <li class="pb-50">
            <fieldset>
              <div class="vs-radio-con vs-radio-secondary">
                <input type="radio" name="userStatus" value="offline">
                <span class="vs-radio">
                  <span class="vs-radio--border"></span>
                  <span class="vs-radio--circle"></span>
                </span>
                <span class="">Offline</span>
              </div>
            </fieldset>
          </li>
        </ul>
      <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 296px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 212px;"></div></div></div>
    </div>
  </div>
  <!--/ User Chat profile area -->
  <!-- Chat Sidebar area -->
  <div class="sidebar-content card">
      <span class="sidebar-close-icon">
        <i class="feather icon-x"></i>
      </span>
      <div class="chat-fixed-search">
          <div class="d-flex align-items-center">
            <div class="sidebar-profile-toggle position-relative d-inline-flex">
              <div class="avatar">
                <img src="vuexy/app-assets/images/portrait/small/avatar-s-11.jpg" alt="user_avatar" height="40" width="40">
                <span class="avatar-status-online"></span>
              </div>
              <div class="bullet-success bullet-sm position-absolute"></div>
            </div>
            <fieldset class="form-group position-relative has-icon-left mx-1 my-0 w-100">
                <input type="text" class="form-control round" id="chat-search" placeholder="Search or start a new chat">
                <div class="form-control-position">
                    <i class="feather icon-search"></i>
                </div>
            </fieldset>
          </div>
      </div>
      <div id="users-list" class="chat-user-list list-group position-relative ps ps--active-y">
          <h3 class="primary p-1 mb-0">Chats</h3>
          <ul class="chat-users-list-wrapper media-list">
            <li class="active">
                <div class="pr-1">
                    <span class="avatar m-0 avatar-md"><img class="media-object rounded-circle" src="vuexy/app-assets/images/portrait/small/avatar-s-3.jpg" height="42" width="42" alt="Generic placeholder image">
                    <i></i>
                    </span>
                </div>
                <div class="user-chat-info">
                  <div class="contact-info">
                    <h5 class="font-weight-bold mb-0">Elizabeth Elliott</h5>
                    <p class="truncate">Cake pie jelly jelly beans. Marzipan lemon drops halvah cake. Pudding cookie lemon drops icing</p>
                  </div>
                  <div class="contact-meta">
                    <span class="float-right mb-25">4:14 PM</span>

                  </div>
                </div>
            </li>
            <li class="">
                <div class="pr-1">
                    <span class="avatar m-0 avatar-md"><img class="media-object rounded-circle" src="vuexy/app-assets/images/portrait/small/avatar-s-7.jpg" height="42" width="42" alt="Generic placeholder image">
                    <i></i>
                    </span>
                </div>
                <div class="user-chat-info">
                  <div class="contact-info">
                    <h5 class="font-weight-bold mb-0">Kristopher Candy</h5>
                    <p class="truncate">Cake pie jelly jelly beans. Marzipan lemon drops halvah cake. Pudding cookie lemon drops icing</p>
                  </div>
                  <div class="contact-meta">
                    <span class="float-right mb-25">9:09 AM</span>
                  </div>
                </div>
            </li>
          </ul>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
          <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
      <div class="ps__rail-y" style="top: 0px; height: 383px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 158px;">
        </div>
     </div>
    </div>
  </div>
  <!--/ Chat Sidebar area -->

            </div>
          </div>
          <div class="content-right">
            <div class="content-wrapper">
              <div class="content-header row">
              </div>
              <div class="content-body"><div class="chat-overlay"></div>
  <section class="chat-app-window">
    <div class="start-chat-area d-none">
        <span class="mb-1 start-chat-icon feather icon-message-square"></span>
        <h4 class="py-50 px-1 sidebar-toggle start-chat-text">Start Conversation</h4>
    </div>
    <div class="active-chat">
      <div class="chat_navbar">
        <header class="chat_header d-flex justify-content-between align-items-center p-1">
          <div class="vs-con-items d-flex align-items-center">
            <div class="sidebar-toggle d-block d-lg-none mr-1"><i class="feather icon-menu font-large-1"></i></div>
            <div class="avatar user-profile-toggle m-0 m-0 mr-1">
              <img src="vuexy/app-assets/images/portrait/small/avatar-s-1.jpg" alt="" height="40" width="40">
              <span class="avatar-status-busy"></span>
            </div>
            <h6 class="mb-0">Felecia Rower</h6>
          </div>
          <span class="favorite"><i class="feather icon-star font-medium-5"></i></span>
        </header>
      </div>
      <div class="user-chats ps ps--active-y">
          <div class="chats">
            <div class="chat">
              <div class="chat-avatar">
                <a class="avatar m-0" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                    <img src="vuexy/app-assets/images/portrait/small/avatar-s-1.jpg" alt="avatar" height="40" width="40">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-content">
                  <p>How can we help? We're here for you!</p>
                </div>
              </div>
            </div>
            <div class="chat chat-left">
              <div class="chat-avatar">
                <a class="avatar m-0" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                  <img src="vuexy/app-assets/images/portrait/small/avatar-s-7.jpg" alt="avatar" height="40" width="40">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-content">
                  <p>Hey John,  I am looking for the best admin template.</p>
                  <p>Could you please help me to find it out?</p>
                </div>
                <div class="chat-content">
                  <p>It should be Bootstrap 4 compatible.</p>
                </div>
              </div>
            </div>
            <div class="divider">
              <div class="divider-text">Yesterday</div>
            </div>
            <div class="chat">
              <div class="chat-avatar">
                <a class="avatar m-0" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                  <img src="vuexy/app-assets/images/portrait/small/avatar-s-1.jpg" alt="avatar" height="40" width="40">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-content">
                  <p>Absolutely!</p>
                </div>
                <div class="chat-content">
                  <p>Vuexy admin is the responsive bootstrap 4 admin template.</p>
                </div>
              </div>
            </div>
            <div class="chat chat-left">
              <div class="chat-avatar">
                <a class="avatar m-0" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                  <img src="vuexy/app-assets/images/portrait/small/avatar-s-7.jpg" alt="avatar" height="40" width="40">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-content">
                  <p>Looks clean and fresh UI.</p>
                </div>
                <div class="chat-content">
                  <p>It's perfect for my next project.</p>
                </div>
                <div class="chat-content">
                  <p>How can I purchase it?</p>
                </div>
              </div>
            </div>
            <div class="chat">
              <div class="chat-avatar">
                <a class="avatar m-0" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                  <img src="vuexy/app-assets/images/portrait/small/avatar-s-1.jpg" alt="avatar" height="40" width="40">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-content">
                  <p>Thanks, from ThemeForest.</p>
                </div>
              </div>
            </div>
            <div class="chat chat-left">
              <div class="chat-avatar">
                <a class="avatar m-0" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                  <img src="vuexy/app-assets/images/portrait/small/avatar-s-7.jpg" alt="avatar" height="40" width="40">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-content">
                  <p>I will purchase it for sure.</p>
                </div>
                <div class="chat-content">
                  <p>Thanks.</p>
                </div>
              </div>
            </div>
            <div class="chat">
              <div class="chat-avatar">
                <a class="avatar m-0" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                  <img src="vuexy/app-assets/images/portrait/small/avatar-s-1.jpg" alt="avatar" height="40" width="40">
                </a>
              </div>
              <div class="chat-body">
                <div class="chat-content">
                  <p>Great, Feel free to get in touch on</p>
                </div>
                <div class="chat-content">
                  <p>https://pixinvent.ticksy.com/</p>
                </div>
              </div>
            </div>
          </div>
      <div class="ps__rail-x" style="left: 0px; bottom: -539px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 539px; right: 0px; height: 306px;"><div class="ps__thumb-y" tabindex="0" style="top: 196px; height: 110px;"></div></div></div>
      <div class="chat-app-form">
        <form class="chat-app-input d-flex" onsubmit="enter_chat();" action="javascript:void(0);">
          <input type="text" class="form-control message mr-1 ml-50" id="iconLeft4-1" placeholder="Type your message">
          <button type="button" class="btn btn-primary send waves-effect waves-light" onclick="enter_chat();"><i class="fa fa-paper-plane-o d-lg-none"></i> <span class="d-none d-lg-block">Send</span></button>
        </form>
      </div>
    </div>
  </section>
  <!-- User Chat profile right area -->
  <div class="user-profile-sidebar">
    <header class="user-profile-header">
      <span class="close-icon">
        <i class="feather icon-x"></i>
      </span>
      <div class="header-profile-sidebar">
        <div class="avatar">
          <img src="vuexy/app-assets/images/portrait/small/avatar-s-1.jpg" alt="user_avatar" height="70" width="70">
          <span class="avatar-status-busy avatar-status-lg"></span>
        </div>
        <h4 class="chat-user-name">Felecia Rower</h4>
      </div>
    </header>
    <div class="user-profile-sidebar-area p-2 ps">
      <h6>About</h6>
      <p>Toffee caramels jelly-o tart gummi bears cake I love ice cream lollipop. Sweet liquorice croissant candy danish dessert icing. Cake macaroon gingerbread toffee sweet.</p>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
  </div>
  <!--/ User Chat profile right area -->

              </div>
            </div>
          </div>
        </div>
@endsection

@section('page_vendor_js')
@endsection
@section('page_js')
  <script src=" {!! asset('vuexy/app-assets/js/scripts/modal/components-modal.min.js') !!} "></script>
  <script src="{!! asset('vuexy/app-assets/js/scripts/pages/app-chat.min.js') !!}">

  </script>
@endsection
@section('js_custom')
@endsection
