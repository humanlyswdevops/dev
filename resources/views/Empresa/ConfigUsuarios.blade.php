@extends('layouts.Vuexy')
@section('title','Grupos')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')

@endsection


@section('content')


<div class="col-md-12 text-center">
    <h2 class="text-center primary">Configuración de usuarios</h2>
</div>

<div id="validacion" 
    data-insert="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Equipo')['insert'])) true @else false @endif"
    data-delete="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Equipo')['delete'])) true @else false @endif"
    data-update="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Equipo')['update'])) true @else false @endif"
></div>
<div class="data-list-view-header">
  <div class="table-responsive">
    <table id="table_estudios" class="table data-list-view">
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Telefono</th>
          <th>Correo electrónico</th>
          {{-- <th>Detalle</th> --}}
          <th>Editar</th>
          <th>Eliminar</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>
</div>


{{-- Modal para editar usuario --}}
<div id="editarModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title float-left">Editar usuario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="formEditar">
                    @csrf
                    <input type="hidden" name="userID" id="inputUserID">

                    <div id="alerts">

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputNombre">Nombre</label>
                            <input type="text" name="nombre"  class="form-control @error('nombre') is-invalid @enderror" value="{{old('nombre')}}" id="inputNombre" required>
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-8">
                            <label for="inputEmail">Correo electrónico</label>
                            <input type="text" name="email"  class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}" id="inputEmail" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputTelefono">Teléfono</label>
                            <input type="text" name="telefono"  class="form-control" id="inputTelefono" required>
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputPuesto">Puesto</label>
                            <input type="text" name="puesto"  class="form-control" id="inputPuesto" required>
                            @error('puesto')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputArea">Área</label>
                            <input type="text" name="area"  class="form-control" id="inputArea" required>
                            @error('area')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputArea">Rol</label>
                            {{-- <input type="text" name="rol"  class="form-control" id="inputRol" required> --}}
                            <select class="form-control" name="rol" id="inputRol">
                                @foreach ($roles as $rol)
                                    <option value="{{ $rol->id }}">{{ $rol->tipo }}</option>
                                @endforeach
                            </select>
                            @error('rol')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-12">
                            <a class="btn btn-sm btn-secondary" href="{{ url('Accesos') }}">Crear un nuevo rol</a>
                        </div>
                    </div>
                    <div class="button-guardar">
                        <button type="submit" class="btn btn-primary" id="btn_enviar">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Modal para añadir un nuevo usuario --}}
<div id="agregarModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title float-left">Añadir un nuevo usuario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="formAgregar">
                    @csrf
                    <div id="alerta_agregar">

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputNombreAgregar">Nombre</label>
                            <input type="text" name="nombre"  class="form-control @error('nombre') is-invalid @enderror" value="{{old('nombre')}}" id="inputNombreAgregar" required>
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-8">
                            <label for="inputEmailAgregar">Correo electrónico</label>
                            <input type="text" name="email"  class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}" id="inputEmailAgregar" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputTelefonoAgregar">Teléfono</label>
                            <input type="text" name="telefono"  class="form-control" id="inputTelefonoAgregar" required>
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputPuestoAgregar">Puesto</label>
                            <input type="text" name="puesto"  class="form-control" id="inputPuestoAgregar" required>
                            @error('puesto')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputAreaAgregar">Área</label>
                            <input type="text" name="area"  class="form-control" id="inputAreaAgregar" required>
                            @error('area')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputArea">Rol</label>
                            <select class="form-control" name="rol">
                                @foreach ($roles as $rol)
                                    <option value="{{ $rol->id }}">{{ $rol->tipo }}</option>
                                @endforeach
                            </select>
                            @error('rol')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-12">
                            <a class="btn btn-sm btn-secondary" href="{{ url('Accesos') }}">Crear un nuevo rol</a>
                        </div>
                    </div>
                    <div class="button-guardar">
                        <button type="submit" class="btn btn-primary" id="btn_enviar">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
@endsection
@section('page_js')

@endsection
@section('js_custom')
  {{-- <script src="{!! asset('public/js/empresa/grupos.js') !!}"></script> --}}
  <script src="resources/js/config-usuarios.js"></script>

@endsection
