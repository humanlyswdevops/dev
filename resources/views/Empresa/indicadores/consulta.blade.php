@extends('layouts.Vuexy')
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/animate/animate.css') !!}">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link rel="stylesheet" href="{{ asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') }}">
@endsection
@section('page_css')
@endsection
@section('title')
Expediente:
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/info_paciente.css') !!}">
<style>
    .centradoHorizontal{
        display: flex;
        justify-content: center;
    }
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
@endsection
@section('content')

<div class="row">
    <div class="col-12">
        <h6>Antecedentes personales patológicos</h6>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header" style="padding-bottom: 1.5rem;">
                <h4 class="card-title">Diabetes</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements visible">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" style="">
                <div class="card-body ">
                    <div id="diabetes-chart" class="height-250"></div>
                    <div id='tabladiabetes-chart'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header" style="padding-bottom: 1.5rem;">
                <h4 class="card-title">Hipertensión Arterial</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements visible">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" style="">
                <div class="card-body">
                    <div id="hipertension-chart" class="height-250"></div>
                    <div id='tablahipertension-chart'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header" style="padding-bottom: 1.5rem;">
                <h4 class="card-title">Cardiopatías</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements visible">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" style="">
                <div class="card-body">
                    <div id="cardiopatia-chart" class="height-250"></div>
                    <div id='tablacardiopatia-chart'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header" style="padding-bottom: 1.5rem;">
                <h4 class="card-title">Enfermedades Tiroideas</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements visible">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" style="">
                <div class="card-body">
                    <div id="tiroides-chart" class="height-250"></div>
                    <div id='tablatiroides-chart'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header" style="padding-bottom: 1.5rem;">
                <h4 class="card-title">Embarazos</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements visible">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" style="">
                <div class="card-body">
                    <div id="embarazos-chart" class="height-300"></div>
                    <div id='tablaembarazos-chart'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header" style="padding-bottom: 1.5rem;">
                <h4 class="card-title">Signos vitales - IMC</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements visible">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" style="">
                <div class="card-body">
                    <div id="imc-chart" class="height-300"></div>
                    <div id='tablaimc-chart'></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_vendor_js')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}" ></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!} "></script>
@endsection
@section('page_js')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
@endsection
@section('js_custom')
<script src="{!! asset('/public/vuexy/app-assets/vendors/js/charts/echarts/echarts.min.js') !!}"></script>


<script>
    
    $( document ).ready(function() {
        iniciarDiabetes();
        iniciarHipertension();
        iniciarCardiopatia();
        iniciarTiroides();
        iniciarEmbarazos();
        iniciarImc();
    });

    function iniciarGrafica(id,cont_m,cont_f,titulo){
        let chartDom = document.getElementById(id);
        let myChart = echarts.init(chartDom);              
        let option;
        option = {
            tooltip: {
            trigger: 'item'
            },
            legend: {
            top: '5%',
            left: 'center'
            },
            series: [
            
                {
                name:titulo,
                type: 'pie',
                radius: ['40%', '70%'],
                avoidLabelOverlap: false,
                itemStyle: {
                borderRadius: 10,
                borderColor: '#fff',
                borderWidth: 2
                },
                label: {
                show: false,
                position: 'center'
                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: '25',
                        fontWeight: 'bold'
                    }
                },
                labelLine: {
                    show: false
                },
                data: [
                    {
                    value: cont_f,
                    name: 'Mujeres',
                    itemStyle: {
                        color: '#fe92b4'
                        }
                    },
                    {
                    value: cont_m,
                    name: 'Hombres',
                    itemStyle: {
                    color: '#84d0f7'
                        }
                    }
                ]
            }
         ]
        };
            echarts.init(chartDom).setOption(option);
            myChart.on('click', function (params) {
            console.log(params);
            let genero;
            genero=params.data.name;
            //alert(genero);
            let nombre;
            nombre=params.seriesName; 
            //alert(nombre);
            PasarDatos(nombre,genero,id);
        });
    }
        /*option = {
            tooltip:{
                trigger:"item",
                formatter:"{a} <br/>{b} : {c} ({d}%)"
            },
            legend:{
                orient:"horizontal",
                left:"left",
                data:[
                    "Hombres",
                    "Mujeres",
                ]
            },
         
            series:[
                {
                    name:titulo,
                    type:"pie",
                    radius: ['40%', '70%'],
                    avoidLabelOverlap: false,
                    itemStyle: {
                        borderRadius: 10,
                        borderColor: '#fff',
                        borderWidth: 2
                    },
                    center:[
                        "50%",
                        "50%"
                    ],
                    data:[
                        {
                            value:cont_m,
                            name:"Hombres",
                            itemStyle: {
                                color: '#84d0f7'
                            }
                        },
                        {
                            value:cont_f,
                            name:"Mujeres",
                            itemStyle: {
                                color: '#fe92b4'
                            }
                        },
                    ],
                    itemStyle:{
                        emphasis:{
                            shadowBlur:10,
                            shadowOffsetX:0,
                            shadowColor:"rgba(0, 0, 0, 0.5)"
                        }
                    }
                }
            ]
        };*/
            
    function PasarDatos(nombre,genero,id){
        //alert('Si entro');
       // alert('Si entro');
        switch (nombre) {
        case 'diabetes':
            datos(genero,nombre,id);
        break;
        case 'hipertension':
            datos(genero,nombre,id);
        break;
        case 'cardiopatia':
            datos(genero,nombre,id);
        break;
        case 'tiroides':
            datos(genero,nombre,id);
        break;
        case 'IMC':
            datos(genero,nombre,id);
        break;
        case 'Embarazos':
            datos(genero,nombre,id);
        break;
        default:
        console.log('No se encontro');
       }    
    }
    function datos(genero,antecedente,id) {   
    
        $.ajax({
        type: "get",
        url: "./vistagenero/"+genero+"/"+antecedente,
        dataType: "json",
        beforeSend: () => {
            $("#tabla"+id).empty();  
            $("#"+id).addClass('animate__animated animate__backOutRight');  
            
                    
            }
        }).done(res => {
            $("#"+id).removeClass('animate__animated animate__backOutRight'); 
            $("#"+id).hide();           
            $('#tabla'+id).show(); 
              
            console.log(res);     
            $('#tabla'+id).append(`
    
            <table class="animate__animated animate__lightSpeedInLeft datatables-ajax table no-footer" id='tablaes${id}'style="width:100%">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido Paterno</th>
                            <th>Apellido Materno</th>
                            <th>Curp</th>
                            <th>Ver</th>
                        </tr>
                    </thead>
                    <tbody id='${id}'>
                    </tbody>
                   
                </table>
                <button class='btn btn-primary btn-sm waves-effect waves-light' id='atr' onclick="atras('${id}')"><img src="http://img.icons8.com/material-outlined/16/ffffff/back--v1.png"/></button>`);
               res.datos.forEach((item, i) => {
                var idbody="tablaes"+id+' tbody';                  
                $("#"+idbody).append(`
                                        <tr>
                                        <td>${item.nombre}</td>
                                         <td>${item.apellido_paterno}</td>    
                                         <td>${item.apellido_materno}</td> 
                                         <td>${item.CURP}</td>                   
                                         <td>
                                         <a href="./empleados/${item.CURP}" class="action-view actions_button">
                                            <i class="feather icon-eye"></i>
                                         </a>    
                                         </td>     
                                        </tr>
                                    `);
                    });  
                    nueva(id);  
                
                
                
        }).fail(err => {
           console.log(err);
           
            
       })
    }
    function nueva(id){
        $("#tablaes"+id).DataTable({
         responsive: !1,
        language: {
        "decimal": "",
        "emptyTable": "No hay información",
         "info": "",
         "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
         "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
         "thousands": ",",
        "lengthMenu": "_MENU_",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
        "first": "Primero",
        "last": "Ultimo",
         "next": "Siguiente",
         "previous": "Anterior"
      }
    }

});
}
    /*function MuestraDatos(genero,id){
     oculta=document.getElementById(id);
     console.log(oculta);
     muestra=document.getElementById('tabla');   
     console.log(muestra);  
     muestra.style.display='block';
     oculta.style.display='none';
     datos(genero);
     muestra.innerHTML += `<table class="table_estudios1 table" id='tabladatos' style="width:100%">
                                <thead>
                                    <tr>
                                     <th>Nombre</th>
                                         <th>Apellido Paterno</th>
                                         <th>Apellido Materno</th>
                                         <th>Curp</th> 
                                         <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>YAZMN  </td>
                                        <td>Carca</td>
                                        <td>carca</td>
                                        <td>CARSDKAS</td>
                                        <td>
                                        <a href="../vistagenero/mujer/Diabetes" class="action-view actions_button">
                                        <i class="feather icon-eye"></i>
                                        </a>
                                        </td>
                                    </tr>
                                </tbody>
                                </table>
                                <button class='btn btn-primary btn-sm waves-effect waves-light' id='atr' onclick="atras('${id}')">Atras</button>`;
        
        {!! $diabetes_m !!}
    }*/
    function atras(id){
        oculta2=document.getElementById('tabla'+id);    
        muestra2=document.getElementById(id);  
        oculta2.style.display='none';  
        muestra2.style.display='block';
       muestra2.classList.add('animate__animated','animate__lightSpeedInRight');
       
       
        tabla=document.getElementById('tabladatos');
        tabla.remove();
        boton=document.getElementById('atr');
        boton.remove();

    }
    
    function iniciarDiabetes() {
        var diabetes_m = {!! count($diabetes_m) !!};
        var diabetes_f = {!! count($diabetes_f) !!};
        iniciarGrafica("diabetes-chart",diabetes_m,diabetes_f,"diabetes");
    }

    function iniciarHipertension() {
        var hipertension_m = {!! count($hipertension_m) !!};
        var hipertension_f = {!! count($hipertension_f) !!};
        iniciarGrafica("hipertension-chart",hipertension_m,hipertension_f,"hipertension");
    }

    function iniciarCardiopatia() {
        var cardiopatia_m = {!! count($cardiopatia_m) !!};
        var cardiopatia_f = {!! count($cardiopatia_f) !!};
        iniciarGrafica("cardiopatia-chart",cardiopatia_m,cardiopatia_f,"cardiopatia");
    }

    function iniciarTiroides() {
        var tiroides_m = {!! count($tiroides_m) !!};
        var tiroides_f = {!! count($tiroides_f) !!};
        iniciarGrafica("tiroides-chart",tiroides_m,tiroides_f,"tiroides");
    }

    function iniciarEmbarazos() {
        let datos = {!! $datos_embarazo !!};
        let id="embarazos-chart";
        let chartDom = document.getElementById("embarazos-chart");
        let myChart = echarts.init(chartDom);
        let option;
        option= {
            
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend:{
                orient:"vertical",
                left:"left",
                data:['Embarazadas','No Embarazadas','Sin Registro']
            },
        
           /* toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: true, readOnly: false},
                    magicType : {
                        show: true,
                        type: ['pie', 'funnel']
                    },
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },*/
            calculable : true,
            series : [
                {
                    name:'Radius Mode',
                    type:'pie',
                    radius : [20, 110],
                    center : ['25%', 200],
                    roseType : 'radius',
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    lableLine: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    data:[
                        
                    ]
                },
                {
                    name:'Embarazos',
                    type:'pie',
                    radius : [30, 110],
                    center : ['50%', '50%'],
                    roseType : 'area',
                    data:[
                        {value:datos["embarazadas"], name:'Embarazadas'},
                        {value:datos["no"], name:'No Embarazadas'},
                        {value:datos["sr"], name:'Sin Registro'}
                    ],
                    itemStyle:{
                        emphasis:{
                            shadowBlur:10,
                            shadowOffsetX:0,
                            shadowColor:"rgba(0, 0, 0, 0.5)"
                        }
                    }
                }
            ]
        };
        /*option = {
            tooltip:{
                trigger:"item",
                formatter:"{a} <br/>{b} : {c} ({d}%)"
            },
            legend:{
                orient:"vertical",
                left:"left",
                data:[
                    "Embarazos",
                    "No embarazadas",
                    "Sin registro",
                ]
            },
            series:[
                {
                name:"Número",
                type: 'pie',                   
                radius: ['40%', '70%'],
                avoidLabelOverlap: false,
                itemStyle: {
                borderRadius: 10,
                borderColor: '#fff',
                borderWidth: 2
            },
                    center:[
                        "50%",
                        "50%"
                    ],
                    data:[
                        {
                            value:datos["embarazadas"],
                            name:"Embarazos",
                        },
                        {
                            value:datos["no"],
                            name:"No embarazadas",
                        },
                        {
                            value:datos["sr"],
                            name:"Sin registro",
                        },
                    ],
                    itemStyle:{
                        emphasis:{
                            shadowBlur:10,
                            shadowOffsetX:0,
                            shadowColor:"rgba(0, 0, 0, 0.5)"
                        }
                    }
                }
            ]
        };*/
        echarts.init(chartDom).setOption(option);
        myChart.on('click', function (params) {
            let genero;
            genero=params.data.name;
            if(genero=='No Embarazadas'){
                genero='No';
            }
            //alert(genero);
            let nombre;
            nombre=params.seriesName; 
            //alert(nombre);
            if(genero!='Sin Registro'){
            PasarDatos(nombre,genero,id);
            }
        });
        
    }
    function iniciarImc() {
        let datos = {!! $datos !!};
        let id="imc-chart";
        let chartDom = document.getElementById("imc-chart");
        let myChart = echarts.init(chartDom);
        let option;
            option= {
            
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend:{
                orient:"vertical",
                left:"left",
                data:['Insuficiencia ponderal','Intervalo normal','Sopreso','Preobesidad','Obesidad','Obesidad clase 1','Obesidad clase 2','Obesidad clase 3','Sin IMC']
            },
        
           /* toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: true, readOnly: false},
                    magicType : {
                        show: true,
                        type: ['pie', 'funnel']
                    },
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },*/
            calculable : true,
            series : [
                {
                    name:'Radius Mode',
                    type:'pie',
                    radius : [20, 110],
                    center : ['25%', 200],
                    roseType : 'radius',
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    lableLine: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    data:[
                        
                    ]
                },
                {
                    name:'IMC',
                    type:'pie',
                    radius : [30, 110],
                    center : ['50%', '50%'],
                    roseType : 'area',
                    data:[
                        {value:datos["Insuficiencia ponderal"]["numero"], name:'Insuficiencia ponderal'},
                        {value:datos["Intervalo normal"]["numero"], name:'Intervalo normal'},
                        {value:datos["Sopreso"]["numero"], name:'Sopreso'},
                        {value:datos["Preobesidad"]["numero"], name:'Preobesidad'},
                        {value:datos["Obesidad"]["numero"], name:'Obesidad'},
                        {value:datos["Obesidad clase 1"]["numero"], name:'Obesidad clase 1'},
                        {value:datos["Obesidad clase 2"]["numero"], name:'Obesidad clase 2'},
                        {value:datos["Obesidad clase 3"]["numero"], name:'Obesidad clase 3'},
                        {value:datos["Sin IMC"]["numero"], name:'Sin IMC'}
                    ],
                    itemStyle:{
                        emphasis:{
                            shadowBlur:10,
                            shadowOffsetX:0,
                            shadowColor:"rgba(0, 0, 0, 0.5)"
                        }
                    }
                }
            ]
        };
        /*option = {
            tooltip:{
                trigger:"item",
                formatter:"{a} <br/>{b} : {c} ({d}%)"
            },
            legend:{
                orient:"vertical",
                left:"left",
                data:[
                    "Insuficiencia ponderal",
                    "Intervalo normal",
                    "Sopreso",
                    "Preobesidad",
                    "Obesidad",
                    "Obesidad clase 1",
                    "Obesidad clase 2",
                    "Obesidad clase 3",
                    "Sin IMC",
                ]
            },
            series:[
                {
                    name:"Indice de masa corporal",
                    type:"pie",
                    radius:"50%",
                    center:[
                        "50%",
                        "50%"
                    ],
                    data:[
                        {
                            value:datos["Insuficiencia ponderal"]["numero"],
                            name:"Insuficiencia ponderal",
                            itemStyle: {
                                color: '#c8ca3c'
                            }
                        },
                        {
                            value:datos["Intervalo normal"]["numero"],
                            name:"Intervalo normal",
                            itemStyle: {
                                color: '#7ae078'
                            }
                        },
                        {
                            value:datos["Sopreso"]["numero"],
                            name:"Sopreso",
                            itemStyle: {
                                color: '#fe9901'
                            }
                        },
                        {
                            value:datos["Preobesidad"]["numero"],
                            name:"Preobesidad",
                            itemStyle: {
                                color: '#ad6800'
                            }
                        },
                        {
                            value:datos["Obesidad"]["numero"],
                            name:"Obesidad",
                            itemStyle: {
                                color: '#ff3f00'
                            }
                        },
                        {
                            value:datos["Obesidad clase 1"]["numero"],
                            name:"Obesidad clase 1",
                            itemStyle: {
                                color: '#dc3600'
                            }
                        },
                        {
                            value:datos["Obesidad clase 2"]["numero"],
                            name:"Obesidad clase 2",
                            itemStyle: {
                                color: '#cd3301'
                            }
                        },
                        {
                            value:datos["Obesidad clase 3"]["numero"],
                            name:"Obesidad clase 3",
                            itemStyle: {
                                color: '#6b1a00'
                            }
                        },
                        {
                            value:datos["Sin IMC"]["numero"],
                            name:"Sin IMC",
                            itemStyle: {
                                color: '#4e4e4e'
                            }
                        },
                    ],
                    itemStyle:{
                        emphasis:{
                            shadowBlur:10,
                            shadowOffsetX:0,
                            shadowColor:"rgba(0, 0, 0, 0.5)"
                        }
                    }
                }
            ]
        };*/
        echarts.init(chartDom).setOption(option);
        myChart.on('click', function (params) {
           
            let genero;
            genero=params.data.name;
            //alert(genero);
            let nombre;
            nombre=params.seriesName; 
            //alert(nombre);
            if(genero!='Sin IMC'){
            PasarDatos(nombre,genero,id);
            }
            
        });
    }
</script>
@endsection

