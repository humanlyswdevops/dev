@extends('layouts.Vuexy')
@section('title')
  Indicadores Consultorio
@endsection
@section('begin_vendor_css')
    <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('content')

<div class="row">
    <div class="col-xl-3 col-md-4 col-sm-6">
      <div class="card text-center">
        <div class="card-content">
          <div class="card-body">
            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
              <div class="avatar-content">
                <i class="feather icon-users text-info font-medium-5"></i>
              </div>
            </div>
            <h2 class="text-bold-700">{{ $medicos }}</h2>
            <p class="mb-0 line-ellipsis">Total de Miembros</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <iframe width="100%" height="500.5" src="https://app.powerbi.com/view?r=eyJrIjoiMmYwM2ZkNzgtYTVkOC00YzA2LWI1OTEtZWY0YWIxZjJhOTU5IiwidCI6IjRmOTUxODRhLTFlMjQtNDQ5Mi04YWFhLTQ1NzE0NTRhNDkzYSJ9" frameborder="0" allowFullScreen="true"></iframe>
    </div>
</div>

@endsection
@section('page_vendor_js')
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
@endsection

@section('js_custom')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>

  <script src="{!! asset('public/js/empresa/indicadores/consultorio.js') !!}"></script>

@endsection
