<fieldset>
  <div class="row">
    @php
    $evolucion = array('Alta' => 'Alta',
    'Tratamiento' => 'En tratamiento/Referencia/Seguimiento domiciliario/Seguimiento terminado',
    'Caso_grave' => 'Caso grave',
    'Caso_no_grave' => 'Caso no grave',
    'Defunción' => 'Defunción');
    @endphp

    <div class="col-md-3">
      <div class="form-group">
        Evolucion:
        <select name="evolucion" class="form-control" id="evolucion">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($evolucion as $key => $value)
              @if ($estudioCaso->evolucion == $key)
                <option selected value={{$key}}>{{$value}}</option>
              @endif
            @endforeach
          @else
          @foreach ($evolucion as $key => $value)
          <option value={{$key}}>{{$value}}</option>
          @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        Si el caso se da de alta:
      </div>
    </div>
    @php
    $evolucionAlta = array('Mejoria' => 'Mejoria',
    'curacion' => 'Curacion',
    'Voluntaria' => 'Voluntaria',
    'Traslado' => 'Traslado' );
    @endphp
    <div class="col-md-3">
      <div class="form-group">
        Especifique la evolucion:
        <select name="casoAlta" class="form-control" id="casoAlta">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($evolucionAlta as $key => $value)
              @if ($estudioCaso->casoAlta == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
          @else
            @foreach ($evolucionAlta as $key => $value)
            <option value={{$value}}>{{$value}}</option>
            @endforeach
        @endif
        </select>
      </div>
    </div>

    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿El caso esta o estuvo ingresado en la UCI durante la enfermedad?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->casoIngUci == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="casoIngUci" id="IngresadoUCI_SI">
            <label class="custom-control-label" for="IngresadoUCI_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="casoIngUci" id="IngresadoUCI_No">
            <label class="custom-control-label" for="IngresadoUCI_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="casoIngUci" id="IngresadoUCI_SI">
            <label class="custom-control-label" for="IngresadoUCI_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="casoIngUci" id="IngresadoUCI_No">
            <label class="custom-control-label" for="IngresadoUCI_No">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿El caso esta o estuvo intubado en algun momento durante la enfermedad?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->casoIntubado == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="casoIntubado" id="entubado_SI">
            <label class="custom-control-label" for="entubado_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="casoIntubado" id="entubado_No">
            <label class="custom-control-label" for="entubado_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="casoIntubado" id="entubado_SI">
            <label class="custom-control-label" for="entubado_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="casoIntubado" id="entubado_No">
            <label class="custom-control-label" for="entubado_No">No</label>
          </div>
        </div>
      </div>
      @endif


    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿El caso tiene o tuvo diagnostico de neumonia durante la enfermedad?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->casoNeumonia == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="casoNeumonia" id="neumonia_SI">
            <label class="custom-control-label" for="neumonia_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="casoNeumonia" id="neumonia_No">
            <label class="custom-control-label" for="neumonia_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="casoNeumonia" id="neumonia_SI">
            <label class="custom-control-label" for="neumonia_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="casoNeumonia" id="neumonia_No">
            <label class="custom-control-label" for="neumonia_No">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="col-md-3">
      <div class="form-group">
        Fecha de egreso
        @if (isset($estudioCaso))
          <input type="date" class="form-control" id="fechaEgreso" name="fechaEgreso" value="{{$estudioCaso->fechaEgreso}}">
        @else
          <input type="date" class="form-control" id="fechaEgreso" name="fechaEgreso">
        @endif
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        Defuncion:
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        Fecha de defuncion
        @if (isset($estudioCaso))
          <input type="date"  class="form-control" id="fechaDefuncion" name="fechaDefuncion" value="{{$estudioCaso->fechaDefuncion}}">
        @else
          <input type="date" class="form-control" id="fechaDefuncion" name="fechaDefuncion">
        @endif
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        Folio de certificado de defuncion
        @if (isset($estudioCaso))
          <input type="text"  name="folioCertDefuncion" class="form-control" value="{{$estudioCaso->folioCertDefuncion}}">
        @else
          <input type="text" name="folioCertDefuncion" class="form-control">
        @endif
      </div>
    </div>
    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">Defuncion por influenza</label>
        </div>
      </div>

      @if (isset($estudioCaso) && $estudioCaso->defuncionInfluenza == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="defuncionInfluenza" id="Defuncion_influenza_SI">
            <label class="custom-control-label" for="Defuncion_influenza_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="defuncionInfluenza" id="Defuncion_influenza_No">
            <label class="custom-control-label" for="Defuncion_influenza_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="defuncionInfluenza" id="Defuncion_influenza_SI">
            <label class="custom-control-label" for="Defuncion_influenza_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="defuncionInfluenza" id="Defuncion_influenza_No">
            <label class="custom-control-label" for="Defuncion_influenza_No">No</label>
          </div>
        </div>
      </div>
      @endif

      <div class="col-md-12">
        <div class="form-group">
          Anexar copia de certificado de defuncion si cumple con definicion operacional de defuncion por influenza o defuncion con influenza
          <input type="file" name="Archivo" id="Archivo" class="form-control">
        </div>
      </div>
    </div>
</fieldset>
