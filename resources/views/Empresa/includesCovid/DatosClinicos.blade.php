<fieldset>
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        Servicio de ingreso <br>
        @if (isset($estudioCaso) )
          <input type="text"  name="servIngreso" class="form-control" value="{{$estudioCaso->servIngreso}}">
        @else
          <input type="text" name="servIngreso" class="form-control">
        @endif
      </div>
    </div>
    @php
      $tipoPaciente = array('Ambulatorio' => 'Ambulatorio',
              'Hospitalizado' => 'Hospitalizado');
    @endphp
    <div class="col-md-3">
      <div class="form-group">
        Tipo de paciente <br>
        <select name="tipoPaciente" class="form-control" id="tipoPac">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso) )
            @foreach ($tipoPaciente as $key => $value)
              @if ($estudioCaso->tipoPaciente == $value)
                <option  selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
          @else
            @foreach ($tipoPaciente as $key => $value)
              <option value={{$value}}>{{$value}}</option>
            @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Hospital:
        @if (isset($estudioCaso) )
          <input type="text"  name="hospital" class="form-control" value="{{$estudioCaso->hospital}}">
        @else
          <input type="text" name="hospital" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Fecha de ingreso a la unidad <br>
        @if (isset($estudioCaso) )
          <input type="date" class="form-control" name="fechaIngUnidad" id="fechaIngUnidad" value="{{$estudioCaso->fechaIngUnidad}}">
        @else
          <input type="date" class="form-control" name="fechaIngUnidad" id="fechaIngUnidad">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Fecha de inicio de sintomas <br>
        @if (isset($estudioCaso) )
          <input type="date"  class="form-control" name="fechaInicioSintomas" id="fechaInicioSintomas" value="{{$estudioCaso->fechaInicioSintomas}}">
        @else
          <input type="date" class="form-control" name="fechaInicioSintomas" id="fechaInicioSintomas">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        A partir de la fecha de inicio de sintomas <br>
        @if (isset($estudioCaso) )
          <input type="tex"  name="aPartirFecIniSintomas" class="form-control" value="{{$estudioCaso->aPartirFecIniSintomas}}">
        @else
          <input type="tex" name="aPartirFecIniSintomas" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-7">
      <div class="form-group">
        ¿Tiene o ha tenido alguno de los siguientes signos y sintomas? <br>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label for="">Co-morbilidad:</label>
      </div>
    </div>
    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Inicio subito de los sintomas:</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->subitoSintomas == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio"  class="custom-control-input bg-success" checked value="Si" name="subitoSintomas" id="inicio_enfermo">
            <label class="custom-control-label" for="inicio_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"  value="No" name="subitoSintomas" id="Inicio_sano">
            <label class="custom-control-label" for="Inicio_sano">No</label>
          </div>
        </div>
        </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="subitoSintomas" id="inicio_enfermo">
            <label class="custom-control-label" for="inicio_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"checked value="No" name="subitoSintomas" id="Inicio_sano">
            <label class="custom-control-label" for="Inicio_sano">No</label>
          </div>
        </div>
        </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Diabetes:</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->diabetes == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="diabetes" id="Diabetes_enfermo">
            <label class="custom-control-label" for="Diabetes_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="diabetes" id="Diabetes_sano">
            <label class="custom-control-label" for="Diabetes_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="diabetes" id="Diabetes_enfermo">
            <label class="custom-control-label" for="Diabetes_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="diabetes" id="Diabetes_sano">
            <label class="custom-control-label" for="Diabetes_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Fiebre</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->fiebre == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="fiebre" id="Fiebre_enfermo">
            <label class="custom-control-label" for="Fiebre_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"  value="No" name="fiebre" id="Fiebre_sano">
            <label class="custom-control-label" for="Fiebre_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="fiebre" id="Fiebre_enfermo">
            <label class="custom-control-label" for="Fiebre_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="fiebre" id="Fiebre_sano">
            <label class="custom-control-label" for="Fiebre_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">EPOC</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->epoc == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="epoc" id="EPOC_enfermo">
            <label class="custom-control-label" for="EPOC_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"  value="No" name="epoc" id="EPOC_sano">
            <label class="custom-control-label" for="EPOC_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="epoc" id="EPOC_enfermo">
            <label class="custom-control-label" for="EPOC_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="epoc" id="EPOC_sano">
            <label class="custom-control-label" for="EPOC_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Tos</label>
        </div>
      </div>

      @if (isset($estudioCaso) && $estudioCaso->tos == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="tos" id="Tos_enfermo">
            <label class="custom-control-label" for="Tos_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"  value="No" name="tos" id="Tos_sano">
            <label class="custom-control-label" for="Tos_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="tos" id="Tos_enfermo">
            <label class="custom-control-label" for="Tos_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="tos" id="Tos_sano">
            <label class="custom-control-label" for="Tos_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Asma</label>
        </div>
      </div>

      @if (isset($estudioCaso) && $estudioCaso->asma == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="asma" id="Asma_enfermo">
            <label class="custom-control-label" for="Asma_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="asma" id="Asma_sano">
            <label class="custom-control-label" for="Asma_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="asma" id="Asma_enfermo">
            <label class="custom-control-label" for="Asma_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="asma" id="Asma_sano">
            <label class="custom-control-label" for="Asma_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Cefalea</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->cefalea == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="cefalea" id="Cefalea_enfermo">
            <label class="custom-control-label" for="Cefalea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="cefalea" id="Cefalea_sano">
            <label class="custom-control-label" for="Cefalea_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="cefalea" id="Cefalea_enfermo">
            <label class="custom-control-label" for="Cefalea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="cefalea" id="Cefalea_sano">
            <label class="custom-control-label" for="Cefalea_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Inmunosupresion</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->inmunosupresion == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="inmunosupresion" id="Inmunosupresion_enfermo">
            <label class="custom-control-label" for="Inmunosupresion_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="inmunosupresion" id="Inmunosupresion_sano">
            <label class="custom-control-label" for="Inmunosupresion_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="inmunosupresion" id="Inmunosupresion_enfermo">
            <label class="custom-control-label" for="Inmunosupresion_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="inmunosupresion" id="Inmunosupresion_sano">
            <label class="custom-control-label" for="Inmunosupresion_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Disnea</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->disnea == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="disnea" id="Disnea_enfermo">
            <label class="custom-control-label" for="Disnea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="disnea" id="Disnea_sano">
            <label class="custom-control-label" for="Disnea_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="disnea" id="Disnea_enfermo">
            <label class="custom-control-label" for="Disnea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="disnea" id="Disnea_sano">
            <label class="custom-control-label" for="Disnea_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Hipertension</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->hipertension == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="hipertension" id="Hipertension_enfermo">
            <label class="custom-control-label" for="Hipertension_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="hipertension" id="Hipertension_sano">
            <label class="custom-control-label" for="Hipertension_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="hipertension" id="Hipertension_enfermo">
            <label class="custom-control-label" for="Hipertension_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="hipertension" id="Hipertension_sano">
            <label class="custom-control-label" for="Hipertension_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Irritabilidad</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->irritabilidad == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="irritabilidad" id="Irritabilidad_enfermo">
            <label class="custom-control-label" for="Irritabilidad_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"  value="No" name="irritabilidad" id="Irritabilidad_sano">
            <label class="custom-control-label" for="Irritabilidad_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="irritabilidad" id="Irritabilidad_enfermo">
            <label class="custom-control-label" for="Irritabilidad_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="irritabilidad" id="Irritabilidad_sano">
            <label class="custom-control-label" for="Irritabilidad_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">VIH/SIDA</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->vihSida == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="vihSida" id="sida_enfermo">
            <label class="custom-control-label" for="sida_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="vihSida" id="sida_sano">
            <label class="custom-control-label" for="sida_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="vihSida" id="sida_enfermo">
            <label class="custom-control-label" for="sida_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="vihSida" id="sida_sano">
            <label class="custom-control-label" for="sida_sano">No</label>
          </div>
        </div>
      </div>>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Diarrea</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->diarrea == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="diarrea" id="Diarrea_enfermo">
            <label class="custom-control-label" for="Diarrea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="diarrea" id="Diarrea_sano">
            <label class="custom-control-label" for="Diarrea_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="diarrea" id="Diarrea_enfermo">
            <label class="custom-control-label" for="Diarrea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="diarrea" id="Diarrea_sano">
            <label class="custom-control-label" for="Diarrea_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Otra condicion</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->otraCondicion == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="otraCondicion" id="Otra_Condicion_enfermo">
            <label class="custom-control-label" for="Otra_Condicion_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="otraCondicion" id="Otra_Condicion_sano">
            <label class="custom-control-label" for="Otra_Condicion_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="otraCondicion" id="Otra_Condicion_enfermo">
            <label class="custom-control-label" for="Otra_Condicion_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="otraCondicion" id="Otra_Condicion_sano">
            <label class="custom-control-label" for="Otra_Condicion_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Dolor toracico</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->dolorToracico == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="dolorToracico" id="Dolor_toracico_enfermo">
            <label class="custom-control-label" for="Dolor_toracico_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="dolorToracico" id="Dolor_toracico_sano">
            <label class="custom-control-label" for="Dolor_toracico_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="dolorToracico" id="Dolor_toracico_enfermo">
            <label class="custom-control-label" for="Dolor_toracico_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="dolorToracico" id="Dolor_toracico_sano">
            <label class="custom-control-label" for="Dolor_toracico_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Enfermedad cardiovascular</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->enfCardiovascular == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="enfCardiovascular" id="Cardiovascular_enfermo">
            <label class="custom-control-label" for="Cardiovascular_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="enfCardiovascular" id="Cardiovascular_sano">
            <label class="custom-control-label" for="Cardiovascular_sano">No</label>
          </div>
        </div>
        </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="enfCardiovascular" id="Cardiovascular_enfermo">
            <label class="custom-control-label" for="Cardiovascular_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="enfCardiovascular" id="Cardiovascular_sano">
            <label class="custom-control-label" for="Cardiovascular_sano">No</label>
          </div>
        </div>
        </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Escalofrios</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->escalofrios == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="escalofrios" id="Escalofrios_enfermo">
            <label class="custom-control-label" for="Escalofrios_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="escalofrios" id="Escalofrios_sano">
            <label class="custom-control-label" for="Escalofrios_sano">No</label>
          </div>
        </div>
        </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="escalofrios" id="Escalofrios_enfermo">
            <label class="custom-control-label" for="Escalofrios_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="escalofrios" id="Escalofrios_sano">
            <label class="custom-control-label" for="Escalofrios_sano">No</label>
          </div>
        </div>
        </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Obesidad</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->obesidad == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="obesidad" id="obesidad_enfermo">
            <label class="custom-control-label" for="obesidad_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="obesidad" id="obesidad_sano">
            <label class="custom-control-label" for="obesidad_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="obesidad" id="obesidad_enfermo">
            <label class="custom-control-label" for="obesidad_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="obesidad" id="obesidad_sano">
            <label class="custom-control-label" for="obesidad_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Odinofagia</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->odinofagia == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="odinofagia" id="Odinofagia_enfermo">
            <label class="custom-control-label" for="Odinofagia_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="odinofagia" id="Odinofagia_sano">
            <label class="custom-control-label" for="Odinofagia_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="odinofagia" id="Odinofagia_enfermo">
            <label class="custom-control-label" for="Odinofagia_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="odinofagia" id="Odinofagia_sano">
            <label class="custom-control-label" for="Odinofagia_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Insuficiencia renal cronica</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->insufRenalCronica == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="insufRenalCronica" id="Irc_enfermo">
            <label class="custom-control-label" for="Irc_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"  value="No" name="insufRenalCronica" id="Irc_sano">
            <label class="custom-control-label" for="Irc_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="insufRenalCronica" id="Irc_enfermo">
            <label class="custom-control-label" for="Irc_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="insufRenalCronica" id="Irc_sano">
            <label class="custom-control-label" for="Irc_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Mialgias</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->mialgias == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="mialgias" id="Mialgias_enfermo">
            <label class="custom-control-label" for="Mialgias_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="mialgias" id="Mialgias_sano">
            <label class="custom-control-label" for="Mialgias_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="mialgias" id="Mialgias_enfermo">
            <label class="custom-control-label" for="Mialgias_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="mialgias" id="Mialgias_sano">
            <label class="custom-control-label" for="Mialgias_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Tabaquismo</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->tabaquismo == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="tabaquismo" id="Tabaquismo_enfermo">
            <label class="custom-control-label" for="Tabaquismo_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="tabaquismo" id="Tabaquismo_sano">
            <label class="custom-control-label" for="Tabaquismo_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="tabaquismo" id="Tabaquismo_enfermo">
            <label class="custom-control-label" for="Tabaquismo_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="tabaquismo" id="Tabaquismo_sano">
            <label class="custom-control-label" for="Tabaquismo_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Artralgias</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->artralgias == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="artralgias" id="Artralgias_enfermo">
            <label class="custom-control-label" for="Artralgias_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="artralgias" id="Artralgias_sano">
            <label class="custom-control-label" for="Artralgias_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="artralgias" id="Artralgias_enfermo">
            <label class="custom-control-label" for="Artralgias_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="artralgias" id="Artralgias_sano">
            <label class="custom-control-label" for="Artralgias_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Otros</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->otros == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="otros" id="Otros_enfermo">
            <label class="custom-control-label" for="Otros_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="otros" id="Otros_sano">
            <label class="custom-control-label" for="Otros_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="otros" id="Otros_enfermo">
            <label class="custom-control-label" for="Otros_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="otros" id="Otros_sano">
            <label class="custom-control-label" for="Otros_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Ataque al estado general</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->ataqueEstGeneral == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="ataqueEstGeneral" id="Ataque_general_enfermo">
            <label class="custom-control-label" for="Ataque_general_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="ataqueEstGeneral" id="Ataque_general_enfermo_sano">
            <label class="custom-control-label" for="Ataque_general_enfermo_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="ataqueEstGeneral" id="Ataque_general_enfermo">
            <label class="custom-control-label" for="Ataque_general_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="ataqueEstGeneral" id="Ataque_general_enfermo_sano">
            <label class="custom-control-label" for="Ataque_general_enfermo_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">
      <div class="col-md-6">
        Especifique otros
        @if (isset($estudioCaso))
          <input type="text"  name="espefiqueOtros" class="form-control" value="{{$estudioCaso->espefiqueOtros}}">
          @else
          <input type="text" name="espefiqueOtros" class="form-control">
        @endif
      </div>
    </div>

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Rinorrea</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->rinorrea == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="rinorrea" id="Rinorrea_enfermo">
            <label class="custom-control-label" for="Rinorrea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="rinorrea" id="Rinorrea_sano">
            <label class="custom-control-label" for="Rinorrea_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="rinorrea" id="Rinorrea_enfermo">
            <label class="custom-control-label" for="Rinorrea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="rinorrea" id="Rinorrea_sano">
            <label class="custom-control-label" for="Rinorrea_sano">No</label>
          </div>
        </div>
      </div>
      @endif


    <div class="row col-md-6">

    </div>

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Polipnea</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->polipnea == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="polipnea" id="Polipnea_enfermo">
            <label class="custom-control-label" for="Polipnea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="polipnea" id="Polipnea_sano">
            <label class="custom-control-label" for="Polipnea_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="polipnea" id="Polipnea_enfermo">
            <label class="custom-control-label" for="Polipnea_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="polipnea" id="Polipnea_sano">
            <label class="custom-control-label" for="Polipnea_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">

    </div>

    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Vomito</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->vomito == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="vomito" id="Vomito_enfermo">
            <label class="custom-control-label" for="Vomito_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="vomito" id="Vomito_sano">
            <label class="custom-control-label" for="Vomito_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="vomito" id="Vomito_enfermo">
            <label class="custom-control-label" for="Vomito_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="vomito" id="Vomito_sano">
            <label class="custom-control-label" for="Vomito_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">

    </div>
    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Dolor abdominal</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->dolorAbdominal == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="dolorAbdominal" id="Dolor_Abdominal_enfermo">
            <label class="custom-control-label" for="Dolor_Abdominal_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="dolorAbdominal" id="Dolor_Abdominal_sano">
            <label class="custom-control-label" for="Dolor_Abdominal_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="dolorAbdominal" id="Dolor_Abdominal_enfermo">
            <label class="custom-control-label" for="Dolor_Abdominal_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="dolorAbdominal" id="Dolor_Abdominal_sano">
            <label class="custom-control-label" for="Dolor_Abdominal_sano">No</label>
          </div>
        </div>
      </div>
      @endif


    <div class="row col-md-6">

    </div>
    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Conjuntivitis</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->conjuntivitis == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="conjuntivitis" id="conjuntivitis_enfermo">
            <label class="custom-control-label" for="conjuntivitis_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="conjuntivitis" id="conjuntivitis_sano">
            <label class="custom-control-label" for="conjuntivitis_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="conjuntivitis" id="conjuntivitis_enfermo">
            <label class="custom-control-label" for="conjuntivitis_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="conjuntivitis" id="conjuntivitis_sano">
            <label class="custom-control-label" for="conjuntivitis_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-6">

    </div>
    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Cianosis</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->cianosis == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="cianosis" id="Cianosis_enfermo">
            <label class="custom-control-label" for="Cianosis_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="cianosis" id="Cianosis_sano">
            <label class="custom-control-label" for="Cianosis_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="cianosis" id="Cianosis_enfermo">
            <label class="custom-control-label" for="Cianosis_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="cianosis" id="Cianosis_sano">
            <label class="custom-control-label" for="Cianosis_sano">No</label>
          </div>
        </div>
      </div>
      @endif


    <div class="row col-md-6">

    </div>
    <div class="row col-md-6">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">Otro</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->otroSintoma == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="otroSintoma" id="Otro_enfermo">
            <label class="custom-control-label" for="Otro_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="otroSintoma" id="Otro_sano">
            <label class="custom-control-label" for="Otro_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="otroSintoma" id="Otro_enfermo">
            <label class="custom-control-label" for="Otro_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="otroSintoma" id="Otro_sano">
            <label class="custom-control-label" for="Otro_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="col-md-12">
      <div class="form-group">
        Diagnostico probable:
      </div>
    </div>
    <div class="row col-md-12">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">1.Enfermedad tipo influenza (ETI)</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->enfTipoInfuenza == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="enfTipoInfuenza" id="influenza_enfermo">
            <label class="custom-control-label" for="influenza_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="enfTipoInfuenza" id="influenza_sano">
            <label class="custom-control-label" for="influenza_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="enfTipoInfuenza" id="influenza_enfermo">
            <label class="custom-control-label" for="influenza_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="enfTipoInfuenza" id="influenza_sano">
            <label class="custom-control-label" for="influenza_sano">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-12">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">2.Infeccion respiratoria aguda grave (IRAG)</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->infRespAguda == 'Si')
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="infRespAguda" id="infeccion_respiratoria_enfermo">
            <label class="custom-control-label" for="infeccion_respiratoria_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="infRespAguda" id="infeccion_respiratoria_sano">
            <label class="custom-control-label" for="infeccion_respiratoria_sano">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-3">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="infRespAguda" id="infeccion_respiratoria_enfermo">
            <label class="custom-control-label" for="infeccion_respiratoria_enfermo">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="infRespAguda" id="infeccion_respiratoria_sano">
            <label class="custom-control-label" for="infeccion_respiratoria_sano">No</label>
          </div>
        </div>
      </div>

      @endif


</fieldset>
