<div class="row">

    <div class="col-12">
        <h4 class="float-left secondary">Personalizar Historial Clínico</h4>
        @if(\Auth::user()->usuarioPrincipal() || isset(Session::get('Historial Clínico')['insert']))
          <button id="btn_antecedenteFormModal_open" type="button" class="btn btn-sm btn-primary float-right">Agregar</button>
        @endif

    </div>

    <div class="col-12">
        <hr>
    </div>
    <div class="col-sm-12 mt-2 mt-sm-2">
        <div id="accordionWrapa50" role="tablist" aria-multiselectable="true">
            <div class="card collapse-icon accordion-icon-rotate">
                <div class="card-content">
                    <div class="card-body p-0">
                        <div class="accordion" id="antecedentes_accordion">
                            @foreach ($antecedentes_form as $antecedente)
                            <div class="collapse-border-item collapse-header card">
                                <div>
                                    <div style="cursor: default !important;" class="card-header" id="heading{{ $antecedente->id }}">
                                        <span class="lead collapse-title">{!! $antecedente->nombre !!}</span>
                                        <div>
                                          @if(\Auth::user()->usuarioPrincipal() || isset(Session::get('Historial Clínico')['insert']))
                                            <button class="add_answer mr-3 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light"
                                            type="button"
                                            data-id="{{ $antecedente->id }}">
                                                <i class="feather icon-plus"></i>
                                            </button>
                                          @endif

                                            <div class="cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse{{ $antecedente->id }}" aria-expanded="false" aria-controls="collapse{{ $antecedente->id }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse{{ $antecedente->id }}" class="collapse" aria-labelledby="heading{{ $antecedente->id }}" data-parent="#antecedentes_accordion">
                                    <div class="card-body">
                                        @if ($antecedente->antecedentesAnswer->count() > 0)
                                            <ul class="list-group pl-1 pl-sm-3">
                                                @foreach ($antecedente->antecedentesAnswer as $answer)
                                                    @if($answer->user_id == Auth::user()->id || $answer->user_id == 0)
                                                        <li class="list-group-item d-flex justify-content-between align-items-center" id="answer{{ $answer->id }}">
                                                            <span> {!! $answer->pregunta !!}</span>
                                                            @if($answer->user_id > 0)
                                                            <div>
                                                                <button data-id="{{ $answer->id }}" class="edit_answer mr-1 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">
                                                                    <i class="feather icon-edit"></i>
                                                                </button>
                                                                <button data-id="{{ $answer->id }}" class="trash_answer btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">
                                                                    <i class="feather icon-trash"></i>
                                                                </button>
                                                            </div>
                                                            @endif
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        @else
                                        <h5>Aún no se han creado preguntas vinculadas</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @foreach ($mis_antecedentes_form as $item)
                            <div class="collapse-border-item collapse-header card">
                                <div>
                                    <div style="cursor: default !important;" class="card-header" id="heading{{ $item->id }}">
                                        <span class="lead collapse-title">{!! $item->nombre !!}</span>
                                        <div>
                                            <button class="add_answer mr-1 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light"
                                            type="button"
                                            data-id="{{ $item->id }}">
                                                <i class="feather icon-plus"></i>
                                            </button>
                                            <button class="edit_form mr-1 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light"
                                            type="button"
                                            data-id="{{ $item->id }}">
                                                <i class="feather icon-edit"></i>
                                            </button>
                                            <button class="trash_form mr-3 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light"
                                            type="button"
                                            data-id="{{ $item->id }}">
                                                <i class="feather icon-trash"></i>
                                            </button>
                                            <div class="cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse{{ $item->id }}" aria-expanded="false" aria-controls="collapse{{ $item->id }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse{{ $item->id }}" class="collapse" aria-labelledby="heading{{ $item->id }}" data-parent="#antecedentes_accordion">
                                    <div class="card-body">
                                        @if ($item->antecedentesAnswer->count() > 0)
                                            <ul class="list-group pl-1 pl-sm-3">
                                                @foreach ($item->antecedentesAnswer as $answer)
                                                    <li class="list-group-item d-flex justify-content-between align-items-center" id="answer{{ $answer->id }}">
                                                        <span> {!! $answer->pregunta !!}</span>
                                                        <div>
                                                            <button data-id="{{ $answer->id }}" class="edit_answer mr-1 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">
                                                                <i class="feather icon-edit"></i>
                                                            </button>
                                                            <button data-id="{{ $answer->id }}" class="trash_answer btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">
                                                                <i class="feather icon-trash"></i>
                                                            </button>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @else
                                        <h5>Aún no se han creado preguntas vinculadas</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            {{-- <div class="collapse-border-item card collapse-header">
                                <div class="card-header" id="heading210" data-toggle="collapse" role="button" data-target="#collapse210"
                                    aria-expanded="false" aria-controls="collapse210">
                                    <span class="lead collapse-title">
                                    Accordion Item 2
                                    </span>
                                </div>
                                <div id="collapse210" class="collapse" aria-labelledby="heading210" data-parent="#accordionExample0">
                                    <div class="card-body">
                                    Jelly tootsie roll sugar plum sesame snaps apple pie. Icing donut pie sesame snaps. Bonbon gummi
                                    bears carrot cake muffin chocolate bar.

                                    Cupcake pastry candy bonbon. Sesame snaps dragée biscuit chocolate bar candy canes sesame snaps.
                                    Lemon drops cake lollipop pastry tart macaroon gummi bears. Powder cheesecake macaroon candy canes
                                    dessert bonbon bonbon candy canes.
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




  <!-- Accordion with border end -->
{{-- Modal para crear un nuevo form antecedente --}}
<div id="antecedenteFormModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Crear nuevo antecedente</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_add_antecedenteForm">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nombre_antecedente">Nombre</label>
                            <input type="text" name="nombre_antecedente"  class="form-control" id="nombre_antecedente" required>
                        </div>
                    </div>
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_antecendenteForm_modal">
                            <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true" style="display: none;"></span>
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Modal para editar un form antecedente --}}
<div id="antecedenteFormModalEdit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Editar antecedente</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_edit_antecedenteForm">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nombre_antecedente_edit">Nombre</label>
                            <input type="text" name="nombre_antecedente_edit"  class="form-control" id="nombre_antecedente_edit" required>
                            <input type="hidden" name="antecedente_form_edit_id" id="antecedente_form_edit_id" value='0'>
                        </div>
                    </div>
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_antecendenteFormEdit_modal">
                            <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true" style="display: none;"></span>
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Modal para eliminar un form antecedente --}}
<div id="antecedenteFormModalDelete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Eliminar antecedente</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_delete_antecedenteForm">
                    <div class="form-row">
                        <div class="col-12">
                          <div class="alert alert-primary">
                            <h2 class="alert-heading">
                              Estas a punto de eliminar el antecedente:
                              <strong id="nombre_delete_form"></strong>
                            </h2>
                            <p>Al eliminar este antecedente, se eliminaran de forma automatica las preguntas vinculadas.</p>
                          </div>

                        </div>
                        <div class="form-group col-md-12">
                            <input type="hidden" name="antecedente_form_delete_id" id="antecedente_form_delete_id" value='0'>
                        </div>
                    </div>
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_antecedenteFormModal_delete">
                            <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true" style="display: none;"></span>
                            Eliminar
                        </button>
                        <button type="button" class="btn btn-sm btn-secondary" class="close" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- MODAL PARA CREAR LOS ANSWER --}}
<div id="antecedenteAnswerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Añadir pregunta</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="answer_form_antecedente">
                    <div class="form-row">
                        <div class="col-12">
                          <div class="alert alert-secondary">
                            <p>Estas a punto de agregar una pregunta al siguiente antecedente: <strong id="form_answer_nombre"></strong></p>
                          </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="nombre_answer">Nombre</label>
                            <input type="text" name="nombre_answer"  class="form-control" id="nombre_answer" required>
                            <input type="hidden" name="antecedente_form_id" id="antecedente_form_id" value='0'>
                            <input type="hidden" name="tipo_campo" id="tipo_campo" value='text'>
                        </div>
                    </div>
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true" style="display: none;"></span>
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


{{-- MODAL PARA EDITAR LOS ANSWER --}}
<div id="antecedenteAnswerModalEdit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Editar pregunta</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="answer_edit_form_antecedente">
                    <div class="form-row">
                        <div class="col-12">
                          <div class="alert alert-secondary">
                            <p>Estas a punto de editar una pregunta al siguiente antecedente: <strong id="form_answer_edit_nombre"></strong></p>
                          </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="nombre_answer_edit">Nombre</label>
                            <input type="text" name="nombre_answer_edit"  class="form-control" id="nombre_answer_edit" required>
                            <input type="hidden" name="answer_id" id="answer_id" value='0'>
                            <input type="hidden" name="tipo_campo_edit" id="tipo_campo_edit" value='text'>
                        </div>
                    </div>
                    <div class="button-guardar  float-right">
                        <button type="submit" class=" btn-sm btn btn-primary">
                            <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true" style="display: none;"></span>
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- MODAL PARA ELIMINAR LOS ANSWER --}}
<div id="antecedenteAnswerModalDelete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Eliminar pregunta</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="answer_delete_form_antecedente">
                    <div class="form-row">
                        <div class="col-12">
                            {{-- <p>Estas a punto de eliminar la pregunta: <strong id="nombre_delete_answer"></strong></p> --}}
                            <div class="alert alert-primary" role="alert">
                              <h4 class="alert-heading">IMPORTANTE:</h4>
                              <p class="mb-0">
                                Al eliminar esta pregunta, se eliminaran de forma automatica las respuestas vinculadas.
                              </p>
                          </div>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="hidden" name="answer_delete_id" id="answer_delete_id" value='0'>
                        </div>
                    </div>
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_antecedenteAnswerModal_delete">
                            <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true" style="display: none;"></span>
                            Eliminar
                        </button>
                        <button type="button" class="btn btn-sm btn-secondary" class="close" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
