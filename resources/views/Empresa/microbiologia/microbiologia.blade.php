
@extends('layouts.Vuexy')
@section('title','Microbiologia')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
@endsection

@section('content')
  <section id="data-list-view" class="data-list-view-header">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header bg-secondary">
            <h4 class="card-title text-white col-md-12 pl-0">
              Microbiologia
              <span class="spinner-border loading float-right spinner-border-sm" role="status" aria-hidden="true" style="display:none"></span>
            </h4>
          </div>
          <div class="card-content collapse show">
            <div class="card-body">
              <form id="microbiologia" method="post">
                <input type="hidden" name="institucion" value="1080">
                <input type="hidden" name="metodo" value="microbiologia">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Fecha Inicial</label>
                      <input required class="form-control" type="date" name="inicial" value="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Fecha Final</label>
                      <input required class="form-control" type="date" name="final" value="">
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary" name="button">
                  Buscar Resultados
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- DataTable starts -->
    <div class="table-responsive col-md-12">
      <table class="table w-100 microbiologiaTable data-list-view" style="width:100% !important">
        <thead>
          <tr>
            <th>Alimento</th>
            <th>Sección</th>
            <th>Examen</th>
            <th>Metodo</th>
            <th>Sucursal</th>
            <th>Fecha</th>
            <th>Ver</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </section>



  <div class="modal-size-lg mr-1 mb-1 d-inline-block">
    <!-- Modal -->
    <div class="modal fade text-left" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h4 class="modal-title" id="myModalLabel17">Resultados</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="col-md-12 loadSass text-center pt-1 pb-1">
              <div class="spinner-border primary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </div>

            <div class="table-responsive tableSassResult">
                  <table class="table mb-0">
                      <thead class="bg-secondary">
                          <tr>
                              <th class="text-white" scope="col">Examen</th>
                              <th class="text-white" scope="col">Prueba</th>
                              <th class="text-white" scope="col">Resultado</th>
                              <th class="text-white" scope="col">Unidad</th>
                          </tr>
                        </thead>
                        <tbody class="result_examen">
                      </tbody>
                  </table>
                </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
@endsection
@section('page_js')

@endsection
@section('js_custom')
  <script src="{!! asset('public/js/empresa/microbiologia/microbiologia.js') !!}"></script>
@endsection
