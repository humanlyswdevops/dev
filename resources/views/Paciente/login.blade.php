@extends('layouts.VuexyLogin')
@section('page_css')
<link rel="stylesheet" type="text/css" href="https://demo.harnishdesign.net/html/oxyy/vendor/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://demo.harnishdesign.net/html/oxyy/vendor/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="https://demo.harnishdesign.net/html/oxyy/css/stylesheet.css" />
@endsection
@section('content')

<!-- Preloader -->
<div class="preloader">
    <div class="lds-ellipsis">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
  <!-- Preloader End -->

  <div id="main-wrapper" class="oxyy-login-register">
    <div class="container-fluid px-0">
      <div class="row no-gutters min-vh-100">
        <!-- Welcome Text
        ========================= -->
        <div class="col-md-8">
          <div class="hero-wrap d-flex align-items-center h-100">
            <div class="hero-mask opacity-0 bg-primary"></div>
            <div class="hero-bg hero-bg-scroll" style="background-image:url('{{ asset('public/img/login_has.svg') }}');"></div>
            <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
              <div class="row no-gutters">
                <div class="col-md-12 mx-auto">
                  <div class="logo mt-4 ml-4 mb-5 mb-md-0">
                      <a class="d-flex" href="" title="has">
                        <img width="120" src="{{ asset('public/new_logo_has.svg') }}">
                      </a>
                   </div>
                </div>
              </div>
              <div class="row no-gutters my-auto">
                <div class="col-10 col-lg-9 mx-auto">
                  {{-- <h1 class="text-11 text-white mb-4">Health Administration Solution</h1> --}}
                  <p class="text-4 text-white line-height-4 mb-5">
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Welcome Text End -->

        <!-- Login Form
        ========================= -->
        <div class="col-md-4 d-flex align-items-center">
          <div class="container my-auto py-5">
            <div class="row">
              <div class="col-11 col-lg-9 col-xl-8 mx-auto">
                <h3 class="font-weight-300 mb-4">Bienvenido nuevamente</h3>
                <form id="loginForm" method="post">
                  <div class="form-group">
                    <label for="emailAddress">Correo Electronico:</label>
                    <input type="email" class="form-control" id="emailAddress" required placeholder="Correo electronico">
                  </div>
                  <div class="form-group">
                    <label for="loginPassword">Contraseña:</label>
                    <input type="password" class="form-control" id="loginPassword" required placeholder="Contraseña">
                  </div>
                  <div class="row">

                    <div class="col-sm text-right"><a class="btn-link" href="">¿Has olvidado la contraseña?</a></div>
                  </div>
                  <button class="btn btn-primary btn-block my-4" type="submit">Iniciar sesión</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Login Form End -->
      </div>
    </div>
  </div>

  <!-- Script -->
  <script src="https://demo.harnishdesign.net/html/oxyy/vendor/jquery/jquery.min.js"></script>
  <script src="https://demo.harnishdesign.net/html/oxyy/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Style Switcher -->
  <script src="https://demo.harnishdesign.net/html/oxyy/js/switcher.min.js"></script>
  <script src="https://demo.harnishdesign.net/html/oxyy/js/theme.js"></script>
  <script src="{{ asset('public/js/patient/patient.js') }}"></script>
@endsection



