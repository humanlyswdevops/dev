<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Humanly Software">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon" href="https://has.humanly-sw.com/public/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/vendors.min.css') !!} ">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    @yield('begin_vendor_css')
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/bootstrap-extended.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/colors.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/components.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/themes/dark-layout.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/themes/semi-dark-layout.min.css') !!}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.min.css') !!} ">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/core/colors/palette-gradient.min.css') !!} ">
    @yield('page_css')
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/assets/css/style.css') !!}">
    @yield('css_custom')
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static menu-content menu-collapsed"
    data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99"
            style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <nav
        class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow bg-secondary">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                                    class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                        class="ficon feather icon-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
                            <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
                            <!--     i.ficon.feather.icon-menu-->
                            {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html" data-toggle="tooltip" data-placement="top" title="Todo"><i class="ficon feather icon-check-square"></i></a>
                            </li> --}}

                            <li class="nav-item d-none d-lg-block text-white">
                                {{ \Session::get('empresa')->nombre }}
                            </li>

                            {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-toggle="tooltip" data-placement="top" title="Email"><i class="ficon feather icon-mail"></i></a>
                            </li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calender.html" data-toggle="tooltip" data-placement="top" title="Calendar"><i class="ficon feather icon-calendar"></i>
                            </a>
                            </li> --}}
                        </ul>
                        
                        
                        
                        {{-- <ul class="nav navbar-nav">
                            <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon feather icon-star warning"></i></a>
                                <div class="bookmark-input search-input">
                                    <div class="bookmark-input-icon"><i class="feather icon-search primary"></i></div>
                                    <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="0" data-search="template-list">
                                    <ul class="search-list search-list-bookmark"></ul>
                                </div>
                                <!-- select.bookmark-select-->
                                <!--   option Chat-->
                                <!--   option email-->
                                <!--   option todo-->
                                <!--   option Calendar-->
                            </li>
                        </ul> --}}
                    </div>
                    <ul class='pacie mt-2'>
                        {{-- <select class="p_emple" style="width:100%;" name="" multiple tabindex="-1">

                        </select> --}}



                    </ul>
                    <ul class="nav navbar-nav float-right">

                        {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="{!! route('chat') !!}" data-toggle="tooltip" data-placement="top" title="Chat"><i class="ficon feather icon-message-square"></i></a>
                      </li> --}}

                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i
                                    class="ficon feather icon-maximize"></i></a></li>

                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#"
                                data-toggle="dropdown">
                                <i class=" ficon feather icon-info "></i>
                                <!--<span class="badge badge-pill badge-danger badge-up">5</span>-->
                            </a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header m-0 p-2">
                                        <h3 class="white">Nuevas</h3>
                                        <span class="notification-title">
                                            Funcionalidades
                                        </span>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list"><a class="d-flex justify-content-between"
                                        href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i
                                                    class="feather icon-plus-square font-medium-5 primary"></i></div>
                                            <div class="media-body">
                                                <h6 class="primary media-heading">Consultas</h6><small
                                                    class="notification-text">lleva el control de tus consultas.</small>
                                            </div><small>
                                                {{-- <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">
                                                  9 hours ago</time> --}}
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-clipboard font-medium-5 success"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="success media-heading red darken-1">Receta Médicas</h6><small
                                                    class="notification-text">
                                                    Receta Electrónica configurable.
                                                </small>
                                            </div>
                                            <small>
                                                {{-- <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">5 hour ago</time> --}}
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-book font-medium-5 danger"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="danger media-heading yellow darken-3">
                                                    Expediente Clínico Electrónico
                                                </h6>
                                                <small class="notification-text">
                                                    Lleva el control de todos tus pacientes.</small>
                                            </div>
                                            <small>
                                                {{-- <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Today</time> --}}
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-bar-chart-2 font-medium-5 info"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="info media-heading">
                                                    Estadísticas clínicas de tu práctica médica
                                                </h6><small class="notification-text">
                                                    Conoce con datos y estadísticas el desempeño real.
                                                </small>
                                            </div>
                                            <small>
                                                {{-- <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Last week</time> --}}
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-users font-medium-5 warning"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="warning media-heading">
                                                    Pasa más timpo con tu paciente
                                                </h6><small class="notification-text">
                                                    <ul>
                                                        <li>Ten toda la información de tus pacientes en una sola
                                                            pantalla.</li>
                                                        <li>Notas de consulta.</li>
                                                        <li>Gráficas de signos vitales.</li>
                                                        <li>Estudios de laboratorio y radiografías en la nube.</li>
                                                        <li>Detección de pacientes duplicados.</li>
                                                        <li>Encuentra fácilmente a tus pacientes.</li>
                                                    </ul>
                                                </small>
                                            </div>
                                            <small>
                                                {{-- <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Last month</time> --}}
                                            </small>
                                        </div>
                                    </a></li>
                                <li class="dropdown-menu-footer" data-toggle="modal" data-target="#xlarge">
                                    <a class="dropdown-item p-1 text-center" href="javascript:void(0)">
                                        Soporte
                                    </a>
                                </li>
                            </ul>
                        </li>



                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#"
                                data-toggle="dropdown">
                                <i class="ficon feather icon-bell"></i>
                                <span class="badge badge-pill badge-danger notificationCount badge-up"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header m-0 p-2">
                                        <span class="notification-title">
                                            Notificaciones
                                        </span>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list notificationList">


                                </li>
                            </ul>
                        </li>



                        <li class="dropdown dropdown-user nav-item"><a
                                class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span
                                        class="user-name text-bold-600">{{ Auth::user()->nombre }}</span><span
                                        class="user-status">{{ Auth::user()->puesto }}</span></div><span><img
                                        id="logo_principal_layaout" class="round" src="{!! asset('storage/app/empresas/' . \Session::get('empresa')->logo) !!}"
                                        alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                {{-- <a class="dropdown-item" href="page-user-profile.html">
                                <i class="feather icon-user"></i>
                                Editar Perfil
                              </a> --}}
                                {{-- <a class="dropdown-item" href="app-email.html"><i class="feather icon-layers"></i> Organización</a> --}}

                                <a class="dropdown-item" href="{!! route('config') !!}">
                                    <i class="feather icon-settings"></i>
                                    Configuración
                                </a>
                                @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Equipo')['select']))
                                    <a class="dropdown-item" href="{{ route('config-usuarios') }}">
                                        <i class="feather icon-users"></i> Equipo
                                    </a>
                                @endif
                                @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Accesos')['select']))
                                    <a class="dropdown-item" href="{{ route('accesos') }}">
                                        <i class="feather icon-lock"></i> Accesos
                                    </a>
                                @endif

                                <div class="dropdown-divider"></div>
                                <form action="{{ route('logout') }}" method="post">
                                    @csrf
                                    <button type="submit" class="dropdown-item" href="auth-login.html">
                                        <i class="feather icon-power"></i>
                                        Cerrar Sesión
                                    </button>
                                </form>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('admin') }}">
                        <img src="{!! asset('public/logo.png') !!}" width="45" alt="">
                        <h2 class="brand-text secondary mb-0">HAS</h2>
                    </a>
                </li>
                <li class="nav-item nav-toggle toolbar_toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                        <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon secondary"
                            data-ticon="icon-disc"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                @if (Auth::user()->usuarioPrincipal() || Session::has('Pacientes'))
                    <li class=" nav-item {{ request()->routeIs('empleados') ? 'active' : '' }}">
                        <a href="{{ route('empleados') }}">
                            <i class="feather icon-users"></i>
                            <span class="menu-title" data-i18n="Documentation">Pacientes</span>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->usuarioPrincipal() || Session::has('Grupos'))
                    <li class=" nav-item  {{ request()->routeIs('grupos') ? 'active' : '' }}">
                        <a href="{{ route('grupos') }}">
                            <i class="feather icon-grid"></i>
                            <span class="menu-title" data-i18n="Documentation">Grupos</span>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Resultados Laboratorio')['select']))
                    <li class=" nav-item {{ request()->routeIs('resultados') ? 'active' : '' }}">
                        <a href="{{ route('resultados') }}">
                            <i class="feather icon-clipboard"></i>
                            <span class="menu-title" data-i18n="Documentation">Resultados</span>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Estadísticas')['select']))
                    <li class=" nav-item"><a href="#">
                            <i class="feather icon-bar-chart-2"></i>
                            <span class="menu-title" data-i18n="Ecommerce">Estadísticas</span></a>
                        <ul class="menu-content">
                            {{-- <li class="{{ request()->routeIs('vistaIndicadores')? 'active':'' }}">
                      <a href="{{ route('vistaIndicadores') }}">
                        <i class="feather icon-circle"></i>
                        <span class="menu-item" data-i18n="Shop">Laboratorio</span>
                      </a>
                    </li>
                    <li class="{{ request()->routeIs('indicadoresConsultorio')? 'active':'' }}">
                      <a href="{!! route('indicadoresConsultorio') !!}"><i class="feather icon-circle"></i>
                        <span class="menu-item" data-i18n="Details">Consultorio</span>
                      </a>
                    </li> --}}
                            <li class="{{ request()->routeIs('vistaIndicadoresConsultas') ? 'active' : '' }}">
                                <a href="{!! route('vistaIndicadoresConsultas') !!}"><i class="feather icon-circle"></i>
                                    <span class="menu-item" data-i18n="Details">Consultorio</span>
                                </a>
                            </li>


                            {{-- <li>
                      <a href="app-ecommerce-wishlist.html"><i class="feather icon-circle"></i>
                        <span class="menu-item" data-i18n="Wish List">Equipo</span>
                      </a>
                    </li> --}}
                        </ul>
                    </li>
                @endif
                {{-- @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Covid 19')['select']))
                <li class=" nav-item  {{ request()->routeIs('covid_indicador')? 'active':'' }}">
                    <a href="{{ route('covid_indicador') }}">
                        <img width="25" src="{!! asset('public/img/coronavirus_bold.svg') !!}" alt="">
                        <span class="menu-title" data-i18n="Documentation"> &nbsp;&nbsp;Covid-19</span>
                    </a>
                </li>
              @endif --}}
                @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Agenda')['select']))
                    <li class=" nav-item  {{ request()->routeIs('agenda') ? 'active' : '' }}">
                        <a href="{{ route('agenda') }}">
                            <i class="feather icon-calendar"></i>
                            <span class="menu-title" data-i18n="Documentation">Agenda</span>
                        </a>
                    </li>
                @endif


                {{-- <li class=" nav-item  {{ request()->routeIs('microbiologia')? 'active':'' }}">
                    <a href="{{ route('microbiologia') }}">
                        <i class="feather icon-aperture"></i>
                        <span class="menu-title" data-i18n="Documentation">Microbiologia</span>
                    </a>
                </li> --}}


                <li class=" nav-item  {{ request()->routeIs('config') ? 'active' : '' }}">
                    <a href="{{ route('config') }}">
                        <i class="feather icon-settings"></i>
                        <span class="menu-title" data-i18n="Documentation">Configuraciones</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">

            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light ">
        <p class="clearfix blue-grey lighten-2 mb-0"><span
                class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a
                    class="text-bold-800 grey darken-2">Health Administration Solution.</a>Todos los derechos
                reservados.</span><span class="float-md-right d-none d-md-block">Humanly Software</span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i
                    class="feather icon-arrow-up"></i></button>
        </p>
    </footer>

    <div class="modal-size-xl mr-1 mb-1 d-inline-block">
        <div class="modal fade text-left" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16"
            style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-center">
                        <h4 class="modal-title col-md-12" id="myModalLabel16">Ayuda / Soporte</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body ">
                        <div class="row text-center">
                            <div class="col-md-12">
                                <h2 class="primary text-center">Health Administration Solution</h2>
                                <h5 class="secondary"> Estamos aquí para ayudarte Contactanos via:</h5>
                                <img src="{!! asset('public/img/contacto.svg') !!}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>


<!-- BEGIN: Vendor JS-->
<script src="{!! asset('public/vuexy/app-assets/vendors/js/vendors.min.js') !!}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
@yield('page_vendor_js')
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{!! asset('public/vuexy/app-assets/js/core/app-menu.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/core/app.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/components.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/customizer.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/footer.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
@yield('page_js')
<!-- END: Page JS-->
<!-- BEGIN: Page JS-->
@yield('js_custom')
<!-- END: Page JS-->
<!-- END: Body-->
<script type="text/javascript">
    $('.toolbar_toggle').on('click', (arguments) => {
        menu = localStorage.getItem('menu');
        clase = $('.menu-content').hasClass('menu-expanded')
        if (menu == undefined) {
            $('.menu-content').removeClass('menu-collapsed')
            $('.menu-content').addClass('menu-expanded')
            $('.main-menu').addClass('expanded');
            localStorage.setItem('menu', true);
        } else {
            $('.menu-content').removeClass('menu-expanded')
            $('.menu-content').addClass('menu-collapsed')
            $('.main-menu').removeClass('expanded')
            localStorage.removeItem('menu');
        }
    });
    $(document).ready(function() {


        menu = localStorage.getItem('menu');
        if (menu) {
            $('.menu-content').removeClass('menu-collapsed')
            $('.menu-content').addClass('menu-expanded')
            $('.main-menu').addClass('expanded');
        } else {
            $('.menu-content').removeClass('menu-expanded')
            $('.menu-content').addClass('menu-collapsed')
            $('.main-menu').removeClass('expanded')
        }
        notification();
        buscar();

    });


    // $('.p_emple').select2({
    //     ajax: {
    //         url: "./buscarempleados",
    //         dataType: 'json',
    //         delay: 250,
    //         data: function(params) {
    //             return {
    //                 term: params.term
    //             };

    //         },
    //         processResults: function(data) {
    //             return {
    //                 results: $.map(data, function(item) {

    //                     return {
    //                         text: item.nombre +" "+ item.apellido_paterno + " "+item.apellido_materno,
    //                         id: item.id,
    //                         name: item.nombre
    //                     }
    //                 })
    //             };
    //         }
    //     },
    //     minimumInputLength: 2,
    //     language: "es",
    //     placeholder: "Pacientes - Buscar",
    //     searching: "Buscando",
    //     language: {
    //         noResults: function() {

    //         },
    //         searching: function() {
    //             return "Buscando..";
    //         },
    //         inputTooShort: function() {
    //             return "Por favor ingresa 2 o mas letras"
    //         }
    //     },
    // });

    function buscar() {
        $.ajax({
            type: 'get',
            dataType: 'json',
            //async:false,
            url: '{{ route('buscarempleados') }}'
        }).done(res => {
            var ruta="{{ route('empleados.show',"")}}";
            console.log(res);
            empleados = res.empleados;
            grupos = res.grupos;
            console.log(empleados);
            console.log(grupos);
            $('.pacie').append(`
                <select class="js-example-basic-single select2-hidden-accessible pacientes"  style="width: 100%" name="state">
                  <option class='select2-hidden-accessible'>Buscar Paciente</option>
                </select>
            `);

            empleados.forEach((item, i) => {
                var CURP=item.curp;
                
                console.log(ruta);
            $('.pacientes').append(`
                <option class='select2-hidden-accessible' value="${item.CURP}"  id=""> ${item.nombre} ${item.apellido_paterno} ${item.apellido_materno}</option>
                
            `);
            
           
           
            
            });
            $('.js-example-basic-single').select2();
            $('.js-example-basic-single').on('select2:select', function (e) {
               var rutita= $('.pacientes').val();
             $(location).attr('href',ruta+"/"+rutita);
           
              
            });
        }).fail(err => {

        })
    }


    function notification() {
        $('.notificationList').empty();
        $('.notificationCount').hide();

        $.ajax({
            type: 'get',
            dataType: 'json',
            //async:false,
            url: '{{ route('notification') }}'
        }).done(res => {
            console.log();

            if (res.length != 0) {
                $('.notificationCount').show();
                $('.notificationCount').text(res.length);
                res.forEach((item, i) => {
                    $('.notificationList').append(`

                      <a class="d-flex justify-content-between" href="/">
                          <div class="media d-flex align-items-start">
                              <div class="media-left">
                                <i class="feather icon-check-circle font-medium-5"></i>
                              </div>
                              <div class="media-body">
                                  <h6 class="secondary media-heading darken-3">
                                    Cita con ${item.empleado.nombre}  ${item.empleado.apellido_paterno}
                                  </h6>
                                  <small class="notification-text">
                                    El dia de mañana tendra cita con ${item.empleado.nombre}  ${item.empleado.apellido_paterno} ${item.empleado.apellido_materno}, motivo de la consulta es: ${item.agenda.reason}
                                  </small>
                                  <small class="mt-1 block">
                                    Horario:   ${item.agenda.start_time} - ${item.agenda.end_time}
                                  </small>
                              </div>
                              <small>
                                  <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Hoy</time>
                                </small>
                          </div>
                      </a>

                    `)


                });

            }
        }).fail(err => {

        })
    }
</script>

</html>

