<div class="vertical-nav" id="sidebar">
    <div class="py-2 px-1 mb-1">
        <div class="media d-flex align-items-center">

            <a href="{{route('lab-menu')}}">
                <img src="https://expedienteclinico.humanly-sw.com/developing/resources/sass/images/login.png" alt="..." width="80" class="mr-3 rounded-circle img-thumbnail shadow-sm">
            </a>
        </div>
        <div class="media d-flex align-items-center">
            <a href="{{route('lab-menu')}}">
                <img src="https://expedienteclinico.humanly-sw.com/developing/resources/sass/images/perfil.jpg" alt="..." width="80" class="mr-3 rounded-circle img-thumbnail shadow-sm">
            </a>
        </div>
    </div>
    <ul class="nav flex-column bg-white mb-0" style="overflow:auto">

        @if (Session::get('privilegioEmpleado') == "admin" || Session::get('privilegioEmpleado') == "recepcion")
            <li class="nav-item">
                <a href="{{route('lab-menu')}}" title="Búsqueda de pacientes"  class="nav-link text-dark">
                    <i class="fa fa-address-book"></i>
                </a>
            </li>
        @endif

        @if (Session::get('permiso') == 2 || Session::get('privilegioEmpleado') == "admin")
        <li class="nav-item">
            <a href="{{route('lab-menu')}}" title="Ingresos" class="nav-link text-dark {{request()->routeIs('Ingresos') ? 'actived':'' }}">
                <i class="fa fa-bell"></i>
            </a>
        </li>
        {{-- <li class="nav-item ">
            <a href="{{route('pacientes')}}" title="Pacientes" class="nav-link text-dark {{request()->routeIs('pacientes') ? 'actived':'' }}">
                <i class="fa fa-users fa-fw"></i>
            </a>
        </li> --}}
        @endif

        @if (Session::get('permiso') == 'radiologo' || Session::get('privilegioEmpleado') == "admin")
        <li class="nav-item ">
            <a href="{{route('pacientes')}}" title="Pacientes" class="nav-link text-dark {{request()->routeIs('pacientes') ? 'actived':'' }}">
                <i class="fas fa-user-md"></i>
            </a>
        </li>
        @endif
        @if (Session::get('permiso')=='medico')
          <li class="nav-item ">
              <a href="{{route('medicina')}}" title="Medicina Laboral" class="nav-link text-dark {{request()->routeIs('medicina_paciente') ? 'actived':'' }} {{request()->routeIs('medicina') ? 'actived':'' }}">
                  <i class="fas fa-user-md"></i>
              </a>
          </li>
          <li class="nav-item ">
              <a href="{{route('historial_empresas')}}" title="Historial Clinico" class="nav-link text-dark {{request()->routeIs('historial_empresas') ? 'actived':'' }}">
                  <i class="fas fa-book-medical"></i>
              </a>
          </li>
        @endif
        <li class="nav-item logout">
            <div class="n1">
                <button type="submit" class="nav-link text-dark">
                    <i class="fa fa-user-cog"></i>
                </button>
            </div>
            <div class="n2">
                <form action="{{ route('labLogout') }}" method="POST">
                    @csrf
                    <button type="submit" class="nav-link text-dark">
                        <i class="fa fa-sign-out-alt fa-fw"></i>
                    </button>
                </form>
            </div>
        </li>
    </ul>
</div>
