<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Humanly Software">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon" href="https://has.humanly-sw.com/public/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/vendors.min.css') !!} ">
    @yield('begin_vendor_css')
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/bootstrap-extended.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/colors.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/components.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/themes/dark-layout.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/themes/semi-dark-layout.min.css') !!}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.min.css') !!} ">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/core/colors/palette-gradient.min.css') !!} ">
    @yield('page_css')
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/assets/css/style.css') !!}">
    @yield('css_custom')
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static menu-content menu-collapsed" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow bg-secondary">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
                            <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
                            <!--     i.ficon.feather.icon-menu-->
                            {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html" data-toggle="tooltip" data-placement="top" title="Todo"><i class="ficon feather icon-check-square"></i></a>
                            </li> --}}

                            <li class="nav-item d-none d-lg-block text-white">
                              Asesores Diagnóstico Clínico
                            </li>
                            {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html" data-toggle="tooltip" data-placement="top" title="Email"><i class="ficon feather icon-mail"></i></a>
                            </li>
                            <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calender.html" data-toggle="tooltip" data-placement="top" title="Calendar"><i class="ficon feather icon-calendar"></i>
                            </a>
                            </li> --}}
                        </ul>
                        {{-- <ul class="nav navbar-nav">
                            <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon feather icon-star warning"></i></a>
                                <div class="bookmark-input search-input">
                                    <div class="bookmark-input-icon"><i class="feather icon-search primary"></i></div>
                                    <input class="form-control input" type="text" placeholder="Explore Vuexy..." tabindex="0" data-search="template-list">
                                    <ul class="search-list search-list-bookmark"></ul>
                                </div>
                                <!-- select.bookmark-select-->
                                <!--   option Chat-->
                                <!--   option email-->
                                <!--   option todo-->
                                <!--   option Calendar-->
                            </li>
                        </ul> --}}
                    </div>
                    <ul class="nav navbar-nav float-right">

                      {{-- <li class="nav-item d-none d-lg-block"><a class="nav-link" href="{!! route('chat') !!}" data-toggle="tooltip" data-placement="top" title="Chat"><i class="ficon feather icon-message-square"></i></a>
                      </li> --}}

                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>


                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                          <i class="ficon feather icon-bell"></i>
                          <span class="badge badge-pill badge-danger notificationCount badge-up"></span>
                         </a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header m-0 p-2">
                                        <span class="notification-title">
                                          Notificaciones
                                        </span>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list notificationList">


                                  </li>
                            </ul>
                        </li>



                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">{{ \Session::get('nombreEmpleado')}}</span><span class="user-status">{{ \Session::get('puestoEmpleado') }}</span></div><span><img class="round" src="{!! asset('resources/sass/images/perfil.jpg') !!}" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-divider"></div>
                                <form action="{{ route('labLogout') }}" method="post">
                                    @csrf
                                    <button type="submit" class="dropdown-item" href="auth-login.html">
                                        <i class="feather icon-power"></i>
                                        Cerrar Sesión
                                    </button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{route('lab-menu')}}">
                        <img src="{!! asset('public/logo.png') !!}" width="45" alt="">
                        <h2 class="brand-text secondary mb-0">HAS</h2>
                    </a>
                  </li>
                <li class="nav-item nav-toggle toolbar_toggle">
                  <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                    <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon secondary" data-ticon="icon-disc"></i>
                  </a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">



              @if (Session::get('privilegioEmpleado') == "admin" || Session::get('privilegioEmpleado') == "recepcion")
                  <li class="nav-item">
                      <a href="{{route('lab-menu')}}" title="Búsqueda de pacientes"  class="nav-link text-dark">
                          <i class="feather icon-grid"></i>
                          <span class="menu-title" data-i18n="Documentation">Grupos</span>
                      </a>
                  </li>
              @endif

              @if (Session::get('permiso') == 2 || Session::get('privilegioEmpleado') == "admin")
              <li class="nav-item">
                  <a href="{{route('lab-menu')}}" title="Ingresos" class="nav-link text-dark {{request()->routeIs('Ingresos') ? 'actived':'' }}">
                      <i class="fa fa-bell"></i>
                  </a>
              </li>
              @endif

              @if (Session::get('permiso') == 'radiologo' || Session::get('privilegioEmpleado') == "admin")
              <li class="nav-item ">
                  <a href="{{route('pacientes')}}" title="Pacientes" class="nav-link text-dark {{request()->routeIs('pacientes') ? 'actived':'' }}">
                      <i class="fas fa-user-md"></i>
                  </a>
              </li>
              @endif
              @if (Session::get('permiso')=='medico')
                {{-- <li class="nav-item {{request()->routeIs('medicina-pacientes') ? 'active':'' }}">
                    <a href="{{route('medicina-pacientes')}}" title="Medicina Laboral" class="nav-link">
                        <i class="feather icon-clipboard"></i>
                        <span class="menu-title" data-i18n="Documentation">Estudios Médicina</span>
                    </a>
                </li>
                <li class="nav-item {{request()->routeIs('historial_empresas') ? 'active':'' }}">
                    <a href="{{route('historial_empresas')}}" title="Historial Clinico" class="nav-link  ">
                        <i class="feather icon-folder"></i>
                        <span class="menu-title" data-i18n="Documentation">Historial Clínico</span>
                    </a>
                </li>
                <li class="nav-item {{Request::is('Historial-Pacientes*') ? 'active':'' }}">
                    <a href="{{route('historial_pacientes')}}" title="Historial Pacientes" class="nav-link  ">
                        <i class="feather icon-users"></i>
                        <span class="menu-title" data-i18n="Documentation">Historial Pacientes</span>
                    </a>
                </li> --}}
              @endif

{{--
                <li class=" nav-item  {{ request()->routeIs('grupos')? 'active':'' }}">
                    <a href="{{ route('grupos') }}">
                        <i class="feather icon-grid"></i>
                        <span class="menu-title" data-i18n="Documentation">Grupos</span>
                    </a>
                </li>

                <li class=" nav-item {{ request()->routeIs('resultados')? 'active':'' }}">
                    <a href="{{ route('resultados') }}">
                        <i class="feather icon-clipboard"></i>
                        <span class="menu-title" data-i18n="Documentation">Resultados</span>
                    </a>
                </li>
                <li class=" nav-item"><a href="#">
                  <i class="feather icon-bar-chart-2"></i>
                  <span class="menu-title" data-i18n="Ecommerce">Estadísticas</span></a>
                  <ul class="menu-content">
                    <li class="{{ request()->routeIs('vistaIndicadores')? 'active':'' }}">
                      <a href="{{ route('vistaIndicadores') }}">
                        <i class="feather icon-circle"></i>
                        <span class="menu-item" data-i18n="Shop">Laboratorio</span>
                      </a>
                    </li>
                    <li class="{{ request()->routeIs('indicadoresConsultorio')? 'active':'' }}">
                      <a href="{!! route('indicadoresConsultorio') !!}"><i class="feather icon-circle"></i>
                        <span class="menu-item" data-i18n="Details">Consultorio</span>
                      </a>
                    </li>
                    <li>
                      <a href="app-ecommerce-wishlist.html"><i class="feather icon-circle"></i>
                        <span class="menu-item" data-i18n="Wish List">Equipo</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class=" nav-item  {{ request()->routeIs('covid_indicador')? 'active':'' }}">
                    <a href="{{ route('covid_indicador') }}">
                        <i class="feather icon-sun"></i>
                        <span class="menu-title" data-i18n="Documentation">Covid-19</span>
                    </a>
                </li>

                <li class=" nav-item  {{ request()->routeIs('agenda')? 'active':'' }}">
                    <a href="{{ route('agenda') }}">
                        <i class="feather icon-calendar"></i>
                        <span class="menu-title" data-i18n="Documentation">Agenda</span>
                    </a>
                </li>

                <li class=" nav-item  {{ request()->routeIs('config')? 'active':'' }}">
                    <a href="{{ route('config') }}">
                        <i class="feather icon-settings"></i>
                        <span class="menu-title" data-i18n="Documentation">Configuraciones</span>
                    </a>
                </li> --}}
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div id="appContentMainLayaout" class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">

            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- END: Content-->



    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light ">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a class="text-bold-800 grey darken-2" href="" target="_blank">Health Administration Solution.</a>Todos los derechos reservados.</span><span class="float-md-right d-none d-md-block">Humanly Software</span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>

</body>


<!-- BEGIN: Vendor JS-->
<script src="{!! asset('public/vuexy/app-assets/vendors/js/vendors.min.js') !!}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
@yield('page_vendor_js')
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{!! asset('public/vuexy/app-assets/js/core/app-menu.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/core/app.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/components.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/customizer.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/footer.min.js') !!}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
@yield('page_js')
<!-- END: Page JS-->
<!-- BEGIN: Page JS-->
@yield('js_custom')
<!-- END: Page JS-->
<!-- END: Body-->
<script type="text/javascript">
  // $('.toolbar_toggle').on('click', (arguments) => {
  //   menu = localStorage.getItem('menu');
  //   clase =  $('.menu-content').hasClass('menu-expanded')
  //   if (menu == undefined) {
  //     $('.menu-content').removeClass('menu-collapsed')
  //     $('.menu-content').addClass('menu-expanded')
  //     $('.main-menu').addClass('expanded');
  //     localStorage.setItem('menu',true);
  //   }else{
  //     $('.menu-content').removeClass('menu-expanded')
  //     $('.menu-content').addClass('menu-collapsed')
  //     $('.main-menu').removeClass('expanded')
  //     localStorage.removeItem('menu');
  //   }
  // });
  // $(document).ready(function() {
  //    menu = localStorage.getItem('menu');
  //    if (menu) {
  //      $('.menu-content').removeClass('menu-collapsed')
  //      $('.menu-content').addClass('menu-expanded')
  //      $('.main-menu').addClass('expanded');
  //    }else{
  //      $('.menu-content').removeClass('menu-expanded')
  //      $('.menu-content').addClass('menu-collapsed')
  //      $('.main-menu').removeClass('expanded')
  //    }
  //    //notification();
  // });

  // function notification(){
  //   $('.notificationList').empty();
  //   $('.notificationCount').hide();
  //
  //   $.ajax({
  //     type:'get',
  //     dataType:'json',
  //     //async:false,
  //     url:'{{route('notification')}}'
  //   }).done(res=>{
  //     console.log();
  //
  //     if (res.length != 0) {
  //       $('.notificationCount').show();
  //       $('.notificationCount').text(res.length);
  //       res.forEach((item, i) => {
  //         $('.notificationList').append(`
  //
  //           <a class="d-flex justify-content-between" href="/">
  //               <div class="media d-flex align-items-start">
  //                   <div class="media-left">
  //                     <i class="feather icon-check-circle font-medium-5"></i>
  //                   </div>
  //                   <div class="media-body">
  //                       <h6 class="secondary media-heading darken-3">
  //                         Cita con ${item.empleado.nombre}  ${item.empleado.apellido_paterno}
  //                       </h6>
  //                       <small class="notification-text">
  //                         El dia de mañana tendra cita con ${item.empleado.nombre}  ${item.empleado.apellido_paterno} ${item.empleado.apellido_materno}, motivo de la consulta es: ${item.agenda.reason}
  //                       </small>
  //                       <small class="mt-1 block">
  //                         Horario:   ${item.agenda.start_time} - ${item.agenda.end_time}
  //                       </small>
  //                   </div>
  //                   <small>
  //                        <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">Hoy</time>
  //                     </small>
  //               </div>
  //           </a>
  //
  //         `)
  //
  //
  //       });
  //
  //     }
  //   }).fail(err=>{
  //
  //   })
  // }
</script>
</html>
