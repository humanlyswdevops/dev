<div class="vertical-nav" id="sidebar">
    <div class="py-2 px-1 mb-1">
        <div class="media d-flex align-items-center">
            <a href="{{route('admin')}}">
                <img src="https://asesoresconsultoreslabs.com/expedienteclinico/Clinica/resources/sass/images/login.png" alt="..." width="80" class="mr-3 rounded-circle img-thumbnail shadow-sm">
            </a>
        </div>

        @if (Session::has('empresa') && Session::get('empresa')->logo != 'null')
            <div class="media d-flex align-items-center">
                <a href="{{route('admin')}}">
                    <img src="{{route('getImage',['filename'=>Session::get('empresa')->logo])}}" alt="..." width="80" class="mr-3 rounded-circle img-thumbnail shadow-sm">


                </a>
            </div>
        @else
            <div class="media d-flex align-items-center">
                <a href="{{route('admin')}}">
                    <img src="https://asesoresconsultoreslabs.com/expedienteclinico/Clinica/resources/sass/images/perfil.jpg" alt="..." width="80" class="mr-3 rounded-circle img-thumbnail shadow-sm">
                </a>
            </div>
        @endif
    </div>

    <ul class="nav flex-column bg-white mb-0">
        <li class="nav-item logout">
            @if (Auth::user()->usuarioPrincipal())
                <div class="n1">
                    <a href="{{ route('config-usuarios') }}" class="nav-link text-dark">
                        <i class="fas fa-user-cog"></i>
                    </a>
                </div>
            @endif

            <div class="n2">
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="nav-link text-dark">
                        <i class="fa fa-sign-out-alt fa-fw"></i>
                    </button>
                </form>
            </div>
        </li>
    </ul>
</div>
