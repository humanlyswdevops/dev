<!DOCTYPE html>
<html {{ str_replace('_', '-', app()->getLocale()) }}>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title')</title>

    <link rel="shortcut icon" type="image/x-icon" href="https://www.laboratorioasesores.com/assets/frontend/img/favicon_asesores.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CMuli:300,400,500,700" rel="stylesheet">
    <!-- BEGIN VENDcvbcvbcvbOR CSS-->
    <link rel="stylesheet" type="text/css" href="https://ac-labs.com.mx/htds/app-assets/css/vendors.css">

    @yield('style_vendor')
    <!-- END VENDOR CSS-->

    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="https://ac-labs.com.mx/htds/app-assets/css/app.css">

    <!-- END ROBUST CSS-->

    <!-- BEGIN Page Level CSS-->
    @yield('page_level_css')
    <!-- END Page Level CSS-->

    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="https://ac-labs.com.mx/htds/assets/css/style.css">

    <!-- END Custom CSS-->
        @yield('styles')
  </head>
  <body>
    @if (\Session::has('empresa'))
      @include('..layouts.menuEmpresa')
      @else
        @include('layouts.menuLaboratorio')
    @endif

      @yield('content')
  </body>

  <!-- BEGIN VENDOR JS-->
  <script src="https://ac-labs.com.mx/htds/app-assets/vendors/js/vendors.min.js"></script>
  <!-- BEGIN VENDOR JS-->

  <!-- BEGIN PAGE VENDOR JS-->
  @yield('page_vendor_js')
  <!-- END PAGE VENDOR JS-->


    <!-- BEGIN ROBUST JS-->
    <script src="https://ac-labs.com.mx/htds/app-assets/js/core/app-menu.js"></script>
    <script src="https://ac-labs.com.mx/htds/app-assets/js/core/app.js"></script>

    <!-- END ROBUST JS-->

    <!-- BEGIN PAGE LEVEL JS-->
    @yield('script')
    <!-- END PAGE LEVEL JS-->
</html>
