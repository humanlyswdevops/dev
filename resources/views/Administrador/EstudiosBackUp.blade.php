<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Estudios</title>
  <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend//img/favicon_asesores.png" type="image/png">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CMuli:300,400,500,700" >
  <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}resources/sass/css/style_admin_menu.css">
  <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}resources/sass/css/Administrador/style_estudios.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link href="{!! asset('/') !!}resources/sass/fontawesome/css/all.css" rel="stylesheet">
</head>
<body>
<!-- End vertical navbar -->
@include('..layouts.menuAdministrador')
<!-- Page content holder -->
<div class="content">

    <div class="estudios col-5">
        <h2>
           Lista de Estudios
        </h2>
        <div class="estudios_content">
            @foreach ($categorias as $categoria)
                <div class="attrib">
                    <i class="fa fa-file-alt fa-fw icon"></i>
                    <a href="#" data-value="{{$categoria->id}}" class="links" >
                        {{$categoria->nombre}}
                    </a>
                </div>
            @endforeach
        </div>
        <h2 class="took">Agregar Estudios</h2>
        <div class="material-switch pull-right">
            <input id="someSwitchOptionInfo" name="someSwitchOption001" type="checkbox" onchange="javascript:showContent()" />
            <label for="someSwitchOptionInfo" class="label-info"></label>
        </div>
    </div>
    <div class="alta col-7">
        <div class="header">
            <div class="header_content">
                <i class="fa fa-file-alt fa-fw head_icon"></i>
                <div>
                    <h2>Alta de estudios</h2>
                    <h6>Laboratorio</h6>
                </div>
            </div>
            <form method="POST" id="altaEstudio">
                @csrf
                <div id="alert_alta">

                </div>
                <div class="form-group">
                    <label for="estudio" id="label_estudio" class="h5">Nombre de Estudio</label>
                    <label for="estudio" id="selected" class="h5 text-center d-block">Selecciona la categoría</label>
                    <input type="text" class="form-control @error('estudio') is-inavlid @enderror" id="estudio" value="{{old('estudio')}}" name="estudio" placeholder="Nombre del Estudio">
                </div>

                <div class="form-group">
                    <label for="inputCodigoAlta">Código del estudio</label>
                    <input type="text" name="codigo"  class="form-control" id="inputCodigoAlta" required>
                </div>

                <div class="form-row">
                    <label for="inputDescripcionAlta">Descripción</label>
                    <textarea id="inputDescripcionAlta" class="form-control" cols="100" rows ="3" name="descripcion" form="altaEstudio" required></textarea>
                </div>

                <div class="form-row">
                    <label for="inputRequisitosAlta">Requisitos</label>
                    <textarea id="inputRequisitosAlta" class="form-control" cols="100" rows ="3" name="requisitos" form="altaEstudio" required></textarea>
                </div>

                <div class="form-group">
                    <label for="inputDuracionAlta">Duración de resultados (en días)</label>
                    <input type="text" name="duracion"  class="form-control" id="inputDuracionAlta" required>
                </div>
                <div class="form-group">
                    <label for="inputCostoAlta">Costo</label>
                    <input type="text" name="costo"  class="form-control" id="inputCostoAlta" required>
                </div>

                @error('estudio')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <button type="submit" id="btn_estudio_enviar" class="btn btn_estudio btn-primary">Guardar</button>
            </form>
            <img src="../resources/sass/images/cru.png" alt="" class="img_estudios">
        </div>
    </div>
    <div class="lista col-7">
        <div class="img">
        </div>
        <div class="content_estudios">
            <table id="table_estudios" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Duración</th>
                        <th>Costo</th>
                        <th>Estado</th>
                        <th>Modificar</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- End demo content -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title float-left">Modificación</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="estudio_form">
                    @csrf
                    <div id="alerts">

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-8">
                          <input type="text" name="id"  class="form-control" id="inputId" hidden>

                            <label for="inputEstudio">Nombre del Estudio</label>
                            <input type="text" name="nombre"  class="form-control @error('nombre') is-invalid @enderror" value="{{old('nombre')}}" id="inputEstudio" required>
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <label for="inputCodigo">Clave del Estudio</label>
                            <input type="text" name="codigo"  class="form-control" id="inputCodigo">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputState">Estado</label>
                            <select id="inputState" name="estado" class="form-control">
                                <option value="1">Activo</option>
                                <option value="2">Inactivo</option>
                            </select>
                        </div>

                        <div class="form-row">
                            <label for="inputDescripcion">Descripción</label>
                            <textarea id="inputDescripcion" class="form-control" cols="86" rows ="4" name="descripcion" form="estudio_form" required></textarea>
                        </div>

                        <div class="form-row">
                            <label for="inputRequisitos">Requisitos</label>
                            <textarea id="inputRequisitos" class="form-control" cols="86" rows ="3" name="requisitos" form="estudio_form" required></textarea>
                        </div>

                        <div class="form-group col-md-8">
                            <label for="inputDuracion">Duración de resultados (en días)</label>
                            <input type="text" name="duracion"  class="form-control" id="inputDuracion" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputCosto">Costo</label>
                            <input type="text" name="costo"  class="form-control" id="inputCosto" required>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="btn_enviar">Modificar</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{!! asset('/') !!}/resources/js/events.js"></script>
</html>
