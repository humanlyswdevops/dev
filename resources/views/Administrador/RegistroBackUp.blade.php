<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <title>Registro de empresa</title>
  <!--<link rel= "shortcut icon" href="assets/images/iconoo.ico"-->
  <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend//img/favicon_asesores.png" type="image/png">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CMuli:300,400,500,700">
  <link href=" {!! asset('resources/sass/fontawesome/css/all.css') !!} " rel="stylesheet">
  <link rel="stylesheet" type="text/css" href=" {!! asset('resources/sass/css/styleScroll.css') !!}">
  <link rel="stylesheet" type="text/css" href=" {!! asset('resources/sass/css/style_admin_menu.css') !!} ">
  <!--wizard-->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link rel="stylesheet" type="text/css" href=" {!! asset('resources/sass/css/styleWizard.css') !!} ">

  <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/forms/selects/selectize.css">
</head>

<body id="escritorio">

  @include('..layouts.menuAdministrador')
  <!-- End vertical navbar -->
  <div class="img">
    <img src=" {!! asset('resources/sass/images/cruzz.png') !!}" alt="">
  </div>
  <div class="imagg">
    <img src=" {!! asset('resources/sass/images/cruzz.png') !!}" alt="">
  </div>
  <div>
    <!--div class="col-md-5"-->
    <div class="container">
      {{-- Form alerts --}}
      <div class="form-alerts">
        <div id="error-alert1" class="alert alert-warning alert-dismissible fade show" role="alert">
          <strong>Error</strong> Todos los campos son obligatorios.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>
      <!--div class="seccion-navegacion">
      <p class="navegacion"> <a href="{{route('admin')}}">Inicio</a> / <span>Registro</span> </p>
    </div-->

      <div class="frases col-12">
        <h1>¡Hagamos crecer el equipo!</h1>
        <h2 class="h2">Registro de empresas</h2>
      </div>
      <div class="wizard1">
        <section>
          <div class="wizard">
            <div class="wizard-inner">

              @if(session('message_error'))
              <div class="alert alert-danger">
                {{session('message_error')}}
              </div>
              @endif

              @if($errors->any())
                <div class="alert alert-danger">
                  Todos los campos son obligatorios.
                </div>
                @endif

                @if(session('message'))
                <div class="alert alert-success">
                  {{session('message')}}
                </div>
                @endif
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab"> Datos Generales
                    </a>
                  </li>

                  <li role="presentation" class="disabled">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab"> Información de la empresa

                    </a>
                  </li>

                </ul>
            </div>

            <form role="form" id="registro_empresa" method="post" action="{{route('registro')}}" enctype="multipart/form-data">
              {{-- <form role="form" id="registro_empresa" method="post" enctype="multipart/form-data"> --}}
              @csrf
              <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">

                  <div class="input-group">
                    <input type="text" class="datos_entrada inputstyle form-control @error('nombreAdmin') is-invalid
    @enderror" id="nombreAdmin" name="nombreAdmin" value="{{old('nombreAdmin')}}" placeholder="Nombre">
                    @error('estadoCivil')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <span class="invalid-feedback" role="alert">
                      <strong>El correo es un campo obligatorio.</strong>
                    </span>
                  </div>

                  <div class="input-group">
                    <input type="email" class="datos_entrada inputstyle form-control @error('correoAdmin') is-invalid
    @enderror" id="correoAdmin" value="{{old('correoAdmin')}}" name="correoAdmin" placeholder="Correo de contacto" value="">
                    @error('correoAdmin')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>


                  <div class="input-group">
                    <input type="text" onkeypress='return validaNumericos(event)' class="datos_entrada inputstyle form-control @error('telefonoAdmin') is-invalid
    @enderror" id="telefonoAdmin" value="{{old('telefonoAdmin')}}" name="telefonoAdmin" maxlength="10" placeholder="Teléfono de contacto">
                    @error('telefonoAdmin')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>


                  <div class="input-group">
                    <input type="text" class="datos_entrada inputstyle form-control @error('puestoAdmin') is-invalid
    @enderror" id="puestoAdmin" value="{{old('puestoAdmin')}}" name="puestoAdmin" placeholder="Puesto">
                    @error('puestoAdmin')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>


                  <div class="input-group">
                    <input type="text" class="datos_entrada inputstyle form-control @error('areaAdmin') is-invalid
    @enderror" id="areaAdmin" value="{{old('areaAdmin')}}" name="areaAdmin" placeholder="Área">
                    @error('areaAdmin')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <ul class="list-inline pull-right">
                    <li><button type="button" class="btn btn-primary next-step" id="im2">Siguiente <i class="fas fa-angle-right"></i></button></li>
                  </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="step2">


                  <div class="imag">
                    <div class="imgen">
                      <img src=" {!! asset('path') !!} ../resources/sass/images/paisa.png" id="load_image" class="img-fluid" alt="">
                    </div>
                    <span class="btn btn-primary btn-file">
                      Subir archivo <input type="file" accept="image/*" name="logo" id="logo">
                    </span>
                    @error('nombre')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>



                  <div class="formularioo">
                    <div class="input-group">
                      <input type="text" class="datos_entrada inputstyle form-control @error('nombre') is-invalid
    @enderror" id="nombre" value="{{old('nombre')}}" name="nombre" placeholder="Nombre de la Empresa">
                      @error('nombre')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>

                    <div class="input-group">
                      <input type="text" class="datos_entrada inputstyle form-control @error('direccion') is-invalid
            @enderror" id="direccion" name="direccion" value="{{old('direccion')}}" placeholder="Dirección">
                      @error('direccion')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>

                    <div class="input-group">
                      <select class="datos_entrada form-control @error('giro') is-invalid @enderror" name='giro' id='id_categoria'>
                      <option selected>Selecciona el giro de la empresa</option>
                      @foreach ($giros as $giro)
                      <option value="{{ $giro->nombre }}">{{ $giro->nombre }}</option>
                      @endforeach
                      </select>
                      <input id="giro" class="datos_entrada form-control @error('giro') is-invalid @enderror" type="text"placeholder="Giro de la empresa" disabled>
                      @error('giro')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror

                    </div>

                    <div class="input-group">
                      <input type="text" onkeypress='return validaNumericos(event)' class="datos_entrada inputstyle form-control @error('telefono') is-invalid
    @enderror" id="telefono" maxlength="10" name="telefono" value="{{old('telefono')}}" placeholder="Teléfono">
                      @error('telefono')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>


                    <div class="input-group">
                      <input type="text" class="datos_entrada inputstyle form-control @error('pagina') is-invalid
    @enderror" id="pagina" name="pagina" value="{{old('pagina')}}" placeholder="Página Web">
                      @error('pagina')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>

                    <div class="input-group">
                      {{-- <input type="text" class="inputstyle form-control @error('estudios') is-invalid
    @enderror" id="empre" name="estudios" value="{{old('estudios')}}" placeholder="Estudios a realizar">
                      @error('estudios')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror --}}
                      <button id="im2" type="button" class="btn btn-block" data-toggle="modal" data-target=".bs-example-modal-lg">
                        Estudios
                      </button>
                    </div>


                    {{-- Modal --}}
                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog ">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title float-left" id="myModalLabel">Seleccione los estudios autorizados por la empresa</h4>
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          </div>
                          <div class="col-12">
                            <h3 class="selected">Categorias</h3>
                          </div>
                          <div class="modal-body">
                            <div class="col-1">

                            </div>
                            @foreach ($categoria as $item)
                            <div class="container-fluid">
                              <div class="row ">
                                <div class="col-4 ">
                                  <i class="fa fa-file-alt fa-fw icon"></i>
                                  <a href="#" data-value="{{$item->id}}" data-nombre="{{$item->nombre}}" onclick="ClickCategoria({{$item->id}})"class="links">
                                    {{$item->nombre}}
                                  </a></div>
                              </div>
                            </div>
                            @endforeach



                          </div>

                          <h3 class="selected" id="selected">Selecciona la categoría</h3>

                          <!-- Default unchecked -->

                        <div class="col-9" style="margin-left:12%">
                          @foreach ($categoria as $categoriarow)
                          <div id="{{$categoriarow->id}}" style="display:none">
                            <select class="selectize-multiple selectized" name="estudios[]" multiple="multiple" tabindex="-1" style="display: none;">
                              @foreach ($estudios as $estudio)
                              @if ($estudio->categoria_id==$categoriarow->id)
                              {{-- <div class="col-6 estudios_input {{$categoriarow->nombre}}"> --}}
                              {{-- <option value="11" selected="selected">Budapest</option> --}}
                              <option value="{{$estudio->id}}" id="{{$estudio->id}}">{{ $estudio->nombre}}</option>


                              {{-- </div> --}}
                              @endif
                              @endforeach
                            </select>
                          </div>
                          @endforeach
                        </div>




                          <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary" id="im2" data-dismiss="modal">Guardar Cambios</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <ul class="list-inline pull-right">
                      <li><button type="button" class="btn btn-default prev-step" id="im2"><i class="fas fa-angle-left"></i> Regresar</button></li>

                      <li><button type="submit" class="btn btn-primary next-step" id="im2">Guardar <i class="fas fa-check"></i></button>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </form>
          </div>
        </section>

      </div>
    </div>
  </div>

  </div>


</body>
<!--wizard-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src=" {!! asset('resources/js/ajax/admin.registroEmpresa.js') !!} "></script>
<script src=" {!! asset('resources/js/wizard.js') !!}"></script>


<script src="../../../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../../../app-assets/js/scripts/customizer.min.js"></script>
<script src="../../../app-assets/js/scripts/forms/select/form-selectize.min.js"></script>

</html>
