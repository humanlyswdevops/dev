@extends('Administrador.AppLayout')


@section('title', 'Estudios')

@section('styles')

@endsection

@section('page_css')
<link rel="stylesheet" href="https://expedienteclinico.humanly-sw.com/dev/public/css/empresa/info_paciente.css">
    <style>
        .imagen-logo{
            object-fit: cover;height:150px;width:150px;border: none;
        }
        #table_estudios_wrapper .row .col-sm-12.col-md-6:nth-child(0n+2) {
            text-align: right;
        }
    </style>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body row">
                <div class="estudios col-12 mb-3">
                    <h2 class="float-left">
                        Lista de Estudios
                    </h2>
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#crearEstudio">
                        Crear nuevo estudio
                    </button>
                </div>
                <div class="col-12 col-md-3 text-center">
                    <a href="#" data-value="0" class="btn btn-outline-primary links" >
                        Todos
                    </a>
                </div>
                @foreach ($categorias as $categoria)
                <div class="col-12 col-md-3 text-center">
                    <a href="#" data-value="{{$categoria->id}}" class="btn btn-outline-primary links" >
                        {{$categoria->nombre}}
                    </a>
                </div>
                @endforeach
                {{-- <div class="col-12">
                    <h2 class="took">Agregar Estudios</h2>
                    <div class="material-switch pull-right">
                        <input id="someSwitchOptionInfo" name="someSwitchOption001" type="checkbox" onchange="javascript:showContent()" />
                        <label for="someSwitchOptionInfo" class="label-info"></label>
                    </div>
                </div> --}}
                {{-- <div class="alta col-7 display-hidden">
                    <div class="header">
                        <div class="header_content">
                            <i class="fa fa-file-alt fa-fw head_icon"></i>
                            <div>
                                <h2>Alta de estudios</h2>
                                <h6>Laboratorio</h6>
                            </div>
                        </div>
                        <form method="POST" id="altaEstudio">
                            @csrf
                            <div id="alert_alta">

                            </div>
                            <div class="form-group">
                                <label for="estudio" id="label_estudio" class="h5">Nombre de Estudio</label>
                                <label for="estudio" id="selected" class="h5 text-center d-block">Selecciona la categoría</label>
                                <input type="text" class="form-control @error('estudio') is-inavlid @enderror" id="estudio" value="{{old('estudio')}}" name="estudio" placeholder="Nombre del Estudio">
                            </div>

                            <div class="form-group">
                                <label for="inputCodigoAlta">Código del estudio</label>
                                <input type="text" name="codigo"  class="form-control" id="inputCodigoAlta" required>
                            </div>

                            <div class="form-row">
                                <label for="inputDescripcionAlta">Descripción</label>
                                <textarea id="inputDescripcionAlta" class="form-control" cols="100" rows ="3" name="descripcion" form="altaEstudio" required></textarea>
                            </div>

                            <div class="form-row">
                                <label for="inputRequisitosAlta">Requisitos</label>
                                <textarea id="inputRequisitosAlta" class="form-control" cols="100" rows ="3" name="requisitos" form="altaEstudio" required></textarea>
                            </div>

                            <div class="form-group">
                                <label for="inputDuracionAlta">Duración de resultados (en días)</label>
                                <input type="text" name="duracion"  class="form-control" id="inputDuracionAlta" required>
                            </div>
                            <div class="form-group">
                                <label for="inputCostoAlta">Costo</label>
                                <input type="text" name="costo"  class="form-control" id="inputCostoAlta" required>
                            </div>

                            @error('estudio')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                            <button type="submit" id="btn_estudio_enviar" class="btn btn_estudio btn-primary">Guardar</button>
                        </form>
                        <img src="../resources/sass/images/cru.png" alt="" class="img_estudios">
                    </div>
                </div> --}}
                <div class="lista col-12">
                    <div class="img">
                    </div>
                    <div class="content_estudios">
                        <table id="table_estudios" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Duración</th>
                                    <th>Costo</th>
                                    <th>Estado</th>
                                    <th>Modificar</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <form method="post" id="estudio_form">
            <div class="modal-header bg-primary">
                <h4 class="modal-title float-left">Modificación</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                @csrf
                <div id="alerts">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <input type="text" name="id"  class="form-control" id="inputId" hidden>
                        <label for="inputEstudio">Nombre del Estudio</label>
                        <input type="text" name="nombre"  class="form-control @error('nombre') is-invalid @enderror" value="{{old('nombre')}}" id="inputEstudio" required>
                        @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="inputCodigo">Clave del Estudio</label>
                        <input type="text" name="codigo"  class="form-control" id="inputCodigo">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputState">Estado</label>
                        <select id="inputState" name="estado" class="form-control">
                            <option value="1">Activo</option>
                            <option value="2">Inactivo</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputDescripcion">Descripción</label>
                        <textarea id="inputDescripcion" class="form-control" cols="86" rows ="4" name="descripcion" form="estudio_form" required></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputRequisitos">Requisitos</label>
                        <textarea id="inputRequisitos" class="form-control" cols="86" rows ="3" name="requisitos" form="estudio_form" required></textarea>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputDuracion">Duración de resultados (en días)</label>
                        <input type="text" name="duracion"  class="form-control" id="inputDuracion" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCosto">Costo</label>
                        <input type="text" name="costo"  class="form-control" id="inputCosto" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="btn_enviar">Modificar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </form>
        </div>
    </div>
</div>

<div id="crearEstudio" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <form method="post" id="altaEstudio">
            <div class="modal-header bg-primary">
                <h4 class="modal-title float-left">Crear nuevo estudio</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                @csrf
                <div id="alert_alta">
                </div>
                <div class="form-group">
                    <label for="estudio" id="label_estudio">Nombre de Estudio</label>
                    <input required type="text" class="form-control @error('estudio') is-inavlid @enderror" id="estudio" value="{{old('estudio')}}" name="estudio">
                </div>

                <div class="form-group">
                    <label for="inputCategoria">Selecciona la categoría</label>
                    <select name="categoria_id" id="inputCategoria" class="form-control">
                        @foreach ($categorias as $categoria)
                        <option value="{!! $categoria->id !!}">{!! $categoria->nombre !!}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="inputCodigoAlta">Código del estudio</label>
                    <input type="text" name="codigo"  class="form-control" id="inputCodigoAlta" required>
                </div>

                <div class="form-group">
                    <label for="inputDescripcionAlta">Descripción</label>
                    <textarea id="inputDescripcionAlta" class="form-control" cols="100" rows ="3" name="descripcion" form="altaEstudio" required></textarea>
                </div>

                <div class="form-group">
                    <label for="inputRequisitosAlta">Requisitos</label>
                    <textarea id="inputRequisitosAlta" class="form-control" cols="100" rows ="3" name="requisitos" form="altaEstudio" required></textarea>
                </div>

                <div class="form-group">
                    <label for="inputDuracionAlta">Duración de resultados (en días)</label>
                    <input type="text" name="duracion"  class="form-control" id="inputDuracionAlta" required>
                </div>
                <div class="form-group">
                    <label for="inputCostoAlta">Costo</label>
                    <input type="text" name="costo"  class="form-control" id="inputCostoAlta" required>
                </div>

                @error('estudio')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_estudio_enviar" class="btn btn_estudio btn-primary">Guardar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{!! asset('/') !!}/resources/js/events.js"></script>

<script>
    $(".links").click(function(){
        $(".links").removeClass('btn-primary');
        $(".links").addClass('btn-outline-primary');
        $(this).removeClass('btn-outline-primary');
        $(this).addClass('btn-primary');
    })
</script>
@endsection
