@extends('layouts.administradorApp')

@section('title', $empresa->nombre)

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"-->
    <link rel="stylesheet" href="{!! asset('/') !!}/resources/sass/css/normalize.css">
    <link rel="stylesheet" href="{!! asset('/') !!}/resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/styleScroll.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/style_admin_menu.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/Administrador/infoEmpresa_styles.css">
@endsection

@section('content')
    <div class="content">
        <div class="alerts">
            @if(session('message'))
                <div class="alert alert-success" >
                {{session('message')}}
                </div>
            @elseif ($errors->any())
                <div class="alert alert-danger" >
                    <p>Ha ocurrido un error.</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <section class="izquierda">
            <div class="name-logo">
                <img id="logo_actual" src="{{route('getImage', ['filename'=>$empresa->logo])}}" alt="Logo de {{ $empresa->nombre }}" class="img-fluid rounded-circle img-thumbnail shadow-sm imagen-logo">
                <h1>{{ $empresa->nombre }}</h1>
                <hr>
                <button type="button" onclick="showModalLogotipo()" class="btn btn-logo">Cambiar logotipo</button>
            </div>
            <div style="margin-top:3%">
                <a type="button"  onclick="EmpresaAdmin('{{$empresa->nombre}}')" class="btn btn-custom">Ir dashboard admin empresa  -></a>
            </div>
        </section>

        <div class="derecha">
            <section class="derecha-arriba">
                <h2>Información de la empresa</h2>
                <div class="datos-empresa">
                    <p><strong>Dirección:</strong> {{ $empresa->direccion }}</p>
                    <p><strong>Giro:</strong> {{ $empresa->giro->nombre }}</p>
                    <p><strong>Teléfono:</strong> {{ $empresa->telefono }}</p>
                    <p><strong>Página web:</strong> {{ $empresa->pagina }}</p>
                </div>
                <div class="botones-empresa">
                    <button class="btn btn-custom" onclick="showEstudiosModal( '{{$encryptedID}}')">Estudios</button>
                    <a href="{{  route('editarEmpresa', $empresa->nombre) }}" class="btn btn-custom">Modificar datos de la empresa</a>
                </div>
            </section>
            <section class="derecha-abajo">
                <h2>Información del contacto de la empresa</h2>
                <div class="datos-contacto">
                    <p><strong>Nombre:</strong> {{ $contacto->nombre }} {{ $contacto->apellido_paterno }} {{ $contacto->apellido_materno }}</p>
                    <p><strong>Correo electrónico:</strong> {{ $contacto->email }}</p>
                    <p><strong>Área:</strong> {{ $contacto->area }}</p>
                    <p><strong>Puesto:</strong> {{ $contacto->puesto }}</p>
                    <p><strong>Teléfono:</strong> {{ $contacto->telefono }}</p>
                </div>
                <div class="botones-contacto">
                    <a href="{{  route('editarContacto', $empresa->nombre) }}" class="btn btn-custom">Modificar datos del contacto</a>
                </div>
            </section>
        </div>
    </div>

    {{-- Modal para cambiar logotipo --}}
    <form role="form" id="registro_empresa" method="post" action="{{route('cambiarLogotipo')}}" enctype="multipart/form-data">
        @csrf
        <div id="modalLogotipo" class="modal fade" role="dialog" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                {{-- Modal content --}}
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title float-left text-white" id="estudio_title">Cambiar logotipo de empresa </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form">
                            <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedID">
                            <img id="load_image" src="{{route('getImage', ['filename' => $empresa->logo])}}" alt="Logo de {{ $empresa->nombre }}" class="img-fluid rounded-circle img-thumbnail shadow-sm imagen-logo">
                            <button type="button" class="btn btn-primary btn-file">
                                Cargar imagen <input type="file" accept="image/*" name="logo" id="logo" >
                            </button>
                            @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success btn-guardar" type="submit">Guardar cambios</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    {{-- Modal para seleccionar estudios --}}
    <form role="form" id="seleccion_estudios" method="post" action="{{route('selectorEstudiosEmpresa')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedIDEstudios">
        <div id="estudiosModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="showEstudiosModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title float-left" id="myModalLabel">Seleccione los estudios autorizados por la empresa</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="categoria">
                            <h3 class="selected">Categorias</h3>
                            @foreach ($categorias as $item)
                                <div class="attrib">
                                <i class="fa fa-file-alt fa-fw icon"></i>
                                <a href="#" data-value="{{$item->id}}" data-nombre="{{$item->nombre}}" class="links">
                                        {{$item->nombre}}
                                </a>
                                </div>
                            @endforeach
                            @if(count($estudios) > 0)
                                <input type="button" id="BtnSeleccionar" class="btn btn-primary im22 col-12" value="Seleccionar todo " >
                            @endif
                        </div>
                        <div class="estudios">
                            <h3 class="selected" id="selected">Seleccione la categoría</h3>
                            <div class="custom-control custom-checkbox">
                                @foreach ($categorias as $categoriarow)
                                    @foreach ($estudios as $estudio)
                                        @if ($estudio->categoria_id == $categoriarow->id)
                                            <div class="col-6 estudios_input {{$categoriarow->nombre}}">
                                                <input type="checkbox" name="estudios[{{$estudio->id}}]" class="custom-control-input" value="{{$estudio->id}}" id="estudio{{$estudio->id}}">
                                                <label class="custom-control-label" for="estudio{{$estudio->id}}">{{ $estudio->nombre}}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="im2">Guardar Cambios</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('specificScripts')

    <script src="{!! asset('/') !!}/resources/js/Administrador/infoEmpresa.js"></script>

@endsection
