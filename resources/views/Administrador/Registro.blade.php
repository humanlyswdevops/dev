@extends('Administrador.AppLayout')
@section('title')
Registro de empresa
@endsection

@section('begin_vendor_css')
    <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.default.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/select2.min.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/extensions/sweetalert.css") !!}">
    <!-- END VENDOR CSS-->
@endsection
@section('page_css')
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/animate/animate.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/selectize/selectize.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/checkboxes-radios.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/wizard.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/pickers/daterange/daterange.css") !!}">
@endsection
@section('css_custom')
    <style>
        .app-content .wizard.wizard-circle > .steps > ul > li:before, .app-content .wizard.wizard-circle > .steps > ul > li:after{
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.done .step{
            border-color: #f26b3e;
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.current .step{
            color: #f26b3e;
            border-color: #f26b3e;
        }
        .app-content .wizard > .actions > ul > li > a{
            background-color: #f26b3e;
            border-radius: .4285rem;
        }
    </style>
@endsection

@section('content')
    <div id="url_general" data-url="{!! url('/') !!}"></div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item"><a href="#">Alta de empresas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inicio</li>
        </ol>
    </nav>
    {{-- inicio de formulario --}}
    <div class="">
        <div class="content-body"><!-- Form wizard with number tabs section start -->
            <section id="number-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <h4 class="card-title text-white">Examen Médico</h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form action="#" method="post" id="form_principal" enctype="multipart/form-data">
                                        <div class="number-tab-steps wizard-circle">
                                            <h6>Datos generales</h6>
                                            <fieldset>
                                                <div class="row" id="seccion_0" data-id="0">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="nombre_completo">Nombre completo</label>
                                                            <input name="nombre_completo" id="nombre_completo" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="correo_contacto">Correo de contacto</label>
                                                            <input name="correo_contacto" id="correo_contacto" type="email" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="puesto">Puesto</label>
                                                            <input name="puesto" id="puesto" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="area">Área</label>
                                                            <input name="area" id="area" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h6>Información de la empresa</h6>
                                            <fieldset>
                                                <div class="row" id="seccion_1" data-id="1">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="nombre_empresa">Nombre de la empresa</label>
                                                            <input name="nombre_empresa" id="nombre_empresa" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="direccion">Dirección</label>
                                                            <input name="direccion" id="direccion" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="giro">Giro de la empresa</label>
                                                            <select class="form-control" name="giro" id="giro">
                                                                @foreach ($giros as $giro)
                                                                    <option value="{{ $giro->nombre }}">{{ $giro->nombre }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="telefono">Teléfono</label>
                                                            <input name="telefono" id="telefono" type="number" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="pagina_web">Página web</label>
                                                            <input name="pagina_web" id="pagina_web" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <fieldset class="form-group">
                                                            <label for="basicInputFile">Logo</label>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="logo" id="logo" accept="image/png, image/jpeg">
                                                                <label class="custom-file-label" for="logo">Seleccionar imagen</label>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <h6>Información SASS</h6>
                                            <fieldset>
                                                <div class="row" id="seccion_2" data-id="2">
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="sucursal_id">Sucursal ID</label>
                                                            <input name="sucursal_id" id="sucursal_id" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="cliente_id">Cliente ID</label>
                                                            <input name="cliente_id" id="cliente_id" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="medico_id">Médico ID</label>
                                                            <input name="medico_id" id="medico_id" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    {{-- fin de formulario --}}
@endsection

@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="{!! url("app-assets/vendors/js/extensions/sweetalert.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/extensions/jquery.steps.min.js") !!}"></script>
{{-- <script src="{!! url("app-assets/vendors/js/forms/select/selectize.min.js") !!}"></script> --}}
<script src="{!! url("app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/pickers/daterange/daterangepicker.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/validation/jquery.validate.min.js") !!}"></script>
<script src="{!! url("app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js") !!}"></script>

{{-- checkbox --}}
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/icheck/icheck.min.js") !!}"></script>
<!-- END PAGE VENDOR JS-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

@endsection


@section('js_custom')
<script src="{!! url("app-assets/js/scripts/forms/checkbox-radio.js") !!}"></script>
<script>
    $(".number-tab-steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        enableAllSteps: true,
        titleTemplate: '<span class="step">#index#</span> #title#',
        cssClass: 'wizard',
        labels: {
            finish: 'Guardar',
            previous: 'Anterior',
            next: 'Siguiente'
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            let validador = false;
            if(currentIndex > newIndex)
            {
                validador = true;
            }else{
                if(currentIndex == 0)
                {
                    validador = validadorPaso1();
                }else if(currentIndex == 1){
                    validador = validadorPaso2();
                }
            }
            return validador;
        },
        onFinished: function(event, currentIndex) {
            // aqui iba lo del paso 2
            let validador = true;

            let sucursal_id = $("#sucursal_id");
            let cliente_id = $("#cliente_id");
            let medico_id = $("#medico_id");

            sucursal_id.removeClass('is-invalid');
            cliente_id.removeClass('is-invalid');
            medico_id.removeClass('is-invalid');
            $(".invalid-feedback").remove();

            if(sucursal_id.val().length <= 0){
                validador = false;
                sucursal_id.addClass('is-invalid');
                sucursal_id.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
            }
            if(cliente_id.val().length <= 0){
                validador = false;
                cliente_id.addClass('is-invalid');
                cliente_id.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
            }
            if(medico_id.val().length <= 0){
                validador = false;
                medico_id.addClass('is-invalid');
                medico_id.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
            }
            console.log(validador);
            if(validador){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    data: {"email" : $("#correo_contacto").val()},
                    url: 'validar_email'
                }).done((res) => {
                    if(res == false){
                        swal("Validación", "El correo electrónico ingresado ya se encuentra registrado, intenta con otro.", "info").then(isConfirm => {
                            if(isConfirm){
                                $("#steps-uid-0-t-0").click();
                                $("#correo_contacto").addClass('is-invalid');
                                $("#correo_contacto").after('<div class="invalid-feedback">Intente con otro correo electrónico.</div>');
                            }
                        });
                    }else{
                        swal({
                            title: "¿Estas seguro de dar de alta esta empresa?",
                            text: "Esta acción es irreversible",
                            icon: "warning",
                            buttons: {
                                cancel: {
                                    text: "Cancelar",
                                    value: null,
                                    visible: true,
                                    className: "",
                                    closeModal: false,
                                },
                                confirm: {
                                    text: "Si, terminar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                            }).then(isConfirm => {
                            if (isConfirm) {

                                let formularioo = $("#form_principal input");
                                var formData = new FormData();
                                var logo_file = document.getElementById("logo");

                                $.map( formularioo, function( val, i ) {
                                    let input = $(val);
                                    formData.append(input.attr('name'), input.val());
                                });

                                formData.append("giro", $("#giro").val());

                                if(logo_file.files.length > 0)
                                {
                                    console.log('paso');
                                    formData.append('logo',logo_file.files[0]);
                                }
                                $.ajax({
                                    type: 'POST',
                                    data: formData,
                                    dataType:'json',
                                    contentType:false,
                                    processData:false,
                                    url: 'registroController'
                                }).done((res) => {
                                    console.log(res);
                                    if(res == "Correcto"){
                                        swal({
                                            title: "La empresa se ha creado correctamente",
                                            text: "¿Quieres ir a la sección de empresas?",
                                            icon: "success",
                                            buttons: {
                                                cancel: {
                                                    text: "No, quedarme aquí",
                                                    value: null,
                                                    visible: true,
                                                    className: "",
                                                    closeModal: false,
                                                },
                                                confirm: {
                                                    text: "Si, ir a empresas",
                                                    value: true,
                                                    visible: true,
                                                    className: "",
                                                    closeModal: false
                                                }
                                            }
                                        }).then(isConfirm => {
                                            if (isConfirm) {
                                                $(location).attr('href',$("#url_general").data('url')+"/Empresas");
                                            }else{
                                                location.reload();
                                            }
                                        }).fail((err) => {
                                            console.log(err);
                                        });
                                    }
                                }).fail((err) => {
                                    console.log(err);
                                });
                            } else {
                                swal("Operacion Cancelada", "Puede continuar...", "error");
                            }
                        });
                    }
                }).fail((err) => {
                    console.log(err);
                });
            }
        }
    });

    function email_formato(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function validadorPaso1() {
        let validador = true;
        let nombre_completo =  $("#nombre_completo");
        let correo_contacto =  $("#correo_contacto");
        let puesto =  $("#puesto");
        let area =  $("#area");
        nombre_completo.removeClass('is-invalid');
        correo_contacto.removeClass('is-invalid');
        puesto.removeClass('is-invalid');
        area.removeClass('is-invalid');
        $(".invalid-feedback").remove();

        if(nombre_completo.val().length <= 0){
            validador = false;
            nombre_completo.addClass('is-invalid');
            nombre_completo.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }
        if(correo_contacto.val().length <= 0){
            validador = false;
            correo_contacto.addClass('is-invalid');
            correo_contacto.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }else{
            if(!email_formato(correo_contacto.val()))
            {
                validador = false;
                correo_contacto.addClass('is-invalid');
                correo_contacto.after('<div class="invalid-feedback">El campo no tiene el formato correcto.</div>');
            }
        }
        if(puesto.val().length <= 0){
            validador = false;
            puesto.addClass('is-invalid');
            puesto.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }
        if(area.val().length <= 0){
            validador = false;
            area.addClass('is-invalid');
            area.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }

        return validador;
    }
    function validadorPaso2() {
        let validador = true;
        let nombre_empresa = $("#nombre_empresa");
        let direccion = $("#direccion");
        let giro = $("#giro");
        let telefono = $("#telefono");
        let pagina_web = $("#pagina_web");
        let logo = $("#logo");

        nombre_empresa.removeClass('is-invalid');
        direccion.removeClass('is-invalid');
        giro.removeClass('is-invalid');
        telefono.removeClass('is-invalid');
        pagina_web.removeClass('is-invalid');
        logo.removeClass('is-invalid');

        $(".invalid-feedback").remove();

        if(nombre_empresa.val().length <= 0){
            validador = false;
            nombre_empresa.addClass('is-invalid');
            nombre_empresa.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }
        if(direccion.val().length <= 0){
            validador = false;
            direccion.addClass('is-invalid');
            direccion.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }
        if(telefono.val().length <= 0){
            validador = false;
            telefono.addClass('is-invalid');
            telefono.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }
        if(pagina_web.val().length <= 0){
            validador = false;
            pagina_web.addClass('is-invalid');
            pagina_web.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }

        if(logo.val().length <= 0){
            validador = false;
            logo.addClass('is-invalid');
            logo.after('<div class="invalid-feedback">El campo no puede estar vacio.</div>');
        }
        return validador;
    }
</script>
@endsection
{{-- form_principal --}}
